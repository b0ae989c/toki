#include "toki.hpp"
auto
main(int argc, char** argv) -> int
{
  toki::simulation::Alpha<
    toki::simulation::detail::Alpha_Name::LinearAdvection_HRIDG,
    toki::simulation::detail::Alpha_Variant::Equation_1D>
    sim(argc, argv);
  sim.start();
  return 0;
}
