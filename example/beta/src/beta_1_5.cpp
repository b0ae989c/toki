#include "toki.hpp"
auto
main(int argc, char** argv) -> int
{
  toki::simulation::Beta<
    toki::simulation::detail::Beta_Name::VlasovPoisson_1D1V_HRIDG,
    toki::simulation::detail::Beta_Variant::PlasmaSheath>
    sim(argc, argv);
  sim.start();
  return 0;
}
