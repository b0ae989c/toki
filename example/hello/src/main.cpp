#include "fmt/format.h"
#include "toki.hpp"
auto
main(int argc, char** argv) -> int
{
  toki::context::Default context(argc, argv);
  context.guard([]() -> void {
    toki::log::info(
      fmt::format("toki version: {}", toki::info::version_string()));
  });
  return EXIT_SUCCESS;
}
