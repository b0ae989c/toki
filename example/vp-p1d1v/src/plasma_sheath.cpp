#include "toki.hpp"
auto
main(int argc, char** argv) -> int
{
  auto constexpr model = toki::timestepper::detail::VP_Model::p1D1V;
  auto constexpr example = toki::timestepper::detail::VP_Example::PlasmaSheath;
  toki::timestepper::VP<model, example> sim(argc, argv);
  sim.start();
  return 0;
}
