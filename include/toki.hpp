#ifndef TOKI_HPP
#define TOKI_HPP
#include "toki/basis.hpp"
#include "toki/config.hpp"
#include "toki/context.hpp"
#include "toki/data.hpp"
#include "toki/domain.hpp"
#include "toki/info.hpp"
#include "toki/log.hpp"
#include "toki/math.hpp"
#include "toki/matrix.hpp"
#include "toki/option.hpp"
#include "toki/quadrature.hpp"
#include "toki/simulation.hpp"
#include "toki/timestepper.hpp"
#include "toki/utility.hpp"
#include "toki/vector.hpp"
#endif
