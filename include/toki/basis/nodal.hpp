#ifndef TOKI_BASIS_NODAL_HPP
#define TOKI_BASIS_NODAL_HPP
#include <array>
#include <functional>
#include <utility>
#include "toki/config.hpp"
#include "toki/vector/vec.hpp"
namespace toki::basis {
struct Nodal
{
  struct Info
  {
    toki::Size point;
    toki::vector::Vec abscissa;
  };
  Info info;
  Nodal();
  Nodal(toki::Size const& point);
  Nodal(Nodal const& other);
  Nodal(Nodal&& other) noexcept;
  ~Nodal();
  auto operator=(Nodal other) -> Nodal&;
  template<int dim>
  auto fill(
    toki::vector::Vec& sample,
    toki::Size const& offset,
    std::function<toki::Number(std::array<toki::Number, dim> const&)> const& op)
    const -> Nodal const&;
  template<int dim>
  auto eval(
    toki::Number& result,
    std::array<toki::Number, dim> const& coord,
    std::array<toki::Size, dim> const& index) const -> Nodal const&;
  template<int dim>
  auto eval(
    std::array<toki::Number, dim> const& coord,
    std::array<toki::Size, dim> const& index) const -> toki::Number;
  template<int dim>
  auto eval(
    toki::Number& result,
    std::array<toki::Number, dim> const& coord,
    std::array<toki::Size, dim> const& index,
    int const target_dim,
    int const derivative_order) const -> Nodal const&;
  template<int dim>
  auto eval(
    std::array<toki::Number, dim> const& coord,
    std::array<toki::Size, dim> const& index,
    int const target_dim,
    int const derivative_order) const -> toki::Number;
  template<int dim>
  auto approx(
    toki::Number& result,
    std::array<toki::Number, dim> const& coord,
    toki::vector::Vec const& sample,
    toki::Size const& offset) const -> Nodal const&;
  template<int dim>
  auto approx(
    std::array<toki::Number, dim> const& coord,
    toki::vector::Vec const& sample,
    toki::Size const& offset) const -> toki::Number;
  template<int dim>
  auto approx(
    toki::Number& result,
    std::array<toki::Number, dim> const& coord,
    toki::vector::Vec const& sample,
    toki::Size const& offset,
    int const target_dim,
    int const derivative_order) const -> Nodal const&;
  template<int dim>
  auto approx(
    std::array<toki::Number, dim> const& coord,
    toki::vector::Vec const& sample,
    toki::Size const& offset,
    int const target_dim,
    int const derivative_order) const -> toki::Number;
  template<int dim>
  auto get_coord(toki::Size const& point) const
    -> std::array<toki::Number, dim>;
  friend auto
  swap(Nodal& lhs, Nodal& rhs) noexcept -> void
  {
    using std::swap;
    swap(lhs.info, rhs.info);
    return;
  }
};
} // namespace toki::basis
#endif
