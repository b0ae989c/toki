#ifndef TOKI_CONFIG_HPP
#define TOKI_CONFIG_HPP
#include <cstdlib>
#include <numbers>
namespace toki {
using Size = std::size_t;
using Number = double;
template<typename Type>
constexpr auto zero = static_cast<Type>(0);
template<typename Type>
constexpr auto one = static_cast<Type>(1);
template<typename Type>
constexpr auto sqrt2 = std::numbers::sqrt2_v<Type>;
template<typename Type>
constexpr auto pi = std::numbers::pi_v<Type>;
template<typename Type>
constexpr auto inv_pi = std::numbers::inv_pi_v<Type>;
template<typename Type>
constexpr auto inv_sqrtpi = std::numbers::inv_sqrtpi_v<Type>;
template<typename Type>
constexpr auto epsilon = static_cast<Type>(1e-9);
} // namespace toki
#endif
