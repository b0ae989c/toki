#ifndef TOKI_CONTEXT_DEFAULT_HPP
#define TOKI_CONTEXT_DEFAULT_HPP
#include <functional>
namespace toki::context {
struct Default
{
  Default();
  Default(int argc, char** argv);
  Default(Default const&) = delete;
  Default(Default&&) noexcept = delete;
  ~Default();
  auto operator=(Default const&) -> Default& = delete;
  auto operator=(Default&&) noexcept -> Default& = delete;
  auto guard(std::function<void()> const& routine) const -> void;
};
} // namespace toki::context
#endif
