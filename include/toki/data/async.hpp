#ifndef TOKI_DATA_ASYNC_HPP
#define TOKI_DATA_ASYNC_HPP
#include <type_traits>
#include "toki/data/detail/async_mpi_dynamic.hpp"
#include "toki/data/detail/async_mpi_static.hpp"
namespace toki::data {
enum struct AsyncBackend
{
  MPI,
};
enum struct AsyncVariant
{
  Static,
  Dynamic,
};
// clang-format off
template<AsyncBackend backend, AsyncVariant variant>
using Async = std::conditional_t<
  backend == AsyncBackend::MPI,
    std::conditional_t<
      variant == AsyncVariant::Static,
      detail::Async_MPI_Static,
    std::conditional_t<
      variant == AsyncVariant::Dynamic,
      detail::Async_MPI_Dynamic,
    void>>,
  void>;
// clang-format on
} // namespace toki::data
#endif
