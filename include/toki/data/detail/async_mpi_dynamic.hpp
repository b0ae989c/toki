#ifndef TOKI_DATA_DETAIL_ASYNC_MPI_DYNAMIC_HPP
#define TOKI_DATA_DETAIL_ASYNC_MPI_DYNAMIC_HPP
#include <functional>
#include <string>
#include <utility>
#include <vector>
#include "boost/container/flat_map.hpp"
#include "mpi.h"
#include "toki/config.hpp"
#include "toki/vector/vec.hpp"
namespace toki::data::detail {
struct Async_MPI_Dynamic
{
  struct Buffer
  {
    toki::vector::Vec content;
    toki::Size size;
    std::vector<toki::Size> offset;
    bool flag_remote{false};
    int remote_mpi_rank{-1};
    std::string remote_name{};
  };
  struct Info
  {
    boost::container::flat_map<std::string, Buffer> buffer;
    boost::container::flat_map<std::string, MPI_Win> window;
  };
  Info info;
  Async_MPI_Dynamic();
  Async_MPI_Dynamic(Async_MPI_Dynamic const& other);
  Async_MPI_Dynamic(Async_MPI_Dynamic&& other) noexcept;
  ~Async_MPI_Dynamic();
  auto operator=(Async_MPI_Dynamic other) -> Async_MPI_Dynamic&;
  auto create_buffer(
    std::string const& name,
    toki::Size const& size,
    toki::Size const& dof,
    bool const replace = false) -> Async_MPI_Dynamic&;
  auto create_buffer(
    std::string const& name,
    toki::Size const& size,
    std::vector<toki::Size> const& offset,
    bool const replace = false) -> Async_MPI_Dynamic&;
  auto create_buffer(
    toki::vector::Vec&& content,
    std::string const& name,
    toki::Size const& size,
    std::vector<toki::Size> const& offset,
    bool const replace = false) -> Async_MPI_Dynamic&;
  auto create_remote_buffer(
    std::string const& name,
    toki::Size const& size,
    toki::Size const& dof,
    int const remote_mpi_rank,
    std::string const& remote_name) -> Async_MPI_Dynamic&;
  auto create_remote_buffer(
    std::string const& name,
    toki::Size const& size,
    std::vector<toki::Size> const& offset,
    int const remote_mpi_rank,
    std::string const& remote_name) -> Async_MPI_Dynamic&;
  auto create_remote_buffer(
    toki::vector::Vec&& content,
    std::string const& name,
    toki::Size const& size,
    std::vector<toki::Size> const& offset,
    int const remote_mpi_rank,
    std::string const& remote_name) -> Async_MPI_Dynamic&;
  auto attach_buffer(
    toki::vector::Vec&& content,
    std::string const& name,
    toki::Size const& size,
    std::vector<toki::Size> const& offset) -> Async_MPI_Dynamic&;
  auto attach_remote_buffer(
    toki::vector::Vec&& content,
    std::string const& name,
    toki::Size const& size,
    std::vector<toki::Size> const& offset,
    int const remote_mpi_rank,
    std::string const& remote_name) -> Async_MPI_Dynamic&;
  auto update_remote_buffer(std::string const& name) -> Async_MPI_Dynamic&;
  auto update_remote_buffer() -> Async_MPI_Dynamic&;
  auto wait(std::string const& name) -> Async_MPI_Dynamic&;
  auto wait() -> Async_MPI_Dynamic&;
  auto get(std::string const& name, toki::Size const& index) const
    -> toki::Number*;
  auto set(
    std::string const& name,
    toki::Number const& value,
    toki::Size const& index,
    toki::Size const& point) -> Async_MPI_Dynamic&;
  auto set(
    std::string const& name,
    toki::Number const& value,
    toki::Size const& index) -> Async_MPI_Dynamic&;
  auto set(
    std::string const& name,
    toki::vector::Vec const& content,
    toki::Size const& offset) -> Async_MPI_Dynamic&;
  auto set(
    std::string const& name,
    std::function<toki::Number(toki::Size const&, toki::Size const&)> const& op)
    -> Async_MPI_Dynamic&;
  auto at(
    std::string const& name,
    toki::Size const& index,
    toki::Size const& point) const -> toki::Number const&;
  friend auto
  swap(Async_MPI_Dynamic& lhs, Async_MPI_Dynamic& rhs) noexcept -> void
  {
    using std::swap;
    swap(lhs.info, rhs.info);
    return;
  }
};
} // namespace toki::data::detail
#endif
