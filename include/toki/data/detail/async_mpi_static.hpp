#ifndef TOKI_DATA_DETAIL_ASYNC_MPI_STATIC_HPP
#define TOKI_DATA_DETAIL_ASYNC_MPI_STATIC_HPP
#include <functional>
#include <string>
#include <utility>
#include "boost/container/flat_map.hpp"
#include "mpi.h"
#include "toki/config.hpp"
#include "toki/vector/vec.hpp"
namespace toki::data::detail {
struct Async_MPI_Static
{
  struct Buffer
  {
    toki::vector::Vec content;
    toki::Size size;
    toki::Size dof;
    bool flag_remote{false};
    int remote_mpi_rank{-1};
    std::string remote_name{};
  };
  struct Info
  {
    boost::container::flat_map<std::string, Buffer> buffer;
    boost::container::flat_map<std::string, MPI_Win> window;
  };
  Info info;
  Async_MPI_Static();
  Async_MPI_Static(Async_MPI_Static const& other);
  Async_MPI_Static(Async_MPI_Static&& other) noexcept;
  ~Async_MPI_Static();
  auto operator=(Async_MPI_Static other) -> Async_MPI_Static&;
  auto create_buffer(
    std::string const& name,
    toki::Size const& size,
    toki::Size const& dof) -> Async_MPI_Static&;
  auto create_remote_buffer(
    std::string const& name,
    toki::Size const& size,
    toki::Size const& dof,
    int const remote_mpi_rank,
    std::string const& remote_name) -> Async_MPI_Static&;
  auto update_remote_buffer(std::string const& name) -> Async_MPI_Static&;
  auto update_remote_buffer() -> Async_MPI_Static&;
  auto wait(std::string const& name) -> Async_MPI_Static&;
  auto wait() -> Async_MPI_Static&;
  auto get(std::string const& name, toki::Size const& index) const
    -> toki::Number*;
  auto set(
    std::string const& name,
    toki::Number const& value,
    toki::Size const& index,
    toki::Size const& point) -> Async_MPI_Static&;
  auto set(
    std::string const& name,
    toki::Number const& value,
    toki::Size const& index) -> Async_MPI_Static&;
  auto set(
    std::string const& name,
    toki::vector::Vec const& content,
    toki::Size const& offset) -> Async_MPI_Static&;
  auto set(
    std::string const& name,
    std::function<toki::Number(toki::Size const&, toki::Size const&)> const& op)
    -> Async_MPI_Static&;
  auto at(
    std::string const& name,
    toki::Size const& index,
    toki::Size const& point) const -> toki::Number const&;
  friend auto
  swap(Async_MPI_Static& lhs, Async_MPI_Static& rhs) noexcept -> void
  {
    using std::swap;
    swap(lhs.info, rhs.info);
    return;
  }
};
} // namespace toki::data::detail
#endif
