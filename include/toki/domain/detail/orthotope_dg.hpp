#ifndef TOKI_DOMAIN_DETAIL_ORTHOTOPE_DG_HPP
#define TOKI_DOMAIN_DETAIL_ORTHOTOPE_DG_HPP
#include <array>
#include <functional>
#include <utility>
#include <vector>
#include "toki/basis/nodal.hpp"
#include "toki/config.hpp"
#include "toki/data/async.hpp"
#include "toki/vector/vec.hpp"
namespace toki::domain::detail {
template<int dim>
struct Orthotope_DG
{
  using Data = toki::data::
    Async<toki::data::AsyncBackend::MPI, toki::data::AsyncVariant::Static>;
  struct Info
  {
    struct Local
    {
      struct MPI
      {
        struct Neighbor
        {
          int n{-1};
          int p{-1};
        };
        int rank{-1};
        std::array<int, dim> index{};
        std::array<Neighbor, dim> neighbor{};
      };
      struct Element
      {
        toki::Size N{};
        std::array<toki::Number, dim> volume{};
      };
      std::array<toki::Size, dim> resolution{};
      std::array<toki::Number, dim> range{};
      std::array<toki::Number, dim> anchor{};
      MPI mpi;
      Element element;
    };
    toki::Size dof;
    toki::Size quadrature_point;
    toki::basis::Nodal basis;
    std::array<toki::Size, dim> resolution;
    std::array<toki::Size, dim> halo;
    std::array<toki::Number, dim> range;
    std::array<toki::Number, dim> anchor;
    std::array<int, dim> mpi_extent;
    Local local{};
  };
  Info info;
  Data data;
  Orthotope_DG();
  Orthotope_DG(
    toki::Size const& dof,
    toki::Size const& quadrature_point,
    std::array<toki::Size, dim> const& resolution,
    std::array<toki::Size, dim> const& halo,
    std::array<toki::Number, dim> const& range,
    std::array<toki::Number, dim> const& anchor,
    std::array<int, dim> const& mpi_extent);
  Orthotope_DG(Orthotope_DG const& other);
  Orthotope_DG(Orthotope_DG&& other) noexcept;
  ~Orthotope_DG();
  auto operator=(Orthotope_DG other) -> Orthotope_DG&;
  auto get_element(
    toki::vector::Vec& output,
    toki::Size const& offset,
    std::array<toki::Size, dim> const& index) -> Orthotope_DG&;
  auto set_element(
    toki::vector::Vec const& input,
    toki::Size const& offset,
    std::array<toki::Size, dim> const& index) -> Orthotope_DG&;
  auto set_element(
    toki::Number const& value,
    std::array<toki::Size, dim> const& index,
    toki::Size const& point) -> Orthotope_DG&;
  auto set_element(
    toki::Number const& value,
    std::array<toki::Size, dim> const& index) -> Orthotope_DG&;
  auto set_element(std::function<toki::Number(
                     std::array<toki::Size, dim> const&,
                     toki::Size const&)> const& op) -> Orthotope_DG&;
  auto set_element(
    std::function<toki::Number(std::array<toki::Number, dim> const&)> const& op,
    bool const average = true) -> Orthotope_DG&;
  auto adjust_element(
    toki::Number const& delta,
    std::array<toki::Size, dim> const& index) -> Orthotope_DG&;
  auto adjust_element(
    toki::vector::Vec const& delta,
    toki::Size const& offset,
    std::array<toki::Size, dim> const& index) -> Orthotope_DG&;
  auto get_stencil(
    toki::vector::Vec& output,
    std::vector<std::array<toki::Size, dim>> const& index_list)
    -> Orthotope_DG&;
  auto get_coord(
    std::array<toki::Size, dim> const& index,
    toki::Size const& point) const -> std::array<toki::Number, dim>;
  auto get_min() const -> toki::Number;
  auto get_max() const -> toki::Number;
  auto halo_dispatch(int const direction) -> Orthotope_DG&;
  auto halo_wait(int const direction) -> Orthotope_DG&;
  auto halo_update(int const direction) -> Orthotope_DG&;
  auto save(std::string const& name, toki::Size const& id) -> Orthotope_DG&;
  friend auto
  swap(Orthotope_DG& lhs, Orthotope_DG& rhs) noexcept -> void
  {
    using std::swap;
    swap(lhs.info, rhs.info);
    swap(lhs.data, rhs.data);
    return;
  }
};
} // namespace toki::domain::detail
#endif
