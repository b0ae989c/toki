#ifndef TOKI_DOMAIN_DETAIL_ORTHOTOPE_DG_P_HPP
#define TOKI_DOMAIN_DETAIL_ORTHOTOPE_DG_P_HPP
#include <array>
#include <functional>
#include <utility>
#include <vector>
#include "boost/container/flat_map.hpp"
#include "boost/container/small_vector.hpp"
#include "toki/basis/nodal.hpp"
#include "toki/config.hpp"
#include "toki/data/async.hpp"
#include "toki/quadrature/lobatto.hpp"
#include "toki/vector/vec.hpp"
namespace toki::domain::detail {
template<int dim>
struct Orthotope_DG_P
{
  using Data = toki::data::
    Async<toki::data::AsyncBackend::MPI, toki::data::AsyncVariant::Dynamic>;
  struct Refinement
  {
    struct Level
    {
      int min;
      int max;
    };
    struct Basis
    {
      using basis_type = toki::basis::Nodal;
      using quadrature_type = toki::quadrature::Lobatto;
      int point;
      basis_type basis;
      quadrature_type quadrature;
    };
    struct Score
    {
      toki::Number min;
      toki::Number max;
      std::vector<toki::Number> oscillation;
      std::vector<toki::Number> flux;
    };
    Level level;
    boost::container::small_vector<toki::Size, 4> dof;
    boost::container::small_vector<Basis, 4> basis;
    std::vector<int> current;
    std::vector<int> target;
    std::vector<toki::Size> data_offset;
    Score score;
  };
  struct Info
  {
    struct Local
    {
      struct MPI
      {
        struct Neighbor
        {
          int n{-1};
          int p{-1};
        };
        int rank{-1};
        std::array<int, dim> index{};
        std::array<Neighbor, dim> neighbor{};
      };
      struct Element
      {
        toki::Size N{};
        std::array<toki::Number, dim> volume{};
      };
      std::array<toki::Size, dim> resolution{};
      std::array<toki::Number, dim> range{};
      std::array<toki::Number, dim> anchor{};
      MPI mpi;
      Element element;
      Refinement refinement;
    };
    std::array<toki::Size, dim> resolution;
    std::array<toki::Size, dim> halo;
    std::array<toki::Number, dim> range;
    std::array<toki::Number, dim> anchor;
    std::array<int, dim> mpi_extent;
    Local local{};
  };
  Info info;
  Data data;
  Orthotope_DG_P();
  Orthotope_DG_P(
    std::vector<toki::Size> const& dof,
    std::array<int, 2> const& level,
    std::array<toki::Number, 2> const& refinement_score,
    std::array<toki::Size, dim> const& resolution,
    std::array<toki::Size, dim> const& halo,
    std::array<toki::Number, dim> const& range,
    std::array<toki::Number, dim> const& anchor,
    std::array<int, dim> const& mpi_extent);
  Orthotope_DG_P(Orthotope_DG_P const& other);
  Orthotope_DG_P(Orthotope_DG_P&& other) noexcept;
  ~Orthotope_DG_P();
  auto operator=(Orthotope_DG_P other) -> Orthotope_DG_P&;
  auto get_element(
    toki::vector::Vec& output,
    toki::Size const& offset,
    std::array<toki::Size, dim> const& index) -> Orthotope_DG_P&;
  auto set_element(
    toki::Number const& value,
    std::array<toki::Size, dim> const& index) -> Orthotope_DG_P&;
  auto set_element(
    toki::vector::Vec const& input,
    toki::Size const& offset,
    std::array<toki::Size, dim> const& index) -> Orthotope_DG_P&;
  auto set_element(
    toki::Number const& value,
    std::array<toki::Size, dim> const& index,
    toki::Size const& point) -> Orthotope_DG_P&;
  auto set_element(std::function<toki::Number(
                     std::array<toki::Size, dim> const&,
                     toki::Size const&)> const& op) -> Orthotope_DG_P&;
  auto set_element(
    std::function<toki::Number(std::array<toki::Number, dim> const&)> const& op,
    bool const average = true) -> Orthotope_DG_P&;
  auto adjust_element(
    toki::Number const& delta,
    std::array<toki::Size, dim> const& index) -> Orthotope_DG_P&;
  auto adjust_element(
    toki::vector::Vec const& delta,
    toki::Size const& offset,
    std::array<toki::Size, dim> const& index) -> Orthotope_DG_P&;
  auto adjust_element(
    toki::Number const& delta,
    std::array<toki::Size, dim> const& index,
    toki::Size const& point) -> Orthotope_DG_P&;
  auto adjust_element(std::function<toki::Number(
                        std::array<toki::Size, dim> const&,
                        toki::Size const&)> const& op) -> Orthotope_DG_P&;
  auto project_element(
    toki::vector::Vec& output,
    int const output_level,
    toki::Size const& output_offset,
    toki::vector::Vec const& input,
    int const input_level,
    toki::Size const& input_offset) -> Orthotope_DG_P&;
  auto get_dof(std::array<toki::Size, dim> const& index) const -> toki::Size;
  auto get_basis(std::array<toki::Size, dim> const& index) const
    -> Refinement::Basis::basis_type const&;
  auto get_quadrature(std::array<toki::Size, dim> const& index) const
    -> Refinement::Basis::quadrature_type const&;
  auto get_data_offset(std::array<toki::Size, dim> const& index) const
    -> toki::Size;
  auto get_refinement_score(std::array<toki::Size, dim> const& index)
    -> toki::Number;
  auto get_coord(
    std::array<toki::Size, dim> const& index,
    toki::Size const& point) const -> std::array<toki::Number, dim>;
  auto halo_dispatch(int const direction) -> Orthotope_DG_P&;
  auto halo_wait(int const direction) -> Orthotope_DG_P&;
  auto halo_update(int const direction) -> Orthotope_DG_P&;
  auto refine(
    bool const& apply_projection = true,
    bool const& aux_domain = false) -> Orthotope_DG_P&;
  auto adjust_remote_buffer(
    std::string const& name,
    int const remote_mpi_rank,
    std::string const& remote_name,
    std::array<toki::Size, 2> const& index_begin,
    std::array<toki::Size, 2> const& index_end) -> Orthotope_DG_P&;
  auto save(std::string const& name, toki::Size const& id) -> Orthotope_DG_P&;
  friend auto
  swap(Orthotope_DG_P& lhs, Orthotope_DG_P& rhs) noexcept -> void
  {
    using std::swap;
    swap(lhs.info, rhs.info);
    swap(lhs.data, rhs.data);
    return;
  }
};
} // namespace toki::domain::detail
#endif
