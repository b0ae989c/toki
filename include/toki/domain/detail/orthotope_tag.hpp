#ifndef TOKI_DOMAIN_DETAIL_ORTHOTOPE_TAG_HPP
#define TOKI_DOMAIN_DETAIL_ORTHOTOPE_TAG_HPP
namespace toki::domain::detail {
enum struct Orthotope_Element
{
  DG,
};
enum struct Orthotope_Adaptivity
{
  None,
  P,
};
} // namespace toki::domain::detail
#endif
