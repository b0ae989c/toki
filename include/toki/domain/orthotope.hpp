#ifndef TOKI_DOMAIN_ORTHOTOPE_HPP
#define TOKI_DOMAIN_ORTHOTOPE_HPP
#include <type_traits>
#include "toki/domain/detail/orthotope_dg.hpp"
#include "toki/domain/detail/orthotope_dg_p.hpp"
#include "toki/domain/detail/orthotope_tag.hpp"
namespace toki::domain {
// clang-format off
template<
  int dim,
  detail::Orthotope_Element element,
  detail::Orthotope_Adaptivity adaptivity>
using Orthotope = std::conditional_t<
  element == detail::Orthotope_Element::DG,
    std::conditional_t<
      adaptivity == detail::Orthotope_Adaptivity::None,
      detail::Orthotope_DG<dim>,
    std::conditional_t<
      adaptivity == detail::Orthotope_Adaptivity::P,
      detail::Orthotope_DG_P<dim>,
    void>>,
  void>;
// clang-format on
} // namespace toki::domain
#endif
