#ifndef TOKI_INFO_HPP
#define TOKI_INFO_HPP
#include <string>
namespace toki::info {
extern int const version_major;
extern int const version_minor;
extern int const version_patch;
auto version_string() -> std::string;
} // namespace toki::info
#endif
