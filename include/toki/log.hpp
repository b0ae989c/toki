#ifndef TOKI_LOG_HPP
#define TOKI_LOG_HPP
#include <string>
#include "mpi.h"
namespace toki::log {
auto info(
  std::string const& message,
  int const mpi_rank = 0,
  MPI_Comm const mpi_communicator = MPI_COMM_WORLD) -> void;
auto error(
  std::string const& message,
  int const mpi_rank = 0,
  MPI_Comm const mpi_communicator = MPI_COMM_WORLD) -> void;
} // namespace toki::log
#endif
