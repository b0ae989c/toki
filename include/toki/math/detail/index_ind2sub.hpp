#ifndef TOKI_MATH_DETAIL_INDEX_IND2SUB_HPP
#define TOKI_MATH_DETAIL_INDEX_IND2SUB_HPP
#include <array>
namespace toki::math::detail {
template<int dim, typename Integer>
auto
index_ind2sub(std::array<Integer, dim> const& size, Integer input)
  -> std::array<Integer, dim>
{
  std::array<Integer, dim> output{};
  output.fill(0);
  for (int d = 0; d < dim; ++d) {
    output[d] = input % size[d];
    input /= size[d];
  }
  return output;
}
} // namespace toki::math::detail
#endif
