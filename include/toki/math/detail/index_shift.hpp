#ifndef TOKI_MATH_DETAIL_INDEX_SHIFT_HPP
#define TOKI_MATH_DETAIL_INDEX_SHIFT_HPP
#include <array>
namespace toki::math::detail {
template<typename Index>
auto
index_shift(
  Index const& size,
  Index output,
  typename Index::size_type const& dim,
  int const direction,
  typename Index::value_type const& input_delta) -> Index
{
  auto delta = (direction >= 0) ? (input_delta % size[dim])
                                : (size[dim] - (input_delta % size[dim]));
  output[dim] = (output[dim] + delta) % size[dim];
  return output;
}
} // namespace toki::math::detail
#endif
