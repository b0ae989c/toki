#ifndef TOKI_MATH_DETAIL_INDEX_SUB2IND_HPP
#define TOKI_MATH_DETAIL_INDEX_SUB2IND_HPP
#include <array>
namespace toki::math::detail {
template<int dim, typename Integer>
auto
index_sub2ind(
  std::array<Integer, dim> const& size,
  std::array<Integer, dim> const& input) -> Integer
{
  Integer output{};
  Integer stride{1};
  for (int d = 0; d < dim; ++d) {
    output += input[d] * stride;
    stride *= size[d];
  }
  return output;
}
} // namespace toki::math::detail
#endif
