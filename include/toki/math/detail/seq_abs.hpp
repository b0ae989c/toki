#ifndef TOKI_MATH_DETAIL_SEQ_ABS_HPP
#define TOKI_MATH_DETAIL_SEQ_ABS_HPP
#include <cmath>
#include <cstdlib>
#include <type_traits>
namespace toki::math::detail {
template<typename Scalar>
auto
seq_abs(Scalar const& a) -> Scalar
{
  if constexpr (std::is_floating_point_v<Scalar>) {
    return std::fabs(a);
  } else if constexpr (std::is_same_v<Scalar, long>) {
    return std::labs(a);
  } else if constexpr (std::is_same_v<Scalar, long long>) {
    return std::llabs(a);
  } else {
    return std::abs(a);
  }
}
} // namespace toki::math::detail
#endif
