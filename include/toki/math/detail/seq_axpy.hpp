#ifndef TOKI_MATH_DETAIL_SEQ_AXPY_HPP
#define TOKI_MATH_DETAIL_SEQ_AXPY_HPP
#include <cmath>
#include <type_traits>
#include "toki/config.hpp"
namespace toki::math::detail {
template<typename VectorX, typename VectorY>
auto
seq_axpy(
  toki::Size const size,
  typename VectorX::value_type const& alpha,
  typename VectorY::value_type const& beta,
  VectorX const& x,
  toki::Size const& x_begin,
  toki::Size const& x_inc,
  VectorY& y,
  toki::Size const& y_begin,
  toki::Size const& y_inc) -> void
{
  static_assert(
    std::is_same_v<typename VectorX::value_type, typename VectorY::value_type>);
  for (toki::Size index = 0; index < size; ++index) {
    y.set(
      std::fma(
        alpha,
        x.at(x_begin + x_inc * index),
        beta * y.at(y_begin + y_inc * index)),
      y_begin + y_inc * index);
  }
  return;
}
} // namespace toki::math::detail
#endif
