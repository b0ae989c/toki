#ifndef TOKI_MATH_DETAIL_SEQ_COPY_HPP
#define TOKI_MATH_DETAIL_SEQ_COPY_HPP
#include <type_traits>
#include "fmt/format.h"
#include "toki/config.hpp"
#include "toki/utility.hpp"
namespace toki::math::detail {
template<typename VectorY, typename VectorX>
auto
seq_copy(
  toki::Size const& size,
  VectorY& y,
  toki::Size const& y_begin,
  toki::Size const& y_inc,
  VectorX const& x,
  toki::Size const& x_begin,
  toki::Size const& x_inc) -> void
{
  static_assert(
    std::is_same_v<typename VectorY::value_type, typename VectorX::value_type>);
  for (toki::Size offset = 0; offset < size; ++offset) {
    toki::utility::confirm(
      y.valid_at(y_begin + y_inc * offset),
      fmt::format("output not valid at {}", y_begin + y_inc * offset));
    toki::utility::confirm(
      x.valid_at(x_begin + x_inc * offset),
      fmt::format("input not valid at {}", x_begin + x_inc * offset));
    y.update(y_begin + y_inc * offset, x.at(x_begin + x_inc * offset));
  }
  return;
}
} // namespace toki::math::detail
#endif
