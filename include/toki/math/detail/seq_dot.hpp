#ifndef TOKI_MATH_DETAIL_SEQ_DOT_HPP
#define TOKI_MATH_DETAIL_SEQ_DOT_HPP
#include <type_traits>
#include "fmt/format.h"
#include "toki/config.hpp"
#include "toki/utility.hpp"
namespace toki::math::detail {
template<typename VectorX, typename VectorY>
auto
seq_dot(
  toki::Size const& size,
  VectorX const& x,
  toki::Size const& x_begin,
  toki::Size const& x_inc,
  VectorY const& y,
  toki::Size const& y_begin,
  toki::Size const& y_inc) -> VectorX::value_type
{
  static_assert(
    std::is_same_v<typename VectorX::value_type, typename VectorY::value_type>);
  auto result = toki::zero<typename VectorX::value_type>;
  for (toki::Size offset = 0; offset < size; ++offset) {
    toki::utility::confirm(
      x.valid_at(x_begin + x_inc * offset),
      fmt::format("lhs not valid at {}", x_begin + x_inc * offset));
    toki::utility::confirm(
      y.valid_at(y_begin + y_inc * offset),
      fmt::format("rhs not valid at {}", y_begin + y_inc * offset));
    result = std::fma(
      x.at(x_begin + x_inc * offset), y.at(y_begin + y_inc * offset), result);
  }
  return result;
}
} // namespace toki::math::detail
#endif
