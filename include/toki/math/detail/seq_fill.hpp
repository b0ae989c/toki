#ifndef TOKI_MATH_DETAIL_SEQ_FILL_HPP
#define TOKI_MATH_DETAIL_SEQ_FILL_HPP
#include <type_traits>
#include "fmt/format.h"
#include "toki/config.hpp"
#include "toki/utility.hpp"
namespace toki::math::detail {
template<typename Scalar, typename Vector>
auto
seq_fill(
  toki::Size const& size,
  Scalar const& alpha,
  Vector& x,
  toki::Size const& x_begin,
  toki::Size const& x_inc) -> void
{
  static_assert(std::is_same_v<Scalar, typename Vector::value_type>);
  for (toki::Size offset = 0; offset < size; ++offset) {
    toki::utility::confirm(
      x.valid_at(x_begin + x_inc * offset),
      fmt::format("x not valid at {}", x_begin + x_inc * offset));
    x.set(alpha, x_begin + x_inc * offset);
  }
  return;
}
} // namespace toki::math::detail
#endif
