#ifndef TOKI_MATH_DETAIL_SEQ_GEMV_HPP
#define TOKI_MATH_DETAIL_SEQ_GEMV_HPP
#include <type_traits>
#include "toki/config.hpp"
#include "toki/math/detail/seq_axpy.hpp"
#include "toki/math/detail/seq_scal.hpp"
namespace toki::math::detail {
template<typename Matrix, typename VectorX, typename VectorY>
auto
seq_gemv(
  toki::Size const& row_max,
  toki::Size const& col_max,
  typename Matrix::value_type const& alpha,
  typename VectorY::value_type const& beta,
  VectorX const& x,
  toki::Size const& x_begin,
  toki::Size const& x_inc,
  VectorY& y,
  toki::Size const& y_begin,
  toki::Size const& y_inc,
  Matrix& A,
  toki::Size const& row_begin,
  toki::Size const& row_inc,
  toki::Size const& col_begin,
  toki::Size const& col_inc) -> void
{
  static_assert(
    std::is_same_v<typename Matrix::value_type, typename VectorX::value_type>);
  static_assert(
    std::is_same_v<typename Matrix::value_type, typename VectorY::value_type>);
  toki::math::detail::seq_scal(row_max, beta, y, y_begin, y_inc);
  for (toki::Size col = 0; col < col_max; ++col) {
    toki::math::detail::seq_axpy(
      row_max,
      alpha * x.at(x_begin + x_inc * col),
      toki::one<typename VectorY::value_type>,
      A,
      row_begin + A.extent[0] * (col_begin + col_inc * col),
      row_inc,
      y,
      y_begin,
      y_inc);
  }
  return;
}
template<typename Matrix, typename VectorX, typename VectorY>
auto
seq_gemv(
  typename Matrix::value_type const& alpha,
  typename VectorY::value_type const& beta,
  VectorX const& x,
  VectorY& y,
  Matrix& A) -> void
{
  return seq_gemv(
    A.extent[0], A.extent[1], alpha, beta, x, 0, 1, y, 0, 1, A, 0, 1, 0, 1);
}
} // namespace toki::math::detail
#endif
