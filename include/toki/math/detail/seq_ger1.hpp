#ifndef TOKI_MATH_DETAIL_SEQ_GER1_HPP
#define TOKI_MATH_DETAIL_SEQ_GER1_HPP
#include <type_traits>
#include "fmt/format.h"
#include "toki/config.hpp"
#include "toki/utility.hpp"
namespace toki::math::detail {
template<typename Matrix, typename VectorX, typename VectorY>
auto
seq_ger1(
  toki::Size const& row_max,
  toki::Size const& col_max,
  typename Matrix::value_type const& alpha,
  VectorX const& x,
  toki::Size const& x_begin,
  toki::Size const& x_inc,
  VectorY const& y,
  toki::Size const& y_begin,
  toki::Size const& y_inc,
  Matrix& A,
  toki::Size const& row_begin,
  toki::Size const& row_inc,
  toki::Size const& col_begin,
  toki::Size const& col_inc) -> void
{
  static_assert(
    std::is_same_v<typename Matrix::value_type, typename VectorX::value_type>);
  static_assert(
    std::is_same_v<typename Matrix::value_type, typename VectorY::value_type>);
  using value_type = typename Matrix::value_type;
  if (alpha == toki::zero<value_type>) {
    // do nothing
  } else if (alpha == toki::one<value_type>) {
    for (toki::Size col = 0; col < col_max; ++col) {
      toki::utility::confirm(
        y.valid_at(y_begin + y_inc * col),
        fmt::format("y is invalid at {}", y_begin + y_inc * col));
      value_type const& tmp = y.at(y_begin + y_inc * col);
      if (tmp != toki::zero<value_type>) {
        for (toki::Size row = 0; row < row_max; ++row) {
          toki::utility::confirm(
            x.valid_at(x_begin + x_inc * row),
            fmt::format("x is invalid at {}", x_begin + x_inc * row));
          toki::utility::confirm(
            A.valid_at(row_begin + row_inc * row, col_begin + col_inc * col),
            fmt::format(
              "output is invalid at [ {}, {} ]",
              row_begin + row_inc * row,
              col_begin + col_inc * col));
          if constexpr (std::is_floating_point_v<value_type>) {
            A.set(
              std::fma(
                x.at(x_begin + x_inc * row),
                tmp,
                A.at(row_begin + row_inc * row, col_begin + col_inc * col)),
              row_begin + row_inc * row,
              col_begin + col_inc * col);
          } else {
            A.set(
              A.at(row_begin + row_inc * row, col_begin + col_inc * col) +
                x.at(x_begin + x_inc * row) * tmp,
              row_begin + row_inc * row,
              col_begin + col_inc * col);
          }
        }
      }
    }
  } else {
    for (toki::Size col = 0; col < col_max; ++col) {
      toki::utility::confirm(
        y.valid_at(y_begin + y_inc * col),
        fmt::format("y is invalid at {}", y_begin + y_inc * col));
      value_type const tmp = alpha * y.at(y_begin + y_inc * col);
      if (tmp != toki::zero<value_type>) {
        for (toki::Size row = 0; row < row_max; ++row) {
          toki::utility::confirm(
            x.valid_at(x_begin + x_inc * row),
            fmt::format("x is invalid at {}", x_begin + x_inc * row));
          toki::utility::confirm(
            A.valid_at(row_begin + row_inc * row, col_begin + col_inc * col),
            fmt::format(
              "output is invalid at [ {}, {} ]",
              row_begin + row_inc * row,
              col_begin + col_inc * col));
          if constexpr (std::is_floating_point_v<value_type>) {
            A.set(
              std::fma(
                x.at(x_begin + x_inc * row),
                tmp,
                A.at(row_begin + row_inc * row, col_begin + col_inc * col)),
              row_begin + row_inc * row,
              col_begin + col_inc * col);
          } else {
            A.set(
              A.at(row_begin + row_inc * row, col_begin + col_inc * col) +
                x.at(x_begin + x_inc * row) * tmp,
              row_begin + row_inc * row,
              col_begin + col_inc * col);
          }
        }
      }
    }
  }
  return;
}
} // namespace toki::math::detail
#endif
