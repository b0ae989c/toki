#ifndef TOKI_MATH_DETAIL_SEQ_GETF_HPP
#define TOKI_MATH_DETAIL_SEQ_GETF_HPP
#include <algorithm>
#include <type_traits>
#include "fmt/format.h"
#include "toki/config.hpp"
#include "toki/math/detail/seq_ger1.hpp"
#include "toki/math/detail/seq_imaxabs.hpp"
#include "toki/math/detail/seq_scal.hpp"
#include "toki/math/detail/seq_swap.hpp"
#include "toki/utility.hpp"
namespace toki::math::detail {
template<typename Matrix, typename Permutation>
auto
seq_getf(
  toki::Size const& row_max,
  toki::Size const& col_max,
  Matrix& A,
  toki::Size const& row_begin,
  toki::Size const& row_inc,
  toki::Size const& col_begin,
  toki::Size const& col_inc,
  Permutation& p,
  toki::Size const& p_begin,
  toki::Size const& p_inc) -> void
{
  static_assert(std::is_same_v<typename Permutation::value_type, toki::Size>);
  auto step_max = std::min(row_max, col_max);
  p.set(row_begin + row_inc * (step_max - 1), p_begin + p_inc * (step_max - 1));
  for (toki::Size row = 0; row < step_max - 1; ++row) {
    auto const row_current = row_begin + row_inc * row;
    auto const col_current = col_begin + col_inc * row;
    // find partial pivot
    auto const pivot =
      toki::math::detail::seq_imaxabs(
        row_max - row, A, row_current + A.extent[0] * col_current, row_inc) %
      A.extent[0];
    p.set(pivot, p_begin + p_inc * row);
    auto const pivot_value = A.at(pivot, col_current);
    toki::utility::confirm(
      pivot_value != toki::zero<toki::Number>,
      fmt::format("pivot value at [ {}, {} ] is zero", pivot, col_current));
    // apply partial pivot
    if (pivot != row_current) {
      toki::math::detail::seq_swap(
        col_max,
        A,
        row_current + A.extent[0] * col_begin,
        A.extent[0],
        A,
        pivot + A.extent[0] * col_begin,
        A.extent[0]);
    }
    // normalize
    if (row != (row_max - 1)) {
      toki::math::detail::seq_scal(
        row_max - row - 1,
        toki::one<toki::Number> / pivot_value,
        A,
        (row_current + row_inc) + A.extent[0] * col_current,
        row_inc);
    }
    // rank-1 update
    if (row != (row_max - 1)) {
      toki::math::detail::seq_ger1(
        row_max - row - 1,
        col_max - row - 1,
        -toki::one<toki::Number>,
        A,
        (row_current + row_inc) + A.extent[0] * col_current,
        row_inc,
        A,
        row_current + A.extent[0] * (col_current + col_inc),
        A.extent[0] * col_inc,
        A,
        row_current + row_inc,
        row_inc,
        col_current + col_inc,
        col_inc);
    }
  }
  return;
}
template<typename Matrix, typename Permutation>
auto
seq_getf(Matrix& A, Permutation& p) -> void
{
  return seq_getf(A.extent[0], A.extent[1], A, 0, 1, 0, 1, p, 0, 1);
}
} // namespace toki::math::detail
#endif
