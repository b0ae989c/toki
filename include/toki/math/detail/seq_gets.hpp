#ifndef TOKI_MATH_DETAIL_SEQ_GETS_HPP
#define TOKI_MATH_DETAIL_SEQ_GETS_HPP
#include <type_traits>
#include "toki/config.hpp"
#include "toki/math/detail/seq_rswp.hpp"
#include "toki/math/detail/seq_trsm.hpp"
#include "toki/math/detail/seq_trsv.hpp"
namespace toki::math::detail {
template<typename Coefficient, typename Permutation, typename Result>
auto
seq_gets(
  toki::Size n_eqn,
  toki::Size n_rhs,
  typename Coefficient::value_type const& alpha,
  Coefficient const& A,
  Permutation const& p,
  Result& b) -> void
{
  static_assert(
    std::
      is_same_v<typename Coefficient::value_type, typename Result::value_type>);
  toki::utility::confirm(n_eqn == A.extent[0], "invalid parameter: A");
  toki::utility::confirm(n_eqn == p.size, "invalid parameter: p");
  toki::utility::confirm(n_eqn == b.extent[0], "invalid parameter: b");
  toki::utility::confirm(n_rhs >= 1, "invalid parameter: n_rhs");
  toki::math::detail::seq_rswp(n_eqn, n_rhs, b, 0, 1, 0, 1, p, 0, 1);
  auto const& one = toki::one<typename Result::value_type>;
  if (n_rhs == 1) {
    toki::math::detail::seq_trsv<true>(n_eqn, alpha, A, 0, 1, 0, 1, b, 0, 1);
    toki::math::detail::seq_trsv<false>(n_eqn, one, A, 0, 1, 0, 1, b, 0, 1);
  } else {
    toki::utility::confirm(n_rhs == b.extent[1], "invalid parameter: b");
    toki::math::detail::seq_trsm<true>(
      n_eqn, n_rhs, alpha, A, 0, 1, 0, 1, b, 0, 1, 0, 1);
    toki::math::detail::seq_trsm<false>(
      n_eqn, n_rhs, one, A, 0, 1, 0, 1, b, 0, 1, 0, 1);
  }
  return;
}
template<typename Coefficient, typename Permutation, typename Result>
auto
seq_gets(Coefficient const& A, Permutation const& p, Result& b) -> void
{
  return seq_gets(
    A.extent[0], 1, toki::one<typename Coefficient::value_type>, A, p, b);
}
} // namespace toki::math::detail
#endif
