#ifndef TOKI_MATH_DETAIL_SEQ_IMAXABS_HPP
#define TOKI_MATH_DETAIL_SEQ_IMAXABS_HPP
#include "fmt/format.h"
#include "toki/config.hpp"
#include "toki/math/detail/seq_abs.hpp"
#include "toki/utility.hpp"
namespace toki::math::detail {
template<typename Vector>
auto
seq_imaxabs(
  toki::Size const& size,
  Vector const& x,
  toki::Size const& x_begin,
  toki::Size const& x_inc) -> toki::Size
{
  toki::utility::confirm(
    x.valid_at(x_begin), fmt::format("x not valid at {}", x_begin));
  toki::Size result{x_begin};
  typename Vector::value_type value{toki::math::detail::seq_abs(x.at(x_begin))};
  for (toki::Size index = 1; index < size; ++index) {
    toki::utility::confirm(
      x.valid_at(x_begin + x_inc * index),
      fmt::format("x not valid at {}", x_begin + x_inc * index));
    if (toki::math::detail::seq_abs(x.at(x_begin + x_inc * index)) > value) {
      result = x_begin + x_inc * index;
      value = x.at(x_begin + x_inc * index);
    }
  }
  return result;
}
} // namespace toki::math::detail
#endif
