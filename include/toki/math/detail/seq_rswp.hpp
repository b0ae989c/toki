#ifndef TOKI_MATH_DETAIL_SEQ_RSWP_HPP
#define TOKI_MATH_DETAIL_SEQ_RSWP_HPP
#include <type_traits>
#include "toki/config.hpp"
#include "toki/math/detail/seq_swap.hpp"
#include "toki/utility.hpp"
namespace toki::math::detail {
template<typename Matrix, typename Permutation>
auto
seq_rswp(
  toki::Size const& row_max,
  toki::Size const& col_max,
  Matrix& A,
  toki::Size const& row_begin,
  toki::Size const& row_inc,
  toki::Size const& col_begin,
  toki::Size const& col_inc,
  Permutation const& p,
  toki::Size const& p_begin,
  toki::Size const& p_inc) -> void
{
  static_assert(std::is_same_v<typename Permutation::value_type, toki::Size>);
  for (toki::Size row = 0; row < row_max; ++row) {
    auto const row_current = row_begin + row * row_inc;
    auto const col_current = col_begin + 0 * col_inc;
    if (p.at(p_begin + row * p_inc) != row_current) {
      toki::math::detail::seq_swap(
        col_max,
        A,
        row_current + A.extent[0] * col_current,
        A.extent[0] * col_inc,
        A,
        p.at(p_begin + row * p_inc) + A.extent[0] * col_current,
        A.extent[0] * col_inc);
    }
  }
  return;
}
} // namespace toki::math::detail
#endif
