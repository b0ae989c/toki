#ifndef TOKI_MATH_DETAIL_SEQ_SCAL_HPP
#define TOKI_MATH_DETAIL_SEQ_SCAL_HPP
#include <type_traits>
#include "fmt/format.h"
#include "toki/config.hpp"
#include "toki/math/detail/seq_fill.hpp"
#include "toki/utility.hpp"
namespace toki::math::detail {
template<typename Scalar, typename Vector>
auto
seq_scal(
  toki::Size const& size,
  Scalar const& alpha,
  Vector& x,
  toki::Size const& x_begin,
  toki::Size const& x_inc) -> void
{
  static_assert(std::is_same_v<Scalar, typename Vector::value_type>);
  if (alpha == toki::zero<Scalar>) {
    toki::math::detail::seq_fill(size, alpha, x, x_begin, x_inc);
  } else if (alpha == toki::one<Scalar>) {
    // do nothing
  } else {
    for (toki::Size offset = 0; offset < size; ++offset) {
      toki::utility::confirm(
        x.valid_at(x_begin + x_inc * offset),
        fmt::format("x not valid at {}", x_begin + x_inc * offset));
      x.set(alpha * x.at(x_begin + x_inc * offset), x_begin + x_inc * offset);
    }
  }
  return;
}
} // namespace toki::math::detail
#endif
