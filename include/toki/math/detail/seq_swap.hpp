#ifndef TOKI_MATH_DETAIL_SEQ_SWAP_HPP
#define TOKI_MATH_DETAIL_SEQ_SWAP_HPP
#include <type_traits>
#include <utility>
#include "fmt/format.h"
#include "toki/config.hpp"
#include "toki/utility.hpp"
namespace toki::math::detail {
template<typename VectorX, typename VectorY>
auto
seq_swap(
  toki::Size const& size,
  VectorX& x,
  toki::Size const& x_begin,
  toki::Size const& x_inc,
  VectorY& y,
  toki::Size const& y_begin,
  toki::Size const& y_inc) -> void
{
  static_assert(
    std::is_same_v<typename VectorX::value_type, typename VectorY::value_type>);
  typename VectorX::value_type tmp{};
  for (toki::Size offset = 0; offset < size; ++offset) {
    toki::utility::confirm(
      x.valid_at(x_begin + x_inc * offset),
      fmt::format("lhs not valid at {}", x_begin + x_inc * offset));
    toki::utility::confirm(
      y.valid_at(y_begin + y_inc * offset),
      fmt::format("rhs not valid at {}", y_begin + y_inc * offset));
    tmp = x.at(x_begin + x_inc * offset);
    x.set(y.at(y_begin + y_inc * offset), x_begin + x_inc * offset);
    y.set(tmp, y_begin + y_inc * offset);
  }
  return;
}
} // namespace toki::math::detail
#endif
