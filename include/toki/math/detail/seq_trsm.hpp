#ifndef TOKI_MATH_DETAIL_SEQ_TRSM_HPP
#define TOKI_MATH_DETAIL_SEQ_TRSM_HPP
#include <type_traits>
#include "toki/config.hpp"
#include "toki/math/detail/seq_trsv.hpp"
namespace toki::math::detail {
template<bool lower, typename MatrixA, typename MatrixB>
auto
seq_trsm(
  toki::Size const& n,
  toki::Size const& B_col_max,
  typename MatrixB::value_type const& alpha,
  MatrixA const& A,
  toki::Size const& A_row_begin,
  toki::Size const& A_row_inc,
  toki::Size const& A_col_begin,
  toki::Size const& A_col_inc,
  MatrixB& B,
  toki::Size const& B_row_begin,
  toki::Size const& B_row_inc,
  toki::Size const& B_col_begin,
  toki::Size const& B_col_inc) -> void
{
  static_assert(
    std::is_same_v<typename MatrixA::value_type, typename MatrixB::value_type>);
  for (toki::Size col = 0; col < B_col_max; ++col) {
    toki::math::detail::seq_trsv<lower>(
      n,
      alpha,
      A,
      A_row_begin,
      A_row_inc,
      A_col_begin,
      A_col_inc,
      B,
      B_row_begin + B.extent[0] * (B_col_begin + B_col_inc * col),
      B_row_inc);
  }
  return;
}
} // namespace toki::math::detail
#endif
