#ifndef TOKI_MATH_DETAIL_SEQ_TRSV_HPP
#define TOKI_MATH_DETAIL_SEQ_TRSV_HPP
#include <type_traits>
#include "toki/config.hpp"
#include "toki/math/detail/seq_fill.hpp"
#include "toki/math/detail/seq_scal.hpp"
namespace toki::math::detail {
template<bool lower, typename Matrix, typename Vector>
auto
seq_trsv(
  toki::Size const& n,
  typename Matrix::value_type const& alpha,
  Matrix const& A,
  toki::Size const& row_begin,
  toki::Size const& row_inc,
  toki::Size const& col_begin,
  toki::Size const& col_inc,
  Vector& b,
  toki::Size const& b_begin,
  toki::Size const& b_inc) -> void
{
  static_assert(
    std::is_same_v<typename Matrix::value_type, typename Vector::value_type>);
  if (alpha == toki::zero<typename Matrix::value_type>) {
    toki::math::detail::seq_fill(n, alpha, b, b_begin, b_inc);
    return;
  }
  if (alpha != toki::one<typename Matrix::value_type>) {
    toki::math::detail::seq_scal(n, alpha, b, b_begin, b_inc);
  }
  if constexpr (lower) {
    for (toki::Size ell = 0; ell < n; ++ell) {
      if (
        b.at(b_begin + b_inc * ell) !=
        toki::zero<typename Vector::value_type>) {
        for (toki::Size row = ell + 1; row < n; ++row) {
          b.set(
            b.at(b_begin + b_inc * row) -
              A.at(row_begin + row_inc * row, col_begin + col_inc * ell) *
                b.at(b_begin + b_inc * ell),
            b_begin + b_inc * row);
        }
      }
    }
  } else {
    for (toki::Size tmp = 0; tmp < n; ++tmp) {
      auto const ell = n - tmp - 1;
      if (
        b.at(b_begin + b_inc * ell) !=
        toki::zero<typename Vector::value_type>) {
        b.set(
          b.at(b_begin + b_inc * ell) /
            A.at(row_begin + row_inc * ell, col_begin + col_inc * ell),
          b_begin + b_inc * ell);
        for (toki::Size row = 0; row < ell; ++row) {
          b.set(
            b.at(b_begin + b_inc * row) -
              A.at(row_begin + row_inc * row, col_begin + col_inc * ell) *
                b.at(b_begin + b_inc * ell),
            b_begin + b_inc * row);
        }
      }
    }
  }
  return;
}
} // namespace toki::math::detail
#endif
