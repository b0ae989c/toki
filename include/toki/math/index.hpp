#ifndef TOKI_MATH_INDEX_HPP
#define TOKI_MATH_INDEX_HPP
#include <utility>
#include "toki/math/detail/index_ind2sub.hpp"
#include "toki/math/detail/index_shift.hpp"
#include "toki/math/detail/index_sub2ind.hpp"
namespace toki::math::index {
template<int dim, typename... Argument>
auto
ind2sub(Argument&&... arg)
{
  return toki::math::detail::index_ind2sub<dim>(std::forward<Argument>(arg)...);
}
template<int dim, typename... Argument>
auto
sub2ind(Argument&&... arg)
{
  return toki::math::detail::index_sub2ind<dim>(std::forward<Argument>(arg)...);
}
template<typename... Argument>
auto
shift(Argument&&... arg)
{
  return toki::math::detail::index_shift(std::forward<Argument>(arg)...);
}
} // namespace toki::math::index
#endif
