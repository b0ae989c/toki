#ifndef TOKI_MATH_PERMUTATION_HPP
#define TOKI_MATH_PERMUTATION_HPP
#include <functional>
#include <memory>
#include <utility>
#include "toki/config.hpp"
#ifdef TOKI_DEBUG
#include "fmt/format.h"
#endif
namespace toki::math::permutation {
struct Permutation
{
  using value_type = toki::Size;
  toki::Size size;
  std::shared_ptr<value_type[]> data;
  Permutation();
  explicit Permutation(toki::Size const& size);
  Permutation(Permutation const& other);
  Permutation(Permutation&& other) noexcept;
  ~Permutation();
  auto operator=(Permutation other) -> Permutation&;
  auto valid_at(toki::Size const& index) const -> bool;
  auto set(value_type const& value, toki::Size const& index) -> Permutation&;
  auto set(std::function<value_type(toki::Size const&)> const& op)
    -> Permutation&;
  auto at(toki::Size const& index) const -> value_type const&;
  auto iota(toki::Size begin, toki::Size const& inc, toki::Size value = 0)
    -> Permutation&;
#ifdef TOKI_DEBUG
  auto
  print() -> Permutation&
  {
    fmt::print("\n");
    for (toki::Size row = 0; row < size; ++row) {
      fmt::print("{};\n", at(row));
    }
    return *this;
  }
#endif
  friend auto
  swap(Permutation& lhs, Permutation& rhs) noexcept -> void
  {
    using std::swap;
    swap(lhs.size, rhs.size);
    swap(lhs.data, rhs.data);
    return;
  }
};
} // namespace toki::math::permutation
#endif
