#ifndef TOKI_MATH_SEQ_HPP
#define TOKI_MATH_SEQ_HPP
#include <utility>
#include "toki/math/detail/seq_abs.hpp"
#include "toki/math/detail/seq_axpy.hpp"
#include "toki/math/detail/seq_copy.hpp"
#include "toki/math/detail/seq_dot.hpp"
#include "toki/math/detail/seq_fill.hpp"
#include "toki/math/detail/seq_gemv.hpp"
#include "toki/math/detail/seq_ger1.hpp"
#include "toki/math/detail/seq_getf.hpp"
#include "toki/math/detail/seq_gets.hpp"
#include "toki/math/detail/seq_imaxabs.hpp"
#include "toki/math/detail/seq_rswp.hpp"
#include "toki/math/detail/seq_scal.hpp"
#include "toki/math/detail/seq_swap.hpp"
#include "toki/math/detail/seq_trsm.hpp"
#include "toki/math/detail/seq_trsv.hpp"
namespace toki::math::seq {
template<typename... Argument>
auto
abs(Argument&&... arg)
{
  return toki::math::detail::seq_abs(std::forward<Argument>(arg)...);
}
template<typename... Argument>
auto
imaxabs(Argument&&... arg)
{
  return toki::math::detail::seq_imaxabs(std::forward<Argument>(arg)...);
}
template<typename... Argument>
auto
fill(Argument&&... arg)
{
  return toki::math::detail::seq_fill(std::forward<Argument>(arg)...);
}
template<typename... Argument>
auto
scal(Argument&&... arg)
{
  return toki::math::detail::seq_scal(std::forward<Argument>(arg)...);
}
template<typename... Argument>
auto
copy(Argument&&... arg)
{
  return toki::math::detail::seq_copy(std::forward<Argument>(arg)...);
}
template<typename... Argument>
auto
swap(Argument&&... arg)
{
  return toki::math::detail::seq_swap(std::forward<Argument>(arg)...);
}
template<typename... Argument>
auto
axpy(Argument&&... arg)
{
  return toki::math::detail::seq_axpy(std::forward<Argument>(arg)...);
}
template<typename... Argument>
auto
rswp(Argument&&... arg)
{
  return toki::math::detail::seq_rswp(std::forward<Argument>(arg)...);
}
template<typename... Argument>
auto
dot(Argument&&... arg)
{
  return toki::math::detail::seq_dot(std::forward<Argument>(arg)...);
}
template<typename... Argument>
auto
ger1(Argument&&... arg)
{
  return toki::math::detail::seq_ger1(std::forward<Argument>(arg)...);
}
template<typename... Argument>
auto
gemv(Argument&&... arg)
{
  return toki::math::detail::seq_gemv(std::forward<Argument>(arg)...);
}
template<typename... Argument>
auto
getf(Argument&&... arg)
{
  return toki::math::detail::seq_getf(std::forward<Argument>(arg)...);
}
template<bool lower, typename... Argument>
auto
trsv(Argument&&... arg)
{
  return toki::math::detail::seq_trsv<lower>(std::forward<Argument>(arg)...);
}
template<bool lower, typename... Argument>
auto
trsm(Argument&&... arg)
{
  return toki::math::detail::seq_trsm<lower>(std::forward<Argument>(arg)...);
}
template<typename... Argument>
auto
gets(Argument&&... arg)
{
  return toki::math::detail::seq_gets(std::forward<Argument>(arg)...);
}
} // namespace toki::math::seq
#endif
