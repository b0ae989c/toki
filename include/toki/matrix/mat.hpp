#ifndef TOKI_MATRIX_MAT_HPP
#define TOKI_MATRIX_MAT_HPP
#include <array>
#include <functional>
#include <memory>
#include <utility>
#include "toki/config.hpp"
#ifdef TOKI_DEBUG
#include "fmt/format.h"
#endif
namespace toki::matrix {
struct Mat
{
  using value_type = toki::Number;
  std::array<toki::Size, 2> extent;
  std::shared_ptr<value_type[]> data;
  Mat();
  Mat(toki::Size const& extent_0, toki::Size const& extent_1);
  Mat(Mat const& other);
  Mat(Mat&& other) noexcept;
  ~Mat();
  auto operator=(Mat other) -> Mat&;
  auto begin() const -> value_type*;
  auto end() const -> value_type*;
  auto valid_at(toki::Size const& offset) const -> bool;
  auto valid_at(toki::Size const& index_0, toki::Size const& index_1) const
    -> bool;
  auto set(value_type const& value, toki::Size const& offset) -> Mat&;
  auto set(
    value_type const& value,
    toki::Size const& index_0,
    toki::Size const& index_1) -> Mat&;
  auto set(std::function<value_type(toki::Size const&)> const& op) -> Mat&;
  auto set(
    std::function<value_type(toki::Size const&, toki::Size const&)> const& op)
    -> Mat&;
  auto at(toki::Size const& offset) const -> value_type const&;
  auto at(toki::Size const& index_0, toki::Size const& index_1) const
    -> value_type const&;
#ifdef TOKI_DEBUG
  auto
  print() const -> Mat const&
  {
    fmt::print("\n");
    for (toki::Size row = 0; row < extent[0]; ++row) {
      for (toki::Size col = 0; col < extent[1]; ++col) {
        fmt::print("{:+.6e}, ", at(row, col));
      }
      fmt::print(";\n");
    }
    return *this;
  }
#endif
  friend auto
  swap(Mat& lhs, Mat& rhs) noexcept -> void
  {
    using std::swap;
    swap(lhs.extent, rhs.extent);
    swap(lhs.data, rhs.data);
    return;
  }
};
} // namespace toki::matrix
#endif
