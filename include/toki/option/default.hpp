#ifndef TOKI_OPTION_DEFAULT_HPP
#define TOKI_OPTION_DEFAULT_HPP
#include <string>
#include <vector>
#include "boost/program_options.hpp"
#include "toki/config.hpp"
namespace toki::option {
struct Default
{
  struct Parameter
  {
    struct
    {
      std::string name;
    } info;
    struct
    {
      std::vector<int> extent;
    } mpi;
    struct
    {
      toki::Size order;
      toki::Number CFL;
    } method;
    struct
    {
      toki::Number max;
      toki::Number delta_max;
      toki::Size step_max;
    } time;
    struct
    {
      std::vector<toki::Size> resolution;
      std::vector<toki::Number> range;
      std::vector<toki::Number> anchor;
    } domain;
    struct
    {
      std::vector<toki::Number> a;
      std::vector<toki::Number> c;
      std::vector<toki::Number> n;
      std::vector<toki::Number> r;
      std::vector<toki::Number> u;
      std::vector<toki::Number> v;
      std::vector<toki::Number> w;
      std::vector<toki::Number> theta;
      std::vector<toki::Number> rho;
      struct
      {
        bool report_error;
        bool apply_source;
      } flag;
    } misc;
  };
  int argc;
  char** argv;
  std::string handle;
  boost::program_options::variables_map data;
  boost::program_options::options_description description;
  Parameter param{};
  Default() = delete;
  Default(int argc, char** argv, std::string const& handle);
  Default(Default const&) = delete;
  Default(Default&&) noexcept = delete;
  ~Default();
  auto operator=(Default const&) -> Default& = delete;
  auto operator=(Default&&) noexcept -> Default& = delete;
  auto parse() -> Default&;
  auto print() -> Default&;
};
} // namespace toki::option
#endif
