#ifndef TOKI_OPTION_VP_HPP
#define TOKI_OPTION_VP_HPP
#include <string>
#include <vector>
#include "boost/program_options.hpp"
#include "toki/config.hpp"
namespace toki::option {
struct VP
{
  struct Parameter
  {
    struct Info
    {
      std::string name;
    };
    struct MPI
    {
      std::vector<int> extent;
    };
    struct Model
    {
      toki::Size species = 1;
    };
    struct Method
    {
      bool adaptivity_p;
      toki::Size order;
      toki::Number CFL;
      std::vector<toki::Number> refinement_score;
    };
    struct Time
    {
      toki::Number max;
      toki::Number delta_min;
      toki::Number delta_max;
      toki::Size step_max;
      std::vector<toki::Number> checkpoint;
    };
    struct Domain
    {
      toki::Size resolution;
      toki::Number range;
    };
    struct Feature
    {
      std::vector<toki::Number> a;
      std::vector<toki::Number> k;
      std::vector<toki::Number> m;
      std::vector<toki::Number> q;
      std::vector<toki::Number> r;
      std::vector<toki::Number> J;
      std::vector<toki::Number> T;
      std::vector<toki::Number> theta;
      std::vector<toki::Number> rho;
      std::vector<toki::Number> omega;
    };
    struct Flag
    {
      struct Info
      {
        bool detail;
      };
      struct Performance
      {
        bool skip_zero_element;
      };
      struct Background
      {
        bool current;
        bool plasma_source;
      };
      struct Refinement
      {
        bool sync_all_species;
      };
      Info info;
      Background background;
      Performance performance;
      Refinement refinement;
    };
    Info info;
    MPI mpi;
    Model model;
    Method method;
    Time time;
    Domain domain;
    Feature feature;
    Flag flag;
  };
  int argc;
  char** argv;
  std::string handle;
  boost::program_options::variables_map data;
  boost::program_options::options_description description;
  Parameter param;
  VP() = delete;
  VP(int argc, char** argv, std::string const& handle);
  VP(VP const&) = delete;
  VP(VP&&) noexcept = delete;
  ~VP();
  auto operator=(VP const&) -> VP& = delete;
  auto operator=(VP&&) noexcept -> VP& = delete;
  auto parse() -> VP&;
  auto print() -> VP&;
};
} // namespace toki::option
#endif
