#ifndef TOKI_QUADRATURE_LOBATTO_HPP
#define TOKI_QUADRATURE_LOBATTO_HPP
#include <array>
#include <functional>
#include <utility>
#include "toki/config.hpp"
#include "toki/vector/vec.hpp"
namespace toki::quadrature {
struct Lobatto
{
  struct Info
  {
    toki::Size point;
    toki::vector::Vec weight;
    toki::vector::Vec abscissa;
  };
  Info info;
  Lobatto();
  Lobatto(toki::Size const& point);
  Lobatto(Lobatto const& other);
  Lobatto(Lobatto&& other) noexcept;
  ~Lobatto();
  auto operator=(Lobatto other) -> Lobatto&;
  template<int dim>
  auto sum(
    toki::Number& result,
    bool const update,
    toki::vector::Vec const& sample,
    toki::Size const& offset) const -> Lobatto const&;
  template<int dim>
  auto sum(toki::vector::Vec const& sample, toki::Size const& offset) const
    -> toki::Number;
  template<int dim>
  auto sum(
    toki::Number& result,
    bool const update,
    std::function<toki::Number(std::array<toki::Number, dim> const&)> const& op)
    const -> Lobatto const&;
  template<int dim>
  auto sum(
    std::function<toki::Number(std::array<toki::Number, dim> const&)> const& op)
    const -> toki::Number;
  template<int dim>
  auto sum(
    toki::Number& result,
    bool const update,
    std::function<toki::Number(
      std::array<toki::Number, dim> const&,
      toki::Size const&)> const& op) const -> Lobatto const&;
  template<int dim>
  auto sum(std::function<toki::Number(
             std::array<toki::Number, dim> const&,
             toki::Size const&)> const& op) const -> toki::Number;
  friend auto
  swap(Lobatto& lhs, Lobatto& rhs) noexcept -> void
  {
    using std::swap;
    swap(lhs.info, rhs.info);
    return;
  }
};
} // namespace toki::quadrature
#endif
