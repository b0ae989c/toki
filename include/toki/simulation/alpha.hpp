#ifndef TOKI_SIMULATION_ALPHA_HPP
#define TOKI_SIMULATION_ALPHA_HPP
#include <type_traits>
#include "toki/simulation/detail/alpha_1_1.hpp"
#include "toki/simulation/detail/alpha_1_2.hpp"
#include "toki/simulation/detail/alpha_tag.hpp"
namespace toki::simulation {
// clang-format off
template<
  detail::Alpha_Name name,
  detail::Alpha_Variant variant>
using Alpha =
std::conditional_t<
  name == detail::Alpha_Name::LinearAdvection_HRIDG,
    std::conditional_t<
      variant == detail::Alpha_Variant::Equation_1D,
      detail::Alpha_1_1,
    std::conditional_t<
      variant == detail::Alpha_Variant::Equation_2D,
      detail::Alpha_1_2,
    void>>,
  void>;
// clang-format on
} // namespace toki::simulation
#endif
