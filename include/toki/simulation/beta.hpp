#ifndef TOKI_SIMULATION_BETA_HPP
#define TOKI_SIMULATION_BETA_HPP
#include <type_traits>
#include "toki/simulation/detail/beta_1_1.hpp"
#include "toki/simulation/detail/beta_1_2.hpp"
#include "toki/simulation/detail/beta_1_3.hpp"
#include "toki/simulation/detail/beta_1_4.hpp"
#include "toki/simulation/detail/beta_1_5.hpp"
#include "toki/simulation/detail/beta_2_1.hpp"
#include "toki/simulation/detail/beta_tag.hpp"
namespace toki::simulation {
// clang-format off
template<
  detail::Beta_Name name,
  detail::Beta_Variant variant>
using Beta =
std::conditional_t<
  name == detail::Beta_Name::VlasovPoisson_1D1V_HRIDG,
    std::conditional_t<
      variant == detail::Beta_Variant::ManufacturedSolution,
      detail::Beta_1_1,
    std::conditional_t<
      variant == detail::Beta_Variant::TwoStream,
      detail::Beta_1_2,
    std::conditional_t<
      variant == detail::Beta_Variant::LandauDamping,
      detail::Beta_1_3,
    std::conditional_t<
      variant == detail::Beta_Variant::BumpOnTail,
      detail::Beta_1_4,
    std::conditional_t<
      variant == detail::Beta_Variant::PlasmaSheath,
      detail::Beta_1_5,
    void>>>>>,
std::conditional_t<
  name == detail::Beta_Name::VlasovPoissonTwoSpecies_1D1V_HRIDG,
    std::conditional_t<
      variant == detail::Beta_Variant::IonAcousticWave,
      detail::Beta_2_1,
    void>,
  void>>;
// clang-format on
} // namespace toki::simulation
#endif
