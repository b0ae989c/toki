#ifndef TOKI_SIMULATION_DETAIL_ALPHA_1_2_HPP
#define TOKI_SIMULATION_DETAIL_ALPHA_1_2_HPP
#include <algorithm>
#include <array>
#include <cmath>
#include <memory>
#include "fmt/format.h"
#include "mpi.h"
#include "toki/config.hpp"
#include "toki/context.hpp"
#include "toki/log.hpp"
#include "toki/option.hpp"
#include "toki/timestepper/hridg.hpp"
#include "toki/utility.hpp"
namespace toki::simulation::detail {
struct Alpha_1_2
{
  using context_type = toki::context::Default;
  using option_type = toki::option::Default;
  using timestepper_type = toki::timestepper::HRIDG<2>;
  std::unique_ptr<context_type> context;
  std::unique_ptr<option_type> option;
  std::unique_ptr<timestepper_type> timestepper;
  Alpha_1_2()
    : context{nullptr}
    , option{nullptr}
    , timestepper{nullptr}
  {
  }
  Alpha_1_2(int argc, char** argv)
    : context(std::make_unique<context_type>(argc, argv))
    , option(std::make_unique<option_type>(argc, argv, "alpha-1-2"))
    , timestepper{nullptr}
  {
  }
  Alpha_1_2(Alpha_1_2 const&) = delete;
  Alpha_1_2(Alpha_1_2&&) noexcept = delete;
  ~Alpha_1_2() {}
  auto operator=(Alpha_1_2 const&) -> Alpha_1_2& = delete;
  auto operator=(Alpha_1_2&&) noexcept -> Alpha_1_2& = delete;
  auto
  start() -> Alpha_1_2&
  {
    context->guard([&]() -> void {
      option->parse();
      if (option->data.contains("help")) {
        option->print();
        return;
      }
      // parameter
      std::array<int, 2> const mpi_extent{
        option->param.mpi.extent[0],
        option->param.mpi.extent[1],
      };
      auto const& order = option->param.method.order;
      std::array<toki::Size, 2> const domain_resolution{
        option->param.domain.resolution[0],
        option->param.domain.resolution[1],
      };
      std::array<toki::Number, 2> const domain_range{
        option->param.domain.range[0],
        option->param.domain.range[1],
      };
      std::array<toki::Number, 2> const domain_anchor{
        option->param.domain.anchor[0],
        option->param.domain.anchor[1],
      };
      auto const& param_a = option->param.misc.a;
      auto const& param_c = option->param.misc.c;
      auto const& param_w = option->param.misc.w;
      toki::Size const variable{1};
      toki::Size const dof{order};
      toki::Size const dof_C{dof * dof};
      toki::Size const dof_P{dof_C * dof};
      // timestepper
      timestepper = std::make_unique<timestepper_type>(option->param.info.name);
      timestepper->info.param.time_max = option->param.time.max;
      timestepper->info.param.time_delta_max = option->param.time.delta_max;
      timestepper->info.param.step_max = option->param.time.step_max;
      timestepper->info.param.CFL = option->param.method.CFL;
      timestepper->info.param.signal_speed =
        std::max(std::fabs(param_c[0]), std::fabs(param_c[1]));
      timestepper->info.param.flag_report_error =
        option->param.misc.flag.report_error;
      timestepper
        ->create_domain(
          "default",
          variable,
          dof_C,
          dof,
          domain_resolution,
          {3, 3},
          domain_range,
          domain_anchor,
          mpi_extent)
        .create_domain(
          "result_P0",
          variable,
          dof_P,
          dof,
          domain_resolution,
          {3, 3},
          domain_range,
          domain_anchor,
          mpi_extent)
        .create_domain(
          "result_P1_0",
          variable,
          dof_P,
          dof,
          domain_resolution,
          {3, 3},
          domain_range,
          domain_anchor,
          mpi_extent)
        .create_domain(
          "result_P1_1",
          variable,
          dof_P,
          dof,
          domain_resolution,
          {3, 3},
          domain_range,
          domain_anchor,
          mpi_extent)
        .create_domain(
          "result_P2",
          variable,
          dof_P,
          dof,
          domain_resolution,
          {3, 3},
          domain_range,
          domain_anchor,
          mpi_extent)
        .create_domain(
          "result_P3",
          variable,
          dof_P,
          dof,
          domain_resolution,
          {3, 3},
          domain_range,
          domain_anchor,
          mpi_extent);
      timestepper->info.op.find_solution =
        [&](
          toki::Size const&,
          toki::Number const& time,
          std::array<toki::Number, 2> const& coord) -> toki::Number {
        auto const& x = coord[0];
        auto const& y = coord[1];
        return (param_a[0] *
                std::sin(2.0 * M_PI * param_w[0] * (x - param_c[0] * time))) *
               (param_a[1] *
                std::sin(2.0 * M_PI * param_w[1] * (y - param_c[1] * time)));
      };
      timestepper
        ->create_flux<toki::timestepper::tag::Flux::Uniform>(
          0,
          [&](toki::Number const&, std::vector<toki::Number> const& input)
            -> toki::Number {
        return param_c[0] * input[0];
      })
        .create_flux<toki::timestepper::tag::Flux::Uniform>(
          1,
          [&](toki::Number const&, std::vector<toki::Number> const& input)
            -> toki::Number {
        return param_c[1] * input[0];
      })
        .create_flux<toki::timestepper::tag::Flux::UniformUpwind>(
          0,
          [&](
            bool const& upwind,
            toki::Number const&,
            std::vector<toki::Number> const& input) -> toki::Number {
        if (upwind) {
          return 0.5 * (param_c[0] + std::fabs(param_c[0])) * input[0];
        } else {
          return 0.5 * (param_c[0] - std::fabs(param_c[0])) * input[0];
        }
      })
        .create_flux<toki::timestepper::tag::Flux::UniformUpwind>(
          1,
          [&](
            bool const& upwind,
            toki::Number const&,
            std::vector<toki::Number> const& input) -> toki::Number {
        if (upwind) {
          return 0.5 * (param_c[1] + std::fabs(param_c[1])) * input[0];
        } else {
          return 0.5 * (param_c[1] - std::fabs(param_c[1])) * input[0];
        }
      });
      // initial condition
      timestepper
        ->set_element(
          "default",
          [&](
            toki::Size const& variable,
            toki::Number const&,
            std::array<toki::Number, 2> const& coord) -> toki::Number {
        toki::utility::confirm(variable == 0, "invalid parameter: variable");
        return timestepper->info.op.find_solution(variable, 0.0, coord);
      }).save(option->param.info.name, "default");
      // launch
      auto& param = timestepper->info.param;
      toki::Number prev_time_delta{0};
      while (!timestepper->stop(param.CFL, param.signal_speed)) {
        if (prev_time_delta != param.time_delta) {
          prev_time_delta = param.time_delta;
          timestepper->prepare();
        }
        // solve
        timestepper->solve_prediction().solve_correction();
        // halo
        timestepper->halo_sync();
        // info
        param.time += param.time_delta;
        param.step += 1;
        toki::log::info(fmt::format(
          "{:09d}│{:+.6e}│{:7.3f}%│dt:{:+.3e}",
          param.step,
          param.time,
          param.time / param.time_max * 100.0,
          param.time_delta));
        MPI_Barrier(MPI_COMM_WORLD);
      }
      if (!option->data.contains("help")) {
        report();
      }
    });
    return *this;
  }
  auto
  report() -> Alpha_1_2&
  {
    timestepper->report(option->param.info.name);
    return *this;
  }
};
} // namespace toki::simulation::detail
#endif
