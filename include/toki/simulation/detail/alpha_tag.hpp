#ifndef TOKI_SIMULATION_DETAIL_ALPHA_TAG_HPP
#define TOKI_SIMULATION_DETAIL_ALPHA_TAG_HPP
namespace toki::simulation::detail {
enum struct Alpha_Name
{
  LinearAdvection_HRIDG,
};
enum struct Alpha_Variant
{
  Equation_1D,
  Equation_2D,
};
} // namespace toki::simulation::detail
#endif
