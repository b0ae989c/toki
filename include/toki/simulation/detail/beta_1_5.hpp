#ifndef TOKI_SIMULATION_DETAIL_BETA_1_5_HPP
#define TOKI_SIMULATION_DETAIL_BETA_1_5_HPP
#include <array>
#include <memory>
#include "fmt/format.h"
#include "mpi.h"
#include "toki/config.hpp"
#include "toki/context.hpp"
#include "toki/log.hpp"
#include "toki/option.hpp"
#include "toki/timestepper/hridg.hpp"
#include "toki/utility.hpp"
namespace toki::simulation::detail {
struct Beta_1_5
{
  using context_type = toki::context::Default;
  using option_type = toki::option::Default;
  using timestepper_type = toki::timestepper::HRIDG<2>;
  std::unique_ptr<context_type> context;
  std::unique_ptr<option_type> option;
  std::unique_ptr<timestepper_type> timestepper;
  Beta_1_5()
    : context{nullptr}
    , option{nullptr}
    , timestepper{nullptr}
  {
  }
  Beta_1_5(int argc, char** argv)
    : context(std::make_unique<context_type>(argc, argv))
    , option(std::make_unique<option_type>(argc, argv, "beta-1-5"))
    , timestepper{nullptr}
  {
  }
  Beta_1_5(Beta_1_5 const&) = delete;
  Beta_1_5(Beta_1_5&&) noexcept = delete;
  ~Beta_1_5() {}
  auto operator=(Beta_1_5 const&) -> Beta_1_5& = delete;
  auto operator=(Beta_1_5&&) noexcept -> Beta_1_5& = delete;
  auto
  start() -> Beta_1_5&
  {
    context->guard([&]() -> void {
      option->parse();
      if (option->data.contains("help")) {
        option->print();
        return;
      }
      // parameter
      std::array<int, 2> const mpi_extent{
        option->param.mpi.extent[0],
        option->param.mpi.extent[1],
      };
      auto const& order = option->param.method.order;
      std::array<toki::Size, 2> const domain_resolution{
        option->param.domain.resolution[0],
        option->param.domain.resolution[1],
      };
      std::array<toki::Number, 2> const domain_range{
        option->param.domain.range[0],
        option->param.domain.range[1],
      };
      std::array<toki::Number, 2> const domain_anchor{
        option->param.domain.anchor[0],
        option->param.domain.anchor[1],
      };
      auto const& param_theta = option->param.misc.theta;
      auto const& param_rho = option->param.misc.rho;
      toki::Size const variable{1};
      toki::Size const dof{order};
      toki::Size const dof_C{dof * dof};
      toki::Size const dof_P{dof_C * dof};
      // timestepper
      timestepper = std::make_unique<timestepper_type>(option->param.info.name);
      timestepper->info.param.time_max = option->param.time.max;
      timestepper->info.param.time_delta_max = option->param.time.delta_max;
      timestepper->info.param.step_max = option->param.time.step_max;
      timestepper->info.param.CFL = option->param.method.CFL;
      timestepper->info.param.signal_speed =
        std::fabs(option->param.domain.anchor[1]);
      timestepper
        ->create_domain(
          "default",
          variable,
          dof_C,
          dof,
          domain_resolution,
          {3, 3},
          domain_range,
          domain_anchor,
          mpi_extent)
        .create_domain(
          "result_P0",
          variable,
          dof_P,
          dof,
          domain_resolution,
          {3, 3},
          domain_range,
          domain_anchor,
          mpi_extent)
        .create_domain(
          "result_P1_0",
          variable,
          dof_P,
          dof,
          domain_resolution,
          {3, 3},
          domain_range,
          domain_anchor,
          mpi_extent)
        .create_domain(
          "result_P1_1",
          variable,
          dof_P,
          dof,
          domain_resolution,
          {3, 3},
          domain_range,
          domain_anchor,
          mpi_extent)
        .create_domain(
          "result_P2",
          variable,
          dof_P,
          dof,
          domain_resolution,
          {3, 3},
          domain_range,
          domain_anchor,
          mpi_extent)
        .create_domain(
          "result_P3",
          variable,
          dof_P,
          dof,
          domain_resolution,
          {3, 3},
          domain_range,
          domain_anchor,
          mpi_extent);
      timestepper->info.op.find_solution =
        [&](
          toki::Size const&,
          toki::Number const& time,
          std::array<toki::Number, 2> const& coord) -> toki::Number {
        toki::utility::confirm(time == 0.0, "no implementation");
        auto const& v0 = coord[1];
        return param_rho[0] / std::sqrt(2.0 * M_PI * param_theta[0]) *
               std::exp(-0.5 * std::pow(v0, 2.0) / param_theta[0]);
      };
      auto const& resolution =
        timestepper->info.domain.at("default").info.local.resolution;
      auto const& halo = timestepper->info.domain.at("default").info.halo;
      auto& velocity = timestepper->cache.field.velocity;
      timestepper
        ->create_flux<toki::timestepper::tag::Flux::External>(
          0,
          [&](
            toki::Number const&,
            std::vector<toki::Number> const& input,
            std::array<toki::Size, 2> const& index,
            toki::Size const& offset) -> toki::Number {
        return velocity[index[0] + resolution[0] * index[1]][0].at(offset) *
               input[0];
      })
        .create_flux<toki::timestepper::tag::Flux::External>(
          1,
          [&](
            toki::Number const&,
            std::vector<toki::Number> const& input,
            std::array<toki::Size, 2> const& index,
            toki::Size const& offset) -> toki::Number {
        return velocity[index[0] + resolution[0] * index[1]][1].at(offset) *
               input[0];
      })
        .create_flux<toki::timestepper::tag::Flux::ExternalUpwind>(
          0,
          [&](
            bool const& upwind,
            toki::Number const&,
            std::vector<toki::Number> const& input,
            std::array<toki::Size, 2> const& index,
            toki::Size const& offset) -> toki::Number {
        auto const& value =
          velocity[index[0] + resolution[0] * index[1]][0].at(offset);
        if (upwind) {
          return 0.5 * (value + std::fabs(value)) * input[0];
        } else {
          return 0.5 * (value - std::fabs(value)) * input[0];
        }
      })
        .create_flux<toki::timestepper::tag::Flux::ExternalUpwind>(
          1,
          [&](
            bool const& upwind,
            toki::Number const&,
            std::vector<toki::Number> const& input,
            std::array<toki::Size, 2> const& index,
            toki::Size const& offset) -> toki::Number {
        auto const& value =
          velocity[index[0] + resolution[0] * index[1]][1].at(offset);
        if (upwind) {
          return 0.5 * (value + std::fabs(value)) * input[0];
        } else {
          return 0.5 * (value - std::fabs(value)) * input[0];
        }
      });
      auto& param = timestepper->info.param;
      auto const& anchor =
        timestepper->info.domain.at("default").info.local.anchor;
      auto const& volume =
        timestepper->info.domain.at("default").info.local.element.volume;
      auto find_E = [&](
                      toki::Size const&,
                      toki::Number const& time_delta,
                      toki::Size const& index_r0,
                      toki::Number const& local_r0) -> toki::Number {
        toki::Number output{};
        if (param.time + time_delta == 0.0) {
          output = 0.0;
        } else {
          auto& domain_f = timestepper->info.domain.at("default");
          auto const& basis = timestepper->info.domain_spec.at("default").basis;
          auto const& integrator =
            timestepper->info.domain_spec.at("default").integrator;
          toki::vector::Vec sample_f(dof * dof);
          toki::vector::Vec sample_EMF(dof + 1);
          toki::Number approx_f{0.0};
          toki::Number approx_v_f{0.0};
          toki::Number approx_v_fr{0.0};
          toki::Number approx_v2_fr{0.0};
          toki::Number approx_v3_fr2{0.0};
          for (toki::Size index_v0 = halo[1];
               index_v0 < resolution[1] - halo[1];
               ++index_v0) {
            auto op_f =
              [&](std::array<toki::Number, 1> const& coord) -> toki::Number {
              auto const& local_v0 = coord[0];
              domain_f.get_element(sample_f, 0, {index_r0, index_v0});
              return basis.approx<2>({local_r0, local_v0}, sample_f, 0);
            };
            auto op_v_f =
              [&](std::array<toki::Number, 1> const& coord) -> toki::Number {
              auto const& local_v0 = coord[0];
              auto const v0 = anchor[1] + (static_cast<toki::Number>(index_v0) +
                                           (local_v0 + 1.0) / 2.0) *
                                            volume[1];
              domain_f.get_element(sample_f, 0, {index_r0, index_v0});
              return v0 * basis.approx<2>({local_r0, local_v0}, sample_f, 0);
            };
            auto op_v_fr =
              [&](std::array<toki::Number, 1> const& coord) -> toki::Number {
              auto const& local_v0 = coord[0];
              auto const v0 = anchor[1] + (static_cast<toki::Number>(index_v0) +
                                           (local_v0 + 1.0) / 2.0) *
                                            volume[1];
              domain_f.get_element(sample_f, 0, {index_r0, index_v0});
              return v0 *
                     basis.approx<2>({local_r0, local_v0}, sample_f, 0, 0, 1);
            };
            auto op_v2_fr =
              [&](std::array<toki::Number, 1> const& coord) -> toki::Number {
              auto const& local_v0 = coord[0];
              auto const v0 = anchor[1] + (static_cast<toki::Number>(index_v0) +
                                           (local_v0 + 1.0) / 2.0) *
                                            volume[1];
              domain_f.get_element(sample_f, 0, {index_r0, index_v0});
              return std::pow(v0, 2.0) *
                     basis.approx<2>({local_r0, local_v0}, sample_f, 0, 0, 1);
            };
            auto op_v3_fr2 =
              [&](std::array<toki::Number, 1> const& coord) -> toki::Number {
              auto const& local_v0 = coord[0];
              auto const v0 = anchor[1] + (static_cast<toki::Number>(index_v0) +
                                           (local_v0 + 1.0) / 2.0) *
                                            volume[1];
              domain_f.get_element(sample_f, 0, {index_r0, index_v0});
              return std::pow(v0, 3.0) *
                     basis.approx<2>({local_r0, local_v0}, sample_f, 0, 0, 2);
            };
            approx_f += integrator.sum<1>(op_f) * volume[1] / 2.0;
            approx_v_f += integrator.sum<1>(op_v_f) * volume[1] / 2.0;
            approx_v_fr += integrator.sum<1>(op_v_fr) * volume[1] / 2.0;
            approx_v2_fr += integrator.sum<1>(op_v2_fr) * volume[1] / 2.0;
            approx_v3_fr2 += integrator.sum<1>(op_v3_fr2) * volume[1] / 2.0;
          }
          // E0_t0
          auto const E0_t0 = basis.approx<1>(
            {local_r0}, timestepper->cache.field.E0[index_r0], 0);
          // E0_t1
          auto const E0_t1 = approx_v_f;
          // E0_t2
          auto const E0_t2 = approx_v2_fr + E0_t0 * approx_f;
          // E0_t3
          auto const E0_t3 = -approx_v3_fr2 -
                             2.0 * (param_rho[0] - approx_f) * approx_v_f -
                             3.0 * E0_t0 * approx_v_fr + approx_v_f * approx_f;
          output = E0_t0 + time_delta * E0_t1 +
                   std::pow(time_delta, 2.0) / 2.0 * E0_t2 +
                   std::pow(time_delta, 3.0) / 6.0 * E0_t3;
        }
        return output;
      };
      // initial condition
      timestepper
        ->set_element(
          "default",
          [&](
            toki::Size const& variable,
            toki::Number const&,
            std::array<toki::Number, 2> const& coord) -> toki::Number {
        toki::utility::confirm(variable == 0, "invalid parameter: variable");
        return timestepper->info.op.find_solution(variable, 0.0, coord);
      }).save(option->param.info.name, "default");
      // update E
      auto const& basis = timestepper->info.domain_spec.at("default").basis;
      timestepper->cache.field.E0.reserve(resolution[0]);
      for (toki::Size index_r0 = 0; index_r0 < resolution[0]; ++index_r0) {
        toki::vector::Vec E0(dof);
        auto op =
          [&](std::array<toki::Number, 1> const& coord) -> toki::Number {
          return find_E(0, 0.0, index_r0, coord[0]);
        };
        basis.fill<1>(E0, 0, op);
        timestepper->cache.field.E0.push_back(E0);
      }
      // launch
      toki::Number prev_time_delta{0};
      while (!timestepper->stop(param.CFL, param.signal_speed)) {
        toki::basis::Nodal basis_velocity(dof + 1);
        if (velocity.size() != resolution[0] * resolution[1]) {
          velocity.resize(resolution[0] * resolution[1]);
        }
        if (prev_time_delta != param.time_delta) {
#pragma omp parallel default(none) shared(                                     \
    dof, resolution, velocity, param, anchor, volume, find_E, basis_velocity)
          {
            toki::Size index_0{};
            toki::Size index_1{};
            // update velocity
#pragma omp for schedule(runtime) collapse(2)
            for (index_1 = 0; index_1 < resolution[1]; ++index_1) {
              for (index_0 = 0; index_0 < resolution[0]; ++index_0) {
                toki::vector::Vec velocity_r0(
                  (dof + 1) * (dof + 1) * (dof + 1));
                auto op_r0 =
                  [&](
                    std::array<toki::Number, 3> const& coord) -> toki::Number {
                  auto const& local_v0 = coord[2];
                  return anchor[1] + (static_cast<toki::Number>(index_1) +
                                      (local_v0 + 1.0) / 2.0) *
                                       volume[1];
                };
                basis_velocity.fill<3>(velocity_r0, 0, op_r0);
                toki::vector::Vec velocity_v0(
                  (dof + 1) * (dof + 1) * (dof + 1));
                auto op_v0 =
                  [&](
                    std::array<toki::Number, 3> const& coord) -> toki::Number {
                  auto const& local_time = coord[0];
                  auto const& local_r0 = coord[1];
                  return -1.0 * find_E(
                                  0,
                                  (local_time + 1.0) / 2.0 * param.time_delta,
                                  index_0,
                                  local_r0);
                };
                basis_velocity.fill<3>(velocity_v0, 0, op_v0);
                velocity[index_0 + resolution[0] * index_1] = {
                  velocity_r0,
                  velocity_v0,
                };
              }
            }
          }
          prev_time_delta = param.time_delta;
        } else {
#pragma omp parallel default(none)                                             \
  shared(dof, resolution, velocity, param, find_E, basis_velocity)
          {
            toki::Size index_0{};
            toki::Size index_1{};
            // update velocity_v0
#pragma omp for schedule(runtime) collapse(2)
            for (index_1 = 0; index_1 < resolution[1]; ++index_1) {
              for (index_0 = 0; index_0 < resolution[0]; ++index_0) {
                toki::vector::Vec velocity_v0(
                  (dof + 1) * (dof + 1) * (dof + 1));
                auto op_v0 =
                  [&](
                    std::array<toki::Number, 3> const& coord) -> toki::Number {
                  auto const& local_time = coord[0];
                  auto const& local_r0 = coord[1];
                  return -1.0 * find_E(
                                  0,
                                  (local_time + 1.0) / 2.0 * param.time_delta,
                                  index_0,
                                  local_r0);
                };
                basis_velocity.fill<3>(velocity_v0, 0, op_v0);
                velocity[index_0 + resolution[0] * index_1][1] = velocity_v0;
              }
            }
          }
        }
        timestepper->prepare();
        // update E
#pragma omp parallel default(none) shared(dof, resolution, param, find_E, basis)
        {
          toki::Size index_r0{};
          toki::vector::Vec E0(dof);
#pragma omp for schedule(runtime)
          for (index_r0 = 0; index_r0 < resolution[0]; ++index_r0) {
            auto op =
              [&](std::array<toki::Number, 1> const& coord) -> toki::Number {
              return find_E(0, param.time_delta, index_r0, coord[0]);
            };
            basis.fill<1>(E0, 0, op);
            timestepper->cache.field.E0[index_r0] = E0;
          }
        }
        // solve
        timestepper->solve_prediction();
        // boundary condition
        int mpi_rank{};
        int mpi_size{};
        MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
        MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
        if (mpi_rank == 0) {
          for (toki::Size index_v0 = 0; index_v0 < resolution[1]; ++index_v0) {
            timestepper->info.domain.at("result_P3")
              .set_element(0.0, {halo[0] - 1, index_v0});
          }
        } else if (mpi_rank == mpi_size - 1) {
          for (toki::Size index_v0 = 0; index_v0 < resolution[1]; ++index_v0) {
            timestepper->info.domain.at("result_P3")
              .set_element(0.0, {resolution[0] - halo[0], index_v0});
          }
        }
        MPI_Barrier(MPI_COMM_WORLD);
        timestepper->solve_correction();
        // halo
        timestepper->halo_sync();
        // info
        param.time += param.time_delta;
        param.step += 1;
        toki::log::info(fmt::format(
          "{:09d}│{:+.6e}│{:7.3f}%│dt:{:+.3e}",
          param.step,
          param.time,
          param.time / param.time_max * 100.0,
          param.time_delta));
        MPI_Barrier(MPI_COMM_WORLD);
      }
      if (!option->data.contains("help")) {
        report();
      }
    });
    return *this;
  }
  auto
  report() -> Beta_1_5&
  {
    timestepper->report(option->param.info.name);
    return *this;
  }
};
} // namespace toki::simulation::detail
#endif
