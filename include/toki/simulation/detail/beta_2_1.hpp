#ifndef TOKI_SIMULATION_DETAIL_BETA_2_1_HPP
#define TOKI_SIMULATION_DETAIL_BETA_2_1_HPP
#include <algorithm>
#include <array>
#include <cmath>
#include <fstream>
#include <memory>
#include <vector>
#include "fmt/format.h"
#include "toki/config.hpp"
#include "toki/context.hpp"
#include "toki/option.hpp"
#include "toki/timestepper/hridg.hpp"
#include "toki/timestepper/tag.hpp"
#include "toki/utility.hpp"
#include "toki/vector/vec.hpp"
namespace toki::simulation::detail {
struct Beta_2_1
{
  using context_type = toki::context::Default;
  using option_type = toki::option::Default;
  using timestepper_type = toki::timestepper::HRIDG<2>;
  struct Timestepper
  {
    std::unique_ptr<timestepper_type> electron;
    std::unique_ptr<timestepper_type> ion;
  };
  struct History
  {
    struct Moment
    {
      std::vector<toki::Number> mass;
      std::vector<toki::Number> momentum;
      std::vector<toki::Number> energy;
    };
    std::vector<toki::Number> E;
    Moment electron;
    Moment ion;
  };
  std::unique_ptr<context_type> context;
  std::unique_ptr<option_type> option;
  Timestepper timestepper{};
  History history{};
  Beta_2_1()
    : context{nullptr}
    , option{nullptr}
  {
  }
  Beta_2_1(int argc, char** argv)
    : context(std::make_unique<context_type>(argc, argv))
    , option(std::make_unique<option_type>(argc, argv, "beta-2-1"))
  {
  }
  Beta_2_1(Beta_2_1 const&) = delete;
  Beta_2_1(Beta_2_1&&) noexcept = delete;
  ~Beta_2_1() {}
  auto operator=(Beta_2_1 const&) -> Beta_2_1& = delete;
  auto operator=(Beta_2_1&&) noexcept -> Beta_2_1& = delete;
  auto
  start() -> Beta_2_1&
  {
    context->guard([&]() -> void {
      option->parse();
      if (option->data.contains("help")) {
        option->print();
        return;
      }
      // parameter
      std::array<int, 2> const mpi_extent{
        option->param.mpi.extent[0],
        option->param.mpi.extent[1],
      };
      auto const& order = option->param.method.order;
      std::array<toki::Size, 2> const domain_resolution{
        option->param.domain.resolution[0],
        option->param.domain.resolution[1],
      };
      std::array<toki::Number, 2> const domain_range{
        option->param.domain.range[0],
        option->param.domain.range[1],
      };
      std::array<toki::Number, 2> const domain_anchor{
        option->param.domain.anchor[0],
        option->param.domain.anchor[1],
      };
      auto const& param_a = option->param.misc.a;
      auto const& param_r = option->param.misc.r;
      toki::Number const m_e = 1.0;
      toki::Number const T_i = 1e-4;
      toki::Size const variable{1};
      toki::Size const dof{order};
      // timestepper.electron
      timestepper.electron = std::make_unique<timestepper_type>(
        fmt::format("{}-electron", option->param.info.name));
      timestepper.electron->info.param.time_max = option->param.time.max;
      timestepper.electron->info.param.time_delta_max =
        option->param.time.delta_max;
      timestepper.electron->info.param.step_max = option->param.time.step_max;
      timestepper.electron->info.param.CFL = option->param.method.CFL;
      timestepper.electron->info.param.signal_speed =
        std::max(std::fabs(option->param.domain.anchor[1]), 1.0 / m_e);
      timestepper.electron->create_domain(
        variable,
        dof,
        domain_resolution,
        domain_range,
        domain_anchor,
        mpi_extent);
      timestepper.electron->info.op.find_solution =
        [&](
          toki::Size const&,
          toki::Number const& time,
          std::array<toki::Number, 2> const& coord) -> toki::Number {
        toki::utility::confirm(time == 0.0, "no implementation");
        auto const& r0 = coord[0];
        auto const& v0 = coord[1];
        toki::Number const m = m_e;
        toki::Number const T = T_i * param_r[1];
        return std::sqrt(m / (2.0 * M_PI * T)) *
               std::exp(-m / (2.0 * T) * std::pow(v0, 2.0)) *
               (1.0 + param_a[0] * std::cos(r0 / 2.0));
      };
      // flux
      auto const& resolution_electron =
        timestepper.electron->info.domain.at("default").info.local.resolution;
      auto& velocity_electron = timestepper.electron->cache.field.velocity;
      timestepper.electron
        ->create_flux<toki::timestepper::tag::Flux::External>(
          0,
          [&](
            toki::Number const&,
            std::vector<toki::Number> const& input,
            std::array<toki::Size, 2> const& index,
            toki::Size const& offset) -> toki::Number {
        return velocity_electron[index[0] + resolution_electron[0] * index[1]]
                                [0]
                                  .at(offset) *
               input[0];
      })
        .create_flux<toki::timestepper::tag::Flux::External>(
          1,
          [&](
            toki::Number const&,
            std::vector<toki::Number> const& input,
            std::array<toki::Size, 2> const& index,
            toki::Size const& offset) -> toki::Number {
        return velocity_electron[index[0] + resolution_electron[0] * index[1]]
                                [1]
                                  .at(offset) *
               input[0];
      })
        .create_flux<toki::timestepper::tag::Flux::ExternalUpwind>(
          0,
          [&](
            bool const& upwind,
            toki::Number const&,
            std::vector<toki::Number> const& input,
            std::array<toki::Size, 2> const& index,
            toki::Size const& offset) -> toki::Number {
        auto const& value =
          velocity_electron[index[0] + resolution_electron[0] * index[1]][0].at(
            offset);
        if (upwind) {
          return 0.5 * (value + std::fabs(value)) * input[0];
        } else {
          return 0.5 * (value - std::fabs(value)) * input[0];
        }
      })
        .create_flux<toki::timestepper::tag::Flux::ExternalUpwind>(
          1,
          [&](
            bool const& upwind,
            toki::Number const&,
            std::vector<toki::Number> const& input,
            std::array<toki::Size, 2> const& index,
            toki::Size const& offset) -> toki::Number {
        auto const& value =
          velocity_electron[index[0] + resolution_electron[0] * index[1]][1].at(
            offset);
        if (upwind) {
          return 0.5 * (value + std::fabs(value)) * input[0];
        } else {
          return 0.5 * (value - std::fabs(value)) * input[0];
        }
      });
      // timestepper.ion
      timestepper.ion = std::make_unique<timestepper_type>(
        fmt::format("{}-ion", option->param.info.name));
      timestepper.ion->info.param.time_max = option->param.time.max;
      timestepper.ion->info.param.time_delta_max = option->param.time.delta_max;
      timestepper.ion->info.param.step_max = option->param.time.step_max;
      timestepper.ion->info.param.CFL = option->param.method.CFL;
      timestepper.ion->info.param.signal_speed =
        std::fabs(option->param.domain.anchor[1]);
      timestepper.ion->create_domain(
        variable,
        dof,
        domain_resolution,
        domain_range,
        domain_anchor,
        mpi_extent);
      timestepper.ion->info.op.find_solution =
        [&](
          toki::Size const&,
          toki::Number const& time,
          std::array<toki::Number, 2> const& coord) -> toki::Number {
        toki::utility::confirm(time == 0.0, "no implementation");
        auto const& r0 = coord[0];
        auto const& v0 = coord[1];
        toki::Number const m = m_e * param_r[0];
        toki::Number const T = T_i;
        return std::sqrt(m / (2.0 * M_PI * T)) *
               std::exp(-m / (2.0 * T) * std::pow(v0, 2.0)) *
               (1.0 + param_a[0] * std::cos(r0 / 2.0));
      };
      // flux
      auto const& resolution_ion =
        timestepper.ion->info.domain.at("default").info.local.resolution;
      auto& velocity_ion = timestepper.ion->cache.field.velocity;
      timestepper.ion
        ->create_flux<toki::timestepper::tag::Flux::External>(
          0,
          [&](
            toki::Number const&,
            std::vector<toki::Number> const& input,
            std::array<toki::Size, 2> const& index,
            toki::Size const& offset) -> toki::Number {
        return velocity_ion[index[0] + resolution_ion[0] * index[1]][0].at(
                 offset) *
               input[0];
      })
        .create_flux<toki::timestepper::tag::Flux::External>(
          1,
          [&](
            toki::Number const&,
            std::vector<toki::Number> const& input,
            std::array<toki::Size, 2> const& index,
            toki::Size const& offset) -> toki::Number {
        return velocity_ion[index[0] + resolution_ion[0] * index[1]][1].at(
                 offset) *
               input[0];
      })
        .create_flux<toki::timestepper::tag::Flux::ExternalUpwind>(
          0,
          [&](
            bool const& upwind,
            toki::Number const&,
            std::vector<toki::Number> const& input,
            std::array<toki::Size, 2> const& index,
            toki::Size const& offset) -> toki::Number {
        auto const& value =
          velocity_ion[index[0] + resolution_ion[0] * index[1]][0].at(offset);
        if (upwind) {
          return 0.5 * (value + std::fabs(value)) * input[0];
        } else {
          return 0.5 * (value - std::fabs(value)) * input[0];
        }
      })
        .create_flux<toki::timestepper::tag::Flux::ExternalUpwind>(
          1,
          [&](
            bool const& upwind,
            toki::Number const&,
            std::vector<toki::Number> const& input,
            std::array<toki::Size, 2> const& index,
            toki::Size const& offset) -> toki::Number {
        auto const& value =
          velocity_ion[index[0] + resolution_ion[0] * index[1]][1].at(offset);
        if (upwind) {
          return 0.5 * (value + std::fabs(value)) * input[0];
        } else {
          return 0.5 * (value - std::fabs(value)) * input[0];
        }
      });
      // E
      auto& param_electron = timestepper.electron->info.param;
      auto& param_ion = timestepper.ion->info.param;
      auto const& anchor =
        timestepper.electron->info.domain.at("default").info.local.anchor;
      auto const& volume = timestepper.electron->info.domain.at("default")
                             .info.local.element.volume;
      [[maybe_unused]] auto find_J0 = [&]() -> toki::Number {
        auto& domain_electron = timestepper.electron->info.domain.at("default");
        auto& domain_ion = timestepper.ion->info.domain.at("default");
        auto const& halo = domain_electron.info.halo;
        auto const& basis =
          timestepper.electron->info.domain_spec.at("default").basis;
        auto const& integrator =
          timestepper.electron->info.domain_spec.at("default").integrator;
        toki::vector::Vec sample_f(dof * dof);
        struct
        {
          toki::Number q{};
          toki::Number approx_v_f{0.0};
        } electron, ion;
        for (toki::Size index_r0 = halo[0];
             index_r0 < resolution_electron[0] - halo[0];
             ++index_r0) {
          for (toki::Size index_v0 = halo[1];
               index_v0 < resolution_electron[1] - halo[1];
               ++index_v0) {
            auto op_electron_v_f =
              [&](std::array<toki::Number, 2> const& coord) -> toki::Number {
              auto const& local_r0 = coord[0];
              auto const& local_v0 = coord[1];
              auto const v0 = anchor[1] + (static_cast<toki::Number>(index_v0) +
                                           (local_v0 + 1.0) / 2.0) *
                                            volume[1];
              domain_electron.get_element(sample_f, 0, {index_r0, index_v0});
              return v0 * basis.approx<2>({local_r0, local_v0}, sample_f, 0);
            };
            electron.approx_v_f +=
              integrator.sum<2>(op_electron_v_f) * volume[0] * volume[1] / 4.0;
            auto op_ion_v_f =
              [&](std::array<toki::Number, 2> const& coord) -> toki::Number {
              auto const& local_r0 = coord[0];
              auto const& local_v0 = coord[1];
              auto const v0 = anchor[1] + (static_cast<toki::Number>(index_v0) +
                                           (local_v0 + 1.0) / 2.0) *
                                            volume[1];
              domain_ion.get_element(sample_f, 0, {index_r0, index_v0});
              return v0 * basis.approx<2>({local_r0, local_v0}, sample_f, 0);
            };
            ion.approx_v_f +=
              integrator.sum<2>(op_ion_v_f) * volume[0] * volume[1] / 4.0;
          }
        }
        electron.q = -1.0;
        ion.q = +1.0;
        return (electron.q * electron.approx_v_f + ion.q * ion.approx_v_f) /
               domain_electron.info.local.range.at(0);
      };
      auto find_E = [&](
                      toki::Size const&,
                      toki::Number const& time_delta,
                      toki::Size const& index_r0,
                      toki::Number const& local_r0) -> toki::Number {
        toki::Number output{};
        if (param_electron.time + time_delta == 0.0) {
          output = 0.0;
        } else {
          auto& domain_electron =
            timestepper.electron->info.domain.at("default");
          auto& domain_ion = timestepper.ion->info.domain.at("default");
          auto const& halo = domain_electron.info.halo;
          auto const& basis =
            timestepper.electron->info.domain_spec.at("default").basis;
          auto const& integrator =
            timestepper.electron->info.domain_spec.at("default").integrator;
          toki::vector::Vec sample_f(dof * dof);
          struct
          {
            toki::Number q{};
            toki::Number m{};
            toki::Number approx_f{0.0};
            toki::Number approx_v_f{0.0};
            toki::Number approx_v_fr{0.0};
            toki::Number approx_v2_fr{0.0};
            toki::Number approx_v3_fr2{0.0};
          } electron, ion;
          for (toki::Size index_v0 = halo[1];
               index_v0 < resolution_electron[1] - halo[1];
               ++index_v0) {
            auto op_electron_f =
              [&](std::array<toki::Number, 1> const& coord) -> toki::Number {
              auto const& local_v0 = coord[0];
              domain_electron.get_element(sample_f, 0, {index_r0, index_v0});
              return basis.approx<2>({local_r0, local_v0}, sample_f, 0);
            };
            auto op_electron_v_f =
              [&](std::array<toki::Number, 1> const& coord) -> toki::Number {
              auto const& local_v0 = coord[0];
              auto const v0 = anchor[1] + (static_cast<toki::Number>(index_v0) +
                                           (local_v0 + 1.0) / 2.0) *
                                            volume[1];
              domain_electron.get_element(sample_f, 0, {index_r0, index_v0});
              return v0 * basis.approx<2>({local_r0, local_v0}, sample_f, 0);
            };
            auto op_electron_v_fr =
              [&](std::array<toki::Number, 1> const& coord) -> toki::Number {
              auto const& local_v0 = coord[0];
              auto const v0 = anchor[1] + (static_cast<toki::Number>(index_v0) +
                                           (local_v0 + 1.0) / 2.0) *
                                            volume[1];
              domain_electron.get_element(sample_f, 0, {index_r0, index_v0});
              return v0 *
                     basis.approx<2>({local_r0, local_v0}, sample_f, 0, 0, 1);
            };
            auto op_electron_v2_fr =
              [&](std::array<toki::Number, 1> const& coord) -> toki::Number {
              auto const& local_v0 = coord[0];
              auto const v0 = anchor[1] + (static_cast<toki::Number>(index_v0) +
                                           (local_v0 + 1.0) / 2.0) *
                                            volume[1];
              domain_electron.get_element(sample_f, 0, {index_r0, index_v0});
              return std::pow(v0, 2.0) *
                     basis.approx<2>({local_r0, local_v0}, sample_f, 0, 0, 1);
            };
            auto op_electron_v3_fr2 =
              [&](std::array<toki::Number, 1> const& coord) -> toki::Number {
              auto const& local_v0 = coord[0];
              auto const v0 = anchor[1] + (static_cast<toki::Number>(index_v0) +
                                           (local_v0 + 1.0) / 2.0) *
                                            volume[1];
              domain_electron.get_element(sample_f, 0, {index_r0, index_v0});
              return std::pow(v0, 3.0) *
                     basis.approx<2>({local_r0, local_v0}, sample_f, 0, 0, 2);
            };
            electron.approx_f +=
              integrator.sum<1>(op_electron_f) * volume[1] / 2.0;
            electron.approx_v_f +=
              integrator.sum<1>(op_electron_v_f) * volume[1] / 2.0;
            electron.approx_v_fr +=
              integrator.sum<1>(op_electron_v_fr) * volume[1] / 2.0;
            electron.approx_v2_fr +=
              integrator.sum<1>(op_electron_v2_fr) * volume[1] / 2.0;
            electron.approx_v3_fr2 +=
              integrator.sum<1>(op_electron_v3_fr2) * volume[1] / 2.0;
            auto op_ion_f =
              [&](std::array<toki::Number, 1> const& coord) -> toki::Number {
              auto const& local_v0 = coord[0];
              domain_ion.get_element(sample_f, 0, {index_r0, index_v0});
              return basis.approx<2>({local_r0, local_v0}, sample_f, 0);
            };
            auto op_ion_v_f =
              [&](std::array<toki::Number, 1> const& coord) -> toki::Number {
              auto const& local_v0 = coord[0];
              auto const v0 = anchor[1] + (static_cast<toki::Number>(index_v0) +
                                           (local_v0 + 1.0) / 2.0) *
                                            volume[1];
              domain_ion.get_element(sample_f, 0, {index_r0, index_v0});
              return v0 * basis.approx<2>({local_r0, local_v0}, sample_f, 0);
            };
            auto op_ion_v_fr =
              [&](std::array<toki::Number, 1> const& coord) -> toki::Number {
              auto const& local_v0 = coord[0];
              auto const v0 = anchor[1] + (static_cast<toki::Number>(index_v0) +
                                           (local_v0 + 1.0) / 2.0) *
                                            volume[1];
              domain_ion.get_element(sample_f, 0, {index_r0, index_v0});
              return v0 *
                     basis.approx<2>({local_r0, local_v0}, sample_f, 0, 0, 1);
            };
            auto op_ion_v2_fr =
              [&](std::array<toki::Number, 1> const& coord) -> toki::Number {
              auto const& local_v0 = coord[0];
              auto const v0 = anchor[1] + (static_cast<toki::Number>(index_v0) +
                                           (local_v0 + 1.0) / 2.0) *
                                            volume[1];
              domain_ion.get_element(sample_f, 0, {index_r0, index_v0});
              return std::pow(v0, 2.0) *
                     basis.approx<2>({local_r0, local_v0}, sample_f, 0, 0, 1);
            };
            auto op_ion_v3_fr2 =
              [&](std::array<toki::Number, 1> const& coord) -> toki::Number {
              auto const& local_v0 = coord[0];
              auto const v0 = anchor[1] + (static_cast<toki::Number>(index_v0) +
                                           (local_v0 + 1.0) / 2.0) *
                                            volume[1];
              domain_ion.get_element(sample_f, 0, {index_r0, index_v0});
              return std::pow(v0, 3.0) *
                     basis.approx<2>({local_r0, local_v0}, sample_f, 0, 0, 2);
            };
            ion.approx_f += integrator.sum<1>(op_ion_f) * volume[1] / 2.0;
            ion.approx_v_f += integrator.sum<1>(op_ion_v_f) * volume[1] / 2.0;
            ion.approx_v_fr += integrator.sum<1>(op_ion_v_fr) * volume[1] / 2.0;
            ion.approx_v2_fr +=
              integrator.sum<1>(op_ion_v2_fr) * volume[1] / 2.0;
            ion.approx_v3_fr2 +=
              integrator.sum<1>(op_ion_v3_fr2) * volume[1] / 2.0;
          }
          electron.q = -1.0;
          electron.m = m_e;
          ion.q = +1.0;
          ion.m = m_e * param_r[0];
          auto const E0_t0 = basis.approx<1>(
            {local_r0}, timestepper.electron->cache.field.E0[index_r0], 0);
          auto const J0 = 0.0;
          auto const E0_t1 =
            J0 - electron.q * electron.approx_v_f - ion.q * ion.approx_v_f;
          auto const E0_t2 =
            electron.q *
              (electron.approx_v2_fr -
               (1.0 / electron.m) * E0_t0 * (electron.q * electron.approx_f)) +
            ion.q * (ion.approx_v2_fr -
                     (1.0 / ion.m) * E0_t0 * (ion.q * ion.approx_f));
          auto const E0_r1 =
            ion.q * ion.approx_f + electron.q * electron.approx_f;
          auto const E0_t3_electron =
            -electron.approx_v3_fr2 +
            (2.0 / electron.m) * E0_r1 * (electron.q * electron.approx_v_f) +
            (3.0 / electron.m) * E0_t0 * (electron.q * electron.approx_v_fr) -
            (1.0 / electron.m) * E0_t1 * (electron.q * electron.approx_f);
          auto const E0_t3_ion =
            -ion.approx_v3_fr2 +
            (2.0 / ion.m) * E0_r1 * (ion.q * ion.approx_v_f) +
            (3.0 / ion.m) * E0_t0 * (ion.q * ion.approx_v_fr) -
            (1.0 / ion.m) * E0_t1 * (ion.q * ion.approx_f);
          auto const E0_t3 = electron.q * E0_t3_electron + ion.q * E0_t3_ion;
          output = E0_t0 + time_delta * E0_t1 +
                   std::pow(time_delta, 2.0) / 2.0 * E0_t2 +
                   std::pow(time_delta, 3.0) / 6.0 * E0_t3;
        }
        return output;
      };
      // initial condition
      timestepper.electron
        ->set_element(
          "default",
          [&](
            toki::Size const& variable,
            toki::Number const&,
            std::array<toki::Number, 2> const& coord) -> toki::Number {
        toki::utility::confirm(variable == 0, "invalid parameter: variable");
        return timestepper.electron->info.op.find_solution(
          variable, 0.0, coord);
      }).save(timestepper.electron->info.name, "default");
      timestepper.ion
        ->set_element(
          "default",
          [&](
            toki::Size const& variable,
            toki::Number const&,
            std::array<toki::Number, 2> const& coord) -> toki::Number {
        toki::utility::confirm(variable == 0, "invalid parameter: variable");
        return timestepper.ion->info.op.find_solution(variable, 0.0, coord);
      }).save(timestepper.ion->info.name, "default");
      // update E
      auto const& basis =
        timestepper.electron->info.domain_spec.at("default").basis;
      timestepper.electron->cache.field.E0.reserve(resolution_electron[0]);
      timestepper.ion->cache.field.E0.reserve(resolution_ion[0]);
      for (toki::Size index_r0 = 0; index_r0 < resolution_electron[0];
           ++index_r0) {
        toki::vector::Vec E0(dof);
        auto op =
          [&](std::array<toki::Number, 1> const& coord) -> toki::Number {
          return find_E(0, 0.0, index_r0, coord[0]);
        };
        basis.fill<1>(E0, 0, op);
        timestepper.electron->cache.field.E0.push_back(E0);
        timestepper.ion->cache.field.E0.push_back(E0);
      }
      record_history_E()
        .record_history_mass()
        .record_history_momentum()
        .record_history_energy();
      // launch
      toki::Number prev_time_delta{0};
      while (!timestepper.electron->stop(
        param_electron.CFL, param_electron.signal_speed)) {
        toki::basis::Nodal basis_velocity(dof + 1);
        if (
          velocity_electron.size() !=
          resolution_electron[0] * resolution_electron[1]) {
          velocity_electron.resize(
            resolution_electron[0] * resolution_electron[1]);
        }
        if (velocity_ion.size() != resolution_ion[0] * resolution_ion[1]) {
          velocity_ion.resize(resolution_ion[0] * resolution_ion[1]);
        }
        if (prev_time_delta != param_electron.time_delta) {
#pragma omp parallel default(none) shared(                                     \
    dof,                                                                       \
      param_r,                                                                 \
      m_e,                                                                     \
      resolution_electron,                                                     \
      resolution_ion,                                                          \
      velocity_electron,                                                       \
      velocity_ion,                                                            \
      param_electron,                                                          \
      anchor,                                                                  \
      volume,                                                                  \
      find_E,                                                                  \
      basis_velocity)
          {
            toki::Size index_0{};
            toki::Size index_1{};
            // update velocity
#pragma omp for schedule(runtime) collapse(2)
            for (index_1 = 0; index_1 < resolution_electron[1]; ++index_1) {
              for (index_0 = 0; index_0 < resolution_electron[0]; ++index_0) {
                toki::vector::Vec velocity_r0(
                  (dof + 1) * (dof + 1) * (dof + 1));
                auto op_r0 =
                  [&](
                    std::array<toki::Number, 3> const& coord) -> toki::Number {
                  auto const& local_v0 = coord[2];
                  return anchor[1] + (static_cast<toki::Number>(index_1) +
                                      (local_v0 + 1.0) / 2.0) *
                                       volume[1];
                };
                basis_velocity.fill<3>(velocity_r0, 0, op_r0);
                toki::vector::Vec velocity_v0(
                  (dof + 1) * (dof + 1) * (dof + 1));
                auto op_v0 =
                  [&](
                    std::array<toki::Number, 3> const& coord) -> toki::Number {
                  auto const& local_time = coord[0];
                  auto const& local_r0 = coord[1];
                  return find_E(
                    0,
                    (local_time + 1.0) / 2.0 * param_electron.time_delta,
                    index_0,
                    local_r0);
                };
                basis_velocity.fill<3>(velocity_v0, 0, op_v0);
                auto velocity_v0_electron = velocity_v0;
                std::transform(
                  velocity_v0_electron.data.get(),
                  velocity_v0_electron.data.get() +
                    velocity_v0_electron.extent[0],
                  velocity_v0_electron.data.get(),
                  [&](toki::Number const& input) -> toki::Number {
                  return (-1.0 / m_e) * input;
                });
                auto velocity_v0_ion = velocity_v0;
                std::transform(
                  velocity_v0_ion.data.get(),
                  velocity_v0_ion.data.get() + velocity_v0_ion.extent[0],
                  velocity_v0_ion.data.get(),
                  [&](toki::Number const& input) -> toki::Number {
                  return (+1.0 / (m_e * param_r[0])) * input;
                });
                velocity_electron[index_0 + resolution_electron[0] * index_1] =
                  {
                    velocity_r0,
                    velocity_v0_electron,
                  };
                velocity_ion[index_0 + resolution_ion[0] * index_1] = {
                  velocity_r0,
                  velocity_v0_ion,
                };
              }
            }
          }
          prev_time_delta = param_electron.time_delta;
        } else {
#pragma omp parallel default(none) shared(                                     \
    dof,                                                                       \
      param_r,                                                                 \
      m_e,                                                                     \
      resolution_electron,                                                     \
      resolution_ion,                                                          \
      velocity_electron,                                                       \
      velocity_ion,                                                            \
      param_electron,                                                          \
      find_E,                                                                  \
      basis_velocity)
          {
            toki::Size index_0{};
            toki::Size index_1{};
            // update velocity_v0
#pragma omp for schedule(runtime) collapse(2)
            for (index_1 = 0; index_1 < resolution_electron[1]; ++index_1) {
              for (index_0 = 0; index_0 < resolution_electron[0]; ++index_0) {
                toki::vector::Vec velocity_v0(
                  (dof + 1) * (dof + 1) * (dof + 1));
                auto op_v0 =
                  [&](
                    std::array<toki::Number, 3> const& coord) -> toki::Number {
                  auto const& local_time = coord[0];
                  auto const& local_r0 = coord[1];
                  return find_E(
                    0,
                    (local_time + 1.0) / 2.0 * param_electron.time_delta,
                    index_0,
                    local_r0);
                };
                basis_velocity.fill<3>(velocity_v0, 0, op_v0);
                auto velocity_v0_electron = velocity_v0;
                std::transform(
                  velocity_v0_electron.data.get(),
                  velocity_v0_electron.data.get() +
                    velocity_v0_electron.extent[0],
                  velocity_v0_electron.data.get(),
                  [&](toki::Number const& input) -> toki::Number {
                  return (-1.0 / m_e) * input;
                });
                auto velocity_v0_ion = velocity_v0;
                std::transform(
                  velocity_v0_ion.data.get(),
                  velocity_v0_ion.data.get() + velocity_v0_ion.extent[0],
                  velocity_v0_ion.data.get(),
                  [&](toki::Number const& input) -> toki::Number {
                  return (+1.0 / (m_e * param_r[0])) * input;
                });
                velocity_electron[index_0 + resolution_electron[0] * index_1]
                                 [1] = velocity_v0_electron;
                velocity_ion[index_0 + resolution_ion[0] * index_1][1] =
                  velocity_v0_ion;
              }
            }
          }
        }
        timestepper.electron->prepare();
        timestepper.ion->prepare();
        // update E
#pragma omp parallel default(none)                                             \
  shared(dof, resolution_electron, param_electron, find_E, basis)
        {
          toki::Size index_r0{};
          toki::vector::Vec E0(dof);
#pragma omp for schedule(runtime)
          for (index_r0 = 0; index_r0 < resolution_electron[0]; ++index_r0) {
            auto op =
              [&](std::array<toki::Number, 1> const& coord) -> toki::Number {
              return find_E(0, param_electron.time_delta, index_r0, coord[0]);
            };
            basis.fill<1>(E0, 0, op);
            timestepper.electron->cache.field.E0[index_r0] = E0;
            timestepper.ion->cache.field.E0[index_r0] = E0;
          }
        }
        record_history_E()
          .record_history_mass()
          .record_history_momentum()
          .record_history_energy();
        // solve
        timestepper.electron->solve_prediction().solve_correction();
        timestepper.ion->solve_prediction().solve_correction();
        // halo
        timestepper.electron->halo_sync();
        timestepper.ion->halo_sync();
        // info
        param_electron.time += param_electron.time_delta;
        param_ion.time += param_electron.time_delta;
        param_electron.step += 1;
        param_ion.step += 1;
        toki::log::info(fmt::format(
          "{:09d}│{:+.6e}│{:7.3f}%│dt:{:+.3e}",
          param_electron.step,
          param_electron.time,
          param_electron.time / param_electron.time_max * 100.0,
          param_electron.time_delta));
        MPI_Barrier(MPI_COMM_WORLD);
      }
      if (!option->data.contains("help")) {
        report();
      }
    });
    return *this;
  }
  auto
  report() -> Beta_2_1&
  {
    timestepper.electron->report(timestepper.electron->info.name);
    timestepper.ion->report(timestepper.ion->info.name);
    // history
    toki::Number buffer_out[]{
      history.E.back(),
      history.electron.mass.back(),
      history.electron.momentum.back(),
      history.electron.energy.back(),
      history.ion.mass.back(),
      history.ion.momentum.back(),
      history.ion.energy.back(),
    };
    toki::Number buffer_in[2 * 3 + 1];
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Allreduce(
      buffer_out, buffer_in, 2 * 3 + 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    toki::log::info(fmt::format("E: {:+.16e}", buffer_in[0]));
    toki::log::info(fmt::format("mass: {:+.16e}", buffer_in[1] + buffer_in[4]));
    toki::log::info(
      fmt::format("momentum: {:+.16e}", buffer_in[2] + buffer_in[5]));
    toki::log::info(fmt::format(
      "energy: {:+.16e}", buffer_in[3] + buffer_in[6] + 0.5 * buffer_in[0]));
    save_history(option->param.info.name);
    return *this;
  }
  auto
  record_history_E() -> Beta_2_1&
  {
    auto const& domain_f = timestepper.electron->info.domain.at("default");
    auto const& halo = domain_f.info.halo;
    auto const& resolution = domain_f.info.local.resolution;
    auto const& basis =
      timestepper.electron->info.domain_spec.at("default").basis;
    auto const& integrator =
      timestepper.electron->info.domain_spec.at("default").integrator;
    toki::Number result_E{0.0};
    for (toki::Size index_r0 = halo[0]; index_r0 < resolution[0] - halo[0];
         ++index_r0) {
      auto op_E =
        [&](std::array<toki::Number, 1> const& coord) -> toki::Number {
        return std::pow(
          basis.approx<1>(
            coord, timestepper.electron->cache.field.E0[index_r0], 0),
          2.0);
      };
      result_E += integrator.sum<1>(op_E);
    }
    result_E *= domain_f.info.local.element.volume[0] / 2.0;
    history.E.push_back(result_E);
    return *this;
  }
  auto
  record_history_mass() -> Beta_2_1&
  {
    auto& domain_f_electron = timestepper.electron->info.domain.at("default");
    auto& domain_f_ion = timestepper.ion->info.domain.at("default");
    auto const& halo = domain_f_electron.info.halo;
    auto const& resolution = domain_f_electron.info.local.resolution;
    auto const& dof = domain_f_electron.info.quadrature_point;
    auto const& basis =
      timestepper.electron->info.domain_spec.at("default").basis;
    auto const& integrator =
      timestepper.electron->info.domain_spec.at("default").integrator;
    toki::vector::Vec buffer(dof * dof);
    toki::Number result_mass_electron{0.0};
    toki::Number result_mass_ion{0.0};
    for (toki::Size index_v0 = halo[1]; index_v0 < resolution[1] - halo[1];
         ++index_v0) {
      for (toki::Size index_r0 = halo[0]; index_r0 < resolution[0] - halo[0];
           ++index_r0) {
        auto op_mass_electron =
          [&](std::array<toki::Number, 2> const& coord) -> toki::Number {
          domain_f_electron.get_element(buffer, 0, {index_r0, index_v0});
          return basis.approx<2>(coord, buffer, 0);
        };
        result_mass_electron += integrator.sum<2>(op_mass_electron);
        auto op_mass_ion =
          [&](std::array<toki::Number, 2> const& coord) -> toki::Number {
          domain_f_ion.get_element(buffer, 0, {index_r0, index_v0});
          return basis.approx<2>(coord, buffer, 0);
        };
        result_mass_ion += integrator.sum<2>(op_mass_ion);
      }
    }
    result_mass_electron *= domain_f_electron.info.local.element.volume[0] *
                            domain_f_electron.info.local.element.volume[1] /
                            4.0;
    result_mass_ion *= domain_f_ion.info.local.element.volume[0] *
                       domain_f_ion.info.local.element.volume[1] / 4.0;
    toki::Number const m_e = 1.0;
    toki::Number const m_i = m_e * option->param.misc.r.at(0);
    history.electron.mass.push_back(result_mass_electron * m_e);
    history.ion.mass.push_back(result_mass_ion * m_i);
    return *this;
  }
  auto
  record_history_momentum() -> Beta_2_1&
  {
    auto& domain_f_electron = timestepper.electron->info.domain.at("default");
    auto& domain_f_ion = timestepper.ion->info.domain.at("default");
    auto const& halo = domain_f_electron.info.halo;
    auto const& resolution = domain_f_electron.info.local.resolution;
    auto const& anchor = domain_f_electron.info.local.anchor;
    auto const& volume = domain_f_electron.info.local.element.volume;
    auto const& dof = domain_f_electron.info.quadrature_point;
    auto const& basis =
      timestepper.electron->info.domain_spec.at("default").basis;
    auto const& integrator =
      timestepper.electron->info.domain_spec.at("default").integrator;
    toki::vector::Vec buffer(dof * dof);
    toki::Number result_momentum_electron{0.0};
    toki::Number result_momentum_ion{0.0};
    for (toki::Size index_v0 = halo[1]; index_v0 < resolution[1] - halo[1];
         ++index_v0) {
      for (toki::Size index_r0 = halo[0]; index_r0 < resolution[0] - halo[0];
           ++index_r0) {
        auto op_momentum_electron =
          [&](std::array<toki::Number, 2> const& coord) -> toki::Number {
          auto const v0 = anchor[1] + (static_cast<toki::Number>(index_v0) +
                                       (coord[1] + 1.0) / 2.0) *
                                        volume[1];
          domain_f_electron.get_element(buffer, 0, {index_r0, index_v0});
          return v0 * basis.approx<2>(coord, buffer, 0);
        };
        result_momentum_electron += integrator.sum<2>(op_momentum_electron);
        auto op_momentum_ion =
          [&](std::array<toki::Number, 2> const& coord) -> toki::Number {
          auto const v0 = anchor[1] + (static_cast<toki::Number>(index_v0) +
                                       (coord[1] + 1.0) / 2.0) *
                                        volume[1];
          domain_f_ion.get_element(buffer, 0, {index_r0, index_v0});
          return v0 * basis.approx<2>(coord, buffer, 0);
        };
        result_momentum_ion += integrator.sum<2>(op_momentum_ion);
      }
    }
    result_momentum_electron *= volume[0] * volume[1] / 4.0;
    result_momentum_ion *= volume[0] * volume[1] / 4.0;
    toki::Number const m_e = 1.0;
    toki::Number const m_i = m_e * option->param.misc.r.at(0);
    history.electron.momentum.push_back(result_momentum_electron * m_e);
    history.ion.momentum.push_back(result_momentum_ion * m_i);
    return *this;
  }
  auto
  record_history_energy() -> Beta_2_1&
  {
    auto& domain_f_electron = timestepper.electron->info.domain.at("default");
    auto& domain_f_ion = timestepper.ion->info.domain.at("default");
    auto const& halo = domain_f_electron.info.halo;
    auto const& resolution = domain_f_electron.info.local.resolution;
    auto const& anchor = domain_f_electron.info.local.anchor;
    auto const& volume = domain_f_electron.info.local.element.volume;
    auto const& dof = domain_f_electron.info.quadrature_point;
    auto const& basis =
      timestepper.electron->info.domain_spec.at("default").basis;
    auto const& integrator =
      timestepper.electron->info.domain_spec.at("default").integrator;
    toki::vector::Vec buffer(dof * dof);
    toki::Number result_energy_electron{0.0};
    toki::Number result_energy_ion{0.0};
    for (toki::Size index_v0 = halo[1]; index_v0 < resolution[1] - halo[1];
         ++index_v0) {
      for (toki::Size index_r0 = halo[0]; index_r0 < resolution[0] - halo[0];
           ++index_r0) {
        auto op_energy_electron =
          [&](std::array<toki::Number, 2> const& coord) -> toki::Number {
          auto const v0 = anchor[1] + (static_cast<toki::Number>(index_v0) +
                                       (coord[1] + 1.0) / 2.0) *
                                        volume[1];
          domain_f_electron.get_element(buffer, 0, {index_r0, index_v0});
          return std::pow(v0, 2.0) * basis.approx<2>(coord, buffer, 0);
        };
        result_energy_electron += integrator.sum<2>(op_energy_electron);
        auto op_energy_ion =
          [&](std::array<toki::Number, 2> const& coord) -> toki::Number {
          auto const v0 = anchor[1] + (static_cast<toki::Number>(index_v0) +
                                       (coord[1] + 1.0) / 2.0) *
                                        volume[1];
          domain_f_ion.get_element(buffer, 0, {index_r0, index_v0});
          return std::pow(v0, 2.0) * basis.approx<2>(coord, buffer, 0);
        };
        result_energy_ion += integrator.sum<2>(op_energy_ion);
      }
    }
    result_energy_electron *= volume[0] * volume[1] / 4.0;
    result_energy_ion *= volume[0] * volume[1] / 4.0;
    toki::Number const m_e = 1.0;
    toki::Number const m_i = m_e * option->param.misc.r.at(0);
    history.electron.energy.push_back(result_energy_electron * m_e);
    history.ion.energy.push_back(result_energy_ion * m_i);
    return *this;
  }
  auto
  save_history(std::string const& name) -> Beta_2_1&
  {
    auto const& mpi_rank =
      timestepper.electron->info.domain.at("default").info.local.mpi.rank;
    auto save =
      [&](std::string const& handle, std::vector<toki::Number>& data) -> void {
      auto const filename =
        fmt::format("{}-history-{}-{:04}.txt", name, handle, mpi_rank);
      std::ofstream file(filename);
      for (auto const& entry : data) {
        file << fmt::format("{:+.16e}\n", entry);
      }
      file.close();
    };
    save("E", history.E);
    save("electron-mass", history.electron.mass);
    save("electron-momentum", history.electron.momentum);
    save("electron-energy", history.electron.energy);
    save("ion-mass", history.ion.mass);
    save("ion-momentum", history.ion.momentum);
    save("ion-energy", history.ion.energy);
    return *this;
  }
};
} // namespace toki::simulation::detail
#endif
