#ifndef TOKI_SIMULATION_DETAIL_BETA_TAG_HPP
#define TOKI_SIMULATION_DETAIL_BETA_TAG_HPP
namespace toki::simulation::detail {
enum struct Beta_Name
{
  VlasovPoisson_1D1V_HRIDG,
  VlasovPoissonTwoSpecies_1D1V_HRIDG,
};
enum struct Beta_Variant
{
  ManufacturedSolution,
  TwoStream,
  LandauDamping,
  BumpOnTail,
  PlasmaSheath,
  IonAcousticWave,
};
} // namespace toki::simulation::detail
#endif
