#ifndef TOKI_TIMESTEPPER_DETAIL_HRIDG_1D_HPP
#define TOKI_TIMESTEPPER_DETAIL_HRIDG_1D_HPP
#include <array>
#include <functional>
#include <string>
#include <utility>
#include <vector>
#include "toki/config.hpp"
#include "toki/math/permutation.hpp"
#include "toki/matrix/mat.hpp"
#include "toki/timestepper/detail/hridg_info.hpp"
#include "toki/timestepper/tag.hpp"
namespace toki::timestepper::detail {
struct HRIDG_1D
{
  using Info = HRIDG_Info<1>;
  struct Cache
  {
    // P0
    toki::matrix::Mat P0_LHS{};
    toki::math::permutation::Permutation P0_LHS_p{};
    toki::matrix::Mat P0_RHS{};
    // P1
    toki::matrix::Mat P1_LHS{};
    toki::math::permutation::Permutation P1_LHS_p{};
    std::array<toki::matrix::Mat, 3> P1_RHS{};
    // P2
    toki::matrix::Mat P2_LHS{};
    toki::math::permutation::Permutation P2_LHS_p{};
    std::array<toki::matrix::Mat, 3> P2_RHS{};
    // C0
    toki::matrix::Mat C0_LHS{};
    toki::math::permutation::Permutation C0_LHS_p{};
    std::array<toki::matrix::Mat, 3> C0_RHS{};
  };
  Info info;
  Cache cache;
  HRIDG_1D();
  explicit HRIDG_1D(std::string const& name);
  HRIDG_1D(HRIDG_1D const& other);
  HRIDG_1D(HRIDG_1D&& other) noexcept;
  ~HRIDG_1D();
  auto operator=(HRIDG_1D other) -> HRIDG_1D&;
  auto create_domain(
    std::string const& name,
    toki::Size const& variable,
    toki::Size const& dof,
    toki::Size const& quadrature_point,
    std::array<toki::Size, 1> const& resolution,
    std::array<toki::Size, 1> const& halo,
    std::array<toki::Number, 1> const& range,
    std::array<toki::Number, 1> const& anchor,
    std::array<int, 1> const& mpi_extent) -> HRIDG_1D&;
  auto set_element(
    std::string const& name,
    toki::Size const& variable,
    toki::Number const& value,
    std::array<toki::Size, 1> const& index,
    toki::Size const& point) -> HRIDG_1D&;
  auto set_element(
    std::string const& name,
    std::function<toki::Number(
      toki::Size const&,
      std::array<toki::Size, 1> const&,
      toki::Size const&)> const& op) -> HRIDG_1D&;
  auto set_element(
    std::string const& name,
    std::function<toki::Number(
      toki::Size const&,
      toki::Number const&,
      std::array<toki::Number, 1> const&)> const& op) -> HRIDG_1D&;
  template<toki::timestepper::tag::Flux flux, typename... Argument>
  auto
  create_flux(Argument&&... arg) -> HRIDG_1D&
  {
    if constexpr (flux == toki::timestepper::tag::Flux::Uniform) {
      return create_flux_uniform(std::forward<Argument>(arg)...);
    } else if (flux == toki::timestepper::tag::Flux::UniformUpwind) {
      return create_flux_uniform_upwind(std::forward<Argument>(arg)...);
    }
    return *this;
  }
  auto create_flux_uniform(
    toki::Size const& direction,
    std::function<toki::Number(
      toki::Number const&,
      std::vector<toki::Number> const&)> const& op) -> HRIDG_1D&;
  auto create_flux_uniform_upwind(
    toki::Size const& direction,
    std::function<toki::Number(
      bool const&,
      toki::Number const&,
      std::vector<toki::Number> const&)> const& op) -> HRIDG_1D&;
  auto prepare() -> HRIDG_1D&;
  auto halo_sync() -> HRIDG_1D&;
  auto build_P0_LHS() -> toki::matrix::Mat;
  auto build_P0_RHS() -> toki::matrix::Mat;
  auto build_P1_LHS() -> toki::matrix::Mat;
  auto build_P1_RHS() -> std::array<toki::matrix::Mat, 3>;
  auto build_C0_LHS() -> toki::matrix::Mat;
  auto build_C0_RHS() -> std::array<toki::matrix::Mat, 3>;
  [[deprecated]]
  auto solve(toki::Size step_max = 0) -> HRIDG_1D&;
  auto stop(toki::Number const& CFL, toki::Number const& signal_speed) -> bool;
  auto solve_prediction() -> HRIDG_1D&;
  auto solve_correction() -> HRIDG_1D&;
  auto report(std::string const& name) -> HRIDG_1D&;
  auto save(std::string const& name, std::string const& domain_name)
    -> HRIDG_1D&;
  friend auto
  swap(HRIDG_1D& lhs, HRIDG_1D& rhs) noexcept -> void
  {
    using std::swap;
    swap(lhs.info, rhs.info);
    swap(lhs.cache, rhs.cache);
    return;
  }
};
} // namespace toki::timestepper::detail
#endif
