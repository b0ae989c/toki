#ifndef TOKI_TIMESTEPPER_DETAIL_HRIDG_2D_HPP
#define TOKI_TIMESTEPPER_DETAIL_HRIDG_2D_HPP
#include <array>
#include <functional>
#include <string>
#include <utility>
#include <vector>
#include "toki/config.hpp"
#include "toki/log.hpp"
#include "toki/math/permutation.hpp"
#include "toki/matrix/mat.hpp"
#include "toki/timestepper/detail/hridg_info.hpp"
#include "toki/timestepper/tag.hpp"
#include "toki/vector/vec.hpp"
namespace toki::timestepper::detail {
struct HRIDG_2D
{
  using Info = HRIDG_Info<2>;
  struct Cache
  {
    struct Field
    {
      std::vector<std::array<toki::vector::Vec, 2>> velocity{};
      std::vector<toki::vector::Vec> E0{};
    };
    struct Component
    {
      toki::matrix::Mat T_RHS{};
      toki::matrix::Mat M{};
      toki::math::permutation::Permutation M_p{};
    };
    // P0
    toki::matrix::Mat P0_LHS{};
    toki::math::permutation::Permutation P0_LHS_p{};
    toki::matrix::Mat P0_RHS{};
    // P1
    toki::matrix::Mat P1_0_LHS{};
    toki::math::permutation::Permutation P1_0_LHS_p{};
    std::array<toki::matrix::Mat, 3> P1_0_RHS{};
    toki::matrix::Mat P1_1_LHS{};
    toki::math::permutation::Permutation P1_1_LHS_p{};
    std::array<toki::matrix::Mat, 3> P1_1_RHS{};
    // P2
    toki::matrix::Mat P2_LHS{};
    toki::math::permutation::Permutation P2_LHS_p{};
    std::array<toki::matrix::Mat, 5> P2_RHS{};
    // P3
    toki::matrix::Mat P3_LHS{};
    toki::math::permutation::Permutation P3_LHS_p{};
    std::array<toki::matrix::Mat, 5> P3_RHS{};
    // C0
    toki::matrix::Mat C0_LHS{};
    toki::math::permutation::Permutation C0_LHS_p{};
    std::array<toki::matrix::Mat, 5> C0_RHS{};
    Field field{};
    Component component{};
  };
  Info info;
  Cache cache;
  HRIDG_2D();
  explicit HRIDG_2D(std::string const& name);
  HRIDG_2D(HRIDG_2D const& other);
  HRIDG_2D(HRIDG_2D&& other) noexcept;
  ~HRIDG_2D();
  auto operator=(HRIDG_2D other) -> HRIDG_2D&;
  auto create_domain(
    std::string const& name,
    toki::Size const& variable,
    toki::Size const& dof,
    toki::Size const& quadrature_point,
    std::array<toki::Size, 2> const& resolution,
    std::array<toki::Size, 2> const& halo,
    std::array<toki::Number, 2> const& range,
    std::array<toki::Number, 2> const& anchor,
    std::array<int, 2> const& mpi_extent) -> HRIDG_2D&;
  auto create_domain(
    toki::Size const& variable,
    toki::Size const& quadrature_point,
    std::array<toki::Size, 2> const& resolution,
    std::array<toki::Number, 2> const& range,
    std::array<toki::Number, 2> const& anchor,
    std::array<int, 2> const& mpi_extent) -> HRIDG_2D&;
  auto set_element(
    std::string const& name,
    toki::Size const& variable,
    toki::Number const& value,
    std::array<toki::Size, 2> const& index,
    toki::Size const& point) -> HRIDG_2D&;
  auto set_element(
    std::string const& name,
    std::function<toki::Number(
      toki::Size const&,
      std::array<toki::Size, 2> const&,
      toki::Size const&)> const& op) -> HRIDG_2D&;
  auto set_element(
    std::string const& name,
    std::function<toki::Number(
      toki::Size const&,
      toki::Number const&,
      std::array<toki::Number, 2> const&)> const& op) -> HRIDG_2D&;
  template<toki::timestepper::tag::Flux flux, typename... Argument>
  auto
  create_flux(Argument&&... arg) -> HRIDG_2D&
  {
    if constexpr (flux == toki::timestepper::tag::Flux::Uniform) {
      return create_flux_uniform(std::forward<Argument>(arg)...);
    } else if constexpr (flux == toki::timestepper::tag::Flux::UniformUpwind) {
      return create_flux_uniform_upwind(std::forward<Argument>(arg)...);
    } else if constexpr (flux == toki::timestepper::tag::Flux::External) {
      return create_flux_external(std::forward<Argument>(arg)...);
    } else if constexpr (flux == toki::timestepper::tag::Flux::ExternalUpwind) {
      return create_flux_external_upwind(std::forward<Argument>(arg)...);
    } else {
      toki::log::error("no implementation");
    }
    return *this;
  }
  auto create_flux_uniform(
    toki::Size const& direction,
    std::function<toki::Number(
      toki::Number const&,
      std::vector<toki::Number> const&)> const& op) -> HRIDG_2D&;
  auto create_flux_uniform_upwind(
    toki::Size const& direction,
    std::function<toki::Number(
      bool const&,
      toki::Number const&,
      std::vector<toki::Number> const&)> const& op) -> HRIDG_2D&;
  auto create_flux_external(
    toki::Size const& direction,
    std::function<toki::Number(
      toki::Number const&,
      std::vector<toki::Number> const&,
      std::array<toki::Size, 2> const&,
      toki::Size const&)> const& op) -> HRIDG_2D&;
  auto create_flux_external_upwind(
    toki::Size const& direction,
    std::function<toki::Number(
      bool const&,
      toki::Number const&,
      std::vector<toki::Number> const&,
      std::array<toki::Size, 2> const&,
      toki::Size const&)> const& op) -> HRIDG_2D&;
  auto prepare() -> HRIDG_2D&;
  auto halo_sync() -> HRIDG_2D&;
  auto build_P0_LHS() -> toki::matrix::Mat;
  auto build_P0_LHS(std::array<toki::Size, 2> const& index)
    -> toki::matrix::Mat;
  auto build_P0_RHS() -> toki::matrix::Mat;
  auto build_P1_LHS(toki::Size const& direction) -> toki::matrix::Mat;
  auto build_P1_LHS(
    toki::Size const& direction,
    std::array<toki::Size, 2> const& index) -> toki::matrix::Mat;
  auto build_P1_RHS(toki::Size const& direction)
    -> std::array<toki::matrix::Mat, 3>;
  auto build_P1_RHS(
    toki::Size const& direction,
    std::array<toki::Size, 2> const& index) -> std::array<toki::matrix::Mat, 3>;
  auto build_P2_LHS() -> toki::matrix::Mat;
  auto build_P2_LHS(std::array<toki::Size, 2> const& index)
    -> toki::matrix::Mat;
  auto build_P2_RHS() -> std::array<toki::matrix::Mat, 5>;
  auto build_P2_RHS(std::array<toki::Size, 2> const& index)
    -> std::array<toki::matrix::Mat, 5>;
  auto build_C0_LHS() -> toki::matrix::Mat;
  auto build_C0_LHS(std::array<toki::Size, 2> const& index)
    -> toki::matrix::Mat;
  auto build_C0_RHS() -> std::array<toki::matrix::Mat, 5>;
  auto build_C0_RHS(std::array<toki::Size, 2> const& index)
    -> std::array<toki::matrix::Mat, 5>;
  [[deprecated]]
  auto solve(toki::Size step_max = 0) -> HRIDG_2D&;
  auto stop(toki::Number const& CFL, toki::Number const& signal_speed) -> bool;
  auto solve_prediction() -> HRIDG_2D&;
  auto solve_correction() -> HRIDG_2D&;
  auto apply_source(
    std::array<toki::Size, 2> const& index,
    toki::vector::Vec& buffer) -> HRIDG_2D&;
  auto report(std::string const& name) -> HRIDG_2D&;
  auto save(std::string const& name, std::string const& domain_name)
    -> HRIDG_2D&;
  friend auto
  swap(HRIDG_2D& lhs, HRIDG_2D& rhs) noexcept -> void
  {
    using std::swap;
    swap(lhs.info, rhs.info);
    swap(lhs.cache, rhs.cache);
    return;
  }
};
} // namespace toki::timestepper::detail
#endif
