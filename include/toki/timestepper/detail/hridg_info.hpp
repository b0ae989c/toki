#ifndef TOKI_TIMESTEPPER_DETAIL_HRIDG_INFO_HPP
#define TOKI_TIMESTEPPER_DETAIL_HRIDG_INFO_HPP
#include <array>
#include <functional>
#include <limits>
#include <string>
#include <vector>
#include "boost/container/flat_map.hpp"
#include "toki/basis/nodal.hpp"
#include "toki/config.hpp"
#include "toki/domain/orthotope.hpp"
#include "toki/quadrature/lobatto.hpp"
#include "toki/timestepper/tag.hpp"
namespace toki::timestepper::detail {
template<int dim>
struct HRIDG_Info
{
  struct DomainSpec
  {
    toki::Size variable;
    toki::Size dof;
    toki::Size quadrature_point;
    toki::basis::Nodal basis;
    toki::quadrature::Lobatto integrator;
  };
  struct Operation
  {
    std::array<
      std::function<
        toki::Number(toki::Number const&, std::vector<toki::Number> const&)>,
      dim>
      get_flux_uniform{};
    std::array<
      std::function<toki::Number(
        bool const&,
        toki::Number const&,
        std::vector<toki::Number> const&)>,
      dim>
      get_flux_uniform_upwind{};
    std::array<
      std::function<toki::Number(
        toki::Number const&,
        std::vector<toki::Number> const&,
        std::array<toki::Size, dim> const&,
        toki::Size const&)>,
      dim>
      get_flux_external{};
    std::array<
      std::function<toki::Number(
        bool const&,
        toki::Number const&,
        std::vector<toki::Number> const&,
        std::array<toki::Size, dim> const&,
        toki::Size const&)>,
      dim>
      get_flux_external_upwind{};
    std::function<toki::Number(
      toki::Size const&,
      toki::Number const&,
      std::array<toki::Number, dim> const&)>
      find_solution{};
    std::function<toki::Number(
      toki::Size const&,
      toki::Number const&,
      std::array<toki::Number, dim> const&)>
      find_source{};
  };
  struct Parameter
  {
    toki::Number time = 0.0;
    toki::Number time_max = 0.0;
    toki::Number time_delta = 0.0;
    toki::Number time_delta_max = 0.0;
    toki::Size step = 0;
    toki::Size step_max = std::numeric_limits<toki::Size>::max();
    toki::Number CFL = 0.0;
    toki::Number signal_speed = 0.0;
    bool flag_report_error = false;
    bool flag_apply_source = false;
  };
  std::string name{"HRIDG"};
  boost::container::flat_map<std::string, DomainSpec> domain_spec{};
  boost::container::flat_map<
    std::string,
    toki::domain::Orthotope<
      dim,
      toki::domain::detail::Orthotope_Element::DG,
      toki::domain::detail::Orthotope_Adaptivity::None>>
    domain{};
  std::array<toki::timestepper::tag::Flux, dim> flux_type{};
  Operation op{};
  Parameter param{};
};
} // namespace toki::timestepper::detail
#endif
