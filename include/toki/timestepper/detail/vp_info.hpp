#ifndef TOKI_TIMESTEPPER_DETAIL_VP_INFO_HPP
#define TOKI_TIMESTEPPER_DETAIL_VP_INFO_HPP
#include <limits>
#include <string>
#include <tuple>
#include <vector>
#include "boost/container/flat_map.hpp"
#include "boost/unordered/concurrent_flat_map.hpp"
#include "toki/basis/nodal.hpp"
#include "toki/config.hpp"
#include "toki/domain/orthotope.hpp"
#include "toki/quadrature/lobatto.hpp"
namespace toki::timestepper::detail {
template<int dim_space, int dim_velocity>
struct VP_Info
{
  using DomainPlasma = toki::domain::Orthotope<
    dim_space + dim_velocity,
    toki::domain::detail::Orthotope_Element::DG,
    toki::domain::detail::Orthotope_Adaptivity::P>;
  using DomainField = toki::domain::Orthotope<
    dim_space,
    toki::domain::detail::Orthotope_Element::DG,
    toki::domain::detail::Orthotope_Adaptivity::None>;
  struct Domain
  {
    boost::container::flat_map<std::string, DomainPlasma> f;
    boost::container::flat_map<std::string, DomainField> E;
    boost::container::flat_map<std::string, std::vector<toki::vector::Vec>>
      flux_Jacobian;
  };
  struct Flag
  {
    struct Performance
    {
      bool skip_zero_element;
    };
    struct Background
    {
      bool current = false;
      bool plasma_source = false;
    };
    struct Report
    {
      bool mass = true;
      bool momentum = true;
      bool kinetic_energy = true;
      bool potential_energy = true;
      bool total_energy = true;
    };
    Performance performance;
    Background background;
    Report report;
  };
  struct Parameter
  {
    struct Model
    {
      toki::Size species = 1;
    };
    struct Method
    {
      bool adaptivity = false;
      toki::Size order_min = 1;
      toki::Size order_max = 1;
      toki::Size dof_total = 0;
      toki::Number CFL = 0.0;
      toki::Number refinement_score_min = 0.0;
      toki::Number refinement_score_max = 1.0;
      toki::Number signal_speed = 0.0;
    };
    struct Time
    {
      toki::Number now = 0.0;
      toki::Number max = 0.0;
      toki::Number delta = 0.0;
      toki::Number delta_min = 1e-8;
      toki::Number delta_max = std::numeric_limits<toki::Number>::max();
      toki::Size step = 0;
      toki::Size step_max = std::numeric_limits<toki::Size>::max();
      std::vector<toki::Number> checkpoint{};
      toki::Size snapshot = 0;
    };
    Model model;
    Method method;
    Time time;
  };
  struct Cache
  {
    boost::concurrent_flat_map<
      std::tuple<toki::Number, toki::Size, toki::Number, toki::Size>,
      toki::Number>
      E;
  };
  struct Record
  {
    struct History
    {
      std::vector<std::vector<int>> refinement;
      std::vector<toki::Number> E_L2;
      std::vector<std::vector<toki::Number>> mass;
      std::vector<std::vector<toki::Number>> momentum;
      std::vector<std::vector<toki::Number>> kinetic_energy;
      std::vector<toki::Number> potential_energy;
    };
    History history;
  };
  std::string name{"VlasovPoisson"};
  Flag flag{};
  Parameter param{};
  Cache cache{};
  Record record{};
  Domain domain{};
};
} // namespace toki::timestepper::detail
#endif
