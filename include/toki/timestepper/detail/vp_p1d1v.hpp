#ifndef TOKI_TIMESTEPPER_DETAIL_VP_P1D1V_HPP
#define TOKI_TIMESTEPPER_DETAIL_VP_P1D1V_HPP
#include <algorithm>
#include <array>
#include <cmath>
#include <fstream>
#include <limits>
#include <memory>
#include <numeric>
#include <vector>
#include "fmt/format.h"
#include "mpi.h"
#include "omp.h"
#include "toki/config.hpp"
#include "toki/context/default.hpp"
#include "toki/log.hpp"
#include "toki/math/index.hpp"
#include "toki/math/permutation.hpp"
#include "toki/math/seq.hpp"
#include "toki/matrix/mat.hpp"
#include "toki/option/vp.hpp"
#include "toki/timestepper/detail/vp_info.hpp"
#include "toki/timestepper/detail/vp_tag.hpp"
#include "toki/vector/vec.hpp"
namespace toki::timestepper::detail {
template<VP_Example example>
struct VP_p1D1V
{
  using Context = toki::context::Default;
  using Option = toki::option::VP;
  using Info = VP_Info<1, 1>;
  std::unique_ptr<Context> context = nullptr;
  std::unique_ptr<Option> option = nullptr;
  Info info{};
  VP_p1D1V() {}
  VP_p1D1V(int argc, char** argv)
    : context(std::make_unique<Context>(argc, argv))
    , option(std::make_unique<Option>(
        argc,
        argv,
        fmt::format("vp-p1d1v-{}", argv[1])))
  {
  }
  VP_p1D1V(VP_p1D1V const&) = delete;
  VP_p1D1V(VP_p1D1V&&) noexcept = delete;
  ~VP_p1D1V() {}
  auto operator=(VP_p1D1V const&) -> VP_p1D1V& = delete;
  auto operator=(VP_p1D1V&&) noexcept -> VP_p1D1V& = delete;
  auto
  start() -> VP_p1D1V&
  {
    context->guard([&]() -> void {
      option->parse();
      // command line help info
      if (option->data.contains("help")) {
        option->print();
        return;
      }
      // option patch
      if constexpr (
        example ==
        toki::timestepper::detail::VP_Example::IonAcousticShockWave) {
        option->param.flag.refinement.sync_all_species = false;
      }
      // prepare
      build();
      apply_initial_condition();
      for (toki::Size order = info.param.method.order_min;
           order < info.param.method.order_max;
           ++order) {
        refine();
        apply_initial_condition();
      }
      if (info.param.method.order_min == info.param.method.order_max) {
        refine();
      }
      stop();
      update_velocity().save();
      // launch
      MPI_Barrier(MPI_COMM_WORLD);
      while (!stop()) {
        // record
        record_E().record_mass().record_momentum().record_energy();
        MPI_Barrier(MPI_COMM_WORLD);
        // step
        update_E().solve_prediction();
        // boundary condition
        do {
          if constexpr (
            example == toki::timestepper::detail::VP_Example::PlasmaSheath) {
            int mpi_size{};
            MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
            auto& f = info.domain.f.at("0-default");
            auto& f_P2 = info.domain.f.at("0-result-P2");
            auto const& resolution = f.info.local.resolution;
            auto const& halo = f.info.halo;
            auto const& mpi_rank = f.info.local.mpi.rank;
            if (mpi_rank == 0) {
              for (toki::Size index_v0 = 0; index_v0 < resolution.at(1);
                   ++index_v0) {
                f_P2.set_element(0.0, {halo.at(0) - 1, index_v0});
              }
            } else if (mpi_rank == mpi_size - 1) {
              for (toki::Size index_v0 = 0; index_v0 < resolution.at(1);
                   ++index_v0) {
                f_P2.set_element(
                  0.0, {resolution.at(0) - halo.at(0), index_v0});
              }
            }
          }
        } while (0);
        MPI_Barrier(MPI_COMM_WORLD);
        solve_correction().halo_sync();
        MPI_Barrier(MPI_COMM_WORLD);
        refine().update_velocity();
        // info
        info.param.time.now += info.param.time.delta;
        info.param.time.step += 1;
        if (
          (info.param.time.snapshot > 0) &&
          (info.param.time.now ==
           info.param.time.checkpoint.at(info.param.time.snapshot - 1))) {
          report();
        }
        info.param.method.dof_total = get_dof_total();
        auto const E0_min = info.domain.E.at("0").get_min();
        auto const E0_max = info.domain.E.at("0").get_max();
        toki::log::info(fmt::format(
          "{:09d}│{:+14.6E}│{:7.3f}%"
          "│dt = {:+14.6E}"
          "│dof = {}"
          "│E0_min = {:+14.6E}"
          "│E0_max = {:+14.6E}",
          info.param.time.step,
          info.param.time.now,
          info.param.time.now / info.param.time.max * 100.0,
          info.param.time.delta,
          info.param.method.dof_total,
          E0_min,
          E0_max));
        if (option->param.flag.info.detail) {
          report();
        }
        MPI_Barrier(MPI_COMM_WORLD);
      }
      // report
      record_E().record_mass().record_momentum().record_energy();
      report();
    });
    return *this;
  }
  auto
  stop() -> bool
  {
    // reach `time.max`
    if (info.param.time.now == info.param.time.max) {
      return true;
    }
    if constexpr (
      example == toki::timestepper::detail::VP_Example::IonAcousticWave ||
      example == toki::timestepper::detail::VP_Example::IonAcousticShockWave) {
      toki::Number h_r0{};
      toki::Number h_v0{};
      toki::Number speed_r0{};
      toki::Number speed_v0{};
      toki::Number const E0_max =
        1.5 * std::max({
                1.0e-1,
                std::fabs(info.domain.E.at("0").get_min()),
                std::fabs(info.domain.E.at("0").get_max()),
              });
      // electron
      h_r0 = info.domain.f.at("0-default").info.local.element.volume.at(0);
      h_v0 = info.domain.f.at("0-default").info.local.element.volume.at(1);
      speed_r0 = std::fabs(info.domain.f.at("0-default").info.anchor.at(1));
      speed_v0 = E0_max * std::fabs(option->param.feature.q.at(0)) /
                 option->param.feature.m.at(0);
      info.param.time.delta = std::min({
        info.param.time.delta_max,
        info.param.method.CFL * h_r0 / speed_r0,
        info.param.method.CFL * h_v0 / speed_v0,
      });
      info.param.method.signal_speed = std::max(speed_r0, speed_v0);
      // ion
      h_r0 = info.domain.f.at("1-default").info.local.element.volume.at(0);
      h_v0 = info.domain.f.at("1-default").info.local.element.volume.at(1);
      speed_r0 = std::max(
        std::fabs(info.domain.f.at("1-default").info.anchor.at(1)),
        std::fabs(
          info.domain.f.at("1-default").info.anchor.at(1) +
          info.domain.f.at("1-default").info.range.at(1)));
      speed_v0 = E0_max * std::fabs(option->param.feature.q.at(1)) /
                 option->param.feature.m.at(1);
      info.param.method.signal_speed =
        std::max({info.param.method.signal_speed, speed_r0, speed_v0});
      info.param.time.delta = std::min({
        info.param.time.delta,
        info.param.method.CFL * h_r0 / speed_r0,
        info.param.method.CFL * h_v0 / speed_v0,
      });
    } else {
      auto const h = find_h();
      info.param.method.signal_speed = find_signal_speed();
      info.param.time.delta = std::min(
        info.param.method.CFL * h / info.param.method.signal_speed,
        info.param.time.delta_max);
    }
    if (info.param.time.now + info.param.time.delta > info.param.time.max) {
      info.param.time.delta = info.param.time.max - info.param.time.now;
    } else if (
      info.param.time.max - (info.param.time.now + info.param.time.delta) <
      info.param.time.delta_min) {
      info.param.time.delta *= 0.5;
    } else if (
      (info.param.time.checkpoint.size() > 0) &&
      (info.param.time.snapshot < info.param.time.checkpoint.size())) {
      auto const& target =
        info.param.time.checkpoint.at(info.param.time.snapshot);
      if (info.param.time.now + info.param.time.delta >= target) {
        info.param.time.delta = target - info.param.time.now;
        info.param.time.snapshot += 1;
      } else if (
        target - (info.param.time.now + info.param.time.delta) <
        info.param.time.delta_min) {
        info.param.time.delta *= 0.5;
      }
    }
    if (info.param.time.step >= info.param.time.step_max) {
      return true;
    }
    return false;
  }
  auto
  report() -> VP_p1D1V&
  {
    save();
    if constexpr (
      example == toki::timestepper::detail::VP_Example::ManufacturedSolution) {
      auto& f = info.domain.f.at("0-default");
      auto const& resolution = f.info.local.resolution;
      auto const& halo = f.info.halo;
      auto const& anchor = f.info.local.anchor;
      auto const& volume = f.info.local.element.volume;
      toki::Number local_diff{};
      toki::Number local_exact{};
      toki::Number buffer_out[2];
      toki::Number buffer_in[2];
      // L2 error: f
      local_diff = 0.0;
      local_exact = 0.0;
      for (toki::Size index_v0 = halo.at(1);
           index_v0 < resolution.at(1) - halo.at(1);
           ++index_v0) {
        for (toki::Size index_r0 = halo.at(0);
             index_r0 < resolution.at(0) - halo.at(0);
             ++index_r0) {
          auto const& basis = f.get_basis({index_r0, index_v0});
          auto const& point_max = basis.info.point;
          auto const dof = f.get_dof({index_r0, index_v0});
          toki::vector::Vec Q(dof);
          f.get_element(Q, 0, {index_r0, index_v0});
          for (toki::Size point = 0; point < dof; ++point) {
            auto const r0 =
              anchor.at(0) +
              (static_cast<toki::Number>(index_r0) +
               (basis.info.abscissa.at(point % point_max) + 1.0) / 2.0) *
                volume.at(0);
            auto const v0 =
              anchor.at(1) +
              (static_cast<toki::Number>(index_v0) +
               (basis.info.abscissa.at(point / point_max) + 1.0) / 2.0) *
                volume.at(1);
            auto const solution =
              (2.0 - std::cos(2.0 * (r0 - M_PI * info.param.time.now))) *
              std::exp(-1.0 / 4.0 * std::pow(4.0 * v0 - 1.0, 2.0));
            local_diff += std::pow(solution - Q.at(point), 2.0);
            local_exact += std::pow(solution, 2.0);
          }
        }
      }
      buffer_out[0] = local_diff;
      buffer_out[1] = local_exact;
      MPI_Barrier(MPI_COMM_WORLD);
      MPI_Allreduce(
        buffer_out, buffer_in, 2, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
      toki::log::info(fmt::format(
        "f0 (L2 error): {:+.6e}", std::sqrt(buffer_in[0] / buffer_in[1])));
      // L2 error: E
      local_diff = 0.0;
      local_exact = 0.0;
      auto& E = info.domain.E.at("0");
      auto const& basis_EMF = E.info.basis;
      for (toki::Size index_r0 = halo.at(0);
           index_r0 < resolution.at(0) - halo.at(0);
           ++index_r0) {
        auto const& dof = basis_EMF.info.point;
        toki::vector::Vec sample(dof);
        E.get_element(sample, 0, {index_r0});
        for (toki::Size point = 0; point < dof; ++point) {
          auto const r0 =
            anchor.at(0) +
            (static_cast<toki::Number>(index_r0) +
             (basis_EMF.info.abscissa.at(point % dof) + 1.0) / 2.0) *
              volume.at(0);
          auto const solution =
            -std::sqrt(M_PI) / 4.0 *
            std::sin(2.0 * (r0 - M_PI * info.param.time.now));
          local_diff += std::pow(solution - sample.at(point), 2.0);
          local_exact += std::pow(solution, 2.0);
        }
      }
      buffer_out[0] = local_diff;
      buffer_out[1] = local_exact;
      MPI_Barrier(MPI_COMM_WORLD);
      MPI_Allreduce(
        buffer_out, buffer_in, 2, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
      toki::log::info(fmt::format(
        "E (L2 error): {:+.6e}", std::sqrt(buffer_in[0] / buffer_in[1])));
    }
    do {
      auto const& mpi_rank = info.domain.f.at("0-default").info.local.mpi.rank;
      auto save_record = [&](
                           std::string const& name,
                           std::vector<toki::Number> const& history) -> void {
        auto const filename =
          fmt::format("{}-{}-history-{:04}.txt", info.name, name, mpi_rank);
        std::ofstream file(filename);
        for (toki::Size offset = 0; offset < history.size(); ++offset) {
          file << fmt::format("{}, {:+.14e}\n", offset, history.at(offset));
        }
        file.close();
        return;
      };
      save_record("E", info.record.history.E_L2);
      for (toki::Size species = 0; species < info.param.model.species;
           ++species) {
        save_record(
          fmt::format("mass-{}", species),
          info.record.history.mass.at(species));
        save_record(
          fmt::format("momentum-{}", species),
          info.record.history.momentum.at(species));
        save_record(
          fmt::format("kinetic-energy-{}", species),
          info.record.history.kinetic_energy.at(species));
        save_record(
          fmt::format("potential-energy-{}", species),
          info.record.history.potential_energy);
      }
    } while (0);
    return *this;
  }
  auto
  save() -> VP_p1D1V&
  {
    // f
    for (toki::Size species = 0; species < info.param.model.species;
         ++species) {
      info.domain.f.at(fmt::format("{}-default", species))
        .save(fmt::format("{}-f-{}", info.name, species), info.param.time.step);
    }
    // E
    info.domain.E.at("0").save(
      fmt::format("{}-E0", info.name), info.param.time.step);
    if (!info.param.method.adaptivity) {
      return *this;
    }
    // refinement
    for (toki::Size species = 0; species < info.param.model.species;
         ++species) {
      auto const& f = info.domain.f.at(fmt::format("{}-default", species));
      auto const& mpi_rank = f.info.local.mpi.rank;
      auto const filename = fmt::format(
        "{}-f-{}-refinement-{:09}-{:04}.txt",
        info.name,
        species,
        info.param.time.step,
        mpi_rank);
      std::ofstream file(filename);
      auto const& data = info.record.history.refinement.back();
      auto const& N = f.info.local.element.N;
      for (toki::Size offset = 0; offset < N; ++offset) {
        auto const index =
          toki::math::index::ind2sub<2>(f.info.local.resolution, offset);
        file << fmt::format(
          "{}, {}, {}, {:+.14e}\n",
          index.at(0),
          index.at(1),
          data.at(offset),
          f.info.local.refinement.score.oscillation.at(offset));
      }
      file.close();
    }
    return *this;
  }
  auto
  build() -> VP_p1D1V&
  {
    auto& param = option->param;
    // name
    info.name = param.info.name;
    // flag
    info.flag.performance.skip_zero_element =
      param.flag.performance.skip_zero_element;
    info.flag.background.current = param.flag.background.current;
    info.flag.background.plasma_source = param.flag.background.plasma_source;
    // parameter
    info.param.model.species = param.model.species;
    info.param.method.adaptivity = param.method.adaptivity_p;
    if (info.param.method.adaptivity) {
      info.param.method.order_min = 1;
    } else {
      info.param.method.order_min = param.method.order;
    }
    info.param.method.order_max = param.method.order;
    info.param.method.CFL = param.method.CFL;
    info.param.method.refinement_score_min =
      param.method.refinement_score.at(0);
    info.param.method.refinement_score_max =
      param.method.refinement_score.at(1);
    info.param.time.max = param.time.max;
    info.param.time.delta_min = param.time.delta_min;
    info.param.time.delta_max = param.time.delta_max;
    info.param.time.step_max = param.time.step_max;
    info.param.time.checkpoint = param.time.checkpoint;
    if constexpr (
      example == toki::timestepper::detail::VP_Example::IonAcousticWave ||
      example == toki ::timestepper::detail::VP_Example::IonAcousticShockWave) {
      param.feature.m.push_back(param.feature.m.at(0) * param.feature.r.at(0));
      param.feature.T.push_back(param.feature.T.at(0));
      param.feature.T.at(0) = param.feature.T.at(1) * param.feature.r.at(1);
    }
    // domain
    build_domain();
    // record
    info.record.history.mass.resize(info.param.model.species);
    info.record.history.momentum.resize(info.param.model.species);
    info.record.history.kinetic_energy.resize(info.param.model.species);
    return *this;
  }
  auto
  build_domain() -> VP_p1D1V&
  {
    // resolution
    std::array<toki::Size, 2> resolution_2D{option->param.domain.resolution, 0};
    std::array<toki::Size, 1> resolution_1D{resolution_2D.at(0)};
    // halo
    std::array<toki::Size, 2> halo_2D{2, 2};
    std::array<toki::Size, 1> halo_1D{halo_2D.at(0)};
    // range
    std::array<toki::Number, 2> range_2D{option->param.domain.range, 0.0};
    std::array<toki::Number, 1> range_1D{range_2D.at(0)};
    // anchor
    std::array<toki::Number, 2> anchor_2D{-range_2D.at(0) / 2.0, 0.0};
    std::array<toki::Number, 1> anchor_1D{anchor_2D.at(0)};
    if constexpr (
      example == toki::timestepper::detail::VP_Example::PlasmaSheath) {
      resolution_2D.at(1) = resolution_2D.at(0) / 2 * 1;
      auto const r = 0.5;
      range_2D.at(1) = range_2D.at(0) * r;
      anchor_2D.at(1) = anchor_2D.at(0) * r;
    } else if constexpr (
      example == toki::timestepper::detail::VP_Example::ManufacturedSolution ||
      example == toki::timestepper::detail::VP_Example::LandauDamping ||
      example == toki::timestepper::detail::VP_Example::IonAcousticWave) {
      resolution_2D.at(1) = resolution_2D.at(0) / 1 * 1;
      auto const r = 1.0;
      range_2D.at(1) = range_2D.at(0) * r;
      anchor_2D.at(1) = anchor_2D.at(0) * r;
    } else if constexpr (
      example == toki::timestepper::detail::VP_Example::TwoStreamInstability ||
      example == toki::timestepper::detail::VP_Example::BumpOnTail) {
      resolution_2D.at(1) = resolution_2D.at(0) / 4 * 5;
      auto const r = 1.25;
      range_2D.at(1) = range_2D.at(0) * r;
      anchor_2D.at(1) = anchor_2D.at(0) * r;
    } else if constexpr (
      example == toki::timestepper::detail::VP_Example::IonAcousticShockWave) {
      resolution_2D.at(1) =
        std::max(static_cast<toki::Size>(256), resolution_2D.at(0) / 1 * 1);
      auto const r = 2.0e-1;
      range_2D.at(1) = range_2D.at(0) * r;
      anchor_2D.at(1) = anchor_2D.at(0) * r;
    } else {
      toki::log::error("no implementation");
    }
    // MPI
    std::array<int, 2> const mpi_extent_2D{
      option->param.mpi.extent.at(0),
      option->param.mpi.extent.at(1),
    };
    std::array<int, 1> const mpi_extent_1D{
      mpi_extent_2D.at(0),
    };
    // dof
    auto const dof_min = info.param.method.order_min;
    auto const level_max =
      info.param.method.order_max - info.param.method.order_min + 1;
    std::vector<toki::Size> dof(level_max);
    std::vector<toki::Size> dof_C(level_max, 1);
    std::vector<toki::Size> dof_P(level_max);
    for (toki::Size level = 0; level < level_max; ++level) {
      dof.at(level) = dof_min + level;
      for (toki::Size dim = 0; dim < 2; ++dim) {
        dof_C.at(level) *= dof.at(level);
      }
      dof_P.at(level) = dof_C.at(level) * dof.at(level);
    }
    // level
    std::array<int, 2> level{
      static_cast<int>(info.param.method.order_min),
      static_cast<int>(info.param.method.order_max),
    };
    // score
    std::array<toki::Number, 2> score{
      info.param.method.refinement_score_min,
      info.param.method.refinement_score_max,
    };
    // domain
    for (toki::Size species = 0; species < info.param.model.species;
         ++species) {
      if (
        species == 1 &&
        example ==
          toki::timestepper::detail::VP_Example::IonAcousticShockWave) {
        auto const r = 4.0e-4;
        range_2D.at(1) = range_2D.at(0) * r;
        anchor_2D.at(1) = anchor_2D.at(0) * r - 2.42e-2;
      }
      info.domain.f.insert_or_assign(
        fmt::format("{}-default", species),
        Info::DomainPlasma(
          dof_C,
          level,
          score,
          resolution_2D,
          halo_2D,
          range_2D,
          anchor_2D,
          mpi_extent_2D));
      info.domain.f.insert_or_assign(
        fmt::format("{}-result-P0", species),
        Info::DomainPlasma(
          dof_P,
          level,
          score,
          resolution_2D,
          halo_2D,
          range_2D,
          anchor_2D,
          mpi_extent_2D));
      info.domain.f.insert_or_assign(
        fmt::format("{}-result-P1-0", species),
        Info::DomainPlasma(
          dof_P,
          level,
          score,
          resolution_2D,
          halo_2D,
          range_2D,
          anchor_2D,
          mpi_extent_2D));
      info.domain.f.insert_or_assign(
        fmt::format("{}-result-P1-1", species),
        Info::DomainPlasma(
          dof_P,
          level,
          score,
          resolution_2D,
          halo_2D,
          range_2D,
          anchor_2D,
          mpi_extent_2D));
      info.domain.f.insert_or_assign(
        fmt::format("{}-result-P2", species),
        Info::DomainPlasma(
          dof_P,
          level,
          score,
          resolution_2D,
          halo_2D,
          range_2D,
          anchor_2D,
          mpi_extent_2D));
    }
    auto const point_EMF = info.param.method.order_max;
    auto const dof_EMF = point_EMF;
    info.domain.E.insert_or_assign(
      "0",
      Info::DomainField(
        dof_EMF,
        point_EMF,
        resolution_1D,
        halo_1D,
        range_1D,
        anchor_1D,
        mpi_extent_1D));
    return *this;
  }
  auto
  refine() -> VP_p1D1V&
  {
    if (!info.param.method.adaptivity) {
      return *this;
    }
    for (toki::Size species = 0; species < info.param.model.species;
         ++species) {
      auto& f = info.domain.f.at(fmt::format("{}-default", species));
      auto& refinement = f.info.local.refinement;
#pragma omp parallel default(none) shared(f, refinement)
      {
        toki::Size offset{};
#pragma omp for schedule(runtime)
        for (offset = 0; offset < f.info.local.element.N; ++offset) {
          auto const index =
            toki::math::index::ind2sub<2>(f.info.local.resolution, offset);
          auto score = f.get_refinement_score(index);
          if (score < refinement.score.min) {
            refinement.target.at(offset) =
              std::max(refinement.level.min, refinement.current.at(offset) - 1);
          } else if (score > refinement.score.max) {
            refinement.target.at(offset) =
              std::min(refinement.level.max, refinement.current.at(offset) + 1);
          }
        }
      }
      // strategy: maximal level along velocity slice
      for (toki::Size index_1 = 0; index_1 < f.info.local.resolution.at(1);
           ++index_1) {
        auto local_level = refinement.level.min;
        for (toki::Size index_0 = 0; index_0 < f.info.local.resolution.at(0);
             ++index_0) {
          auto const offset = toki::math::index::sub2ind<2>(
            f.info.local.resolution,
            std::array<toki::Size, 2>{index_0, index_1});
          local_level = std::max(local_level, refinement.target.at(offset));
        }
        auto level = local_level;
        MPI_Barrier(MPI_COMM_WORLD);
        MPI_Allreduce(
          &local_level,
          &level,
          1,
          MPI_UNSIGNED_LONG_LONG,
          MPI_MAX,
          MPI_COMM_WORLD);
        for (toki::Size index_0 = 0; index_0 < f.info.local.resolution.at(0);
             ++index_0) {
          auto const offset = toki::math::index::sub2ind<2>(
            f.info.local.resolution,
            std::array<toki::Size, 2>{index_0, index_1});
          refinement.target.at(offset) = level;
        }
      }
    }
    if (
      (info.param.model.species > 1) &&
      (option->param.flag.refinement.sync_all_species)) {
      auto target = info.domain.f.at("0-default").info.local.refinement.target;
      for (toki::Size species = 1; species < info.param.model.species;
           ++species) {
        auto& f = info.domain.f.at(fmt::format("{}-default", species));
        auto& refinement = f.info.local.refinement;
        for (toki::Size offset = 0; offset < f.info.local.element.N; ++offset) {
          target.at(offset) =
            std::max(target.at(offset), refinement.target.at(offset));
        }
      }
      for (toki::Size species = 0; species < info.param.model.species;
           ++species) {
        info.domain.f.at(fmt::format("{}-default", species))
          .info.local.refinement.target = target;
      }
    }
    info.record.history.refinement.push_back(
      info.domain.f.at("0-default").info.local.refinement.target);
    for (toki::Size species = 0; species < info.param.model.species;
         ++species) {
      auto& f = info.domain.f.at(fmt::format("{}-default", species));
      for (auto const& name : std::vector<std::string>{
             "result-P0",
             "result-P1-0",
             "result-P1-1",
             "result-P2",
           }) {
        auto& f_tmp = info.domain.f.at(fmt::format("{}-{}", species, name));
        f_tmp.info.local.refinement.target = f.info.local.refinement.target;
        f_tmp.refine(false, true);
      }
      f.refine();
    }
    return *this;
  }
  auto
  record_E() -> VP_p1D1V&
  {
    auto& E0 = info.domain.E.at("0");
    auto const& dof = E0.info.quadrature_point;
    auto const& basis = E0.info.basis;
    auto const& resolution = E0.info.local.resolution;
    auto const& halo = E0.info.halo;
    toki::Number result = 0.0;
    toki::vector::Vec sample(dof);
    toki::quadrature::Lobatto quadrature(dof + 1);
    for (toki::Size index_r0 = halo.at(0);
         index_r0 < resolution.at(0) - halo.at(0);
         ++index_r0) {
      E0.get_element(sample, 0, {index_r0});
      auto op = [&](std::array<toki::Number, 1> const& coord) -> toki::Number {
        return std::pow(basis.approx<1>(coord, sample, 0), 2.0);
      };
      result += quadrature.sum<1>(op);
    }
    result = result * E0.info.local.element.volume.at(0) / 2.0;
    info.record.history.E_L2.push_back(result);
    return *this;
  }
  auto
  record_mass() -> VP_p1D1V&
  {
    for (toki::Size species = 0; species < info.param.model.species;
         ++species) {
      auto& f = info.domain.f.at(fmt::format("{}-default", species));
      auto const& resolution = f.info.local.resolution;
      auto const& halo = f.info.halo;
      toki::Number result = 0.0;
      for (toki::Size index_v0 = halo.at(1);
           index_v0 < resolution.at(1) - halo.at(1);
           ++index_v0) {
        for (toki::Size index_r0 = halo.at(0);
             index_r0 < resolution.at(0) - halo.at(0);
             ++index_r0) {
          toki::vector::Vec sample(f.get_dof({index_r0, index_v0}));
          f.get_element(sample, 0, {index_r0, index_v0});
          auto const& basis = f.get_basis({index_r0, index_v0});
          auto op =
            [&](std::array<toki::Number, 2> const& coord) -> toki::Number {
            return basis.approx<2>(coord, sample, 0);
          };
          result += f.get_quadrature({index_r0, index_v0}).sum<2>(op);
        }
      }
      auto const& volume = f.info.local.element.volume;
      result = result * volume.at(0) * volume.at(1) / 4.0;
      info.record.history.mass.at(species).push_back(result);
    }
    return *this;
  }
  auto
  record_momentum() -> VP_p1D1V&
  {
    for (toki::Size species = 0; species < info.param.model.species;
         ++species) {
      auto& f = info.domain.f.at(fmt::format("{}-default", species));
      auto const& resolution = f.info.local.resolution;
      auto const& halo = f.info.halo;
      auto const& anchor = f.info.local.anchor;
      auto const& volume = f.info.local.element.volume;
      toki::Number result = 0.0;
      for (toki::Size index_v0 = halo.at(1);
           index_v0 < resolution.at(1) - halo.at(1);
           ++index_v0) {
        for (toki::Size index_r0 = halo.at(0);
             index_r0 < resolution.at(0) - halo.at(0);
             ++index_r0) {
          toki::vector::Vec sample(f.get_dof({index_r0, index_v0}));
          f.get_element(sample, 0, {index_r0, index_v0});
          auto const& basis = f.get_basis({index_r0, index_v0});
          auto op =
            [&](std::array<toki::Number, 2> const& coord) -> toki::Number {
            auto const v0 =
              anchor.at(1) + (static_cast<toki::Number>(index_v0) +
                              (coord.at(1) + 1.0) / 2.0) *
                               volume.at(1);
            return v0 * basis.approx<2>(coord, sample, 0);
          };
          result += f.get_quadrature({index_r0, index_v0}).sum<2>(op);
        }
      }
      result = result * volume.at(0) * volume.at(1) / 4.0;
      info.record.history.momentum.at(species).push_back(result);
    }
    return *this;
  }
  auto
  record_energy() -> VP_p1D1V&
  {
    // kinetic energy
    for (toki::Size species = 0; species < info.param.model.species;
         ++species) {
      auto& f = info.domain.f.at(fmt::format("{}-default", species));
      auto const& resolution = f.info.local.resolution;
      auto const& halo = f.info.halo;
      auto const& anchor = f.info.local.anchor;
      auto const& volume = f.info.local.element.volume;
      toki::Number result = 0.0;
      for (toki::Size index_v0 = halo.at(1);
           index_v0 < resolution.at(1) - halo.at(1);
           ++index_v0) {
        for (toki::Size index_r0 = halo.at(0);
             index_r0 < resolution.at(0) - halo.at(0);
             ++index_r0) {
          toki::vector::Vec sample(f.get_dof({index_r0, index_v0}));
          f.get_element(sample, 0, {index_r0, index_v0});
          auto const& basis = f.get_basis({index_r0, index_v0});
          auto op =
            [&](std::array<toki::Number, 2> const& coord) -> toki::Number {
            auto const v0 =
              anchor.at(1) + (static_cast<toki::Number>(index_v0) +
                              (coord.at(1) + 1.0) / 2.0) *
                               volume.at(1);
            return std::pow(v0, 2.0) * basis.approx<2>(coord, sample, 0);
          };
          result += f.get_quadrature({index_r0, index_v0}).sum<2>(op);
        }
      }
      result = 0.5 * result * volume.at(0) * volume.at(1) / 4.0;
      info.record.history.kinetic_energy.at(species).push_back(result);
    }
    // potential energy
    do {
      auto& E0 = info.domain.E.at("0");
      auto const& dof = E0.info.quadrature_point;
      auto const& basis = E0.info.basis;
      auto const& resolution = E0.info.local.resolution;
      auto const& halo = E0.info.halo;
      toki::Number result = 0.0;
      toki::vector::Vec sample(dof);
      toki::quadrature::Lobatto quadrature(dof + 1);
      for (toki::Size index_r0 = halo.at(0);
           index_r0 < resolution.at(0) - halo.at(0);
           ++index_r0) {
        E0.get_element(sample, 0, {index_r0});
        auto op =
          [&](std::array<toki::Number, 1> const& coord) -> toki::Number {
          return std::pow(basis.approx<1>(coord, sample, 0), 2.0);
        };
        result += quadrature.sum<1>(op);
      }
      result = 0.5 * result * E0.info.local.element.volume.at(0) / 2.0;
      info.record.history.potential_energy.push_back(result);
    } while (0);
    return *this;
  }
  auto
  apply_initial_condition() -> VP_p1D1V&
  {
    auto const& feature = option->param.feature;
    // f
    if constexpr (
      example == toki::timestepper::detail::VP_Example::ManufacturedSolution) {
      auto op = [&](std::array<toki::Number, 2> const& coord) -> toki::Number {
        toki::Number const t = 0.0;
        auto const& r0 = coord.at(0);
        auto const& v0 = coord.at(1);
        return (2.0 - std::cos(2.0 * (r0 - M_PI * t))) *
               std::exp(-1.0 / 4.0 * std::pow(4.0 * v0 - 1.0, 2.0));
      };
      info.domain.f.at("0-default").set_element(op);
    } else if constexpr (
      example == toki::timestepper::detail::VP_Example::LandauDamping) {
      auto op = [&](std::array<toki::Number, 2> const& coord) -> toki::Number {
        auto const& r0 = coord.at(0);
        auto const& v0 = coord.at(1);
        auto const& a = feature.a.at(0);
        auto const& k = feature.k.at(0);
        return 1.0 / std::sqrt(2.0 * M_PI) * (1.0 + a * std::cos(k * r0)) *
               std::exp(-std::pow(v0, 2.0) / 2.0);
      };
      info.domain.f.at("0-default").set_element(op);
    } else if constexpr (
      example == toki::timestepper::detail::VP_Example::TwoStreamInstability) {
      auto op = [&](std::array<toki::Number, 2> const& coord) -> toki::Number {
        auto const& r0 = coord.at(0);
        auto const& v0 = coord.at(1);
        auto const& a = feature.a.at(0);
        auto const& k = feature.k.at(0);
        return std::pow(v0, 2.0) / std::sqrt(2.0 * M_PI) *
               (1.0 + a * std::cos(k * r0)) *
               std::exp(-std::pow(v0, 2.0) / 2.0);
      };
      info.domain.f.at("0-default").set_element(op);
    } else if constexpr (
      example == toki::timestepper::detail::VP_Example::BumpOnTail) {
      auto op = [&](std::array<toki::Number, 2> const& coord) -> toki::Number {
        auto const& r0 = coord.at(0);
        auto const& v0 = coord.at(1);
        toki::Number const f = 0.9 * std::exp(-std::pow(v0, 2.0) / 2.0) +
                               0.2 * std::exp(-std::pow((v0 - 4.5) / 0.5, 2.0));
        auto const& a = feature.a.at(0);
        auto const& k = feature.k.at(0);
        return f / std::sqrt(2.0 * M_PI) * (1.0 + a * std::cos(k * r0));
      };
      info.domain.f.at("0-default").set_element(op);
    } else if constexpr (
      example == toki::timestepper::detail::VP_Example::PlasmaSheath) {
      auto op = [&](std::array<toki::Number, 2> const& coord) -> toki::Number {
        auto const& v0 = coord.at(1);
        auto const& rho = feature.rho.at(0);
        auto const& theta = feature.theta.at(0);
        return rho / std::sqrt(2.0 * M_PI * theta) *
               std::exp(-0.5 * std::pow(v0, 2.0) / theta);
      };
      info.domain.f.at("0-default").set_element(op);
    } else if constexpr (
      example == toki::timestepper::detail::VP_Example::IonAcousticWave) {
      auto op_electron =
        [&](std::array<toki::Number, 2> const& coord) -> toki::Number {
        auto const& r0 = coord.at(0);
        auto const& v0 = coord.at(1);
        auto const& a = feature.a.at(0);
        auto const& k = feature.k.at(0);
        auto const& m = feature.m.at(0);
        auto const& T = feature.T.at(0);
        return std::sqrt(m / (2.0 * M_PI * T)) *
               std::exp(-m / (2.0 * T) * std::pow(v0, 2.0)) *
               (1.0 + a * std::cos(k * r0));
      };
      auto op_ion =
        [&](std::array<toki::Number, 2> const& coord) -> toki::Number {
        auto const& r0 = coord.at(0);
        auto const& v0 = coord.at(1);
        auto const& a = feature.a.at(1);
        auto const& k = feature.k.at(1);
        auto const& m = feature.m.at(1);
        auto const& T = feature.T.at(1);
        return std::sqrt(m / (2.0 * M_PI * T)) *
               std::exp(-m / (2.0 * T) * std::pow(v0, 2.0)) *
               (1.0 + a * std::cos(k * r0));
      };
      info.domain.f.at("0-default").set_element(op_electron);
      info.domain.f.at("1-default").set_element(op_ion);
    } else if constexpr (
      example == toki::timestepper::detail::VP_Example::IonAcousticShockWave) {
      auto op_electron =
        [&](std::array<toki::Number, 2> const& coord) -> toki::Number {
        auto r0 = coord.at(0);
        auto v0 = coord.at(1);
        auto const& a = feature.a.at(0);
        auto const& k = feature.k.at(0);
        auto const& m = feature.m.at(0);
        auto const& T = feature.T.at(0);
        auto const& omega = feature.omega.at(0);
        r0 += 0.5 * option->param.domain.range;
        v0 += omega / k * (1.0 - a * std::sin(k * r0));
        return std::sqrt(m / (2.0 * M_PI * T)) *
               std::exp(-m / (2.0 * T) * std::pow(v0, 2.0)) *
               (1.0 + a * std::sin(k * r0));
      };
      auto op_ion =
        [&](std::array<toki::Number, 2> const& coord) -> toki::Number {
        auto r0 = coord.at(0);
        auto v0 = coord.at(1);
        auto const& a = feature.a.at(1);
        auto const& k = feature.k.at(1);
        auto const& m = feature.m.at(1);
        auto const& T = feature.T.at(1);
        auto const& omega = feature.omega.at(1);
        r0 += 0.5 * option->param.domain.range;
        v0 += omega / k * (1.0 - a * std::sin(k * r0));
        return std::sqrt(m / (2.0 * M_PI * T)) *
               std::exp(-m / (2.0 * T) * std::pow(v0, 2.0)) *
               (1.0 + a * std::sin(k * r0));
      };
      info.domain.f.at("0-default").set_element(op_electron);
      info.domain.f.at("1-default").set_element(op_ion);
    } else {
      toki::log::error("no implementation");
    }
    // E
    if constexpr (
      example == toki::timestepper::detail::VP_Example::ManufacturedSolution) {
      auto op = [&](std::array<toki::Number, 1> const& coord) -> toki::Number {
        auto const& r0 = coord.at(0);
        return -std::sqrt(M_PI) / 4.0 * std::sin(2.0 * r0);
      };
      info.domain.E.at("0").set_element(op);
    } else if constexpr (
      example == toki::timestepper::detail::VP_Example::LandauDamping ||
      example == toki::timestepper::detail::VP_Example::TwoStreamInstability ||
      example == toki::timestepper::detail::VP_Example::BumpOnTail) {
      auto op = [&](std::array<toki::Number, 1> const& coord) -> toki::Number {
        auto const& r0 = coord.at(0);
        auto const& a = feature.a.at(0);
        auto const& k = feature.k.at(0);
        return -(1.0 / k) * a * std::sin(k * r0);
      };
      info.domain.E.at("0").set_element(op);
    } else if constexpr (
      example == toki::timestepper::detail::VP_Example::PlasmaSheath) {
      auto op = [&](std::array<toki::Number, 1> const&) -> toki::Number {
        return 0.0;
      };
      info.domain.E.at("0").set_element(op);
    } else if constexpr (
      example == toki::timestepper::detail::VP_Example::IonAcousticWave) {
      auto op = [&](std::array<toki::Number, 1> const& coord) -> toki::Number {
        auto const& r0 = coord.at(0);
        auto const& a_electron = feature.a.at(0);
        auto const& a_ion = feature.a.at(1);
        auto const& k_electron = feature.k.at(0);
        auto const& k_ion = feature.k.at(1);
        return (-1.0 / k_electron) * a_electron * std::sin(k_electron * r0) +
               (+1.0 / k_ion) * a_ion * std::sin(k_ion * r0);
      };
      info.domain.E.at("0").set_element(op);
    } else if constexpr (
      example == toki::timestepper::detail::VP_Example::IonAcousticShockWave) {
      auto op = [&](std::array<toki::Number, 1> const& coord) -> toki::Number {
        auto r0 = coord.at(0);
        auto const& a_electron = feature.a.at(0);
        auto const& a_ion = feature.a.at(1);
        auto const& k_electron = feature.k.at(0);
        auto const& k_ion = feature.k.at(1);
        r0 += 0.5 * option->param.domain.range;
        return -(
          (-1.0 / k_electron) * a_electron * std::cos(k_electron * r0) +
          (+1.0 / k_ion) * a_ion * std::cos(k_ion * r0));
      };
      info.domain.E.at("0").set_element(op);
    } else {
      toki::log::error("no implementation");
    }
    return *this;
  }
  auto
  update_velocity() -> VP_p1D1V&
  {
    // cache E
    do {
      auto const& order_min = info.param.method.order_min;
      auto const& order_max = info.param.method.order_max;
      auto& f = info.domain.f.at("0-default");
      auto const& resolution_r0 = f.info.local.resolution.at(0);
#pragma omp parallel default(none)                                             \
  shared(order_min, order_max, f, resolution_r0)
      {
        toki::Size offset{};
#pragma omp for schedule(static, 1)
        for (offset = 0; offset < resolution_r0; ++offset) {
          auto const index =
            toki::math::index::ind2sub<2>(f.info.local.resolution, offset);
          for (toki::Size dof = order_min; dof <= order_max + 1; ++dof) {
            toki::basis::Nodal basis(dof);
            std::array<toki::Size, 3> size_sample{dof, dof, dof};
            for (toki::Size point = 0; point < dof * dof; ++point) {
              auto const index_point =
                toki::math::index::ind2sub<3>(size_sample, point);
              auto const local_time = basis.info.abscissa.at(index_point[0]);
              auto const time_delta =
                (local_time + 1.0) / 2.0 * info.param.time.delta;
              auto const local_r0 = basis.info.abscissa.at(index_point[1]);
              find_E(0, time_delta, index[0], local_r0, dof, false);
            }
          }
        }
      }
    } while (0);
    for (toki::Size species = 0; species < info.param.model.species;
         ++species) {
      auto& f = info.domain.f.at(fmt::format("{}-default", species));
      auto const& volume = f.info.local.element.volume;
      auto const& anchor = f.info.local.anchor;
      auto const& q = option->param.feature.q.at(species);
      auto const& m = option->param.feature.m.at(species);
      std::vector<toki::vector::Vec> velocity_r0(f.info.local.element.N);
      std::vector<toki::vector::Vec> velocity_v0(f.info.local.element.N);
#pragma omp parallel default(none)                                             \
  shared(f, volume, anchor, q, m, velocity_r0, velocity_v0)
      {
        toki::Size offset{};
        // r0
#pragma omp for schedule(runtime)
        for (offset = 0; offset < f.info.local.element.N; ++offset) {
          auto const index =
            toki::math::index::ind2sub<2>(f.info.local.resolution, offset);
          auto const dof = f.get_basis(index).info.point + 1;
          auto const dof_P = dof * dof * dof;
          auto& quadrature = f.get_quadrature(index);
          toki::utility::confirm(
            dof == quadrature.info.point, "invalid velocity_r0");
          std::array<toki::Size, 3> size_sample{dof, dof, dof};
          velocity_r0.at(offset) = toki::vector::Vec(dof_P);
          for (toki::Size point = 0; point < dof_P; ++point) {
            auto const index_point =
              toki::math::index::ind2sub<3>(size_sample, point);
            auto const local_v0 = quadrature.info.abscissa.at(index_point[2]);
            auto const v0 = anchor[1] + (static_cast<toki::Number>(index[1]) +
                                         (local_v0 + 1.0) / 2.0) *
                                          volume[1];
            velocity_r0.at(offset).set(v0, point);
          }
        }
#pragma omp barrier
        // v0
#pragma omp for schedule(runtime)
        for (offset = 0; offset < f.info.local.element.N; ++offset) {
          auto const index =
            toki::math::index::ind2sub<2>(f.info.local.resolution, offset);
          auto const dof = f.get_basis(index).info.point + 1;
          auto const dof_P = dof * dof * dof;
          auto& quadrature = f.get_quadrature(index);
          toki::utility::confirm(
            dof == quadrature.info.point, "invalid velocity_v0");
          std::array<toki::Size, 3> size_sample{dof, dof, dof};
          velocity_v0.at(offset) = toki::vector::Vec(dof_P);
          for (toki::Size point = 0; point < dof_P; ++point) {
            auto const index_point =
              toki::math::index::ind2sub<3>(size_sample, point);
            auto const local_time = quadrature.info.abscissa.at(index_point[0]);
            auto const time_delta =
              (local_time + 1.0) / 2.0 * info.param.time.delta;
            auto const local_r0 = quadrature.info.abscissa.at(index_point[1]);
            velocity_v0.at(offset).set(
              q / m * find_E(0, time_delta, index[0], local_r0, dof, true),
              point);
          }
        }
      }
      toki::utility::confirm(
        velocity_r0.size() == f.info.local.element.N, "invalid velocity_r0");
      toki::utility::confirm(
        velocity_v0.size() == f.info.local.element.N, "invalid velocity_v0");
      info.domain.flux_Jacobian.insert_or_assign(
        fmt::format("{}-r0", species), velocity_r0);
      info.domain.flux_Jacobian.insert_or_assign(
        fmt::format("{}-v0", species), velocity_v0);
    }
    return *this;
  }
  auto
  update_E() -> VP_p1D1V&
  {
    auto& E0 = info.domain.E.at("0");
    auto const& resolution = E0.info.local.resolution;
    auto const& dof = E0.info.quadrature_point;
#pragma omp parallel default(none) shared(E0, resolution, dof)
    {
      toki::vector::Vec buffer(dof);
      toki::Size index_r0{};
#pragma omp for schedule(static, 1)
      for (index_r0 = 0; index_r0 < resolution[0]; ++index_r0) {
        auto op =
          [&](std::array<toki::Number, 1> const& coord) -> toki::Number {
          return find_E(
            0,
            info.param.time.delta,
            index_r0,
            coord.at(0),
            info.param.method.order_max,
            false);
        };
        E0.info.basis.fill<1>(buffer, 0, op);
        E0.set_element(buffer, 0, {index_r0});
      }
    }
    return *this;
  }
  auto
  find_h() -> toki::Number
  {
    auto result = std::min(
      info.domain.f.at("0-default").info.local.element.volume.at(0),
      info.domain.f.at("0-default").info.local.element.volume.at(1));
    for (toki::Size species = 1; species < info.param.model.species;
         ++species) {
      result = std::min({
        result,
        info.domain.f.at(fmt::format("{}-default", species))
          .info.local.element.volume.at(0),
        info.domain.f.at(fmt::format("{}-default", species))
          .info.local.element.volume.at(1),
      });
    }
    return result;
  }
  auto
  find_signal_speed() -> toki::Number
  {
    if constexpr (
      example == toki::timestepper::detail::VP_Example::ManufacturedSolution) {
      return 150.0;
    } else if constexpr (
      example == toki::timestepper::detail::VP_Example::LandauDamping ||
      example == toki::timestepper::detail::VP_Example::TwoStreamInstability ||
      example == toki::timestepper::detail::VP_Example::BumpOnTail ||
      example == toki::timestepper::detail::VP_Example::PlasmaSheath) {
      return std::fabs(info.domain.f.at("0-default").info.anchor.at(1));
    } else {
      toki::log::error("no implementation");
    }
    return 0.0;
  }
  auto
  find_E(
    toki::Size const&,
    toki::Number const& time_delta,
    toki::Size const& index_r0,
    toki::Number const& local_r0,
    toki::Size const& order,
    bool const use_cache) -> toki::Number
  {
    if (use_cache) {
      toki::Number result{};
      info.cache.E.visit(
        typename decltype(info.cache.E)::key_type{
          time_delta, index_r0, local_r0, order},
        [&](auto const& entry) {
        result = entry.second;
      });
      return result;
    }
    toki::Number rho = 0.0;
    toki::Number E0_t0 = 0.0;
    toki::Number E0_t1 = 0.0;
    toki::Number E0_t2 = 0.0;
    toki::Number E0_t3 = 0.0;
    do {
      auto& E0 = info.domain.E.at("0");
      auto const& dof = E0.info.quadrature_point;
      toki::vector::Vec sample(dof);
      E0.get_element(sample, 0, {index_r0});
      E0_t0 = E0.info.basis.approx<1>({local_r0}, sample, 0);
    } while (0);
    for (toki::Size species = 0; species < info.param.model.species;
         ++species) {
      auto const name = fmt::format("{}-default", species);
      auto& f = info.domain.f.at(name);
      auto const& resolution = f.info.local.resolution;
      auto const& halo = f.info.halo;
      auto const& volume = f.info.local.element.volume;
      struct
      {
        toki::Number f = 0.0;
      } approx;
      for (toki::Size index_v0 = halo[1]; index_v0 < resolution[1] - halo[1];
           ++index_v0) {
        auto const& basis = f.get_basis({index_r0, index_v0});
        auto const& dof = basis.info.point;
        toki::vector::Vec sample(dof * dof);
        f.get_element(sample, 0, {index_r0, index_v0});
        auto op_f =
          [&](std::array<toki::Number, 1> const& coord) -> toki::Number {
          auto const& local_v0 = coord.at(0);
          return basis.approx<2>({local_r0, local_v0}, sample, 0);
        };
        auto const& quadrature = f.get_quadrature({index_r0, index_v0});
        approx.f += quadrature.sum<1>(op_f) * volume[1] / 2.0;
      }
      auto const& q = option->param.feature.q.at(species);
      rho += q * approx.f;
    }
    if (info.param.model.species == 1) {
      rho += option->param.feature.rho.at(0);
    }
    for (toki::Size species = 0; species < info.param.model.species;
         ++species) {
      auto const name = fmt::format("{}-default", species);
      auto& f = info.domain.f.at(name);
      auto const& resolution = f.info.local.resolution;
      auto const& halo = f.info.halo;
      auto const& anchor = f.info.local.anchor;
      auto const& volume = f.info.local.element.volume;
      struct
      {
        toki::Number v_f = 0.0;
      } approx;
      for (toki::Size index_v0 = halo[1]; index_v0 < resolution[1] - halo[1];
           ++index_v0) {
        auto const& basis = f.get_basis({index_r0, index_v0});
        auto const& dof = basis.info.point;
        toki::vector::Vec sample(dof * dof);
        f.get_element(sample, 0, {index_r0, index_v0});
        auto op_v_f =
          [&](std::array<toki::Number, 1> const& coord) -> toki::Number {
          auto const& local_v0 = coord.at(0);
          auto const v0 = anchor[1] + (static_cast<toki::Number>(index_v0) +
                                       (local_v0 + 1.0) / 2.0) *
                                        volume[1];
          return v0 * basis.approx<2>({local_r0, local_v0}, sample, 0);
        };
        auto const& quadrature = f.get_quadrature({index_r0, index_v0});
        approx.v_f += quadrature.sum<1>(op_v_f) * volume[1] / 2.0;
      }
      auto const& q = option->param.feature.q.at(species);
      E0_t1 += -q * approx.v_f;
    }
    if (info.flag.background.current) {
      E0_t1 += option->param.feature.J.at(0);
    }
    for (toki::Size species = 0; species < info.param.model.species;
         ++species) {
      auto const name = fmt::format("{}-default", species);
      auto& f = info.domain.f.at(name);
      auto const& resolution = f.info.local.resolution;
      auto const& halo = f.info.halo;
      auto const& anchor = f.info.local.anchor;
      auto const& volume = f.info.local.element.volume;
      struct
      {
        toki::Number f = 0.0;
        toki::Number v_f = 0.0;
        toki::Number v_fr = 0.0;
        toki::Number v2_fr = 0.0;
        toki::Number v3_fr2 = 0.0;
      } approx;
      for (toki::Size index_v0 = halo[1]; index_v0 < resolution[1] - halo[1];
           ++index_v0) {
        auto const& basis = f.get_basis({index_r0, index_v0});
        auto const& dof = basis.info.point;
        toki::vector::Vec sample(dof * dof);
        f.get_element(sample, 0, {index_r0, index_v0});
        auto op_f =
          [&](std::array<toki::Number, 1> const& coord) -> toki::Number {
          auto const& local_v0 = coord.at(0);
          return basis.approx<2>({local_r0, local_v0}, sample, 0);
        };
        auto op_v_f =
          [&](std::array<toki::Number, 1> const& coord) -> toki::Number {
          auto const& local_v0 = coord.at(0);
          auto const v0 = anchor[1] + (static_cast<toki::Number>(index_v0) +
                                       (local_v0 + 1.0) / 2.0) *
                                        volume[1];
          return v0 * basis.approx<2>({local_r0, local_v0}, sample, 0);
        };
        auto op_v_fr =
          [&](std::array<toki::Number, 1> const& coord) -> toki::Number {
          auto const& local_v0 = coord.at(0);
          auto const v0 = anchor[1] + (static_cast<toki::Number>(index_v0) +
                                       (local_v0 + 1.0) / 2.0) *
                                        volume[1];
          return v0 * basis.approx<2>({local_r0, local_v0}, sample, 0, 0, 1);
        };
        auto op_v2_fr =
          [&](std::array<toki::Number, 1> const& coord) -> toki::Number {
          auto const& local_v0 = coord.at(0);
          auto const v0 = anchor[1] + (static_cast<toki::Number>(index_v0) +
                                       (local_v0 + 1.0) / 2.0) *
                                        volume[1];
          return std::pow(v0, 2.0) *
                 basis.approx<2>({local_r0, local_v0}, sample, 0, 0, 1);
        };
        auto op_v3_fr2 =
          [&](std::array<toki::Number, 1> const& coord) -> toki::Number {
          auto const& local_v0 = coord.at(0);
          auto const v0 = anchor[1] + (static_cast<toki::Number>(index_v0) +
                                       (local_v0 + 1.0) / 2.0) *
                                        volume[1];
          return std::pow(v0, 3.0) *
                 basis.approx<2>({local_r0, local_v0}, sample, 0, 0, 2);
        };
        auto const& quadrature = f.get_quadrature({index_r0, index_v0});
        approx.f += quadrature.sum<1>(op_f) * volume[1] / 2.0;
        approx.v_f += quadrature.sum<1>(op_v_f) * volume[1] / 2.0;
        approx.v_fr += quadrature.sum<1>(op_v_fr) * volume[1] / 2.0;
        approx.v2_fr += quadrature.sum<1>(op_v2_fr) * volume[1] / 2.0;
        approx.v3_fr2 += quadrature.sum<1>(op_v3_fr2) * volume[1] / 2.0;
      }
      auto const& q = option->param.feature.q.at(species);
      auto const& m = option->param.feature.m.at(species);
      E0_t2 += q * approx.v2_fr - q * q / m * E0_t0 * approx.f;
      E0_t3 += -q * approx.v3_fr2 + 2.0 * q * q / m * rho * approx.v_f +
               3.0 * q * q / m * E0_t0 * approx.v_fr -
               q * q / m * E0_t1 * approx.f;
    }
    if constexpr (
      example == toki::timestepper::detail::VP_Example::ManufacturedSolution) {
      auto& f = info.domain.f.at("0-default");
      auto const& anchor = f.info.local.anchor;
      auto const& volume = f.info.local.element.volume;
      auto const r0 = anchor.at(0) + (static_cast<toki::Number>(index_r0) +
                                      (local_r0 + 1.0) / 2.0) *
                                       volume.at(0);
      auto const time = info.param.time.now + time_delta;
      auto const s = 2.0 * r0 - 2.0 * M_PI * time;
      auto const n = 2.0 / M_2_SQRTPI;
      auto const C1 = +n / 4.0 + n / 8.0 * (4 * M_PI - 1.0) * std::cos(s);
      auto const C2 =
        (3.0 * n + 4.0 * M_PI - 16.0 * std::pow(n, 5.0)) / 16.0 * std::sin(-s) +
        M_PI / 16.0 * std::sin(2.0 * s);
      auto const C3 =
        -M_PI / 4.0 +
        (7.0 * n + 16.0 * M_PI - 64.0 * std::pow(n, 7)) / 32.0 * std::cos(s) -
        3.0 * M_PI / 16.0 * std::cos(2.0 * s);
      E0_t1 += C1;
      E0_t2 += C2;
      E0_t3 += C3;
    }
    auto const E0 =
      E0_t0 +
      (info.param.method.order_max >= 1 ? 1.0 : 0.0) * time_delta * E0_t1 +
      (info.param.method.order_max >= 2 ? 1.0 : 0.0) *
        std::pow(time_delta, 2.0) / 2.0 * E0_t2 +
      (info.param.method.order_max >= 3 ? 1.0 : 0.0) *
        std::pow(time_delta, 3.0) / 6.0 * E0_t3;
    info.cache.E.insert_or_assign({time_delta, index_r0, local_r0, order}, E0);
    return E0;
  }
  auto
  find_source(
    toki::Size const&,
    toki::Number const& time,
    std::array<toki::Number, 2> const& coord) -> toki::Number
  {
    if (!info.flag.background.plasma_source) {
      return 0.0;
    }
    if constexpr (
      example == toki::timestepper::detail::VP_Example::ManufacturedSolution) {
      auto const& r0 = coord.at(0);
      auto const& v0 = coord.at(1);
      auto const s = 2.0 * r0 - 2.0 * M_PI * time;
      return 0.5 * std::sin(s) *
             std::exp(-1.0 / 4.0 * std::pow(4.0 * v0 - 1.0, 2.0)) *
             ((2.0 * std::sqrt(M_PI) + 1.0) *
                (4.0 * v0 - 2.0 * std::sqrt(M_PI)) -
              std::sqrt(M_PI) * (4.0 * v0 - 1.0) * std::cos(s));
    } else {
      toki::log::error("no implementation");
    }
    return 0.0;
  }
  auto
  solve_prediction() -> VP_p1D1V&
  {
    // P0
    for (toki::Size species = 0; species < info.param.model.species;
         ++species) {
      auto& f = info.domain.f.at(fmt::format("{}-default", species));
      auto const& resolution = f.info.local.resolution;
#pragma omp parallel default(none) shared(species, f, resolution)
      {
        toki::Size index_r0{};
        toki::Size index_v0{};
#pragma omp for schedule(runtime) collapse(2)
        for (index_v0 = 0; index_v0 < resolution[1]; ++index_v0) {
          for (index_r0 = 0; index_r0 < resolution[0]; ++index_r0) {
            auto const dof = f.get_basis({index_r0, index_v0}).info.point;
            auto const dof_C = dof * dof;
            auto const dof_P = dof_C * dof;
            toki::vector::Vec Q(dof_C);
            toki::vector::Vec W(1 * dof_P);
            toki::vector::Vec RHS(dof_P);
            f.get_element(Q, 0, {index_r0, index_v0});
            if (
              option->param.flag.performance.skip_zero_element &&
              Q.get_norm() < toki::epsilon<toki::Number>) {
              continue;
            }
            // component
            auto P0_L = build_P0_L(species, {index_r0, index_v0});
            toki::math::permutation::Permutation P0_L_p(dof_P);
            toki::math::seq::getf(P0_L, P0_L_p);
            auto const P0_R = build_P0_R(species, {index_r0, index_v0});
            // solve
            toki::math::seq::gemv(
              toki::one<toki::Number>, toki::zero<toki::Number>, Q, RHS, P0_R);
            apply_source(species, {index_r0, index_v0}, RHS, 0);
            toki::math::seq::gets(P0_L, P0_L_p, RHS);
            // update
            info.domain.f.at(fmt::format("{}-result-P0", species))
              .set_element(RHS, 0, {index_r0, index_v0});
          }
        }
      }
    }
    // P1
    for (toki::Size species = 0; species < info.param.model.species;
         ++species) {
      auto& f = info.domain.f.at(fmt::format("{}-default", species));
      auto& f_P0 = info.domain.f.at(fmt::format("{}-result-P0", species));
      auto& f_P1_0 = info.domain.f.at(fmt::format("{}-result-P1-0", species));
      auto& f_P1_1 = info.domain.f.at(fmt::format("{}-result-P1-1", species));
      auto const& resolution = f.info.local.resolution;
      // P1-0
#pragma omp parallel default(none) shared(species, f, f_P0, f_P1_0, resolution)
      {
        toki::Size index_r0{};
        toki::Size index_v0{};
#pragma omp for schedule(runtime) collapse(2)
        for (index_v0 = 0; index_v0 < resolution[1]; ++index_v0) {
          for (index_r0 = 1; index_r0 < resolution[0] - 1; ++index_r0) {
            auto const dof = f_P1_0.get_basis({index_r0, index_v0}).info.point;
            auto const dof_max = std::max({
              f_P1_0.get_basis({index_r0, index_v0}).info.point,
              f_P0.get_basis({index_r0 - 1, index_v0}).info.point,
              f_P0.get_basis({index_r0 + 1, index_v0}).info.point,
            });
            auto const dof_C = dof * dof;
            auto const dof_C_max = dof_max * dof_max;
            auto const dof_P = dof_C * dof;
            auto const dof_P_max = dof_C_max * dof_max;
            toki::vector::Vec Q(dof_C);
            toki::vector::Vec W(2 * dof_P_max);
            toki::vector::Vec RHS(dof_P);
            f.get_element(Q, 0, {index_r0, index_v0});
            f_P0.get_element(W, 0 * dof_P_max, {index_r0 - 1, index_v0})
              .get_element(W, 1 * dof_P_max, {index_r0 + 1, index_v0});
            if (
              option->param.flag.performance.skip_zero_element &&
              W.get_norm() < toki::epsilon<toki::Number>) {
              continue;
            }
            auto P1_L = build_P1_L(species, {index_r0, index_v0}, 0);
            toki::math::permutation::Permutation P1_L_p(dof_P);
            toki::math::seq::getf(P1_L, P1_L_p);
            auto const P1_R = build_P1_R(species, {index_r0, index_v0}, 0);
            toki::math::seq::gemv(
              toki::one<toki::Number>,
              toki::zero<toki::Number>,
              Q,
              RHS,
              P1_R[0]);
            for (toki::Size tag = 0; tag < 2; ++tag) {
              toki::math::seq::gemv(
                dof_P,
                P1_R[tag + 1].extent[1],
                toki::one<toki::Number>,
                toki::one<toki::Number>,
                W,
                tag * dof_P_max,
                1,
                RHS,
                0,
                1,
                P1_R[tag + 1],
                0,
                1,
                0,
                1);
            }
            apply_source(species, {index_r0, index_v0}, RHS, 0);
            if (
              option->param.flag.performance.skip_zero_element &&
              RHS.get_norm() < toki::epsilon<toki::Number>) {
              continue;
            }
            toki::math::seq::gets(P1_L, P1_L_p, RHS);
            f_P1_0.set_element(RHS, 0, {index_r0, index_v0});
          }
        }
      }
      // P1-1
#pragma omp parallel default(none) shared(species, f, f_P0, f_P1_1, resolution)
      {
        toki::Size index_r0{};
        toki::Size index_v0{};
#pragma omp for schedule(runtime) collapse(2)
        for (index_v0 = 1; index_v0 < resolution[1] - 1; ++index_v0) {
          for (index_r0 = 0; index_r0 < resolution[0]; ++index_r0) {
            auto const dof = f_P1_1.get_basis({index_r0, index_v0}).info.point;
            auto const dof_max = std::max({
              f_P1_1.get_basis({index_r0, index_v0}).info.point,
              f_P0.get_basis({index_r0, index_v0 - 1}).info.point,
              f_P0.get_basis({index_r0, index_v0 + 1}).info.point,
            });
            auto const dof_C = dof * dof;
            auto const dof_C_max = dof_max * dof_max;
            auto const dof_P = dof_C * dof;
            auto const dof_P_max = dof_C_max * dof_max;
            toki::vector::Vec Q(dof_C);
            toki::vector::Vec W(2 * dof_P_max);
            toki::vector::Vec RHS(dof_P);
            f.get_element(Q, 0, {index_r0, index_v0});
            f_P0.get_element(W, 0 * dof_P_max, {index_r0, index_v0 - 1})
              .get_element(W, 1 * dof_P_max, {index_r0, index_v0 + 1});
            if (
              option->param.flag.performance.skip_zero_element &&
              W.get_norm() < toki::epsilon<toki::Number>) {
              continue;
            }
            auto P1_L = build_P1_L(species, {index_r0, index_v0}, 1);
            toki::math::permutation::Permutation P1_L_p(dof_P);
            toki::math::seq::getf(P1_L, P1_L_p);
            auto const P1_R = build_P1_R(species, {index_r0, index_v0}, 1);
            toki::math::seq::gemv(
              toki::one<toki::Number>,
              toki::zero<toki::Number>,
              Q,
              RHS,
              P1_R[0]);
            for (toki::Size tag = 0; tag < 2; ++tag) {
              toki::math::seq::gemv(
                dof_P,
                P1_R[tag + 1].extent[1],
                toki::one<toki::Number>,
                toki::one<toki::Number>,
                W,
                tag * dof_P_max,
                1,
                RHS,
                0,
                1,
                P1_R[tag + 1],
                0,
                1,
                0,
                1);
            }
            apply_source(species, {index_r0, index_v0}, RHS, 0);
            if (
              option->param.flag.performance.skip_zero_element &&
              RHS.get_norm() < toki::epsilon<toki::Number>) {
              continue;
            }
            toki::math::seq::gets(P1_L, P1_L_p, RHS);
            f_P1_1.set_element(RHS, 0, {index_r0, index_v0});
          }
        }
      }
    }
    // P2
    for (toki::Size species = 0; species < info.param.model.species;
         ++species) {
      auto& f = info.domain.f.at(fmt::format("{}-default", species));
      auto& f_P1_0 = info.domain.f.at(fmt::format("{}-result-P1-0", species));
      auto& f_P1_1 = info.domain.f.at(fmt::format("{}-result-P1-1", species));
      auto& f_P2 = info.domain.f.at(fmt::format("{}-result-P2", species));
      auto const& resolution = f_P2.info.local.resolution;
#pragma omp parallel default(none)                                             \
  shared(species, f, f_P1_0, f_P1_1, f_P2, resolution)
      {
        toki::Size index_r0{};
        toki::Size index_v0{};
#pragma omp for schedule(runtime) collapse(2)
        for (index_v0 = 1; index_v0 < resolution[1] - 1; ++index_v0) {
          for (index_r0 = 1; index_r0 < resolution[0] - 1; ++index_r0) {
            auto const dof = f_P2.get_basis({index_r0, index_v0}).info.point;
            auto const dof_max = std::max({
              f_P2.get_basis({index_r0, index_v0}).info.point,
              f_P1_1.get_basis({index_r0 - 1, index_v0}).info.point,
              f_P1_1.get_basis({index_r0 + 1, index_v0}).info.point,
              f_P1_0.get_basis({index_r0, index_v0 - 1}).info.point,
              f_P1_0.get_basis({index_r0, index_v0 + 1}).info.point,
            });
            auto const dof_C = dof * dof;
            auto const dof_C_max = dof_max * dof_max;
            auto const dof_P = dof_C * dof;
            auto const dof_P_max = dof_C_max * dof_max;
            toki::vector::Vec Q(dof_C);
            toki::vector::Vec W(4 * dof_P_max);
            toki::vector::Vec RHS(dof_P);
            f.get_element(Q, 0, {index_r0, index_v0});
            f_P1_1.get_element(W, 0 * dof_P_max, {index_r0 - 1, index_v0})
              .get_element(W, 1 * dof_P_max, {index_r0 + 1, index_v0});
            f_P1_0.get_element(W, 2 * dof_P_max, {index_r0, index_v0 - 1})
              .get_element(W, 3 * dof_P_max, {index_r0, index_v0 + 1});
            if (
              option->param.flag.performance.skip_zero_element &&
              W.get_norm() < toki::epsilon<toki::Number>) {
              continue;
            }
            auto P2_L = build_P2_L(species, {index_r0, index_v0});
            toki::math::permutation::Permutation P2_L_p(dof_P);
            toki::math::seq::getf(P2_L, P2_L_p);
            auto const P2_R = build_P2_R(species, {index_r0, index_v0});
            toki::math::seq::gemv(
              toki::one<toki::Number>,
              toki::zero<toki::Number>,
              Q,
              RHS,
              P2_R[0]);
            for (toki::Size tag = 0; tag < 4; ++tag) {
              toki::math::seq::gemv(
                dof_P,
                P2_R[tag + 1].extent[1],
                toki::one<toki::Number>,
                toki::one<toki::Number>,
                W,
                tag * dof_P_max,
                1,
                RHS,
                0,
                1,
                P2_R[tag + 1],
                0,
                1,
                0,
                1);
            }
            apply_source(species, {index_r0, index_v0}, RHS, 0);
            if (
              option->param.flag.performance.skip_zero_element &&
              RHS.get_norm() < toki::epsilon<toki::Number>) {
              continue;
            }
            toki::math::seq::gets(P2_L, P2_L_p, RHS);
            f_P2.set_element(RHS, 0, {index_r0, index_v0});
          }
        }
      }
    }
    return *this;
  }
  auto
  solve_correction() -> VP_p1D1V&
  {
    for (toki::Size species = 0; species < info.param.model.species;
         ++species) {
      auto& f = info.domain.f.at(fmt::format("{}-default", species));
      auto& f_P2 = info.domain.f.at(fmt::format("{}-result-P2", species));
      auto const& resolution = f.info.local.resolution;
      auto const& halo = f.info.halo;
      // C0
#pragma omp parallel default(none) shared(species, f, f_P2, resolution, halo)
      {
        toki::Size index_r0{};
        toki::Size index_v0{};
#pragma omp for schedule(runtime) collapse(2)
        for (index_v0 = halo[1]; index_v0 < resolution[1] - halo[1];
             ++index_v0) {
          for (index_r0 = halo[0]; index_r0 < resolution[0] - halo[0];
               ++index_r0) {
            auto const dof = f.get_basis({index_r0, index_v0}).info.point;
            auto const dof_max = std::max({
              f.get_basis({index_r0, index_v0}).info.point,
              f.get_basis({index_r0 - 1, index_v0}).info.point,
              f.get_basis({index_r0 + 1, index_v0}).info.point,
              f.get_basis({index_r0, index_v0 - 1}).info.point,
              f.get_basis({index_r0, index_v0 + 1}).info.point,
            });
            auto const dof_C = dof * dof;
            auto const dof_C_max = dof_max * dof_max;
            auto const dof_P_max = dof_C_max * dof_max;
            toki::vector::Vec W(5 * dof_P_max);
            toki::vector::Vec RHS(dof_C);
            f_P2.get_element(W, 0 * dof_P_max, {index_r0, index_v0})
              .get_element(W, 1 * dof_P_max, {index_r0 - 1, index_v0})
              .get_element(W, 2 * dof_P_max, {index_r0 + 1, index_v0})
              .get_element(W, 3 * dof_P_max, {index_r0, index_v0 - 1})
              .get_element(W, 4 * dof_P_max, {index_r0, index_v0 + 1});
            if (
              option->param.flag.performance.skip_zero_element &&
              W.get_norm() < toki::epsilon<toki::Number>) {
              continue;
            }
            auto C0_L = build_C0_L(species, {index_r0, index_v0});
            toki::math::permutation::Permutation C0_L_p(dof_C);
            toki::math::seq::getf(C0_L, C0_L_p);
            auto const C0_R = build_C0_R(species, {index_r0, index_v0});
            toki::math::seq::gemv(
              toki::one<toki::Number>,
              toki::zero<toki::Number>,
              W,
              RHS,
              C0_R[0]);
            for (toki::Size tag = 0; tag < 4; ++tag) {
              toki::math::seq::gemv(
                dof_C,
                C0_R[tag + 1].extent[1],
                toki::one<toki::Number>,
                toki::one<toki::Number>,
                W,
                (tag + 1) * dof_P_max,
                1,
                RHS,
                0,
                1,
                C0_R[tag + 1],
                0,
                1,
                0,
                1);
            }
            apply_source(species, {index_r0, index_v0}, RHS, 0);
            if (
              option->param.flag.performance.skip_zero_element &&
              RHS.get_norm() < toki::epsilon<toki::Number>) {
              continue;
            }
            toki::math::seq::gets(C0_L, C0_L_p, RHS);
            f.adjust_element(RHS, 0, {index_r0, index_v0});
          }
        }
      }
    }
    // projection
    for (toki::Size species = 0; species < info.param.model.species;
         ++species) {
      auto& f = info.domain.f.at(fmt::format("{}-default", species));
      auto const& resolution = f.info.local.resolution;
      auto const& halo = f.info.halo;
#pragma omp parallel default(none) shared(species, f, resolution, halo)
      {
        toki::Size index_r0{};
        toki::Size index_v0{};
#pragma omp for schedule(runtime) collapse(2)
        for (index_v0 = halo[1]; index_v0 < resolution[1] - halo[1];
             ++index_v0) {
          for (index_r0 = halo[0]; index_r0 < resolution[0] - halo[0];
               ++index_r0) {
            auto const dof = f.get_basis({index_r0, index_v0}).info.point;
            toki::quadrature::Lobatto quadrature(dof);
            auto const dof_C = dof * dof;
            toki::vector::Vec Q(dof_C);
            f.get_element(Q, 0, {index_r0, index_v0});
            if (
              option->param.flag.performance.skip_zero_element &&
              Q.get_norm() < toki::epsilon<toki::Number>) {
              continue;
            }
            auto const Q_average = std::max(
              quadrature.sum<2>(Q, 0) / 4.0,
              std::numeric_limits<toki::Number>::epsilon());
            if (Q.get_min() < 0.0) {
              int const k_max = 10;
              auto Q_hat = Q;
              auto U = Q;
              for (toki::Size point = 0; point < Q.extent.at(0); ++point) {
                Q_hat.set(Q_average, point);
                U.set(
                  (Q_average - Q.at(point)) / static_cast<toki::Number>(k_max),
                  point);
              }
              bool search_stop = false;
              for (int k = 0; (!search_stop) && (k < k_max); ++k) {
                for (toki::Size point = 0; point < Q.extent.at(0); ++point) {
                  Q.set(Q.at(point) + U.at(point), point);
                }
                bool search_continue = false;
                for (toki::Size point = 0; point < Q.extent.at(0); ++point) {
                  if (Q.at(point) < 0.0) {
                    search_continue = true;
                    break;
                  }
                }
                search_stop = !search_continue;
              }
              f.set_element(Q, 0, {index_r0, index_v0});
            }
          }
        }
      }
    }
    return *this;
  }
  auto
  get_flux(
    toki::Size const& species,
    toki::Size const& direction,
    toki::Number const& input,
    std::array<toki::Size, 2> const& index,
    toki::Size const& point) -> toki::Number
  {
    auto& f = info.domain.f.at(fmt::format("{}-default", species));
    if (direction == 0) {
      auto const& velocity =
        info.domain.flux_Jacobian.at(fmt::format("{}-r0", species))
          .at(toki::math::index::sub2ind<2>(f.info.local.resolution, index))
          .at(point);
      return velocity * input;
    } else if (direction == 1) {
      auto const& velocity =
        info.domain.flux_Jacobian.at(fmt::format("{}-v0", species))
          .at(toki::math::index::sub2ind<2>(f.info.local.resolution, index))
          .at(point);
      return velocity * input;
    } else {
      toki::log::error("no implementation");
    }
    return 0.0;
  }
  auto
  get_flux_upwind(
    bool const upwind,
    toki::Size const& species,
    toki::Size const& direction,
    toki::Number const& input,
    std::array<toki::Size, 2> const& index,
    toki::Size const& point) -> toki::Number
  {
    auto& f = info.domain.f.at(fmt::format("{}-default", species));
    if (direction == 0) {
      auto const& velocity =
        info.domain.flux_Jacobian.at(fmt::format("{}-r0", species))
          .at(toki::math::index::sub2ind<2>(f.info.local.resolution, index))
          .at(point);
      if (upwind && (velocity > 0.0)) {
        return velocity * input;
      } else if ((!upwind) && (velocity < 0.0)) {
        return velocity * input;
      } else {
        return 0.0;
      }
    } else if (direction == 1) {
      auto const& velocity =
        info.domain.flux_Jacobian.at(fmt::format("{}-v0", species))
          .at(toki::math::index::sub2ind<2>(f.info.local.resolution, index))
          .at(point);
      if (upwind && (velocity > 0.0)) {
        return velocity * input;
      } else if ((!upwind) && (velocity < 0.0)) {
        return velocity * input;
      } else {
        return 0.0;
      }
    } else {
      toki::log::error("no implementation");
    }
    return 0.0;
  }
  auto
  get_dof_total() -> toki::Size
  {
    toki::Size result = 0;
    for (toki::Size species = 0; species < info.param.model.species;
         ++species) {
      auto const& refinement =
        info.domain.f.at(fmt::format("{}-default", species))
          .info.local.refinement.current;
      result += std::transform_reduce(
        refinement.begin(),
        refinement.end(),
        0,
        std::plus<toki::Size>{},
        [](toki::Size const& v) -> toki::Size {
        return v * v;
      });
    }
    return result * option->param.mpi.extent.at(0);
  }
  auto
  apply_source(
    toki::Size const& species,
    std::array<toki::Size, 2> const& index,
    toki::vector::Vec& buffer,
    toki::Size const& offset) -> VP_p1D1V&
  {
    if (!info.flag.background.plasma_source) {
      return *this;
    }
    auto& f = info.domain.f.at(fmt::format("{}-default", species));
    auto const& basis = f.get_basis(index);
    auto const& quadrature = f.get_quadrature(index);
    auto const& point_max = basis.info.point;
    auto const& anchor = f.info.local.anchor;
    auto const& volume = f.info.local.element.volume;
    for (toki::Size point_v0 = 0; point_v0 < point_max; ++point_v0) {
      for (toki::Size point_r0 = 0; point_r0 < point_max; ++point_r0) {
        if (buffer.extent.at(0) == point_max * point_max) {
          auto op =
            [&](std::array<toki::Number, 3> const& coord) -> toki::Number {
            auto const value_r0 = basis.eval<1>({coord.at(1)}, {point_r0});
            auto const value_v0 = basis.eval<1>({coord.at(2)}, {point_v0});
            auto const value_source = find_source(
              species,
              info.param.time.now +
                ((coord.at(0) + 1.0) / 2.0) * info.param.time.delta,
              {
                anchor.at(0) + (static_cast<toki::Number>(index.at(0)) +
                                ((coord.at(1) + 1.0) / 2.0)) *
                                 volume.at(0),
                anchor.at(1) + (static_cast<toki::Number>(index.at(1)) +
                                ((coord.at(2) + 1.0) / 2.0)) *
                                 volume.at(1),
              });
            return value_r0 * value_v0 * value_source;
          };
          auto const buffer_offset = offset + point_r0 + point_max * point_v0;
          buffer.set(
            buffer.at(buffer_offset) +
              quadrature.sum<3>(op) * info.param.time.delta / 2.0,
            buffer_offset);
        } else {
          for (toki::Size point_time = 0; point_time < point_max;
               ++point_time) {
            auto op =
              [&](std::array<toki::Number, 3> const& coord) -> toki::Number {
              auto const value_time =
                basis.eval<1>({coord.at(0)}, {point_time});
              auto const value_r0 = basis.eval<1>({coord.at(1)}, {point_r0});
              auto const value_v0 = basis.eval<1>({coord.at(2)}, {point_v0});
              auto const value_source = find_source(
                species,
                info.param.time.now +
                  ((coord.at(0) + 1.0) / 2.0) * info.param.time.delta,
                {
                  anchor.at(0) + (static_cast<toki::Number>(index.at(0)) +
                                  ((coord.at(1) + 1.0) / 2.0)) *
                                   volume.at(0),
                  anchor.at(1) + (static_cast<toki::Number>(index.at(1)) +
                                  ((coord.at(2) + 1.0) / 2.0)) *
                                   volume.at(1),
                });
              return value_time * value_r0 * value_v0 * value_source;
            };
            auto const buffer_offset =
              offset + point_time +
              point_max * (point_r0 + point_max * point_v0);
            buffer.set(
              buffer.at(buffer_offset) +
                quadrature.sum<3>(op) * info.param.time.delta / 2.0,
              buffer_offset);
          }
        }
      }
    }
    return *this;
  }
  auto
  build_P0_L(toki::Size const& species, std::array<toki::Size, 2> const& index)
    -> toki::matrix::Mat
  {
    auto& f = info.domain.f.at(fmt::format("{}-default", species));
    auto const& volume = f.info.local.element.volume;
    auto const& basis = f.get_basis(index);
    auto const& quadrature = f.get_quadrature(index);
    auto const dof = basis.info.point;
    auto const dof_C = dof * dof;
    auto const dof_P = dof_C * dof;
    std::array<toki::Size, 3> size_col{dof, dof, dof};
    std::array<toki::Size, 3> size_row{dof, dof, dof};
    auto apply_flux_time =
      [&](
        toki::Number& result,
        std::array<toki::Number, 3> coord,
        std::array<toki::Size, 3> const& index_col,
        std::array<toki::Size, 3> const& index_row) -> void {
      if (
        info.flag.performance.skip_zero_element && (index_row[0] != dof - 1)) {
        return;
      }
      toki::Number result_flux = 1.0;
      coord.at(0) = +1.0;
      // col
      result_flux *= basis.eval<3>(coord, index_col);
      // row
      result_flux *= basis.eval<3>(coord, index_row);
      // update
      result += 0.5 * result_flux;
      return;
    };
    auto apply_flux_space = [&](
                              toki::Number& result,
                              std::array<toki::Number, 3> const& coord,
                              toki::Size const& target,
                              std::array<toki::Size, 3> const& index_col,
                              std::array<toki::Size, 3> const& index_row,
                              toki::Size const& point) -> void {
      // flux: -
      if (
        info.flag.performance.skip_zero_element &&
        (index_col[target + 1] == 0) && (index_row[target + 1] == 0)) {
        toki::Number result_flux = 1.0;
        auto tmp_coord = coord;
        tmp_coord.at(target + 1) = -1.0;
        // col
        result_flux *= get_flux(
          species, target, basis.eval<3>(tmp_coord, index_col), index, point);
        // row
        result_flux *= basis.eval<3>(tmp_coord, index_row);
        // update
        result +=
          0.5 * (-info.param.time.delta / volume.at(target)) * result_flux;
      }
      // flux: +
      if (
        info.flag.performance.skip_zero_element &&
        (index_col[target + 1] == dof - 1) &&
        (index_row[target + 1] == dof - 1)) {
        toki::Number result_flux = 1.0;
        auto tmp_coord = coord;
        tmp_coord.at(target + 1) = +1.0;
        // col
        result_flux *= get_flux(
          species, target, basis.eval<3>(tmp_coord, index_col), index, point);
        // row
        result_flux *= basis.eval<3>(tmp_coord, index_row);
        // update
        result +=
          0.5 * (+info.param.time.delta / volume.at(target)) * result_flux;
      }
      return;
    };
    auto apply_evolution = [&](
                             toki::Number& result,
                             std::array<toki::Number, 3> const& coord,
                             std::array<toki::Size, 3> const& index_col,
                             std::array<toki::Size, 3> const& index_row,
                             toki::Size const& point) -> void {
      toki::Number result_evolution = 1.0;
      // col
      result_evolution *= basis.eval<3>(coord, index_col);
      // row
      auto const result_evolution_time = basis.eval<3>(coord, index_row, 0, 1);
      toki::Number result_evolution_space[2];
      for (toki::Size target = 0; target < 2; ++target) {
        result_evolution_space[target] = get_flux(
          species,
          target,
          basis.eval<3>(coord, index_row, target + 1, 1),
          index,
          point);
      }
      // update
      result +=
        result_evolution *
        (-result_evolution_time +
         (-info.param.time.delta / volume.at(0) * result_evolution_space[0]) +
         (-info.param.time.delta / volume.at(1) * result_evolution_space[1]));
      return;
    };
    toki::matrix::Mat output(dof_P, dof_P);
    for (toki::Size col = 0; col < output.extent[1]; ++col) {
      auto index_col = toki::math::index::ind2sub<3>(size_col, col);
      for (toki::Size row = 0; row < output.extent[0]; ++row) {
        auto index_row = toki::math::index::ind2sub<3>(size_row, row);
        auto op = [&](
                    std::array<toki::Number, 3> const& coord,
                    toki::Size const& point) -> toki::Number {
          toki::Number result = 0.0;
          // flux: time
          apply_flux_time(result, coord, index_col, index_row);
          // flux: space
          apply_flux_space(result, coord, 0, index_col, index_row, point);
          apply_flux_space(result, coord, 1, index_col, index_row, point);
          // evolution: space
          apply_evolution(result, coord, index_col, index_row, point);
          return result;
        };
        output.set(quadrature.sum<3>(op), row, col);
      }
    }
    return output;
  }
  auto
  build_P0_R(toki::Size const& species, std::array<toki::Size, 2> const& index)
    -> toki::matrix::Mat
  {
    auto& f = info.domain.f.at(fmt::format("{}-default", species));
    auto const& basis = f.get_basis(index);
    auto const& quadrature = f.get_quadrature(index);
    auto const dof = basis.info.point;
    auto const dof_C = dof * dof;
    auto const dof_P = dof_C * dof;
    std::array<toki::Size, 2> size_col{dof, dof};
    std::array<toki::Size, 3> size_row{dof, dof, dof};
    toki::matrix::Mat output(dof_P, dof_C);
    for (toki::Size col = 0; col < output.extent[1]; ++col) {
      auto index_col = toki::math::index::ind2sub<2>(size_col, col);
      for (toki::Size row = 0; row < output.extent[0]; ++row) {
        auto index_row = toki::math::index::ind2sub<3>(size_row, row);
        if (info.flag.performance.skip_zero_element && (index_row[0] != 0)) {
          continue;
        }
        auto op =
          [&](std::array<toki::Number, 3> const& coord) -> toki::Number {
          toki::Number result = 0.0;
          // flux: time
          do {
            toki::Number result_flux = 1.0;
            // col
            result_flux *= basis.eval<2>({coord.at(1), coord.at(2)}, index_col);
            // row
            result_flux *=
              basis.eval<3>({-1.0, coord.at(1), coord.at(2)}, index_row);
            // update
            result += 0.5 * result_flux;
          } while (0);
          return result;
        };
        output.set(quadrature.sum<3>(op), row, col);
      }
    }
    return output;
  }
  auto
  build_P1_L(
    toki::Size const& species,
    std::array<toki::Size, 2> const& index,
    toki::Size const& tag) -> toki::matrix::Mat
  {
    auto& f = info.domain.f.at(fmt::format("{}-default", species));
    auto const& volume = f.info.local.element.volume;
    auto const& basis = f.get_basis(index);
    auto const& quadrature = f.get_quadrature(index);
    auto const dof = basis.info.point;
    auto const dof_C = dof * dof;
    auto const dof_P = dof_C * dof;
    std::array<toki::Size, 3> size_col{dof, dof, dof};
    std::array<toki::Size, 3> size_row{dof, dof, dof};
    // \note same as in `build_P0_L`
    auto apply_flux_time =
      [&](
        toki::Number& result,
        std::array<toki::Number, 3> coord,
        std::array<toki::Size, 3> const& index_col,
        std::array<toki::Size, 3> const& index_row) -> void {
      if (
        info.flag.performance.skip_zero_element && (index_row[0] != dof - 1)) {
        return;
      }
      toki::Number result_flux = 1.0;
      coord.at(0) = +1.0;
      // col
      result_flux *= basis.eval<3>(coord, index_col);
      // row
      result_flux *= basis.eval<3>(coord, index_row);
      // update
      result += 0.5 * result_flux;
      return;
    };
    // \note similar to `build_P2_L`
    auto apply_flux_space = [&](
                              toki::Number& result,
                              std::array<toki::Number, 3> const& coord,
                              toki::Size const& target,
                              std::array<toki::Size, 3> const& index_col,
                              std::array<toki::Size, 3> const& index_row,
                              toki::Size const& point) -> void {
      // flux: -
      if (
        info.flag.performance.skip_zero_element &&
        (index_col[target + 1] == 0) && (index_row[target + 1] == 0)) {
        toki::Number result_flux = 1.0;
        auto tmp_coord = coord;
        // col
        tmp_coord.at(target + 1) = -1.0;
        if (target == tag) {
          result_flux *= get_flux_upwind(
            false,
            species,
            target,
            basis.eval<3>(tmp_coord, index_col),
            index,
            point);
        } else {
          result_flux *= get_flux(
            species, target, basis.eval<3>(tmp_coord, index_col), index, point);
        }
        // row
        tmp_coord.at(target + 1) = -1.0;
        result_flux *= basis.eval<3>(tmp_coord, index_row);
        // update
        result +=
          0.5 * (-info.param.time.delta / volume.at(target)) * result_flux;
      }
      // flux: +
      if (
        info.flag.performance.skip_zero_element &&
        (index_col[target + 1] == dof - 1) &&
        (index_row[target + 1] == dof - 1)) {
        toki::Number result_flux = 1.0;
        auto tmp_coord = coord;
        // col
        tmp_coord.at(target + 1) = +1.0;
        if (target == tag) {
          result_flux *= get_flux_upwind(
            true,
            species,
            target,
            basis.eval<3>(tmp_coord, index_col),
            index,
            point);
        } else {
          result_flux *= get_flux(
            species, target, basis.eval<3>(tmp_coord, index_col), index, point);
        }
        // row
        tmp_coord.at(target + 1) = +1.0;
        result_flux *= basis.eval<3>(tmp_coord, index_row);
        // update
        result +=
          0.5 * (+info.param.time.delta / volume.at(target)) * result_flux;
      }
      return;
    };
    // \note same as in `build_P0_L`
    auto apply_evolution = [&](
                             toki::Number& result,
                             std::array<toki::Number, 3> const& coord,
                             std::array<toki::Size, 3> const& index_col,
                             std::array<toki::Size, 3> const& index_row,
                             toki::Size const& point) -> void {
      toki::Number result_evolution = 1.0;
      // col
      result_evolution *= basis.eval<3>(coord, index_col);
      // row
      auto const result_evolution_time = basis.eval<3>(coord, index_row, 0, 1);
      toki::Number result_evolution_space[2];
      for (toki::Size target = 0; target < 2; ++target) {
        result_evolution_space[target] = get_flux(
          species,
          target,
          basis.eval<3>(coord, index_row, target + 1, 1),
          index,
          point);
      }
      // update
      result +=
        result_evolution *
        (-result_evolution_time +
         (-info.param.time.delta / volume.at(0) * result_evolution_space[0]) +
         (-info.param.time.delta / volume.at(1) * result_evolution_space[1]));
      return;
    };
    toki::matrix::Mat output(dof_P, dof_P);
    for (toki::Size col = 0; col < output.extent[1]; ++col) {
      auto index_col = toki::math::index::ind2sub<3>(size_col, col);
      for (toki::Size row = 0; row < output.extent[0]; ++row) {
        auto index_row = toki::math::index::ind2sub<3>(size_row, row);
        auto op = [&](
                    std::array<toki::Number, 3> const& coord,
                    toki::Size const& point) -> toki::Number {
          toki::Number result = 0.0;
          // flux: time
          apply_flux_time(result, coord, index_col, index_row);
          // flux: space
          apply_flux_space(result, coord, 0, index_col, index_row, point);
          apply_flux_space(result, coord, 1, index_col, index_row, point);
          // evolution: space
          apply_evolution(result, coord, index_col, index_row, point);
          return result;
        };
        output.set(quadrature.sum<3>(op), row, col);
      }
    }
    return output;
  }
  auto
  build_P1_R(
    toki::Size const& species,
    std::array<toki::Size, 2> const& index_test,
    toki::Size const& tag) -> std::array<toki::matrix::Mat, 3>
  {
    auto& f = info.domain.f.at(fmt::format("{}-default", species));
    auto const& volume = f.info.local.element.volume;
    std::array<toki::matrix::Mat, 3> output{};
    auto const dof_test = f.get_basis(index_test).info.point;
    auto const dof_C_test = dof_test * dof_test;
    auto const dof_P_test = dof_C_test * dof_test;
    auto const& basis_test = f.get_basis(index_test);
    do {
      auto const& quadrature = f.get_quadrature(index_test);
      std::array<toki::Size, 2> size_col{dof_test, dof_test};
      std::array<toki::Size, 3> size_row{dof_test, dof_test, dof_test};
      // \note same as in `build_P0_R`
      output.at(0) = toki::matrix::Mat(dof_P_test, dof_C_test);
      for (toki::Size col = 0; col < output.at(0).extent[1]; ++col) {
        auto index_col = toki::math::index::ind2sub<2>(size_col, col);
        for (toki::Size row = 0; row < output.at(0).extent[0]; ++row) {
          auto index_row = toki::math::index::ind2sub<3>(size_row, row);
          if (info.flag.performance.skip_zero_element && (index_row[0] != 0)) {
            continue;
          }
          auto op =
            [&](std::array<toki::Number, 3> const& coord) -> toki::Number {
            toki::Number result = 0.0;
            // flux: time
            do {
              toki::Number result_flux = 1.0;
              // col
              result_flux *=
                basis_test.eval<2>({coord.at(1), coord.at(2)}, index_col);
              // row
              result_flux *=
                basis_test.eval<3>({-1.0, coord.at(1), coord.at(2)}, index_row);
              // update
              result += 0.5 * result_flux;
            } while (0);
            return result;
          };
          output.at(0).set(quadrature.sum<3>(op), row, col);
        }
      }
    } while (0);
    do {
      // \note similar to `build_C0_R`
      auto apply_flux_space_branch =
        [&](
          toki::Number& result,
          toki::Number const& sign,
          std::array<toki::Number, 3> const& coord,
          std::array<toki::Size, 3> const& index_col,
          std::array<toki::Size, 3> const& index_row,
          std::array<toki::Size, 2> const& tmp_index,
          toki::Size const& point) -> void {
        auto const dof = f.get_basis(tmp_index).info.point;
        auto const& basis = f.get_basis(tmp_index);
        if (sign < 0.0) {
          if (
            info.flag.performance.skip_zero_element &&
            (index_col[tag + 1] != dof - 1) && (index_row[tag + 1] != 0)) {
            return;
          }
        } else {
          if (
            info.flag.performance.skip_zero_element &&
            (index_col[tag + 1] != 0) && (index_row[tag + 1] != dof_test - 1)) {
            return;
          }
        }
        toki::Number result_flux = 1.0;
        auto tmp_coord = coord;
        // col
        tmp_coord.at(tag + 1) = -sign;
        result_flux *= get_flux_upwind(
          sign < 0.0,
          species,
          tag,
          basis.eval<3>(tmp_coord, index_col),
          tmp_index,
          point);
        // row
        tmp_coord.at(tag + 1) = +sign;
        result_flux *= basis_test.eval<3>(tmp_coord, index_row);
        // update
        result +=
          0.5 * (-sign * info.param.time.delta / volume.at(tag)) * result_flux;
        return;
      };
      toki::Number sign[2]{-1.0, +1.0};
      auto const& direction = tag;
      for (int id = 0; id < 2; ++id) {
        auto index = index_test;
        index[direction] = index_test[direction] - 1 + id * 2;
        auto const& quadrature = f.get_quadrature(index);
        auto const dof = f.get_basis(index).info.point;
        auto const dof_P = dof * dof * dof;
        std::array<toki::Size, 3> size_col{dof, dof, dof};
        std::array<toki::Size, 3> size_row{dof_test, dof_test, dof_test};
        output.at(id + 1) = toki::matrix::Mat(dof_P_test, dof_P);
        for (toki::Size col = 0; col < output.at(id + 1).extent[1]; ++col) {
          auto index_col = toki::math::index::ind2sub<3>(size_col, col);
          for (toki::Size row = 0; row < output.at(id + 1).extent[0]; ++row) {
            auto index_row = toki::math::index::ind2sub<3>(size_row, row);
            auto op = [&](
                        std::array<toki::Number, 3> const& coord,
                        toki::Size const& point) -> toki::Number {
              toki::Number result = 0.0;
              // flux: space
              apply_flux_space_branch(
                result, sign[id], coord, index_col, index_row, index, point);
              return result;
            };
            output.at(id + 1).set(quadrature.sum<3>(op), row, col);
          }
        }
      }
    } while (0);
    return output;
  }
  auto
  build_P2_L(toki::Size const& species, std::array<toki::Size, 2> const& index)
    -> toki::matrix::Mat
  {
    auto& f = info.domain.f.at(fmt::format("{}-default", species));
    auto const& volume = f.info.local.element.volume;
    auto const& basis = f.get_basis(index);
    auto const& quadrature = f.get_quadrature(index);
    auto const dof = basis.info.point;
    auto const dof_C = dof * dof;
    auto const dof_P = dof_C * dof;
    std::array<toki::Size, 3> size_col{dof, dof, dof};
    std::array<toki::Size, 3> size_row{dof, dof, dof};
    // \note same as in `build_P0_L`
    auto apply_flux_time =
      [&](
        toki::Number& result,
        std::array<toki::Number, 3> coord,
        std::array<toki::Size, 3> const& index_col,
        std::array<toki::Size, 3> const& index_row) -> void {
      if (
        info.flag.performance.skip_zero_element && (index_row[0] != dof - 1)) {
        return;
      }
      toki::Number result_flux = 1.0;
      coord.at(0) = +1.0;
      // col
      result_flux *= basis.eval<3>(coord, index_col);
      // row
      result_flux *= basis.eval<3>(coord, index_row);
      // update
      result += 0.5 * result_flux;
      return;
    };
    auto apply_flux_space = [&](
                              toki::Number& result,
                              std::array<toki::Number, 3> const& coord,
                              toki::Size const& target,
                              std::array<toki::Size, 3> const& index_col,
                              std::array<toki::Size, 3> const& index_row,
                              toki::Size const& point) -> void {
      // flux: -
      if (
        info.flag.performance.skip_zero_element &&
        (index_col[target + 1] == 0) && (index_row[target + 1] == 0)) {
        toki::Number result_flux = 1.0;
        auto tmp_coord = coord;
        // col
        tmp_coord.at(target + 1) = -1.0;
        result_flux *= get_flux_upwind(
          false,
          species,
          target,
          basis.eval<3>(tmp_coord, index_col),
          index,
          point);
        // row
        tmp_coord.at(target + 1) = -1.0;
        result_flux *= basis.eval<3>(tmp_coord, index_row);
        // update
        result +=
          0.5 * (-info.param.time.delta / volume.at(target)) * result_flux;
      }
      // flux: +
      if (
        info.flag.performance.skip_zero_element &&
        (index_col[target + 1] == dof - 1) &&
        (index_row[target + 1] == dof - 1)) {
        toki::Number result_flux = 1.0;
        auto tmp_coord = coord;
        // col
        tmp_coord.at(target + 1) = +1.0;
        result_flux *= get_flux_upwind(
          true,
          species,
          target,
          basis.eval<3>(tmp_coord, index_col),
          index,
          point);
        // row
        tmp_coord.at(target + 1) = +1.0;
        result_flux *= basis.eval<3>(tmp_coord, index_row);
        // update
        result +=
          0.5 * (+info.param.time.delta / volume.at(target)) * result_flux;
      }
      return;
    };
    // \note same as in `build_P0_L`
    auto apply_evolution = [&](
                             toki::Number& result,
                             std::array<toki::Number, 3> const& coord,
                             std::array<toki::Size, 3> const& index_col,
                             std::array<toki::Size, 3> const& index_row,
                             toki::Size const& point) -> void {
      toki::Number result_evolution = 1.0;
      // col
      result_evolution *= basis.eval<3>(coord, index_col);
      // row
      auto const result_evolution_time = basis.eval<3>(coord, index_row, 0, 1);
      toki::Number result_evolution_space[2];
      for (toki::Size target = 0; target < 2; ++target) {
        result_evolution_space[target] = get_flux(
          species,
          target,
          basis.eval<3>(coord, index_row, target + 1, 1),
          index,
          point);
      }
      // update
      result +=
        result_evolution *
        (-result_evolution_time +
         (-info.param.time.delta / volume.at(0) * result_evolution_space[0]) +
         (-info.param.time.delta / volume.at(1) * result_evolution_space[1]));
      return;
    };
    toki::matrix::Mat output(dof_P, dof_P);
    for (toki::Size col = 0; col < output.extent[1]; ++col) {
      auto index_col = toki::math::index::ind2sub<3>(size_col, col);
      for (toki::Size row = 0; row < output.extent[0]; ++row) {
        auto index_row = toki::math::index::ind2sub<3>(size_row, row);
        auto op = [&](
                    std::array<toki::Number, 3> const& coord,
                    toki::Size const& point) -> toki::Number {
          toki::Number result = 0.0;
          // flux: time
          apply_flux_time(result, coord, index_col, index_row);
          // flux: space
          apply_flux_space(result, coord, 0, index_col, index_row, point);
          apply_flux_space(result, coord, 1, index_col, index_row, point);
          // evolution: space
          apply_evolution(result, coord, index_col, index_row, point);
          return result;
        };
        output.set(quadrature.sum<3>(op), row, col);
      }
    }
    return output;
  }
  auto
  build_P2_R(
    toki::Size const& species,
    std::array<toki::Size, 2> const& index_test)
    -> std::array<toki::matrix::Mat, 5>
  {
    auto& f = info.domain.f.at(fmt::format("{}-default", species));
    auto const& volume = f.info.local.element.volume;
    std::array<toki::matrix::Mat, 5> output{};
    auto const dof_test = f.get_basis(index_test).info.point;
    auto const dof_C_test = dof_test * dof_test;
    auto const dof_P_test = dof_C_test * dof_test;
    auto const& basis_test = f.get_basis(index_test);
    do {
      auto const& quadrature = f.get_quadrature(index_test);
      std::array<toki::Size, 2> size_col{dof_test, dof_test};
      std::array<toki::Size, 3> size_row{dof_test, dof_test, dof_test};
      // \note same as in `build_P0_R`
      output.at(0) = toki::matrix::Mat(dof_P_test, dof_C_test);
      for (toki::Size col = 0; col < output.at(0).extent[1]; ++col) {
        auto index_col = toki::math::index::ind2sub<2>(size_col, col);
        for (toki::Size row = 0; row < output.at(0).extent[0]; ++row) {
          auto index_row = toki::math::index::ind2sub<3>(size_row, row);
          if (info.flag.performance.skip_zero_element && (index_row[0] != 0)) {
            continue;
          }
          auto op =
            [&](std::array<toki::Number, 3> const& coord) -> toki::Number {
            toki::Number result = 0.0;
            // flux: time
            do {
              toki::Number result_flux = 1.0;
              // col
              result_flux *=
                basis_test.eval<2>({coord.at(1), coord.at(2)}, index_col);
              // row
              result_flux *=
                basis_test.eval<3>({-1.0, coord.at(1), coord.at(2)}, index_row);
              // update
              result += 0.5 * result_flux;
            } while (0);
            return result;
          };
          output.at(0).set(quadrature.sum<3>(op), row, col);
        }
      }
    } while (0);
    do {
      // \note same as in `build_C0_R`
      auto apply_flux_space_branch =
        [&](
          toki::Number& result,
          toki::Number const& sign,
          std::array<toki::Number, 3> const& coord,
          toki::Size const& target,
          std::array<toki::Size, 3> const& index_col,
          std::array<toki::Size, 3> const& index_row,
          std::array<toki::Size, 2> const& tmp_index,
          toki::Size const& point) -> void {
        auto const dof = f.get_basis(tmp_index).info.point;
        auto const& basis = f.get_basis(tmp_index);
        if (sign < 0.0) {
          if (
            info.flag.performance.skip_zero_element &&
            (index_col[target + 1] != dof - 1) &&
            (index_row[target + 1] != 0)) {
            return;
          }
        } else {
          if (
            info.flag.performance.skip_zero_element &&
            (index_col[target + 1] != 0) &&
            (index_row[target + 1] != dof_test - 1)) {
            return;
          }
        }
        toki::Number result_flux = 1.0;
        auto tmp_coord = coord;
        // col
        tmp_coord.at(target + 1) = -sign;
        result_flux *= get_flux_upwind(
          sign < 0.0,
          species,
          target,
          basis.eval<3>(tmp_coord, index_col),
          tmp_index,
          point);
        // row
        tmp_coord.at(target + 1) = +sign;
        result_flux *= basis_test.eval<3>(tmp_coord, index_row);
        // update
        result += 0.5 * (-sign * info.param.time.delta / volume.at(target)) *
                  result_flux;
        return;
      };
      toki::Number sign[2]{-1.0, +1.0};
      for (int id = 0; id < 4; ++id) {
        auto index = index_test;
        auto const direction = id / 2;
        auto const tag = id % 2;
        index[direction] = index_test[direction] - 1 + tag * 2;
        auto const& quadrature = f.get_quadrature(index);
        auto const dof = f.get_basis(index).info.point;
        auto const dof_P = dof * dof * dof;
        std::array<toki::Size, 3> size_col{dof, dof, dof};
        std::array<toki::Size, 3> size_row{dof_test, dof_test, dof_test};
        output.at(id + 1) = toki::matrix::Mat(dof_P_test, dof_P);
        for (toki::Size col = 0; col < output.at(id + 1).extent[1]; ++col) {
          auto index_col = toki::math::index::ind2sub<3>(size_col, col);
          for (toki::Size row = 0; row < output.at(id + 1).extent[0]; ++row) {
            auto index_row = toki::math::index::ind2sub<3>(size_row, row);
            auto op = [&](
                        std::array<toki::Number, 3> const& coord,
                        toki::Size const& point) -> toki::Number {
              toki::Number result = 0.0;
              // flux: space
              apply_flux_space_branch(
                result,
                sign[tag],
                coord,
                direction,
                index_col,
                index_row,
                index,
                point);
              return result;
            };
            output.at(id + 1).set(quadrature.sum<3>(op), row, col);
          }
        }
      }
    } while (0);
    return output;
  }
  auto
  build_C0_L(toki::Size const& species, std::array<toki::Size, 2> const& index)
    -> toki::matrix::Mat
  {
    auto& f = info.domain.f.at(fmt::format("{}-default", species));
    auto const& basis = f.get_basis(index);
    auto const& quadrature = f.get_quadrature(index);
    auto const dof = basis.info.point;
    auto const dof_C = dof * dof;
    std::array<toki::Size, 2> size_col{dof, dof};
    std::array<toki::Size, 2> size_row{dof, dof};
    toki::matrix::Mat output(dof_C, dof_C);
    for (toki::Size col = 0; col < output.extent[1]; ++col) {
      auto index_col = toki::math::index::ind2sub<2>(size_col, col);
      for (toki::Size row = 0; row < output.extent[0]; ++row) {
        auto index_row = toki::math::index::ind2sub<2>(size_row, row);
        auto op =
          [&](std::array<toki::Number, 3> const& coord) -> toki::Number {
          toki::Number result = 1.0;
          // col
          result *= basis.eval<2>({coord.at(1), coord.at(2)}, index_col);
          // row
          result *= basis.eval<2>({coord.at(1), coord.at(2)}, index_row);
          return 0.5 * result;
        };
        output.set(quadrature.sum<3>(op), row, col);
      }
    }
    return output;
  }
  auto
  build_C0_R(
    toki::Size const& species,
    std::array<toki::Size, 2> const& index_test)
    -> std::array<toki::matrix::Mat, 5>
  {
    auto& f = info.domain.f.at(fmt::format("{}-default", species));
    auto const& volume = f.info.local.element.volume;
    std::array<toki::matrix::Mat, 5> output{};
    auto const dof_test = f.get_basis(index_test).info.point;
    auto const dof_C_test = dof_test * dof_test;
    auto const& basis_test = f.get_basis(index_test);
    auto apply_flux_space = [&](
                              toki::Number& result,
                              toki::Number const& sign,
                              std::array<toki::Number, 3> const& coord,
                              toki::Size const& target,
                              std::array<toki::Size, 3> const& index_col,
                              std::array<toki::Size, 2> const& index_row,
                              std::array<toki::Size, 2> const& tmp_index,
                              toki::Size const& point) -> void {
      auto const dof = f.get_basis(tmp_index).info.point;
      auto const& basis = f.get_basis(tmp_index);
      if (sign < 0.0) {
        if (
          info.flag.performance.skip_zero_element &&
          (index_col[target + 1] != 0) && (index_row[target] != 0)) {
          return;
        }
      } else {
        if (
          info.flag.performance.skip_zero_element &&
          (index_col[target + 1] != dof - 1) &&
          (index_row[target] != dof_test - 1)) {
          return;
        }
      }
      toki::Number result_flux = 1.0;
      std::array<toki::Number, 2> tmp_coord{coord.at(1), coord.at(2)};
      // col
      tmp_coord.at(target) = sign;
      result_flux *= get_flux_upwind(
        sign > 0.0,
        species,
        target,
        basis.eval<3>(
          {coord.at(0), tmp_coord.at(0), tmp_coord.at(1)}, index_col),
        tmp_index,
        point);
      // row
      tmp_coord.at(target) = sign;
      result_flux *= basis_test.eval<2>(tmp_coord, index_row);
      // update
      result +=
        0.5 * (-sign * info.param.time.delta / volume.at(target)) * result_flux;
      return;
    };
    auto apply_flux_space_branch = [&](
                                     toki::Number& result,
                                     toki::Number const& sign,
                                     std::array<toki::Number, 3> const& coord,
                                     toki::Size const& target,
                                     std::array<toki::Size, 3> const& index_col,
                                     std::array<toki::Size, 2> const& index_row,
                                     std::array<toki::Size, 2> const& tmp_index,
                                     toki::Size const& point) -> void {
      auto const dof = f.get_basis(tmp_index).info.point;
      auto const& basis = f.get_basis(tmp_index);
      if (sign < 0.0) {
        if (
          info.flag.performance.skip_zero_element &&
          (index_col[target + 1] != dof - 1) && (index_row[target] != 0)) {
          return;
        }
      } else {
        if (
          info.flag.performance.skip_zero_element &&
          (index_col[target + 1] != 0) && (index_row[target] != dof_test - 1)) {
          return;
        }
      }
      toki::Number result_flux = 1.0;
      std::array<toki::Number, 2> tmp_coord{coord.at(1), coord.at(2)};
      // col
      tmp_coord.at(target) = -sign;
      result_flux *= get_flux_upwind(
        sign < 0.0,
        species,
        target,
        basis.eval<3>(
          {coord.at(0), tmp_coord.at(0), tmp_coord.at(1)}, index_col),
        tmp_index,
        point);
      // row
      tmp_coord.at(target) = sign;
      result_flux *= basis_test.eval<2>(tmp_coord, index_row);
      // update
      result +=
        0.5 * (-sign * info.param.time.delta / volume.at(target)) * result_flux;
      return;
    };
    auto apply_evolution = [&](
                             toki::Number& result,
                             std::array<toki::Number, 3> const& coord,
                             std::array<toki::Size, 3> const& index_col,
                             std::array<toki::Size, 2> const& index_row,
                             std::array<toki::Size, 2> const& tmp_index,
                             toki::Size const& point) -> void {
      auto const& basis = f.get_basis(tmp_index);
      toki::Number result_evolution = 1.0;
      // col
      result_evolution *= basis.eval<3>(coord, index_col);
      // row
      std::array<toki::Number, 2> tmp_coord{coord.at(1), coord.at(2)};
      toki::Number result_evolution_space[2];
      for (toki::Size target = 0; target < 2; ++target) {
        result_evolution_space[target] = get_flux(
          species,
          target,
          basis_test.eval<2>(tmp_coord, index_row, target, 1),
          tmp_index,
          point);
      }
      // update
      result +=
        result_evolution *
        ((info.param.time.delta / volume.at(0) * result_evolution_space[0]) +
         (info.param.time.delta / volume.at(1) * result_evolution_space[1]));
      return;
    };
    for (int id = 0; id < 5; ++id) {
      auto index = index_test;
      if (id == 0) {
      } else if (id <= 2 * 1) {
        index[0] = index_test[0] - 1 + (id - 1) * 2;
      } else {
        index[1] = index_test[1] - 1 + (id - 3) * 2;
      }
      auto const& quadrature = f.get_quadrature(index);
      auto const dof = f.get_basis(index).info.point;
      auto const dof_P = dof * dof * dof;
      std::array<toki::Size, 3> size_col{dof, dof, dof};
      std::array<toki::Size, 2> size_row{dof_test, dof_test};
      output[id] = toki::matrix::Mat(dof_C_test, dof_P);
      if (id == 0) {
        for (toki::Size col = 0; col < output[id].extent[1]; ++col) {
          auto index_col = toki::math::index::ind2sub<3>(size_col, col);
          for (toki::Size row = 0; row < output[id].extent[0]; ++row) {
            auto index_row = toki::math::index::ind2sub<2>(size_row, row);
            auto op = [&](
                        std::array<toki::Number, 3> const& coord,
                        toki::Size const& point) -> toki::Number {
              toki::Number result = 0.0;
              // flux: space
              for (toki::Size target = 0; target < 2; ++target) {
                apply_flux_space(
                  result,
                  -1.0,
                  coord,
                  target,
                  index_col,
                  index_row,
                  index,
                  point);
                apply_flux_space(
                  result,
                  +1.0,
                  coord,
                  target,
                  index_col,
                  index_row,
                  index,
                  point);
              }
              // evolution
              apply_evolution(
                result, coord, index_col, index_row, index, point);
              return result;
            };
            output[id].set(quadrature.sum<3>(op), row, col);
          }
        }
      } else {
        toki::Number sign[2]{-1.0, +1.0};
        auto const direction = (id - 1) / 2;
        auto const tag = (id - 1) % 2;
        for (toki::Size col = 0; col < output[id].extent[1]; ++col) {
          auto index_col = toki::math::index::ind2sub<3>(size_col, col);
          for (toki::Size row = 0; row < output[id].extent[0]; ++row) {
            auto index_row = toki::math::index::ind2sub<2>(size_row, row);
            auto op = [&](
                        std::array<toki::Number, 3> const& coord,
                        toki::Size const& point) -> toki::Number {
              toki::Number result = 0.0;
              // flux: space
              apply_flux_space_branch(
                result,
                sign[tag],
                coord,
                direction,
                index_col,
                index_row,
                index,
                point);
              return result;
            };
            output[id].set(quadrature.sum<3>(op), row, col);
          }
        }
      }
    }
    return output;
  }
  auto
  halo_sync() -> VP_p1D1V&
  {
    // E halo
    for (toki::Size dim = 0; dim < 1; ++dim) {
      auto const name = fmt::format("{}", dim);
      info.domain.E.at(name).halo_dispatch(dim).halo_wait(dim);
      info.domain.E.at(name).halo_update(dim);
    }
    // f halo
    for (toki::Size species = 0; species < info.param.model.species;
         ++species) {
      auto const name = fmt::format("{}-default", species);
      for (toki::Size dim = 0; dim < 1; ++dim) {
        info.domain.f.at(name).halo_dispatch(dim).halo_wait(dim);
        info.domain.f.at(name).halo_update(dim);
      }
    }
    return *this;
  }
};
} // namespace toki::timestepper::detail
#endif
