#ifndef TOKI_TIMESTEPPER_DETAIL_VP_TAG_HPP
#define TOKI_TIMESTEPPER_DETAIL_VP_TAG_HPP
namespace toki::timestepper::detail {
enum struct VP_Model
{
  p1D1V,
};
enum struct VP_Example
{
  ManufacturedSolution,
  LandauDamping,
  TwoStreamInstability,
  BumpOnTail,
  PlasmaSheath,
  IonAcousticWave,
  IonAcousticShockWave,
};
} // namespace toki::timestepper::detail
#endif
