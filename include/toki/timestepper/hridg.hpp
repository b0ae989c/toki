#ifndef TOKI_TIMESTEPPER_HRIDG_HPP
#define TOKI_TIMESTEPPER_HRIDG_HPP
#include <type_traits>
#include "toki/timestepper/detail/hridg_1d.hpp"
#include "toki/timestepper/detail/hridg_2d.hpp"
namespace toki::timestepper {
template<int dim>
using HRIDG = std::conditional_t<
  dim == 1,
  detail::HRIDG_1D,
  std::conditional_t<dim == 2, detail::HRIDG_2D, void>>;
}
#endif
