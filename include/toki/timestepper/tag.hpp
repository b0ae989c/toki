#ifndef TOKI_TIMESTEPPER_TAG_HPP
#define TOKI_TIMESTEPPER_TAG_HPP
namespace toki::timestepper::tag {
enum struct Flux
{
  Zero,
  Uniform,
  UniformUpwind,
  External,
  ExternalUpwind,
};
} // namespace toki::timestepper::tag
#endif
