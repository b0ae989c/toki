#ifndef TOKI_TIMESTEPPER_VP_HPP
#define TOKI_TIMESTEPPER_VP_HPP
#include <type_traits>
#include "toki/timestepper/detail/vp_p1d1v.hpp"
#include "toki/timestepper/detail/vp_tag.hpp"
namespace toki::timestepper {
// clang-format off
template<
  detail::VP_Model model,
  detail::VP_Example example>
using VP =
std::conditional_t<
  model == detail::VP_Model::p1D1V,
    detail::VP_p1D1V<example>,
  void>;
// clang-format on
} // namespace toki::timestepper
#endif
