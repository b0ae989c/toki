#ifndef TOKI_UTILITY_HPP
#define TOKI_UTILITY_HPP
#include <string>
#include "toki/config.hpp"
namespace toki::utility {
auto confirm(bool const& value, std::string const& message) -> void;
auto eq(toki::Number const& lhs, toki::Number const& rhs) -> bool;
} // namespace toki::utility
#endif
