#ifndef TOKI_VECTOR_VEC_HPP
#define TOKI_VECTOR_VEC_HPP
#include <array>
#include <functional>
#include <memory>
#include <utility>
#include "toki/config.hpp"
#ifdef TOKI_DEBUG
#include "fmt/format.h"
#endif
namespace toki::vector {
struct Vec
{
  using value_type = toki::Number;
  std::array<toki::Size, 1> extent;
  std::shared_ptr<value_type[]> data;
  Vec();
  explicit Vec(toki::Size const& extent_0);
  Vec(Vec const& other);
  Vec(Vec&& other) noexcept;
  ~Vec();
  auto operator=(Vec other) -> Vec&;
  auto valid_at(toki::Size const& index_0) const -> bool;
  auto set(value_type const& value, toki::Size const& index_0) -> Vec&;
  auto set(std::function<value_type(toki::Size const&)> const& op) -> Vec&;
  auto at(toki::Size const& index_0) const -> value_type const&;
  auto get_min() const -> value_type;
  auto get_max() const -> value_type;
  auto get_norm() const -> value_type;
  auto get_norm(
    toki::Size const& offset,
    toki::Size const& size,
    toki::Size const& stride) const -> value_type;
#ifdef TOKI_DEBUG
  auto
  print() const -> Vec const&
  {
    fmt::print("\n");
    for (toki::Size row = 0; row < extent[0]; ++row) {
      fmt::print("{:+.6e};\n", at(row));
    }
    return *this;
  }
#endif
  friend auto
  swap(Vec& lhs, Vec& rhs) noexcept -> void
  {
    using std::swap;
    swap(lhs.extent, rhs.extent);
    swap(lhs.data, rhs.data);
    return;
  }
};
} // namespace toki::vector
#endif
