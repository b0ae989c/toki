#include "toki/basis/nodal.hpp"
#include "toki/log.hpp"
namespace toki::basis {
Nodal::Nodal()
  : Nodal(1)
{
}
Nodal::Nodal(toki::Size const& point)
  : info({
      .point = point,
      .abscissa = toki::vector::Vec(point),
    })
{
  if (info.point == 1) {
    info.abscissa.set(+0.000000000000000, 0);
  } else if (info.point == 2) {
    info.abscissa.set(-1.0000000000000, 0).set(+1.000000000000000, 1);
  } else if (info.point == 3) {
    info.abscissa.set(-1.0000000000000, 0)
      .set(+0.000000000000000, 1)
      .set(+1.000000000000000, 2);
  } else if (info.point == 4) {
    info.abscissa.set(-1.0000000000000, 0)
      .set(-0.447213595499958, 1)
      .set(+0.447213595499958, 2)
      .set(+1.000000000000000, 3);
  } else if (info.point == 5) {
    info.abscissa.set(-1.0000000000000, 0)
      .set(-0.654653670707977, 1)
      .set(+0.000000000000000, 2)
      .set(+0.654653670707977, 3)
      .set(+1.000000000000000, 4);
  } else if (info.point == 6) {
    info.abscissa.set(-1.0000000000000, 0)
      .set(-0.7650553239294647, 1)
      .set(-0.2852315164806451, 2)
      .set(+0.2852315164806451, 3)
      .set(+0.7650553239294647, 4)
      .set(+1.0000000000000, 5);
  } else {
    toki::log::error("no implementation");
  }
}
Nodal::Nodal(Nodal const& other)
  : info(other.info)
{
}
Nodal::Nodal(Nodal&& other) noexcept
  : info(std::exchange(other.info, decltype(info){}))
{
}
Nodal::~Nodal() {}
auto
Nodal::operator=(Nodal other) -> Nodal&
{
  swap(*this, other);
  return *this;
}
} // namespace toki::basis
