#include <cmath>
#include "boost/preprocessor/iteration/local.hpp"
#include "toki/basis/nodal.hpp"
#include "toki/math/index.hpp"
#include "toki/utility.hpp"
namespace toki::basis {
template<int dim>
auto
Nodal::approx(
  toki::Number& result,
  std::array<toki::Number, dim> const& coord,
  toki::vector::Vec const& sample,
  toki::Size const& offset) const -> Nodal const&
{
  toki::utility::confirm(sample.valid_at(offset), "invalid parameter: offset");
  std::array<toki::Size, dim> size{};
  toki::Size dof_total{1};
  for (int d = 0; d < dim; ++d) {
    size[d] = info.point;
    dof_total *= info.point;
  }
  toki::utility::confirm(
    sample.valid_at(offset + dof_total - 1), "invalid parameter: sample");
  result = 0.0;
  for (toki::Size dof = 0; dof < dof_total; ++dof) {
    auto index = toki::math::index::ind2sub<dim>(size, dof);
    result = std::fma(eval<dim>(coord, index), sample.at(offset + dof), result);
  }
  return *this;
}
template<int dim>
auto
Nodal::approx(
  std::array<toki::Number, dim> const& coord,
  toki::vector::Vec const& sample,
  toki::Size const& offset) const -> toki::Number
{
  toki::Number result{};
  approx<dim>(result, coord, sample, offset);
  return result;
}
template<int dim>
auto
Nodal::approx(
  toki::Number& result,
  std::array<toki::Number, dim> const& coord,
  toki::vector::Vec const& sample,
  toki::Size const& offset,
  int const target_dim,
  int const derivative_order) const -> Nodal const&
{
  toki::utility::confirm(sample.valid_at(offset), "invalid parameter: offset");
  std::array<toki::Size, dim> size{};
  toki::Size dof_total{1};
  for (int d = 0; d < dim; ++d) {
    size[d] = info.point;
    dof_total *= info.point;
  }
  toki::utility::confirm(
    sample.valid_at(offset + dof_total - 1), "invalid parameter: sample");
  result = 0.0;
  for (toki::Size dof = 0; dof < dof_total; ++dof) {
    auto index = toki::math::index::ind2sub<dim>(size, dof);
    result = std::fma(
      eval<dim>(coord, index, target_dim, derivative_order),
      sample.at(offset + dof),
      result);
  }
  return *this;
}
template<int dim>
auto
Nodal::approx(
  std::array<toki::Number, dim> const& coord,
  toki::vector::Vec const& sample,
  toki::Size const& offset,
  int const target_dim,
  int const derivative_order) const -> toki::Number
{
  toki::Number result{};
  approx<dim>(result, coord, sample, offset, target_dim, derivative_order);
  return result;
}
} // namespace toki::basis
namespace toki::basis {
#define BOOST_PP_LOCAL_MACRO(dim)                                              \
  template auto Nodal::approx<dim>(                                            \
    toki::Number&,                                                             \
    std::array<toki::Number, dim> const&,                                      \
    toki::vector::Vec const&,                                                  \
    toki::Size const&) const -> Nodal const&;                                  \
  template auto Nodal::approx<dim>(                                            \
    std::array<toki::Number, dim> const&,                                      \
    toki::vector::Vec const&,                                                  \
    toki::Size const&) const -> toki::Number;                                  \
  template auto Nodal::approx<dim>(                                            \
    toki::Number&,                                                             \
    std::array<toki::Number, dim> const&,                                      \
    toki::vector::Vec const&,                                                  \
    toki::Size const&,                                                         \
    int const,                                                                 \
    int const) const -> Nodal const&;                                          \
  template auto Nodal::approx<dim>(                                            \
    std::array<toki::Number, dim> const&,                                      \
    toki::vector::Vec const&,                                                  \
    toki::Size const&,                                                         \
    int const,                                                                 \
    int const) const -> toki::Number;                                          \
  /* */
#define BOOST_PP_LOCAL_LIMITS (1, 4)
#include BOOST_PP_LOCAL_ITERATE()
} // namespace toki::basis
