#include "boost/preprocessor/iteration/local.hpp"
#include "toki/basis/nodal.hpp"
#include "toki/log.hpp"
#include "toki/math/index.hpp"
#include "toki/utility.hpp"
namespace toki::basis {
template<int dim>
auto
Nodal::eval(
  toki::Number& result,
  std::array<toki::Number, dim> const& coord,
  std::array<toki::Size, dim> const& index) const -> Nodal const&
{
  if constexpr (dim == 1) {
    using toki::utility::eq;
    for (toki::Size ell = 0; ell < info.point; ++ell) {
      if (toki::utility::eq(info.abscissa.at(ell), coord[0])) {
        result = (index[0] == ell ? 1.0 : 0.0);
        return *this;
      }
    }
    result = 1.0;
    for (toki::Size ell = 0; ell < info.point; ++ell) {
      if (ell == index[0]) {
        continue;
      }
      result *= (coord[0] - info.abscissa.at(ell)) /
                (info.abscissa.at(index[0]) - info.abscissa.at(ell));
      if (result == 0.0) {
        return *this;
      }
    }
  } else {
    result = 1.0;
    for (int d = 0; d < dim; ++d) {
      result *= eval<1>({coord[d]}, {index[d]});
      if (result == 0.0) {
        return *this;
      }
    }
  }
  return *this;
}
template<int dim>
auto
Nodal::eval(
  std::array<toki::Number, dim> const& coord,
  std::array<toki::Size, dim> const& index) const -> toki::Number
{
  toki::Number result{};
  eval<dim>(result, coord, index);
  return result;
}
template<int dim>
auto
Nodal::eval(
  toki::Number& result,
  std::array<toki::Number, dim> const& coord,
  std::array<toki::Size, dim> const& index,
  int const target_dim,
  int const derivative_order) const -> Nodal const&
{
  if constexpr (dim == 1) {
    result = 0.0;
    if (derivative_order == 0) {
      eval<dim>(result, coord, index);
    } else if (derivative_order == 1) {
      for (toki::Size ell_0 = 0; ell_0 < info.point; ++ell_0) {
        if (ell_0 == index[0]) {
          continue;
        }
        toki::Number tmp =
          1.0 / (info.abscissa.at(index[0]) - info.abscissa.at(ell_0));
        for (toki::Size ell_1 = 0; ell_1 < info.point; ++ell_1) {
          if ((ell_1 == ell_0) || (ell_1 == index[0])) {
            continue;
          }
          tmp *= (coord[0] - info.abscissa.at(ell_1)) /
                 (info.abscissa.at(index[0]) - info.abscissa.at(ell_1));
          if (tmp == 0.0) {
            break;
          }
        }
        result += tmp;
      }
    } else if (derivative_order == 2) {
      for (toki::Size ell_0 = 0; ell_0 < info.point; ++ell_0) {
        if (ell_0 == index[0]) {
          continue;
        }
        toki::Number tmp_0 = 0.0;
        for (toki::Size ell_1 = 0; ell_1 < info.point; ++ell_1) {
          if ((ell_1 == index[0]) || (ell_1 == ell_0)) {
            continue;
          }
          toki::Number tmp_1 = 1.0;
          for (toki::Size ell_2 = 0; ell_2 < info.point; ++ell_2) {
            if ((ell_2 == index[0]) || (ell_2 == ell_0) || (ell_2 == ell_1)) {
              continue;
            }
            tmp_1 *= (coord[0] - info.abscissa.at(ell_2)) /
                     (info.abscissa.at(index[0]) - info.abscissa.at(ell_2));
            if (tmp_1 == 0.0) {
              break;
            }
          }
          tmp_0 +=
            tmp_1 / (info.abscissa.at(index[0]) - info.abscissa.at(ell_1));
        }
        result +=
          tmp_0 / (info.abscissa.at(index[0]) - info.abscissa.at(ell_0));
      }
    } else {
      toki::log::error("no implementation");
    }
  } else {
    toki::utility::confirm(target_dim < dim, "invalid parameter: target_dim");
    result = 1.0;
    for (int d = 0; d < dim; ++d) {
      if (d == target_dim) {
        result *= eval<1>({coord[d]}, {index[d]}, 0, derivative_order);
      } else {
        result *= eval<1>({coord[d]}, {index[d]});
      }
      if (result == 0.0) {
        return *this;
      }
    }
  }
  return *this;
}
template<int dim>
auto
Nodal::eval(
  std::array<toki::Number, dim> const& coord,
  std::array<toki::Size, dim> const& index,
  int const target_dim,
  int const derivative_order) const -> toki::Number
{
  toki::Number result{};
  eval<dim>(result, coord, index, target_dim, derivative_order);
  return result;
}
} // namespace toki::basis
namespace toki::basis {
#define BOOST_PP_LOCAL_MACRO(dim)                                              \
  template auto Nodal::eval<dim>(                                              \
    toki::Number&,                                                             \
    std::array<toki::Number, dim> const&,                                      \
    std::array<toki::Size, dim> const&) const -> Nodal const&;                 \
  template auto Nodal::eval<dim>(                                              \
    std::array<toki::Number, dim> const&, std::array<toki::Size, dim> const&)  \
    const -> toki::Number;                                                     \
  template auto Nodal::eval<dim>(                                              \
    toki::Number&,                                                             \
    std::array<toki::Number, dim> const&,                                      \
    std::array<toki::Size, dim> const&,                                        \
    int const,                                                                 \
    int const) const -> Nodal const&;                                          \
  template auto Nodal::eval<dim>(                                              \
    std::array<toki::Number, dim> const&,                                      \
    std::array<toki::Size, dim> const&,                                        \
    int const,                                                                 \
    int const) const -> toki::Number;                                          \
  /* */
#define BOOST_PP_LOCAL_LIMITS (1, 4)
#include BOOST_PP_LOCAL_ITERATE()
} // namespace toki::basis
