#include "boost/preprocessor/iteration/local.hpp"
#include "toki/basis/nodal.hpp"
#include "toki/math/index.hpp"
#include "toki/utility.hpp"
namespace toki::basis {
template<int dim>
auto
Nodal::fill(
  toki::vector::Vec& sample,
  toki::Size const& offset,
  std::function<toki::Number(std::array<toki::Number, dim> const&)> const& op)
  const -> Nodal const&
{
  toki::utility::confirm(sample.valid_at(offset), "invalid parameter: offset");
  std::array<toki::Size, dim> size{};
  toki::Size dof_total{1};
  for (int d = 0; d < dim; ++d) {
    size[d] = info.point;
    dof_total *= info.point;
  }
  toki::utility::confirm(
    sample.valid_at(offset + dof_total - 1), "invalid parameter: sample");
  std::array<toki::Number, dim> coord{};
  for (toki::Size dof = 0; dof < dof_total; ++dof) {
    auto index = toki::math::index::ind2sub<dim>(size, dof);
    for (int d = 0; d < dim; ++d) {
      coord[d] = info.abscissa.at(index[d]);
    }
    sample.set(op(coord), offset + dof);
  }
  return *this;
}
} // namespace toki::basis
namespace toki::basis {
#define BOOST_PP_LOCAL_MACRO(dim)                                              \
  template auto Nodal::fill<dim>(                                              \
    toki::vector::Vec&,                                                        \
    toki::Size const&,                                                         \
    std::function<toki::Number(std::array<toki::Number, dim> const&)> const&)  \
    const -> Nodal const&;                                                     \
  /* */
#define BOOST_PP_LOCAL_LIMITS (1, 4)
#include BOOST_PP_LOCAL_ITERATE()
} // namespace toki::basis
