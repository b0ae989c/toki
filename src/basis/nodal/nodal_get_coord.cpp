#include "boost/preprocessor/iteration/local.hpp"
#include "toki/basis/nodal.hpp"
#include "toki/math/index.hpp"
namespace toki::basis {
template<int dim>
auto
Nodal::get_coord(toki::Size const& point) const -> std::array<toki::Number, dim>
{
  std::array<toki::Size, dim> size{};
  for (int d = 0; d < dim; ++d) {
    size[d] = info.point;
  }
  std::array<toki::Number, dim> coord;
  auto index = toki::math::index::ind2sub<dim>(size, point);
  for (int d = 0; d < dim; ++d) {
    coord[d] = info.abscissa.at(index[d]);
  }
  return coord;
}
} // namespace toki::basis
namespace toki::basis {
#define BOOST_PP_LOCAL_MACRO(dim)                                              \
  template auto Nodal::get_coord<dim>(toki::Size const&)                       \
    const -> std::array<toki::Number, dim>;                                    \
  /* */
#define BOOST_PP_LOCAL_LIMITS (1, 4)
#include BOOST_PP_LOCAL_ITERATE()
} // namespace toki::basis
