#include "toki/context/default.hpp"
#include "mpi.h"
namespace toki::context {
Default::Default()
{
  MPI_Init(NULL, NULL);
}
Default::Default(int argc, char** argv)
{
  MPI_Init(&argc, &argv);
}
Default::~Default()
{
  MPI_Finalize();
}
} // namespace toki::context
