#include "mpi.h"
#include "toki/context/default.hpp"
namespace toki::context {
auto
Default::guard(std::function<void()> const& routine) const -> void
{
  routine();
  MPI_Barrier(MPI_COMM_WORLD);
  return;
}
} // namespace toki::context
