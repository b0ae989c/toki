#include "toki/data/detail/async_mpi_dynamic.hpp"
namespace toki::data::detail {
Async_MPI_Dynamic::Async_MPI_Dynamic()
  : info{}
{
}
Async_MPI_Dynamic::Async_MPI_Dynamic(Async_MPI_Dynamic const& other)
  : info(other.info)
{
  for (auto& item : info.window) {
    auto const& remote_name = item.first;
    auto const& offset = info.buffer.at(remote_name).offset;
    item.second = MPI_Win{};
    MPI_Win_create(
      info.buffer.at(remote_name).content.data.get(),
      sizeof(toki::Number) * offset.back(),
      sizeof(toki::Number),
      MPI_INFO_NULL,
      MPI_COMM_WORLD,
      &(item.second));
    MPI_Win_fence(0, item.second);
  }
}
Async_MPI_Dynamic::Async_MPI_Dynamic(Async_MPI_Dynamic&& other) noexcept
  : info(std::exchange(other.info, decltype(info){}))
{
}
Async_MPI_Dynamic::~Async_MPI_Dynamic()
{
  for (auto& item : info.window) {
    MPI_Win_fence(0, item.second);
    MPI_Win_free(&(item.second));
  }
}
auto
Async_MPI_Dynamic::operator=(Async_MPI_Dynamic other) -> Async_MPI_Dynamic&
{
  swap(*this, other);
  return *this;
}
} // namespace toki::data::detail
