#include "toki/data/detail/async_mpi_dynamic.hpp"
#include "toki/utility.hpp"
namespace toki::data::detail {
auto
Async_MPI_Dynamic::at(
  std::string const& name,
  toki::Size const& index,
  toki::Size const& point) const -> toki::Number const&
{
  toki::utility::confirm(info.buffer.contains(name), "invalid parameter: name");
  auto& buffer = info.buffer.at(name);
  if (index) {
    return buffer.content.at(point + buffer.offset.at(index - 1));
  } else {
    return buffer.content.at(point);
  }
}
} // namespace toki::data::detail
