#include "toki/data/detail/async_mpi_dynamic.hpp"
#include "toki/utility.hpp"
namespace toki::data::detail {
auto
Async_MPI_Dynamic::attach_buffer(
  toki::vector::Vec&& content,
  std::string const& name,
  toki::Size const& size,
  std::vector<toki::Size> const& offset) -> Async_MPI_Dynamic&
{
  toki::utility::confirm(size <= offset.back(), "invalid parameter: offset");
  toki::utility::confirm(
    offset.back() == content.extent[0], "invalid parameter: content");
  if (!info.buffer.contains(name)) {
    return create_buffer(
      std::forward<toki::vector::Vec>(content), name, size, offset);
  }
  info.buffer.at(name).content = std::move(content);
  info.buffer.at(name).size = size;
  info.buffer.at(name).offset = offset;
  return *this;
}
} // namespace toki::data::detail
