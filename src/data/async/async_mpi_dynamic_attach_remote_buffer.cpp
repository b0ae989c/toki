#include "toki/data/detail/async_mpi_dynamic.hpp"
#include "toki/utility.hpp"
namespace toki::data::detail {
auto
Async_MPI_Dynamic::attach_remote_buffer(
  toki::vector::Vec&& content,
  std::string const& name,
  toki::Size const& size,
  std::vector<toki::Size> const& offset,
  int const remote_mpi_rank,
  std::string const& remote_name) -> Async_MPI_Dynamic&
{
  toki::utility::confirm(size <= offset.back(), "invalid parameter: offset");
  toki::utility::confirm(
    offset.back() == content.extent[0], "invalid parameter: content");
  if (!info.buffer.contains(name)) {
    return create_remote_buffer(
      std::forward<toki::vector::Vec>(content),
      name,
      size,
      offset,
      remote_mpi_rank,
      remote_name);
  }
  info.buffer.at(name).content = std::move(content);
  info.buffer.at(name).size = size;
  info.buffer.at(name).offset = offset;
  info.buffer.at(name).flag_remote = true;
  info.buffer.at(name).remote_mpi_rank = remote_mpi_rank;
  info.buffer.at(name).remote_name = remote_name;
  create_buffer(remote_name, size, offset, true);
  if (info.window.contains(remote_name)) {
    MPI_Win_fence(0, info.window.at(remote_name));
    MPI_Win_free(&(info.window.at(remote_name)));
  }
  info.window.insert_or_assign(remote_name, MPI_Win{});
  MPI_Win_create(
    info.buffer.at(remote_name).content.data.get(),
    sizeof(toki::Number) * offset.back(),
    sizeof(toki::Number),
    MPI_INFO_NULL,
    MPI_COMM_WORLD,
    &(info.window.at(remote_name)));
  MPI_Win_fence(0, info.window.at(remote_name));
  return *this;
}
} // namespace toki::data::detail
