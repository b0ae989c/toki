#include "toki/data/detail/async_mpi_dynamic.hpp"
#include "toki/utility.hpp"
namespace toki::data::detail {
auto
Async_MPI_Dynamic::create_buffer(
  std::string const& name,
  toki::Size const& size,
  toki::Size const& dof,
  bool const replace) -> Async_MPI_Dynamic&
{
  if (!replace) {
    toki::utility::confirm(
      !info.buffer.contains(name), "invalid parameter: name");
  }
  std::vector<toki::Size> offset{};
  offset.reserve(size);
  for (toki::Size index = 0; index < size; ++index) {
    offset.push_back(dof * (index + 1));
  }
  return create_buffer(name, size, offset, replace);
}
auto
Async_MPI_Dynamic::create_buffer(
  std::string const& name,
  toki::Size const& size,
  std::vector<toki::Size> const& offset,
  bool const replace) -> Async_MPI_Dynamic&
{
  if (!replace) {
    toki::utility::confirm(
      !info.buffer.contains(name), "invalid parameter: name");
  }
  toki::utility::confirm(size <= offset.back(), "invalid parameter: offset");
  info.buffer.insert_or_assign(
    name,
    Buffer{
      .content = toki::vector::Vec(offset.back()),
      .size = size,
      .offset = offset,
    });
  return *this;
}
auto
Async_MPI_Dynamic::create_buffer(
  toki::vector::Vec&& content,
  std::string const& name,
  toki::Size const& size,
  std::vector<toki::Size> const& offset,
  bool const replace) -> Async_MPI_Dynamic&
{
  if (!replace) {
    toki::utility::confirm(
      !info.buffer.contains(name), "invalid parameter: name");
  }
  toki::utility::confirm(size <= offset.back(), "invalid parameter: offset");
  toki::utility::confirm(
    offset.back() == content.extent[0], "invalid parameter: content");
  info.buffer.insert_or_assign(
    name,
    Buffer{
      .content = std::move(content),
      .size = size,
      .offset = offset,
    });
  return *this;
}
} // namespace toki::data::detail
