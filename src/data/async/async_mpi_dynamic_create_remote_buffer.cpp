#include "toki/data/detail/async_mpi_dynamic.hpp"
#include "toki/utility.hpp"
namespace toki::data::detail {
auto
Async_MPI_Dynamic::create_remote_buffer(
  std::string const& name,
  toki::Size const& size,
  toki::Size const& dof,
  int const remote_mpi_rank,
  std::string const& remote_name) -> Async_MPI_Dynamic&
{
  toki::utility::confirm(
    !info.buffer.contains(name), "invalid parameter: name");
  toki::utility::confirm(
    !info.buffer.contains(remote_name), "invalid parameter: remote_name");
  std::vector<toki::Size> offset{};
  offset.reserve(size);
  for (toki::Size index = 0; index < size; ++index) {
    offset.push_back(dof * (index + 1));
  }
  return create_remote_buffer(name, size, offset, remote_mpi_rank, remote_name);
}
auto
Async_MPI_Dynamic::create_remote_buffer(
  std::string const& name,
  toki::Size const& size,
  std::vector<toki::Size> const& offset,
  int const remote_mpi_rank,
  std::string const& remote_name) -> Async_MPI_Dynamic&
{
  toki::utility::confirm(
    !info.buffer.contains(name), "invalid parameter: name");
  toki::utility::confirm(
    !info.buffer.contains(remote_name), "invalid parameter: remote_name");
  toki::utility::confirm(size == offset.back(), "invalid parameter: offset");
  info.buffer.insert_or_assign(
    name,
    Buffer{
      .content = toki::vector::Vec(offset.back()),
      .size = size,
      .offset = offset,
      .flag_remote = true,
      .remote_mpi_rank = remote_mpi_rank,
      .remote_name = remote_name,
    });
  create_buffer(remote_name, size, offset);
  info.window.insert_or_assign(remote_name, MPI_Win{});
  MPI_Win_create(
    info.buffer.at(remote_name).content.data.get(),
    sizeof(toki::Number) * offset.back(),
    sizeof(toki::Number),
    MPI_INFO_NULL,
    MPI_COMM_WORLD,
    &(info.window.at(remote_name)));
  MPI_Win_fence(0, info.window.at(remote_name));
  return *this;
}
auto
Async_MPI_Dynamic::create_remote_buffer(
  toki::vector::Vec&& content,
  std::string const& name,
  toki::Size const& size,
  std::vector<toki::Size> const& offset,
  int const remote_mpi_rank,
  std::string const& remote_name) -> Async_MPI_Dynamic&
{
  toki::utility::confirm(
    !info.buffer.contains(name), "invalid parameter: name");
  toki::utility::confirm(
    !info.buffer.contains(remote_name), "invalid parameter: remote_name");
  toki::utility::confirm(size == offset.back(), "invalid parameter: offset");
  toki::utility::confirm(
    offset.back() == content.extent[0], "invalid parameter: content");
  info.buffer.insert_or_assign(
    name,
    Buffer{
      .content = std::move(content),
      .size = size,
      .offset = offset,
      .flag_remote = true,
      .remote_mpi_rank = remote_mpi_rank,
      .remote_name = remote_name,
    });
  create_buffer(remote_name, size, offset);
  info.window.insert_or_assign(remote_name, MPI_Win{});
  MPI_Win_create(
    info.buffer.at(remote_name).content.data.get(),
    sizeof(toki::Number) * offset.back(),
    sizeof(toki::Number),
    MPI_INFO_NULL,
    MPI_COMM_WORLD,
    &(info.window.at(remote_name)));
  MPI_Win_fence(0, info.window.at(remote_name));
  return *this;
}
} // namespace toki::data::detail
