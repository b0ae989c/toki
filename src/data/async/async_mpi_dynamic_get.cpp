#include "toki/data/detail/async_mpi_dynamic.hpp"
#include "toki/utility.hpp"
namespace toki::data::detail {
auto
Async_MPI_Dynamic::get(std::string const& name, toki::Size const& index) const
  -> toki::Number*
{
  toki::utility::confirm(info.buffer.contains(name), "invalid parameter: name");
  toki::utility::confirm(
    info.buffer.at(name).content.valid_at(index), "invalid parameter: index");
  auto& buffer = info.buffer.at(name);
  if (index) {
    return buffer.content.data.get() + buffer.offset.at(index - 1);
  } else {
    return buffer.content.data.get();
  }
}
} // namespace toki::data::detail
