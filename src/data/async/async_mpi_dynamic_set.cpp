#include <algorithm>
#include "toki/data/detail/async_mpi_dynamic.hpp"
#include "toki/utility.hpp"
namespace toki::data::detail {
auto
Async_MPI_Dynamic::set(
  std::string const& name,
  toki::Number const& value,
  toki::Size const& index,
  toki::Size const& point) -> Async_MPI_Dynamic&
{
  toki::utility::confirm(info.buffer.contains(name), "invalid parameter: name");
  toki::utility::confirm(
    index < info.buffer.at(name).size, "invalid parameter: index");
  auto& buffer = info.buffer.at(name);
  auto offset = point;
  if (index) {
    offset += buffer.offset.at(index - 1);
  }
  buffer.content.set(value, offset);
  return *this;
}
auto
Async_MPI_Dynamic::set(
  std::string const& name,
  toki::Number const& value,
  toki::Size const& index) -> Async_MPI_Dynamic&
{
  toki::utility::confirm(info.buffer.contains(name), "invalid parameter: name");
  toki::utility::confirm(
    index < info.buffer.at(name).size, "invalid parameter: index");
  auto& buffer = info.buffer.at(name);
  toki::Size offset_begin = 0;
  if (index) {
    offset_begin = buffer.offset.at(index - 1);
  }
  for (toki::Size offset = offset_begin; offset < buffer.offset.at(index);
       ++offset) {
    buffer.content.set(value, offset);
  }
  return *this;
}
auto
Async_MPI_Dynamic::set(
  std::string const& name,
  toki::vector::Vec const& content,
  toki::Size const& offset) -> Async_MPI_Dynamic&
{
  toki::utility::confirm(info.buffer.contains(name), "invalid parameter: name");
  toki::utility::confirm(content.valid_at(offset), "invalid parameter: offset");
  auto& buffer = info.buffer.at(name);
  toki::utility::confirm(
    content.valid_at(offset + buffer.offset.back() - 1),
    "invalid parameter: offset");
  std::copy(
    content.data.get() + offset,
    content.data.get() + offset + buffer.offset.back(),
    buffer.content.data.get());
  return *this;
}
auto
Async_MPI_Dynamic::set(
  std::string const& name,
  std::function<toki::Number(toki::Size const&, toki::Size const&)> const& op)
  -> Async_MPI_Dynamic&
{
  toki::utility::confirm(info.buffer.contains(name), "invalid parameter: name");
  auto& buffer = info.buffer.at(name);
  for (toki::Size point = 0; point < buffer.offset.at(0); ++point) {
    buffer.content.set(op(0, point), point);
  }
  for (toki::Size index = 1; index < buffer.offset.size(); ++index) {
    auto const point_max =
      buffer.offset.at(index) - buffer.offset.at(index - 1);
    for (toki::Size point = 0; point < point_max; ++point) {
      buffer.content.set(op(index, point), point + buffer.offset.at(index - 1));
    }
  }
  return *this;
}
} // namespace toki::data::detail
