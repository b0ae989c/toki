#include "toki/data/detail/async_mpi_dynamic.hpp"
#include "toki/utility.hpp"
namespace toki::data::detail {
auto
Async_MPI_Dynamic::wait(std::string const& name) -> Async_MPI_Dynamic&
{
  toki::utility::confirm(info.buffer.contains(name), "invalid parameter: name");
  toki::utility::confirm(
    info.buffer.at(name).flag_remote, "invalid parameter: name");
  MPI_Win_fence(0, info.window.at(info.buffer.at(name).remote_name));
  return *this;
}
auto
Async_MPI_Dynamic::wait() -> Async_MPI_Dynamic&
{
  for (auto const& item : info.buffer) {
    if (item.second.flag_remote) {
      wait(item.first);
    }
  }
  return *this;
}
} // namespace toki::data::detail
