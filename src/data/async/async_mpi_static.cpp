#include "toki/data/detail/async_mpi_static.hpp"
namespace toki::data::detail {
Async_MPI_Static::Async_MPI_Static()
  : info{}
{
}
Async_MPI_Static::Async_MPI_Static(Async_MPI_Static const& other)
  : info(other.info)
{
  for (auto& item : info.window) {
    auto const& remote_name = item.first;
    auto const& size = info.buffer.at(remote_name).size;
    auto const& dof = info.buffer.at(remote_name).dof;
    item.second = MPI_Win{};
    MPI_Win_create(
      info.buffer.at(remote_name).content.data.get(),
      sizeof(toki::Number) * size * dof,
      sizeof(toki::Number),
      MPI_INFO_NULL,
      MPI_COMM_WORLD,
      &(item.second));
    MPI_Win_fence(0, item.second);
  }
}
Async_MPI_Static::Async_MPI_Static(Async_MPI_Static&& other) noexcept
  : info(std::exchange(other.info, decltype(info){}))
{
}
Async_MPI_Static::~Async_MPI_Static()
{
  for (auto& item : info.window) {
    MPI_Win_fence(0, item.second);
    MPI_Win_free(&(item.second));
  }
}
auto
Async_MPI_Static::operator=(Async_MPI_Static other) -> Async_MPI_Static&
{
  swap(*this, other);
  return *this;
}
} // namespace toki::data::detail
