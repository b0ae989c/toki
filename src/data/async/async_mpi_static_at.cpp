#include "toki/data/detail/async_mpi_static.hpp"
#include "toki/utility.hpp"
namespace toki::data::detail {
auto
Async_MPI_Static::at(
  std::string const& name,
  toki::Size const& index,
  toki::Size const& point) const -> toki::Number const&
{
  toki::utility::confirm(info.buffer.contains(name), "invalid parameter: name");
  auto& buffer = info.buffer.at(name);
  return buffer.content.at(point + buffer.dof * index);
}
} // namespace toki::data::detail
