#include "toki/data/detail/async_mpi_static.hpp"
#include "toki/utility.hpp"
namespace toki::data::detail {
auto
Async_MPI_Static::create_buffer(
  std::string const& name,
  toki::Size const& size,
  toki::Size const& dof) -> Async_MPI_Static&
{
  toki::utility::confirm(
    !info.buffer.contains(name), "invalid parameter: name");
  info.buffer.insert_or_assign(
    name,
    Buffer{
      .content = toki::vector::Vec(size * dof),
      .size = size,
      .dof = dof,
    });
  return *this;
}
} // namespace toki::data::detail
