#include "toki/data/detail/async_mpi_static.hpp"
#include "toki/utility.hpp"
namespace toki::data::detail {
auto
Async_MPI_Static::create_remote_buffer(
  std::string const& name,
  toki::Size const& size,
  toki::Size const& dof,
  int const remote_mpi_rank,
  std::string const& remote_name) -> Async_MPI_Static&
{
  toki::utility::confirm(
    !info.buffer.contains(name), "invalid parameter: name");
  toki::utility::confirm(
    !info.buffer.contains(remote_name), "invalid parameter: remote_name");
  info.buffer.insert_or_assign(
    name,
    Buffer{
      .content = toki::vector::Vec(size * dof),
      .size = size,
      .dof = dof,
      .flag_remote = true,
      .remote_mpi_rank = remote_mpi_rank,
      .remote_name = remote_name,
    });
  create_buffer(remote_name, size, dof);
  info.window.insert_or_assign(remote_name, MPI_Win{});
  MPI_Win_create(
    info.buffer.at(remote_name).content.data.get(),
    sizeof(toki::Number) * size * dof,
    sizeof(toki::Number),
    MPI_INFO_NULL,
    MPI_COMM_WORLD,
    &(info.window.at(remote_name)));
  MPI_Win_fence(0, info.window.at(remote_name));
  return *this;
}
} // namespace toki::data::detail
