#include "toki/data/detail/async_mpi_static.hpp"
#include "toki/utility.hpp"
namespace toki::data::detail {
auto
Async_MPI_Static::get(std::string const& name, toki::Size const& index) const
  -> toki::Number*
{
  toki::utility::confirm(info.buffer.contains(name), "invalid parameter: name");
  toki::utility::confirm(
    info.buffer.at(name).content.valid_at(index), "invalid parameter: index");
  auto& buffer = info.buffer.at(name);
  return buffer.content.data.get() + index * buffer.dof;
}
} // namespace toki::data::detail
