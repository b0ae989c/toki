#include <algorithm>
#include "toki/data/detail/async_mpi_static.hpp"
#include "toki/utility.hpp"
namespace toki::data::detail {
auto
Async_MPI_Static::set(
  std::string const& name,
  toki::Number const& value,
  toki::Size const& index,
  toki::Size const& point) -> Async_MPI_Static&
{
  toki::utility::confirm(info.buffer.contains(name), "invalid parameter: name");
  toki::utility::confirm(
    index < info.buffer.at(name).size, "invalid parameter: index");
  auto& buffer = info.buffer.at(name);
  buffer.content.set(value, point + buffer.dof * index);
  return *this;
}
auto
Async_MPI_Static::set(
  std::string const& name,
  toki::Number const& value,
  toki::Size const& index) -> Async_MPI_Static&
{
  toki::utility::confirm(info.buffer.contains(name), "invalid parameter: name");
  toki::utility::confirm(
    index < info.buffer.at(name).size, "invalid parameter: index");
  auto& buffer = info.buffer.at(name);
  auto const& dof = buffer.dof;
  for (toki::Size point = 0; point < dof; ++point) {
    buffer.content.set(value, point + dof * index);
  }
  return *this;
}
auto
Async_MPI_Static::set(
  std::string const& name,
  toki::vector::Vec const& content,
  toki::Size const& offset) -> Async_MPI_Static&
{
  toki::utility::confirm(info.buffer.contains(name), "invalid parameter: name");
  toki::utility::confirm(content.valid_at(offset), "invalid parameter: offset");
  auto& buffer = info.buffer.at(name);
  toki::utility::confirm(
    content.valid_at(offset + buffer.size - 1), "invalid: parameter: offset");
  std::copy(
    content.data.get() + offset,
    content.data.get() + offset + buffer.size,
    buffer.content.data.get());
  return *this;
}
auto
Async_MPI_Static::set(
  std::string const& name,
  std::function<toki::Number(toki::Size const&, toki::Size const&)> const& op)
  -> Async_MPI_Static&
{
  toki::utility::confirm(info.buffer.contains(name), "invalid parameter: name");
  auto& buffer = info.buffer.at(name);
  auto const dof = buffer.dof;
  buffer.content.set([&](toki::Size const& offset) -> toki::Number {
    auto const index = offset / dof;
    auto const point = offset % dof;
    return op(index, point);
  });
  return *this;
}
} // namespace toki::data::detail
