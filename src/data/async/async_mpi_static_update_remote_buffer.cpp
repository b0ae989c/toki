#include "toki/data/detail/async_mpi_static.hpp"
#include "toki/utility.hpp"
namespace toki::data::detail {
auto
Async_MPI_Static::update_remote_buffer(std::string const& name)
  -> Async_MPI_Static&
{
  auto& buffer = info.buffer.at(name);
  toki::utility::confirm(buffer.flag_remote, "invalid parameter: name");
  toki::utility::confirm(
    buffer.remote_mpi_rank >= 0, "invalid parameter: name");
  auto const buffer_size = buffer.size * buffer.dof;
  MPI_Get(
    buffer.content.data.get(),
    buffer_size,
    MPI_DOUBLE,
    buffer.remote_mpi_rank,
    0,
    buffer_size,
    MPI_DOUBLE,
    info.window.at(buffer.remote_name));
  return *this;
}
auto
Async_MPI_Static::update_remote_buffer() -> Async_MPI_Static&
{
  for (auto const& item : info.buffer) {
    if (item.second.flag_remote) {
      update_remote_buffer(item.first);
    }
  }
  return *this;
}
} // namespace toki::data::detail
