#include "toki/data/detail/async_mpi_static.hpp"
#include "toki/utility.hpp"
namespace toki::data::detail {
auto
Async_MPI_Static::wait(std::string const& name) -> Async_MPI_Static&
{
  toki::utility::confirm(info.buffer.contains(name), "invalid parameter: name");
  toki::utility::confirm(
    info.buffer.at(name).flag_remote, "invalid parameter: name");
  MPI_Win_fence(0, info.window.at(info.buffer.at(name).remote_name));
  return *this;
}
auto
Async_MPI_Static::wait() -> Async_MPI_Static&
{
  for (auto const& item : info.buffer) {
    if (item.second.flag_remote) {
      wait(item.first);
    }
  }
  return *this;
}
} // namespace toki::data::detail
