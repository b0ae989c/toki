set(CPP_SOURCE
    orthotope_dg_get_element.cpp
    orthotope_dg_set_element.cpp
    orthotope_dg_adjust_element.cpp
    orthotope_dg_get_stencil.cpp
    orthotope_dg_get_coord.cpp
    orthotope_dg_get_min.cpp
    orthotope_dg_get_max.cpp
    orthotope_dg_1d.cpp
    orthotope_dg_1d_halo_dispatch.cpp
    orthotope_dg_1d_halo_wait.cpp
    orthotope_dg_1d_halo_update.cpp
    orthotope_dg_1d_save.cpp
    orthotope_dg_2d.cpp
    orthotope_dg_2d_halo_dispatch.cpp
    orthotope_dg_2d_halo_wait.cpp
    orthotope_dg_2d_halo_update.cpp
    orthotope_dg_2d_save.cpp
    orthotope_dg_p_get_element.cpp
    orthotope_dg_p_set_element.cpp
    orthotope_dg_p_adjust_element.cpp
    orthotope_dg_p_project_element.cpp
    orthotope_dg_p_get_dof.cpp
    orthotope_dg_p_get_basis.cpp
    orthotope_dg_p_get_quadrature.cpp
    orthotope_dg_p_get_data_offset.cpp
    orthotope_dg_p_get_refinement_score.cpp
    orthotope_dg_p_get_coord.cpp
    orthotope_dg_p_1d.cpp
    orthotope_dg_p_1d_halo_dispatch.cpp
    orthotope_dg_p_1d_halo_wait.cpp
    orthotope_dg_p_1d_halo_update.cpp
    orthotope_dg_p_2d.cpp
    orthotope_dg_p_2d_halo_dispatch.cpp
    orthotope_dg_p_2d_halo_wait.cpp
    orthotope_dg_p_2d_halo_update.cpp
    orthotope_dg_p_2d_refine.cpp
    orthotope_dg_p_2d_adjust_remote_buffer.cpp
    orthotope_dg_p_2d_save.cpp
)

target_sources(${TOKI_LIB_DEBUG} PRIVATE ${CPP_SOURCE})

target_sources(${TOKI_LIB_RELEASE} PRIVATE ${CPP_SOURCE})
