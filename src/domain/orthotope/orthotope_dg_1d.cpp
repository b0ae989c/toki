#include "mpi.h"
#include "toki/domain/detail/orthotope_dg.hpp"
#include "toki/math.hpp"
namespace toki::domain::detail {
template<int dim>
Orthotope_DG<dim>::Orthotope_DG()
  : info{}
  , data{}
{
}
template<int dim>
Orthotope_DG<dim>::Orthotope_DG(
  toki::Size const& dof,
  toki::Size const& quadrature_point,
  std::array<toki::Size, dim> const& resolution,
  std::array<toki::Size, dim> const& halo,
  std::array<toki::Number, dim> const& range,
  std::array<toki::Number, dim> const& anchor,
  std::array<int, dim> const& mpi_extent)
  : info({
      .dof = dof,
      .quadrature_point = quadrature_point,
      .basis = toki::basis::Nodal(quadrature_point),
      .resolution = resolution,
      .halo = halo,
      .range = range,
      .anchor = anchor,
      .mpi_extent = mpi_extent,
    })
  , data{}
{
  // mpi
  MPI_Comm_rank(MPI_COMM_WORLD, &(info.local.mpi.rank));
  info.local.mpi.index =
    toki::math::index::ind2sub<dim>(info.mpi_extent, info.local.mpi.rank);
  for (int d = 0; d < dim; ++d) {
    info.local.mpi.neighbor[d].n = toki::math::index::sub2ind<dim>(
      info.mpi_extent,
      toki::math::index::shift(
        info.mpi_extent, info.local.mpi.index, d, -1, 1));
    info.local.mpi.neighbor[d].p = toki::math::index::sub2ind<dim>(
      info.mpi_extent,
      toki::math::index::shift(
        info.mpi_extent, info.local.mpi.index, d, +1, 1));
  }
  info.local.element.N = 1;
  for (int d = 0; d < dim; ++d) {
    info.local.resolution[d] = info.resolution[d] / info.mpi_extent[d];
    info.local.element.volume[d] =
      info.range[d] / static_cast<toki::Number>(info.resolution[d]);
    info.local.range[d] = info.local.element.volume[d] *
                          static_cast<toki::Number>(info.local.resolution[d]);
    info.local.anchor[d] =
      info.anchor[d] +
      info.local.range[d] * static_cast<toki::Number>(info.local.mpi.index[d]);
    // halo
    info.local.resolution[d] += 2 * info.halo[d];
    info.local.range[d] += info.local.element.volume[d] *
                           static_cast<toki::Number>(2 * info.halo[d]);
    info.local.anchor[d] -=
      info.local.element.volume[d] * static_cast<toki::Number>(info.halo[d]);
    // element.N
    info.local.element.N *= info.local.resolution[d];
  }
  // data
  data.create_buffer("main", info.local.element.N, info.dof);
  data.create_remote_buffer(
    "halo_outer_n",
    info.halo[0],
    info.dof,
    info.local.mpi.neighbor[0].n,
    "halo_inner_p");
  data.create_remote_buffer(
    "halo_outer_p",
    info.halo[0],
    info.dof,
    info.local.mpi.neighbor[0].p,
    "halo_inner_n");
}
template<int dim>
Orthotope_DG<dim>::Orthotope_DG(Orthotope_DG const& other)
  : info(other.info)
  , data(other.data)
{
}
template<int dim>
Orthotope_DG<dim>::Orthotope_DG(Orthotope_DG&& other) noexcept
  : info(std::exchange(other.info, decltype(info){}))
  , data(std::exchange(other.data, decltype(data){}))
{
}
template<int dim>
Orthotope_DG<dim>::~Orthotope_DG()
{
}
template<int dim>
auto
Orthotope_DG<dim>::operator=(Orthotope_DG other) -> Orthotope_DG&
{
  swap(*this, other);
  return *this;
}
} // namespace toki::domain::detail
template struct toki::domain::detail::Orthotope_DG<1>;
