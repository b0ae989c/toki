#include <cstring>
#include <fstream>
#include <string>
#include "boost/endian.hpp"
#include "fmt/format.h"
#include "toki/basis/nodal.hpp"
#include "toki/domain/detail/orthotope_dg.hpp"
namespace toki::domain::detail {
template<int dim>
auto
Orthotope_DG<dim>::save(std::string const& name, toki::Size const& id)
  -> Orthotope_DG&
{
  auto write_string = [](
                        std::ofstream& file,
                        std::string const& line,
                        bool const newline = true) -> void {
    file.write(line.c_str(), line.size());
    if (newline) {
      file.write("\n", 1);
    }
    return;
  };
  auto write_double =
    [](std::ofstream& file, double value, bool const newline = true) -> void {
    char buffer[8];
    boost::endian::native_to_big_inplace(value);
    std::memcpy(buffer, &value, 8);
    file.write(buffer, 8);
    if (newline) {
      file.write("\n", 1);
    }
  };
  auto title = fmt::format("{}-{:09}-{:04}", name, id, info.local.mpi.rank);
  auto filename = fmt::format("{}.vtk", title);
  std::ofstream file(filename, std::ofstream::binary);
  // header
  write_string(file, "# vtk DataFile Version 3.0");
  write_string(file, title);
  write_string(file, "BINARY");
  // metadata
  write_string(file, "DATASET RECTILINEAR_GRID");
  // coordinate
  auto const& point = info.quadrature_point;
  toki::basis::Nodal basis(point);
  std::array<toki::Size, dim> N{
    info.local.resolution[0] - 2 * info.halo[0],
    info.local.resolution[1] - 2 * info.halo[1],
  };
  write_string(
    file, fmt::format("DIMENSIONS {} {} 1", point * N[0], point * N[1]));
  write_string(file, fmt::format("X_COORDINATES {} double", point * N[0]));
  for (toki::Size index_0 = 0; index_0 < N[0]; ++index_0) {
    for (toki::Size ell = 0; ell < point; ++ell) {
      write_double(
        file,
        info.local.anchor[0] +
          (static_cast<toki::Number>(index_0 + info.halo[0]) +
           (basis.info.abscissa.at(ell) + 1.0) / 2.0) *
            info.local.element.volume[0],
        false);
    }
  }
  write_string(file, fmt::format("\nY_COORDINATES {} double", point * N[1]));
  for (toki::Size index_1 = 0; index_1 < N[1]; ++index_1) {
    for (toki::Size ell = 0; ell < point; ++ell) {
      write_double(
        file,
        info.local.anchor[1] +
          (static_cast<toki::Number>(index_1 + info.halo[1]) +
           (basis.info.abscissa.at(ell) + 1.0) / 2.0) *
            info.local.element.volume[1],
        false);
    }
  }
  write_string(file, "\nZ_COORDINATES 1 double");
  write_double(file, 0.0);
  // mesh
  write_string(file, fmt::format("POINT_DATA {}", info.dof * N[0] * N[1]));
  write_string(file, fmt::format("SCALARS {} double 1", name));
  write_string(file, "LOOKUP_TABLE default");
  toki::vector::Vec sample(info.dof);
  toki::Number value{};
  for (toki::Size index_1 = 0; index_1 < N[1]; ++index_1) {
    for (toki::Size index_basis_1 = 0; index_basis_1 < point; ++index_basis_1) {
      for (toki::Size index_0 = 0; index_0 < N[0]; ++index_0) {
        for (toki::Size index_basis_0 = 0; index_basis_0 < point;
             ++index_basis_0) {
          get_element(
            sample,
            0,
            std::array<toki::Size, dim>{
              index_0 + info.halo[0],
              index_1 + info.halo[1],
            });
          basis.approx<2>(
            value,
            {
              basis.info.abscissa.at(index_basis_0),
              basis.info.abscissa.at(index_basis_1),
            },
            sample,
            0);
          write_double(file, value, false);
        }
      }
    }
  }
  file.close();
  return *this;
}
} // namespace toki::domain::detail
template struct toki::domain::detail::Orthotope_DG<2>;
