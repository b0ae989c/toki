#include "toki/domain/detail/orthotope_dg.hpp"
#include "toki/math/index.hpp"
#include "toki/utility.hpp"
namespace toki::domain::detail {
template<int dim>
auto
Orthotope_DG<dim>::adjust_element(
  toki::Number const& delta,
  std::array<toki::Size, dim> const& index) -> Orthotope_DG&
{
  auto const& dof = data.info.buffer.at("main").dof;
  auto& content = data.info.buffer.at("main").content;
  auto const offset_element =
    toki::math::index::sub2ind<dim>(info.local.resolution, index);
  for (toki::Size point = 0; point < dof; ++point) {
    content.set(
      content.at(point + dof * offset_element) + delta,
      point + dof * offset_element);
  }
  return *this;
}
template<int dim>
auto
Orthotope_DG<dim>::adjust_element(
  toki::vector::Vec const& delta,
  toki::Size const& offset,
  std::array<toki::Size, dim> const& index) -> Orthotope_DG&
{
  auto const& dof = data.info.buffer.at("main").dof;
  auto& content = data.info.buffer.at("main").content;
  auto const offset_element =
    toki::math::index::sub2ind<dim>(info.local.resolution, index);
  for (toki::Size point = 0; point < dof; ++point) {
    content.set(
      content.at(point + dof * offset_element) + delta.at(offset + point),
      point + dof * offset_element);
  }
  return *this;
}
} // namespace toki::domain::detail
template struct toki::domain::detail::Orthotope_DG<1>;
template struct toki::domain::detail::Orthotope_DG<2>;
