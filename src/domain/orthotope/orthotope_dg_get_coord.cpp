#include "toki/basis/nodal.hpp"
#include "toki/domain/detail/orthotope_dg.hpp"
namespace toki::domain::detail {
template<int dim>
auto
Orthotope_DG<dim>::get_coord(
  std::array<toki::Size, dim> const& index,
  toki::Size const& point) const -> std::array<toki::Number, dim>
{
  auto coord = info.basis.template get_coord<dim>(point);
  for (int d = 0; d < dim; ++d) {
    coord[d] = info.local.anchor[d] +
               (static_cast<toki::Number>(index[d]) + (coord[d] + 1.0) / 2.0) *
                 info.local.element.volume[d];
  }
  return coord;
}
} // namespace toki::domain::detail
template struct toki::domain::detail::Orthotope_DG<1>;
template struct toki::domain::detail::Orthotope_DG<2>;
