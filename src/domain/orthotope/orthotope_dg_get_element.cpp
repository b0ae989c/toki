#include <algorithm>
#include "toki/domain/detail/orthotope_dg.hpp"
#include "toki/math/index.hpp"
#include "toki/utility.hpp"
namespace toki::domain::detail {
template<int dim>
auto
Orthotope_DG<dim>::get_element(
  toki::vector::Vec& output,
  toki::Size const& offset,
  std::array<toki::Size, dim> const& index) -> Orthotope_DG&
{
  toki::utility::confirm(output.valid_at(offset), "invalid parameter: offset");
  toki::utility::confirm(
    output.valid_at(offset + info.dof - 1), "invalid parameter: offset");
  auto const offset_element =
    toki::math::index::sub2ind<dim>(info.local.resolution, index);
  auto const src = data.get("main", offset_element);
  std::copy(src, src + info.dof, output.data.get() + offset);
  return *this;
}
} // namespace toki::domain::detail
template struct toki::domain::detail::Orthotope_DG<1>;
template struct toki::domain::detail::Orthotope_DG<2>;
