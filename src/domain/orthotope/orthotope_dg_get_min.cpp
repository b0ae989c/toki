#include "toki/domain/detail/orthotope_dg.hpp"
namespace toki::domain::detail {
template<int dim>
auto
Orthotope_DG<dim>::get_min() const -> toki::Number
{
  return data.info.buffer.at("main").content.get_min();
}
} // namespace toki::domain::detail
template struct toki::domain::detail::Orthotope_DG<1>;
template struct toki::domain::detail::Orthotope_DG<2>;
