#include "toki/domain/detail/orthotope_dg.hpp"
#include "toki/math/index.hpp"
namespace toki::domain::detail {
template<int dim>
auto
Orthotope_DG<dim>::get_stencil(
  toki::vector::Vec& output,
  std::vector<std::array<toki::Size, dim>> const& index_list) -> Orthotope_DG&
{
  toki::Size offset = 0;
  for (auto const& index : index_list) {
    get_element(output, offset, index);
    offset += info.dof;
  }
  return *this;
}
} // namespace toki::domain::detail
template struct toki::domain::detail::Orthotope_DG<1>;
template struct toki::domain::detail::Orthotope_DG<2>;
