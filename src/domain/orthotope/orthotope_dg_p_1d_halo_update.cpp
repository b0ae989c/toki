#include "toki/domain/detail/orthotope_dg_p.hpp"
#include "toki/log.hpp"
namespace toki::domain::detail {
template<int dim>
auto
Orthotope_DG_P<dim>::halo_update(int const direction) -> Orthotope_DG_P&
{
  MPI_Barrier(MPI_COMM_WORLD);
  auto const& resolution = info.local.resolution;
  auto const& halo = info.halo;
  if (direction == 0) {
    // buffer: outer_n
    for (toki::Size index_0 = 0; index_0 < halo[0]; ++index_0) {
      auto const dof = get_dof({index_0});
      for (toki::Size point = 0; point < dof; ++point) {
        auto const& value = data.at("halo_outer_n", index_0, point);
        data.set("main", value, index_0, point);
      }
    }
    // buffer: outer_p
    for (toki::Size index_0 = 0; index_0 < halo[0]; ++index_0) {
      auto const dof = get_dof({index_0});
      for (toki::Size point = 0; point < dof; ++point) {
        auto const& value = data.at("halo_outer_p", index_0, point);
        data.set("main", value, index_0 + (resolution[0] - halo[0]), point);
      }
    }
  } else {
    toki::log::error("invalid parameter: direction");
  }
  return *this;
}
} // namespace toki::domain::detail
template struct toki::domain::detail::Orthotope_DG_P<1>;
