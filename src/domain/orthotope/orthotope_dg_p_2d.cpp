#include "mpi.h"
#include "toki/domain/detail/orthotope_dg_p.hpp"
#include "toki/math/index.hpp"
namespace toki::domain::detail {
template<int dim>
Orthotope_DG_P<dim>::Orthotope_DG_P()
  : info{}
  , data{}
{
}
template<int dim>
Orthotope_DG_P<dim>::Orthotope_DG_P(
  std::vector<toki::Size> const& dof,
  std::array<int, 2> const& level,
  std::array<toki::Number, 2> const& refinement_score,
  std::array<toki::Size, dim> const& resolution,
  std::array<toki::Size, dim> const& halo,
  std::array<toki::Number, dim> const& range,
  std::array<toki::Number, dim> const& anchor,
  std::array<int, dim> const& mpi_extent)
  : info({
      .resolution = resolution,
      .halo = halo,
      .range = range,
      .anchor = anchor,
      .mpi_extent = mpi_extent,
    })
  , data{}
{
  // mpi
  MPI_Comm_rank(MPI_COMM_WORLD, &(info.local.mpi.rank));
  info.local.mpi.index =
    toki::math::index::ind2sub<dim>(info.mpi_extent, info.local.mpi.rank);
  for (int d = 0; d < dim; ++d) {
    info.local.mpi.neighbor[d].n = toki::math::index::sub2ind<dim>(
      info.mpi_extent,
      toki::math::index::shift(
        info.mpi_extent, info.local.mpi.index, d, -1, 1));
    info.local.mpi.neighbor[d].p = toki::math::index::sub2ind<dim>(
      info.mpi_extent,
      toki::math::index::shift(
        info.mpi_extent, info.local.mpi.index, d, +1, 1));
  }
  info.local.element.N = 1;
  for (int d = 0; d < dim; ++d) {
    info.local.resolution[d] = info.resolution[d] / info.mpi_extent[d];
    info.local.element.volume[d] =
      info.range[d] / static_cast<toki::Number>(info.resolution[d]);
    info.local.range[d] = info.local.element.volume[d] *
                          static_cast<toki::Number>(info.local.resolution[d]);
    info.local.anchor[d] =
      info.anchor[d] +
      info.local.range[d] * static_cast<toki::Number>(info.local.mpi.index[d]);
    // halo
    info.local.resolution[d] += 2 * info.halo[d];
    info.local.range[d] += info.local.element.volume[d] *
                           static_cast<toki::Number>(2 * info.halo[d]);
    info.local.anchor[d] -=
      info.local.element.volume[d] * static_cast<toki::Number>(info.halo[d]);
    // total element
    info.local.element.N *= info.local.resolution[d];
  }
  // refinement
  info.local.refinement.level.min = level[0];
  info.local.refinement.level.max = level[1];
  for (auto const& value : dof) {
    info.local.refinement.dof.push_back(value);
  }
  for (int point = info.local.refinement.level.min;
       point <= info.local.refinement.level.max;
       ++point) {
    info.local.refinement.basis.push_back({
      .point = point,
      .basis = typename Refinement::Basis::basis_type(point),
      .quadrature = typename Refinement::Basis::quadrature_type(point + 1),
    });
  }
  info.local.refinement.current.resize(
    info.local.element.N, info.local.refinement.level.min);
  info.local.refinement.target = info.local.refinement.current;
  // refinement offset
  info.local.refinement.data_offset.push_back(info.local.refinement.dof[0]);
  for (toki::Size offset = 1; offset < info.local.element.N; ++offset) {
    info.local.refinement.data_offset.push_back(
      info.local.refinement.dof[0] + info.local.refinement.data_offset.back());
  }
  // refinement score
  info.local.refinement.score.min = refinement_score[0];
  info.local.refinement.score.max = refinement_score[1];
  info.local.refinement.score.oscillation.resize(info.local.element.N, 1.0);
  info.local.refinement.score.flux.resize(info.local.element.N, 1.0);
  // data
  data.create_buffer(
    "main", info.local.element.N, info.local.refinement.dof[0]);
  data.create_remote_buffer(
    "halo_outer_nz",
    info.halo[0] * info.local.resolution[1],
    info.local.refinement.dof[0],
    info.local.mpi.neighbor[0].n,
    "halo_inner_pz");
  data.create_remote_buffer(
    "halo_outer_pz",
    info.halo[0] * info.local.resolution[1],
    info.local.refinement.dof[0],
    info.local.mpi.neighbor[0].p,
    "halo_inner_nz");
  data.create_remote_buffer(
    "halo_outer_zn",
    info.local.resolution[0] * info.halo[1],
    info.local.refinement.dof[0],
    info.local.mpi.neighbor[1].n,
    "halo_inner_zp");
  data.create_remote_buffer(
    "halo_outer_zp",
    info.local.resolution[0] * info.halo[1],
    info.local.refinement.dof[0],
    info.local.mpi.neighbor[1].p,
    "halo_inner_zn");
}
template<int dim>
Orthotope_DG_P<dim>::Orthotope_DG_P(Orthotope_DG_P const& other)
  : info(other.info)
  , data(other.data)
{
}
template<int dim>
Orthotope_DG_P<dim>::Orthotope_DG_P(Orthotope_DG_P&& other) noexcept
  : info(std::exchange(other.info, decltype(info){}))
  , data(std::exchange(other.data, decltype(data){}))
{
}
template<int dim>
Orthotope_DG_P<dim>::~Orthotope_DG_P()
{
}
template<int dim>
auto
Orthotope_DG_P<dim>::operator=(Orthotope_DG_P other) -> Orthotope_DG_P&
{
  swap(*this, other);
  return *this;
}
} // namespace toki::domain::detail
template struct toki::domain::detail::Orthotope_DG_P<2>;
