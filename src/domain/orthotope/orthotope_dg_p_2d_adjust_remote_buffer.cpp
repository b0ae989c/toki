#include "fmt/format.h"
#include "toki/domain/detail/orthotope_dg_p.hpp"
#include "toki/utility.hpp"
namespace toki::domain::detail {
template<int dim>
auto
Orthotope_DG_P<dim>::adjust_remote_buffer(
  std::string const& name,
  int const remote_mpi_rank,
  std::string const& remote_name,
  std::array<toki::Size, 2> const& index_begin,
  std::array<toki::Size, 2> const& index_end) -> Orthotope_DG_P&
{
  toki::Size halo_size = 0;
  std::array<toki::Size, 2> const length{
    index_end[0] - index_begin[0],
    index_end[1] - index_begin[1],
  };
  std::vector<toki::Size> halo_offset(length[0] * length[1]);
  for (toki::Size index_1 = index_begin[1]; index_1 < index_end[1]; ++index_1) {
    for (toki::Size index_0 = index_begin[0]; index_0 < index_end[0];
         ++index_0) {
      halo_size += get_dof({index_0, index_1});
      halo_offset.at(
        (index_0 - index_begin[0]) + length[0] * (index_1 - index_begin[1])) =
        halo_size;
    }
  }
  toki::utility::confirm(halo_size == halo_offset.back(), "halo size mismatch");
  toki::vector::Vec halo_content(halo_size);
  data.attach_remote_buffer(
    std::move(halo_content),
    name,
    halo_size,
    halo_offset,
    remote_mpi_rank,
    remote_name);
  return *this;
}
} // namespace toki::domain::detail
template struct toki::domain::detail::Orthotope_DG_P<2>;
