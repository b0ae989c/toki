#include "mpi.h"
#include "toki/domain/detail/orthotope_dg_p.hpp"
#include "toki/log.hpp"
#include "toki/math/index.hpp"
namespace toki::domain::detail {
template<int dim>
auto
Orthotope_DG_P<dim>::halo_dispatch(int const direction) -> Orthotope_DG_P&
{
  auto const& resolution = info.local.resolution;
  auto const& halo = info.halo;
  if (direction == 0) {
    // buffer: inner_nz
    for (toki::Size index_1 = 0; index_1 < resolution[1]; ++index_1) {
      for (toki::Size index_0 = 0; index_0 < halo[0]; ++index_0) {
        auto const dof = get_dof({index_0, index_1});
        for (toki::Size point = 0; point < dof; ++point) {
          auto const& value = data.at(
            "main",
            toki::math::index::sub2ind<dim>(
              resolution,
              std::array<toki::Size, 2>{index_0 + halo[0], index_1}),
            point);
          data.set("halo_inner_nz", value, index_0 + halo[0] * index_1, point);
        }
      }
    }
    // buffer: inner_pz
    for (toki::Size index_1 = 0; index_1 < resolution[1]; ++index_1) {
      for (toki::Size index_0 = 0; index_0 < halo[0]; ++index_0) {
        auto const dof = get_dof({index_0, index_1});
        for (toki::Size point = 0; point < dof; ++point) {
          auto const& value = data.at(
            "main",
            toki::math::index::sub2ind<dim>(
              resolution,
              std::array<toki::Size, 2>{
                index_0 + (resolution[0] - 2 * halo[0]), index_1}),
            point);
          data.set("halo_inner_pz", value, index_0 + halo[0] * index_1, point);
        }
      }
    }
    // dispatch
    MPI_Barrier(MPI_COMM_WORLD);
    data.update_remote_buffer("halo_outer_nz")
      .update_remote_buffer("halo_outer_pz");
  } else if (direction == 1) {
    // buffer: inner_zn
    for (toki::Size index_1 = 0; index_1 < halo[1]; ++index_1) {
      for (toki::Size index_0 = 0; index_0 < resolution[0]; ++index_0) {
        auto const dof = get_dof({index_0, index_1});
        for (toki::Size point = 0; point < dof; ++point) {
          auto const& value = data.at(
            "main",
            toki::math::index::sub2ind<dim>(
              resolution,
              std::array<toki::Size, 2>{index_0, index_1 + halo[1]}),
            point);
          data.set(
            "halo_inner_zn", value, index_0 + resolution[0] * index_1, point);
        }
      }
    }
    // buffer: inner_zp
    for (toki::Size index_1 = 0; index_1 < halo[1]; ++index_1) {
      for (toki::Size index_0 = 0; index_0 < resolution[0]; ++index_0) {
        auto const dof = get_dof({index_0, index_1});
        for (toki::Size point = 0; point < dof; ++point) {
          auto const& value = data.at(
            "main",
            toki::math::index::sub2ind<dim>(
              resolution,
              std::array<toki::Size, 2>{
                index_0, index_1 + (resolution[1] - 2 * halo[1])}),
            point);
          data.set(
            "halo_inner_zp", value, index_0 + resolution[0] * index_1, point);
        }
      }
    }
    // dispatch
    MPI_Barrier(MPI_COMM_WORLD);
    data.update_remote_buffer("halo_outer_zn")
      .update_remote_buffer("halo_outer_zp");
  } else {
    toki::log::error("invalid parameter: direction");
  }
  return *this;
}
} // namespace toki::domain::detail
template struct toki::domain::detail::Orthotope_DG_P<2>;
