#include "toki/domain/detail/orthotope_dg_p.hpp"
#include "toki/log.hpp"
#include "toki/math/index.hpp"
namespace toki::domain::detail {
template<int dim>
auto
Orthotope_DG_P<dim>::halo_update(int const direction) -> Orthotope_DG_P&
{
  MPI_Barrier(MPI_COMM_WORLD);
  auto const& resolution = info.local.resolution;
  auto const& halo = info.halo;
  if (direction == 0) {
    // buffer: outer_nz
    for (toki::Size index_1 = 0; index_1 < resolution[1]; ++index_1) {
      for (toki::Size index_0 = 0; index_0 < halo[0]; ++index_0) {
        auto const dof = get_dof({index_0, index_1});
        for (toki::Size point = 0; point < dof; ++point) {
          auto const& value =
            data.at("halo_outer_nz", index_0 + halo[0] * index_1, point);
          data.set(
            "main",
            value,
            toki::math::index::sub2ind<dim>(
              resolution, std::array<toki::Size, dim>{index_0, index_1}),
            point);
        }
      }
    }
    // buffer: outer_pz
    for (toki::Size index_1 = 0; index_1 < resolution[1]; ++index_1) {
      for (toki::Size index_0 = 0; index_0 < halo[0]; ++index_0) {
        auto const dof = get_dof({index_0, index_1});
        for (toki::Size point = 0; point < dof; ++point) {
          auto const& value =
            data.at("halo_outer_pz", index_0 + halo[0] * index_1, point);
          data.set(
            "main",
            value,
            toki::math::index::sub2ind<dim>(
              resolution,
              std::array<toki::Size, dim>{
                index_0 + (resolution[0] - halo[0]), index_1}),
            point);
        }
      }
    }
  } else if (direction == 1) {
    // buffer: outer_zn
    for (toki::Size index_1 = 0; index_1 < halo[1]; ++index_1) {
      for (toki::Size index_0 = 0; index_0 < resolution[0]; ++index_0) {
        auto const dof = get_dof({index_0, index_1});
        for (toki::Size point = 0; point < dof; ++point) {
          auto const& value =
            data.at("halo_outer_zn", index_0 + resolution[0] * index_1, point);
          data.set(
            "main",
            value,
            toki::math::index::sub2ind<dim>(
              resolution, std::array<toki::Size, dim>{index_0, index_1}),
            point);
        }
      }
    }
    // buffer: outer_zp
    for (toki::Size index_1 = 0; index_1 < halo[1]; ++index_1) {
      for (toki::Size index_0 = 0; index_0 < resolution[0]; ++index_0) {
        auto const dof = get_dof({index_0, index_1});
        for (toki::Size point = 0; point < dof; ++point) {
          auto const& value =
            data.at("halo_outer_zp", index_0 + resolution[0] * index_1, point);
          data.set(
            "main",
            value,
            toki::math::index::sub2ind<dim>(
              resolution,
              std::array<toki::Size, dim>{
                index_0, index_1 + (resolution[1] - halo[1])}),
            point);
        }
      }
    }
  } else {
  }
  return *this;
}
} // namespace toki::domain::detail
template struct toki::domain::detail::Orthotope_DG_P<2>;
