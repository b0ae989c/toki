#include "toki/domain/detail/orthotope_dg_p.hpp"
#include "toki/log.hpp"
namespace toki::domain::detail {
template<int dim>
auto
Orthotope_DG_P<dim>::halo_wait(int const direction) -> Orthotope_DG_P&
{
  if (direction == 0) {
    data.wait("halo_outer_nz");
    data.wait("halo_outer_pz");
  } else if (direction == 1) {
    data.wait("halo_outer_zn");
    data.wait("halo_outer_zp");
  } else {
    toki::log::error("invalid parameter: direction");
  }
  return *this;
}
} // namespace toki::domain::detail
template struct toki::domain::detail::Orthotope_DG_P<2>;
