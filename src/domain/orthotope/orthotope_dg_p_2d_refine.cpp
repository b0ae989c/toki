#include <utility>
#include <vector>
#include "omp.h"
#include "toki/domain/detail/orthotope_dg_p.hpp"
#include "toki/math/index.hpp"
#include "toki/utility.hpp"
namespace toki::domain::detail {
template<int dim>
auto
Orthotope_DG_P<dim>::refine(
  bool const& apply_projection,
  bool const& aux_domain) -> Orthotope_DG_P&
{
  auto& refinement = info.local.refinement;
  if (!aux_domain) {
    bool flag_refine = false;
    for (toki::Size offset = 0; offset < info.local.element.N; ++offset) {
      if (refinement.target.at(offset) != refinement.current.at(offset)) {
        flag_refine = true;
        break;
      }
    }
    if (!flag_refine) {
      return *this;
    }
  }
  toki::Size buffer_size =
    refinement.dof.at(refinement.target.at(0) - refinement.level.min);
  std::vector<toki::Size> buffer_offset(info.local.element.N);
  buffer_offset.at(0) = buffer_size;
  for (toki::Size offset_element = 1; offset_element < info.local.element.N;
       ++offset_element) {
    auto const dof = refinement.dof.at(
      refinement.target.at(offset_element) - refinement.level.min);
    buffer_size += dof;
    buffer_offset.at(offset_element) =
      buffer_offset.at(offset_element - 1) + dof;
  }
  toki::vector::Vec output(buffer_size);
  if (apply_projection) {
    auto const& input = data.info.buffer.at("main").content;
#pragma omp parallel default(none)                                             \
  shared(info, refinement, buffer_offset, output, input)
    {
      toki::Size offset_element{};
#pragma omp for schedule(runtime)
      for (offset_element = 0; offset_element < info.local.element.N;
           ++offset_element) {
        auto const output_level = refinement.target.at(offset_element);
        toki::Size output_offset = 0;
        if (offset_element) {
          output_offset = buffer_offset.at(offset_element - 1);
        }
        auto const input_level = refinement.current.at(offset_element);
        toki::Size input_offset = 0;
        if (offset_element) {
          input_offset = refinement.data_offset.at(offset_element - 1);
        }
        toki::utility::confirm(
          input_offset == get_data_offset(toki::math::index::ind2sub<2>(
                            info.local.resolution, offset_element)),
          "input_offset mismatch");
        project_element(
          output,
          output_level,
          output_offset,
          input,
          input_level,
          input_offset);
      }
    }
  }
  refinement.current = refinement.target;
  refinement.data_offset = buffer_offset;
  data.attach_buffer(std::move(output), "main", buffer_size, buffer_offset);
  if (aux_domain) {
    return *this;
  }
  adjust_remote_buffer(
    "halo_outer_nz",
    info.local.mpi.neighbor[0].n,
    "halo_inner_pz",
    {0, 0},
    {info.halo[0], info.local.resolution[1]});
  adjust_remote_buffer(
    "halo_outer_pz",
    info.local.mpi.neighbor[0].p,
    "halo_inner_nz",
    {info.local.resolution[0] - info.halo[0], 0},
    {info.local.resolution[0], info.local.resolution[1]});
  adjust_remote_buffer(
    "halo_outer_zn",
    info.local.mpi.neighbor[1].n,
    "halo_inner_zp",
    {0, 0},
    {info.local.resolution[0], info.halo[1]});
  adjust_remote_buffer(
    "halo_outer_zp",
    info.local.mpi.neighbor[1].p,
    "halo_inner_zn",
    {0, info.local.resolution[1] - info.halo[1]},
    {info.local.resolution[0], info.local.resolution[1]});
  return *this;
}
} // namespace toki::domain::detail
template struct toki::domain::detail::Orthotope_DG_P<2>;
