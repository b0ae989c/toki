#include <algorithm>
#include <functional>
#include "toki/domain/detail/orthotope_dg_p.hpp"
#include "toki/math/index.hpp"
#include "toki/utility.hpp"
namespace toki::domain::detail {
template<int dim>
auto
Orthotope_DG_P<dim>::adjust_element(
  toki::Number const& delta,
  std::array<toki::Size, dim> const& index) -> Orthotope_DG_P&
{
  auto const dof = get_dof(index);
  auto const offset_element =
    toki::math::index::sub2ind<dim>(info.local.resolution, index);
  auto const src = data.get("main", offset_element);
  std::transform(
    src, src + dof, src, [&](toki::Number const& input) -> toki::Number {
    return input + delta;
  });
  return *this;
}
template<int dim>
auto
Orthotope_DG_P<dim>::adjust_element(
  toki::vector::Vec const& delta,
  toki::Size const& offset,
  std::array<toki::Size, dim> const& index) -> Orthotope_DG_P&
{
  toki::utility::confirm(delta.valid_at(offset), "invalid parameter: offset");
  auto const dof = get_dof(index);
  toki::utility::confirm(
    delta.valid_at(offset + dof - 1), "invalid parameter: offset");
  auto const offset_element =
    toki::math::index::sub2ind<dim>(info.local.resolution, index);
  auto const src = data.get("main", offset_element);
  std::transform(
    src, src + dof, delta.data.get() + offset, src, std::plus<toki::Number>());
  return *this;
}
template<int dim>
auto
Orthotope_DG_P<dim>::adjust_element(
  toki::Number const& delta,
  std::array<toki::Size, dim> const& index,
  toki::Size const& point) -> Orthotope_DG_P&
{
  toki::utility::confirm(point < get_dof(index), "invalid parameter: point");
  auto const offset_element =
    toki::math::index::sub2ind<dim>(info.local.resolution, index);
  data.set(
    "main",
    data.at("main", offset_element, point) + delta,
    offset_element,
    point);
  return *this;
}
template<int dim>
auto
Orthotope_DG_P<dim>::adjust_element(
  std::function<
    toki::Number(std::array<toki::Size, dim> const&, toki::Size const&)> const&
    op) -> Orthotope_DG_P&
{
  data.set(
    "main",
    [&](toki::Size const& offset_element, toki::Size const& point)
      -> toki::Number {
    return data.at("main", offset_element, point) +
           op(
             toki::math::index::ind2sub<dim>(
               info.local.resolution, offset_element),
             point);
  });
  return *this;
}
} // namespace toki::domain::detail
template struct toki::domain::detail::Orthotope_DG_P<1>;
template struct toki::domain::detail::Orthotope_DG_P<2>;
