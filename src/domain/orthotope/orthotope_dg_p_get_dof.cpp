#include "toki/domain/detail/orthotope_dg_p.hpp"
#include "toki/math/index.hpp"
namespace toki::domain::detail {
template<int dim>
auto
Orthotope_DG_P<dim>::get_dof(std::array<toki::Size, dim> const& index) const
  -> toki::Size
{
  auto const offset_element =
    toki::math::index::sub2ind<dim>(info.local.resolution, index);
  auto const& buffer = data.info.buffer.at("main");
  if (offset_element == 0) {
    return buffer.offset.at(0);
  } else {
    return buffer.offset.at(offset_element) -
           buffer.offset.at(offset_element - 1);
  }
}
} // namespace toki::domain::detail
template struct toki::domain::detail::Orthotope_DG_P<1>;
template struct toki::domain::detail::Orthotope_DG_P<2>;
