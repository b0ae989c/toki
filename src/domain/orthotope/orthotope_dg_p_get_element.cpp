#include <algorithm>
#include "toki/domain/detail/orthotope_dg_p.hpp"
#include "toki/math/index.hpp"
#include "toki/utility.hpp"
namespace toki::domain::detail {
template<int dim>
auto
Orthotope_DG_P<dim>::get_element(
  toki::vector::Vec& output,
  toki::Size const& offset,
  std::array<toki::Size, dim> const& index) -> Orthotope_DG_P&
{
  toki::utility::confirm(output.valid_at(offset), "invalid parameter: offset");
  auto const dof = get_dof(index);
  toki::utility::confirm(
    output.valid_at(offset + dof - 1), "invalid parameter: offset");
  auto const offset_element =
    toki::math::index::sub2ind<dim>(info.local.resolution, index);
  auto const src = data.get("main", offset_element);
  auto const dst = output.data.get() + offset;
  std::copy(src, src + dof, dst);
  return *this;
}
} // namespace toki::domain::detail
template struct toki::domain::detail::Orthotope_DG_P<1>;
template struct toki::domain::detail::Orthotope_DG_P<2>;
