#include "toki/domain/detail/orthotope_dg_p.hpp"
#include "toki/math/index.hpp"
namespace toki::domain::detail {
template<int dim>
auto
Orthotope_DG_P<dim>::get_quadrature(std::array<toki::Size, dim> const& index)
  const -> Refinement::Basis::quadrature_type const&
{
  auto offset = toki::math::index::sub2ind<dim>(info.local.resolution, index);
  return info.local.refinement.basis
    .at(
      info.local.refinement.current.at(offset) -
      info.local.refinement.level.min)
    .quadrature;
}
} // namespace toki::domain::detail
template struct toki::domain::detail::Orthotope_DG_P<1>;
template struct toki::domain::detail::Orthotope_DG_P<2>;
