#include <cmath>
#include "toki/domain/detail/orthotope_dg_p.hpp"
#include "toki/math/index.hpp"
namespace toki::domain::detail {
template<int dim>
auto
Orthotope_DG_P<dim>::get_refinement_score(
  std::array<toki::Size, dim> const& index) -> toki::Number
{
  auto& refinement = info.local.refinement;
  auto const& basis = get_basis(index);
  auto const& quadrature = get_quadrature(index);
  auto const dof = get_dof(index);
  toki::vector::Vec sample(dof);
  get_element(sample, 0, index);
  auto op_average =
    [&](std::array<toki::Number, dim> const& coord) -> toki::Number {
    return basis.template approx<dim>(coord, sample, 0);
  };
  auto average = quadrature.template sum<dim>(op_average);
  for (int d = 0; d < dim; ++d) {
    average *= 0.5;
  }
  auto op_numerator =
    [&](std::array<toki::Number, dim> const& coord) -> toki::Number {
    return std::pow(
      basis.template approx<dim>(coord, sample, 0) - average, 2.0);
  };
  auto numerator = std::pow(quadrature.template sum<dim>(op_numerator), 0.5);
  for (int d = 0; d < dim; ++d) {
    numerator *= 0.5;
  }
  auto const offset =
    toki::math::index::sub2ind<dim>(info.local.resolution, index);
  if (dof == 1) {
    refinement.score.oscillation.at(offset) =
      std::max(std::log10(average), -100.0);
  } else if (numerator > 1e-9) {
    refinement.score.oscillation.at(offset) =
      std::log10(numerator / static_cast<toki::Number>(dof));
  } else {
    refinement.score.oscillation.at(offset) = -100.0;
  }
  return refinement.score.oscillation.at(offset);
}
} // namespace toki::domain::detail
template struct toki::domain::detail::Orthotope_DG_P<1>;
template struct toki::domain::detail::Orthotope_DG_P<2>;
