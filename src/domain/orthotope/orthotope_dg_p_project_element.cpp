#include <array>
#include "toki/domain/detail/orthotope_dg_p.hpp"
#include "toki/math/index.hpp"
#include "toki/utility.hpp"
namespace toki::domain::detail {
template<int dim>
auto
Orthotope_DG_P<dim>::project_element(
  toki::vector::Vec& output,
  int const output_level,
  toki::Size const& output_offset,
  toki::vector::Vec const& input,
  int const input_level,
  toki::Size const& input_offset) -> Orthotope_DG_P&
{
  toki::utility::confirm(
    output.valid_at(output_offset), "invalid parameter: output_offset");
  toki::utility::confirm(
    input.valid_at(input_offset), "invalid parameter: input_offset");
  toki::Size output_dof = 1;
  toki::Size input_dof = 1;
  for (int d = 0; d < dim; ++d) {
    output_dof *= output_level;
    input_dof *= input_level;
  }
  toki::utility::confirm(
    output.valid_at(output_offset + output_dof - 1),
    "invalid parameter: output_level");
  toki::utility::confirm(
    input.valid_at(input_offset + input_dof - 1),
    "invalid parameter: input_level");
  auto const& output_basis =
    info.local.refinement.basis
      .at(output_level - info.local.refinement.level.min)
      .basis;
  auto const& input_basis = info.local.refinement.basis
                              .at(input_level - info.local.refinement.level.min)
                              .basis;
  std::array<toki::Size, dim> size;
  size.fill(output_level);
  for (toki::Size point = 0; point < output_dof; ++point) {
    auto index = toki::math::index::ind2sub<dim>(size, point);
    std::array<toki::Number, dim> coord;
    for (int d = 0; d < dim; ++d) {
      coord[d] = output_basis.info.abscissa.at(index[d]);
    }
    output.set(
      input_basis.template approx<dim>(coord, input, input_offset),
      output_offset + point);
  }
  // volume-preserving projection
  auto const& quadrature =
    info.local.refinement.basis
      .at(info.local.refinement.level.max - info.local.refinement.level.min)
      .quadrature;
  auto op_average_input =
    [&](std::array<toki::Number, dim> const& coord) -> toki::Number {
    return input_basis.template approx<dim>(coord, input, input_offset);
  };
  auto const average_input =
    quadrature.template sum<dim>(op_average_input) / 4.0;
  auto op_average_output =
    [&](std::array<toki::Number, dim> const& coord) -> toki::Number {
    return output_basis.template approx<dim>(coord, output, output_offset);
  };
  auto const average_output =
    quadrature.template sum<dim>(op_average_output) / 4.0;
  for (toki::Size point = 0; point < output_dof; ++point) {
    auto const value =
      output.at(output_offset + point) + (average_input - average_output);
    output.set(value, output_offset + point);
  }
  return *this;
}
} // namespace toki::domain::detail
template struct toki::domain::detail::Orthotope_DG_P<1>;
template struct toki::domain::detail::Orthotope_DG_P<2>;
