#include <algorithm>
#include "toki/domain/detail/orthotope_dg_p.hpp"
#include "toki/log.hpp"
#include "toki/math/index.hpp"
#include "toki/utility.hpp"
namespace toki::domain::detail {
template<int dim>
auto
Orthotope_DG_P<dim>::set_element(
  toki::Number const& value,
  std::array<toki::Size, dim> const& index) -> Orthotope_DG_P&
{
  auto const dof = get_dof(index);
  auto const offset_element =
    toki::math::index::sub2ind<dim>(info.local.resolution, index);
  auto const src = data.get("main", offset_element);
  std::fill(src, src + dof, value);
  return *this;
}
template<int dim>
auto
Orthotope_DG_P<dim>::set_element(
  toki::vector::Vec const& input,
  toki::Size const& offset,
  std::array<toki::Size, dim> const& index) -> Orthotope_DG_P&
{
  toki::utility::confirm(input.valid_at(offset), "invalid parameter: offset");
  auto const dof = get_dof(index);
  toki::utility::confirm(
    input.valid_at(offset + dof - 1), "invalid parameter: offset");
  auto const offset_element =
    toki::math::index::sub2ind<dim>(info.local.resolution, index);
  auto const src = input.data.get() + offset;
  auto const dst = data.get("main", offset_element);
  std::copy(src, src + dof, dst);
  return *this;
}
template<int dim>
auto
Orthotope_DG_P<dim>::set_element(
  toki::Number const& value,
  std::array<toki::Size, dim> const& index,
  toki::Size const& point) -> Orthotope_DG_P&
{
  toki::utility::confirm(point < get_dof(index), "invalid parameter: point");
  auto const offset_element =
    toki::math::index::sub2ind<dim>(info.local.resolution, index);
  data.set("main", value, offset_element, point);
  return *this;
}
template<int dim>
auto
Orthotope_DG_P<dim>::set_element(
  std::function<
    toki::Number(std::array<toki::Size, dim> const&, toki::Size const&)> const&
    op) -> Orthotope_DG_P&
{
  data.set(
    "main",
    [&](toki::Size const& offset_element, toki::Size const& point)
      -> toki::Number {
    return op(
      toki::math::index::ind2sub<dim>(info.local.resolution, offset_element),
      point);
  });
  return *this;
}
template<int dim>
auto
Orthotope_DG_P<dim>::set_element(
  std::function<toki::Number(std::array<toki::Number, dim> const&)> const& op,
  bool const average) -> Orthotope_DG_P&
{
  data.set(
    "main",
    [&](toki::Size const& offset_element, toki::Size const& point)
      -> toki::Number {
    auto const index =
      toki::math::index::ind2sub<dim>(info.local.resolution, offset_element);
    if (!average) {
      return op(get_coord(index, point));
    }
    if (get_dof(index) == 1) {
      auto const& volume = info.local.element.volume;
      auto const coord = get_coord(index, point);
      if constexpr (dim == 1) {
        return (op({coord.at(0) - 0.5 * volume.at(0)}) +
                op({coord.at(0) + 0.5 * volume.at(0)})) /
               2.0;
      } else if constexpr (dim == 2) {
        return (op({coord.at(0) - 0.5 * volume.at(0), coord.at(1)}) +
                op({coord.at(0) + 0.5 * volume.at(0), coord.at(1)}) +
                op({coord.at(0), coord.at(1) - 0.5 * volume.at(1)}) +
                op({coord.at(0), coord.at(1) + 0.5 * volume.at(1)})) /
               4.0;
      } else {
        toki::log::error("no implementation");
      }
    } else {
      return op(get_coord(index, point));
    }
  });
  return *this;
}
} // namespace toki::domain::detail
template struct toki::domain::detail::Orthotope_DG_P<1>;
template struct toki::domain::detail::Orthotope_DG_P<2>;
