#include "toki/domain/detail/orthotope_dg.hpp"
#include "toki/log.hpp"
#include "toki/math/index.hpp"
#include "toki/utility.hpp"
namespace toki::domain::detail {
template<int dim>
auto
Orthotope_DG<dim>::set_element(
  toki::vector::Vec const& input,
  toki::Size const& offset,
  std::array<toki::Size, dim> const& index) -> Orthotope_DG&
{
  auto const& dof = data.info.buffer.at("main").dof;
  auto& content = data.info.buffer.at("main").content;
  auto const offset_element =
    toki::math::index::sub2ind<dim>(info.local.resolution, index);
  for (toki::Size ell = 0; ell < dof; ++ell) {
    content.set(input.at(offset + ell), ell + dof * offset_element);
  }
  return *this;
}
template<int dim>
auto
Orthotope_DG<dim>::set_element(
  toki::Number const& value,
  std::array<toki::Size, dim> const& index,
  toki::Size const& point) -> Orthotope_DG&
{
  auto const offset =
    toki::math::index::sub2ind<dim>(info.local.resolution, index);
  toki::utility::confirm(
    data.info.buffer.at("main").content.valid_at(offset),
    "invalid parameter: index");
  toki::utility::confirm(point < info.dof, "invalid parameter: point");
  data.set("main", value, offset, point);
  return *this;
}
template<int dim>
auto
Orthotope_DG<dim>::set_element(
  toki::Number const& value,
  std::array<toki::Size, dim> const& index) -> Orthotope_DG&
{
  auto const offset =
    toki::math::index::sub2ind<dim>(info.local.resolution, index);
  toki::utility::confirm(
    data.info.buffer.at("main").content.valid_at(offset),
    "invalid parameter: index");
  data.set("main", value, offset);
  return *this;
}
template<int dim>
auto
Orthotope_DG<dim>::set_element(std::function<toki::Number(
                                 std::array<toki::Size, dim> const&,
                                 toki::Size const&)> const& op) -> Orthotope_DG&
{
  data.set(
    "main",
    [&](toki::Size const& offset, toki::Size const& ell) -> toki::Number {
    return op(
      toki::math::index::ind2sub<dim>(info.local.resolution, offset), ell);
  });
  return *this;
}
template<int dim>
auto
Orthotope_DG<dim>::set_element(
  std::function<toki::Number(std::array<toki::Number, dim> const&)> const& op,
  bool const average) -> Orthotope_DG&
{
  data.set(
    "main",
    [&](toki::Size const& offset_element, toki::Size const& point)
      -> toki::Number {
    auto const index =
      toki::math::index::ind2sub<dim>(info.local.resolution, offset_element);
    if (!average) {
      return op(get_coord(index, point));
    }
    if (info.dof == 1) {
      auto const& volume = info.local.element.volume;
      auto const coord = get_coord(index, point);
      if constexpr (dim == 1) {
        return (op({coord.at(0) - 0.5 * volume.at(0)}) +
                op({coord.at(0) + 0.5 * volume.at(0)})) /
               2.0;
      } else if constexpr (dim == 2) {
        return (op({coord.at(0) - 0.5 * volume.at(0), coord.at(1)}) +
                op({coord.at(0) + 0.5 * volume.at(0), coord.at(1)}) +
                op({coord.at(0), coord.at(1) - 0.5 * volume.at(1)}) +
                op({coord.at(0), coord.at(1) + 0.5 * volume.at(1)})) /
               4.0;
      } else {
        toki::log::error("no implementation");
      }
    } else {
      return op(get_coord(index, point));
    }
  });
  return *this;
}
} // namespace toki::domain::detail
template struct toki::domain::detail::Orthotope_DG<1>;
template struct toki::domain::detail::Orthotope_DG<2>;
