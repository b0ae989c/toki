#include "toki/info.hpp"
#include "fmt/format.h"
namespace toki::info {
int const version_major = 1;
int const version_minor = 0;
int const version_patch = 0;
auto
version_string() -> std::string
{
  return fmt::format("{}.{}.{}", version_major, version_minor, version_patch);
}
} // namespace toki::info
