#include "fmt/format.h"
#include "toki/log.hpp"
namespace toki::log {
auto
info(
  std::string const& message,
  int const mpi_rank,
  MPI_Comm const mpi_communicator) -> void
{
  int mpi_initialized{};
  MPI_Initialized(&mpi_initialized);
  if (mpi_initialized) {
    int local_mpi_rank{};
    MPI_Comm_rank(mpi_communicator, &local_mpi_rank);
    if (local_mpi_rank == mpi_rank) {
      fmt::print("[INFO] {}\n", message);
    }
  } else {
    fmt::print("[INFO] {}\n", message);
  }
  return;
}
} // namespace toki::log
