#include "toki/math/permutation.hpp"
#include <algorithm>
namespace toki::math::permutation {
Permutation::Permutation()
  : size(0)
  , data{nullptr}
{
}
Permutation::Permutation(toki::Size const& size)
  : size(size)
  , data(std::make_shared<Permutation::value_type[]>(size, 0))
{
}
Permutation::Permutation(Permutation const& other)
  : size(other.size)
  , data(std::make_shared_for_overwrite<Permutation::value_type[]>(other.size))
{
  std::copy(other.data.get(), other.data.get() + size, data.get());
}
Permutation::Permutation(Permutation&& other) noexcept
  : size(std::exchange(other.size, decltype(size){}))
  , data(std::exchange(other.data, decltype(data){}))
{
}
Permutation::~Permutation() {}
auto
Permutation::operator=(Permutation other) -> Permutation&
{
  swap(*this, other);
  return *this;
}
} // namespace toki::math::permutation
