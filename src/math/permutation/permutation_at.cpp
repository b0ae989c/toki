#include "toki/math/permutation.hpp"
#include "toki/utility.hpp"
namespace toki::math::permutation {
auto
Permutation::at(toki::Size const& index) const -> value_type const&
{
  toki::utility::confirm(valid_at(index), "invalid parameter: index");
  return data[index];
}
} // namespace toki::math::permutation
