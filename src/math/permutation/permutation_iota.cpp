#include "toki/math/permutation.hpp"
#include "toki/utility.hpp"
namespace toki::math::permutation {
auto
Permutation::iota(toki::Size begin, toki::Size const& inc, toki::Size value)
  -> Permutation&
{
  toki::utility::confirm(inc > 0, "invalid parameter: inc");
  while (valid_at(begin)) {
    set(value++, begin);
    begin += inc;
  }
  return *this;
}
} // namespace toki::math::permutation
