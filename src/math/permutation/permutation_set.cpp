#include "toki/math/permutation.hpp"
#include "toki/utility.hpp"
namespace toki::math::permutation {
auto
Permutation::set(value_type const& value, toki::Size const& index)
  -> Permutation&
{
  toki::utility::confirm(valid_at(index), "invalid parameter: index");
  data[index] = value;
  return *this;
}
auto
Permutation::set(std::function<value_type(toki::Size const&)> const& op)
  -> Permutation&
{
  for (toki::Size index = 0; index < size; ++index) {
    set(op(index), index);
  }
  return *this;
}
} // namespace toki::math::permutation
