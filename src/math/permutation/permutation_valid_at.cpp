#include "toki/math/permutation.hpp"
namespace toki::math::permutation {
auto
Permutation::valid_at(toki::Size const& index) const -> bool
{
  return index < size;
}
} // namespace toki::math::permutation
