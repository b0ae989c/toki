#include "toki/matrix/mat.hpp"
#include <algorithm>
namespace toki::matrix {
Mat::Mat()
  : extent({0, 0})
  , data{nullptr}
{
}
Mat::Mat(toki::Size const& extent_0, toki::Size const& extent_1)
  : extent({extent_0, extent_1})
  , data(std::make_shared<Mat::value_type[]>(extent_0 * extent_1, 0))
{
}
Mat::Mat(Mat const& other)
  : extent(other.extent)
  , data(std::make_shared_for_overwrite<Mat::value_type[]>(
      other.extent[0] * other.extent[1]))
{
  std::copy(other.begin(), other.end(), begin());
}
Mat::Mat(Mat&& other) noexcept
  : extent(std::exchange(other.extent, decltype(extent){}))
  , data(std::exchange(other.data, decltype(data){}))
{
}
Mat::~Mat() {}
auto
Mat::operator=(Mat other) -> Mat&
{
  swap(*this, other);
  return *this;
}
auto
Mat::begin() const -> Mat::value_type*
{
  return data.get();
}
auto
Mat::end() const -> Mat::value_type*
{
  return begin() + extent[0] * extent[1];
}
} // namespace toki::matrix
