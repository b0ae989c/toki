#include "toki/matrix/mat.hpp"
#include "toki/utility.hpp"
namespace toki::matrix {
auto
Mat::at(toki::Size const& offset) const -> value_type const&
{
  toki::utility::confirm(valid_at(offset), "invalid parameter: offset");
  return data[offset];
}
auto
Mat::at(toki::Size const& index_0, toki::Size const& index_1) const
  -> Mat::value_type const&
{
  toki::utility::confirm(index_0 < extent[0], "invalid parameter: index_0");
  toki::utility::confirm(index_1 < extent[1], "invalid parameter: index_1");
  return at(index_0 + extent[0] * index_1);
}
} // namespace toki::matrix
