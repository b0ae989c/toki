#include "toki/matrix/mat.hpp"
#include "toki/utility.hpp"
namespace toki::matrix {
auto
Mat::set(value_type const& value, toki::Size const& offset) -> Mat&
{
  toki::utility::confirm(valid_at(offset), "invalid parameter: offset");
  data[offset] = value;
  return *this;
}
auto
Mat::set(
  Mat::value_type const& value,
  toki::Size const& index_0,
  toki::Size const& index_1) -> Mat&
{
  toki::utility::confirm(index_0 < extent[0], "invalid parameter: index_0");
  toki::utility::confirm(index_1 < extent[1], "invalid parameter: index_1");
  return set(value, index_0 + extent[0] * index_1);
}
auto
Mat::set(std::function<value_type(toki::Size const&)> const& op) -> Mat&
{
  for (toki::Size offset = 0; offset < extent[0] * extent[1]; ++offset) {
    set(op(offset), offset);
  }
  return *this;
}
auto
Mat::set(
  std::function<Mat::value_type(toki::Size const&, toki::Size const&)> const&
    op) -> Mat&
{
  for (toki::Size index_1 = 0; index_1 < extent[1]; ++index_1) {
    for (toki::Size index_0 = 0; index_0 < extent[0]; ++index_0) {
      set(op(index_0, index_1), index_0, index_1);
    }
  }
  return *this;
}
} // namespace toki::matrix
