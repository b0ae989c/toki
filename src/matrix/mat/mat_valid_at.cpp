#include "toki/matrix/mat.hpp"
namespace toki::matrix {
auto
Mat::valid_at(toki::Size const& offset) const -> bool
{
  return offset < (extent[0] * extent[1]);
}
auto
Mat::valid_at(toki::Size const& index_0, toki::Size const& index_1) const
  -> bool
{
  return (index_0 < extent[0]) && (index_1 < extent[1]);
}
} // namespace toki::matrix
