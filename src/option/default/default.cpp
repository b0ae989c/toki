#include "toki/option/default.hpp"
namespace toki::option {
Default::Default(int argc, char** argv, std::string const& handle)
  : argc(argc)
  , argv(argv)
  , handle(handle)
  , data{}
  , description("Options")
{
}
Default::~Default() {}
} // namespace toki::option
