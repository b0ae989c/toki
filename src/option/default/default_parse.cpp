#include <limits>
#include "fmt/format.h"
#include "toki/config.hpp"
#include "toki/option/default.hpp"
namespace toki::option {
auto
Default::parse() -> Default&
{
  using boost::program_options::value;
  description.add_options()               //
    ("help", "Display available options") //
    ("info.name",
     value<std::string>(&param.info.name)
       ->value_name("string")
       ->default_value("toki", "\"toki\""),
     "Simulation name") //
    ("mpi.extent",
     value<std::vector<int>>(&param.mpi.extent)
       ->value_name("[integer]")
       ->multitoken(),
     "MPI extent") //
    ("method.order",
     value<toki::Size>(&param.method.order)
       ->value_name("integer")
       ->default_value(4, "4"),
     "Method order") //
    ("method.CFL",
     value<toki::Number>(&param.method.CFL)
       ->value_name("number")
       ->default_value(1.0, "1.0"),
     "CFL number") //
    ("time.max",
     value<toki::Number>(&param.time.max)
       ->value_name("number")
       ->default_value(1.0, "1.0"),
     "Total simulation time") //
    ("time.delta_max",
     value<toki::Number>(&param.time.delta_max)
       ->value_name("number")
       ->default_value(1.0, "1.0"),
     "Timestep size limit") //
    ("time.step_max",
     value<toki::Size>(&param.time.step_max)
       ->value_name("integer")
       ->default_value(std::numeric_limits<toki::Size>::max(), "+inf"),
     "Timestep limit") //
    ("domain.resolution",
     value<std::vector<toki::Size>>(&param.domain.resolution)
       ->value_name("[integer]")
       ->multitoken(),
     "Domain resolution") //
    ("domain.range",
     value<std::vector<toki::Number>>(&param.domain.range)
       ->value_name("[number]")
       ->multitoken(),
     "Domain range") //
    ("domain.anchor",
     value<std::vector<toki::Number>>(&param.domain.anchor)
       ->value_name("[number]")
       ->multitoken(),
     "Domain anchor") //
    ("misc.a",
     value<std::vector<toki::Number>>(&param.misc.a)
       ->value_name("[number]")
       ->multitoken(),
     "Parameter: a") //
    ("misc.c",
     value<std::vector<toki::Number>>(&param.misc.c)
       ->value_name("[number]")
       ->multitoken(),
     "Parameter: c") //
    ("misc.n",
     value<std::vector<toki::Number>>(&param.misc.n)
       ->value_name("[number]")
       ->multitoken(),
     "Parameter: n") //
    ("misc.r",
     value<std::vector<toki::Number>>(&param.misc.r)
       ->value_name("[number]")
       ->multitoken(),
     "Parameter: r") //
    ("misc.u",
     value<std::vector<toki::Number>>(&param.misc.u)
       ->value_name("[number]")
       ->multitoken(),
     "Parameter: u") //
    ("misc.v",
     value<std::vector<toki::Number>>(&param.misc.v)
       ->value_name("[number]")
       ->multitoken(),
     "Parameter: v") //
    ("misc.w",
     value<std::vector<toki::Number>>(&param.misc.w)
       ->value_name("[number]")
       ->multitoken(),
     "Parameter: w") //
    ("misc.theta",
     value<std::vector<toki::Number>>(&param.misc.theta)
       ->value_name("[number]")
       ->multitoken(),
     "Parameter: theta") //
    ("misc.rho",
     value<std::vector<toki::Number>>(&param.misc.rho)
       ->value_name("[number]")
       ->multitoken(),
     "Parameter: rho") //
    ("misc.flag.report_error",
     value<bool>(&param.misc.flag.report_error)
       ->value_name("bool")
       ->default_value(false, "false"),
     "Flag: report_error") //
    ("misc.flag.apply_source",
     value<bool>(&param.misc.flag.apply_source)
       ->value_name("bool")
       ->default_value(false, "false"),
     "Flag: apply_source") //
    ;
  // command line
  boost::program_options::store(
    boost::program_options::parse_command_line(argc, argv, description), data);
  // config file
  boost::program_options::store(
    boost::program_options::parse_config_file(
      fmt::format("{}.ini", handle).c_str(), description),
    data);
  // notify
  boost::program_options::notify(data);
  return *this;
}
} // namespace toki::option
