#include <iostream>
#include "mpi.h"
#include "toki/option/default.hpp"
namespace toki::option {
auto
Default::print() -> Default&
{
  int mpi_rank{-1};
  MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
  if (!mpi_rank) {
    description.print(std::cout);
  }
  return *this;
}
} // namespace toki::option
