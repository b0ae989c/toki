#include "toki/option/vp.hpp"
namespace toki::option {
VP::VP(int argc, char** argv, std::string const& handle)
  : argc(argc)
  , argv(argv)
  , handle(handle)
  , data{}
  , description("Vlasov-Poisson options")
  , param{}
{
}
VP::~VP() {}
} // namespace toki::option
