#include <limits>
#include "fmt/format.h"
#include "toki/option/vp.hpp"
namespace toki::option {
auto
VP::parse() -> VP&
{
  using boost::program_options::value;
  description.add_options()("help", "Display available options");
  description.add_options()(
    "info.name",
    value<std::string>(&param.info.name)
      ->value_name("[string]")
      ->default_value("toki-vp", "\"toki-vp\""),
    "Simulation name");
  description.add_options()(
    "mpi.extent",
    value<std::vector<int>>(&param.mpi.extent)
      ->value_name("[integer...]")
      ->default_value(std::vector<int>(3, 1), "[1, 1, 1]")
      ->multitoken(),
    "MPI extent");
  description.add_options()(
    "model.species",
    value<toki::Size>(&param.model.species)
      ->value_name("[integer]")
      ->default_value(1, "1"),
    "Plasma species");
  description.add_options()(
    "method.adaptivity_p",
    value<bool>(&param.method.adaptivity_p)
      ->value_name("bool")
      ->default_value(false, "false"),
    "P adaptivity");
  description.add_options()(
    "method.order",
    value<toki::Size>(&param.method.order)
      ->value_name("[integer]")
      ->default_value(2, "2"),
    "Method order");
  description.add_options()(
    "method.CFL",
    value<toki::Number>(&param.method.CFL)
      ->value_name("[number]")
      ->default_value(1.0, "1.0"),
    "CFL number");
  description.add_options()(
    "method.refinement_score",
    value<std::vector<toki::Number>>(&param.method.refinement_score)
      ->value_name("[number, number]")
      ->default_value(std::vector<toki::Number>{0.1, 1.0}, "[0.1, 1.0]")
      ->multitoken(),
    "Refinement score");
  description.add_options()(
    "time.max",
    value<toki::Number>(&param.time.max)
      ->value_name("[number]")
      ->default_value(1.0, "1.0"),
    "Total simulation time");
  description.add_options()(
    "time.delta_min",
    value<toki::Number>(&param.time.delta_min)
      ->value_name("[number]")
      ->default_value(1e-9, "1e-9"),
    "Minimal timestep size");
  description.add_options()(
    "time.delta_max",
    value<toki::Number>(&param.time.delta_max)
      ->value_name("[number]")
      ->default_value(1.0, "1.0"),
    "Maximal timestep size");
  description.add_options()(
    "time.step_max",
    value<toki::Size>(&param.time.step_max)
      ->value_name("[integer]")
      ->default_value(std::numeric_limits<toki::Size>::max(), "+inf"),
    "Total timestep");
  description.add_options()(
    "time.checkpoint",
    value<std::vector<toki::Number>>(&param.time.checkpoint)
      ->value_name("[number...]")
      ->default_value(std::vector<toki::Number>{}, "[]")
      ->multitoken(),
    "Checkpoint time");
  description.add_options()(
    "domain.resolution",
    value<toki::Size>(&param.domain.resolution)
      ->value_name("[integer]")
      ->default_value(32, "32"),
    "Reference domain resolution");
  description.add_options()(
    "domain.range",
    value<toki::Number>(&param.domain.range)
      ->value_name("[number]")
      ->default_value(4.0 * toki::pi<toki::Number>, "4 pi"),
    "Reference domain range");
  description.add_options()(
    "feature.a",
    value<std::vector<toki::Number>>(&param.feature.a)
      ->value_name("[number...]")
      ->multitoken(),
    "Feature \"a\"");
  description.add_options()(
    "feature.k",
    value<std::vector<toki::Number>>(&param.feature.k)
      ->value_name("[number...]")
      ->multitoken(),
    "Feature \"k\"");
  description.add_options()(
    "feature.m",
    value<std::vector<toki::Number>>(&param.feature.m)
      ->value_name("[number...]")
      ->multitoken(),
    "Feature \"m\"");
  description.add_options()(
    "feature.q",
    value<std::vector<toki::Number>>(&param.feature.q)
      ->value_name("[number...]")
      ->multitoken(),
    "Feature \"q\"");
  description.add_options()(
    "feature.r",
    value<std::vector<toki::Number>>(&param.feature.r)
      ->value_name("[number...]")
      ->multitoken(),
    "Feature \"r\"");
  description.add_options()(
    "feature.J",
    value<std::vector<toki::Number>>(&param.feature.J)
      ->value_name("[number...]")
      ->multitoken(),
    "Feature \"J\"");
  description.add_options()(
    "feature.T",
    value<std::vector<toki::Number>>(&param.feature.T)
      ->value_name("[number...]")
      ->multitoken(),
    "Feature \"T\"");
  description.add_options()(
    "feature.theta",
    value<std::vector<toki::Number>>(&param.feature.theta)
      ->value_name("[number...]")
      ->multitoken(),
    "Feature \"theta\"");
  description.add_options()(
    "feature.rho",
    value<std::vector<toki::Number>>(&param.feature.rho)
      ->value_name("[number...]")
      ->multitoken(),
    "Feature \"rho\"");
  description.add_options()(
    "feature.omega",
    value<std::vector<toki::Number>>(&param.feature.omega)
      ->value_name("[number...]")
      ->multitoken(),
    "Feature \"omega\"");
  description.add_options()(
    "flag.info.detail",
    value<bool>(&param.flag.info.detail)
      ->value_name("[bool]")
      ->default_value(false, "false"),
    "Flag \"info.detail\"");
  description.add_options()(
    "flag.performance.skip_zero_element",
    value<bool>(&param.flag.performance.skip_zero_element)
      ->value_name("[bool]")
      ->default_value(true, "true"),
    "Flag \"performance.skip_zero_element\"");
  description.add_options()(
    "flag.background.current",
    value<bool>(&param.flag.background.current)
      ->value_name("[bool]")
      ->default_value(false, "false"),
    "Flag \"background.current\"");
  description.add_options()(
    "flag.background.plasma_source",
    value<bool>(&param.flag.background.plasma_source)
      ->value_name("[bool]")
      ->default_value(false, "false"),
    "Flag \"background.plasma_source\"");
  description.add_options()(
    "flag.refinement.sync_all_species",
    value<bool>(&param.flag.refinement.sync_all_species)
      ->value_name("[bool]")
      ->default_value(true, "true"),
    "Flag \"refinement.sync_all_species\"");
  // command line
  boost::program_options::store(
    boost::program_options::parse_command_line(argc, argv, description), data);
  // config file
  boost::program_options::store(
    boost::program_options::parse_config_file(
      fmt::format("{}.ini", handle).c_str(), description),
    data);
  // notify
  boost::program_options::notify(data);
  return *this;
}
} // namespace toki::option
