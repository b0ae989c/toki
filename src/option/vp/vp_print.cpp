#include <iostream>
#include "mpi.h"
#include "toki/option/vp.hpp"
namespace toki::option {
auto
VP::print() -> VP&
{
  int mpi_rank{-1};
  MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
  if (!mpi_rank) {
    description.print(std::cout);
  }
  return *this;
}
} // namespace toki::option
