#include "toki/quadrature/lobatto.hpp"
#include "toki/log.hpp"
namespace toki::quadrature {
Lobatto::Lobatto()
  : Lobatto(1)
{
}
Lobatto::Lobatto(toki::Size const& point)
  : info({
      .point = point,
      .weight = toki::vector::Vec(point),
      .abscissa = toki::vector::Vec(point),
    })
{
  if (info.point == 1) {
    info.abscissa.set(+0.000000000000000, 0);
    info.weight.set(+2.000000000000000, 0);
  } else if (info.point == 2) {
    info.abscissa.set(-1.0000000000000, 0).set(+1.000000000000000, 1);
    info.weight.set(+1.000000000000000, 0).set(+1.000000000000000, 1);
  } else if (info.point == 3) {
    info.abscissa.set(-1.0000000000000, 0)
      .set(+0.000000000000000, 1)
      .set(+1.000000000000000, 2);
    info.weight.set(+0.333333333333333, 0)
      .set(+1.333333333333333, 1)
      .set(+0.333333333333333, 2);
  } else if (info.point == 4) {
    info.abscissa.set(-1.0000000000000, 0)
      .set(-0.447213595499958, 1)
      .set(+0.447213595499958, 2)
      .set(+1.000000000000000, 3);
    info.weight.set(+0.166666666666667, 0)
      .set(+0.833333333333333, 1)
      .set(+0.833333333333333, 2)
      .set(+0.166666666666667, 3);
  } else if (info.point == 5) {
    info.abscissa.set(-1.0000000000000, 0)
      .set(-0.654653670707977, 1)
      .set(+0.000000000000000, 2)
      .set(+0.654653670707977, 3)
      .set(+1.000000000000000, 4);
    info.weight.set(+0.100000000000000, 0)
      .set(+0.544444444444444, 1)
      .set(+0.711111111111111, 2)
      .set(+0.544444444444444, 3)
      .set(+0.100000000000000, 4);
  } else if (info.point == 6) {
    info.abscissa.set(-1.0000000000000, 0)
      .set(-0.7650553239294647, 1)
      .set(-0.2852315164806451, 2)
      .set(+0.2852315164806451, 3)
      .set(+0.7650553239294647, 4)
      .set(+1.0000000000000, 5);
    info.weight.set(+0.06666666666666667, 0)
      .set(+0.3784749562978470, 1)
      .set(+0.5548583770354863, 2)
      .set(+0.5548583770354863, 3)
      .set(+0.3784749562978470, 4)
      .set(+0.06666666666666667, 5);
  } else {
    toki::log::error("no implementation");
  }
}
Lobatto::Lobatto(Lobatto const& other)
  : info(other.info)
{
}
Lobatto::Lobatto(Lobatto&& other) noexcept
  : info(std::exchange(other.info, decltype(info){}))
{
}
Lobatto::~Lobatto() {}
auto
Lobatto::operator=(Lobatto other) -> Lobatto&
{
  swap(*this, other);
  return *this;
}
} // namespace toki::quadrature
