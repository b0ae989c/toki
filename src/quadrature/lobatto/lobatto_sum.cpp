#include <cmath>
#include "boost/preprocessor/iteration/local.hpp"
#include "toki/math/index.hpp"
#include "toki/quadrature/lobatto.hpp"
#include "toki/utility.hpp"
namespace toki::quadrature {
template<int dim>
auto
Lobatto::sum(
  toki::Number& result,
  bool const update,
  toki::vector::Vec const& sample,
  toki::Size const& offset) const -> Lobatto const&
{
  if (!update) {
    result = 0.0;
  }
  toki::utility::confirm(sample.valid_at(offset), "invalid parameter: offset");
  std::array<toki::Size, dim> size{};
  toki::Size dof_total{1};
  for (int d = 0; d < dim; ++d) {
    size[d] = info.point;
    dof_total *= info.point;
  }
  toki::utility::confirm(
    sample.valid_at(offset + dof_total - 1), "invalid parameter: sample");
  for (toki::Size dof = 0; dof < dof_total; ++dof) {
    auto index = toki::math::index::ind2sub<dim>(size, dof);
    toki::Number weight{1.0};
    for (int d = 0; d < dim; ++d) {
      weight *= info.weight.at(index[d]);
    }
    result = std::fma(weight, sample.at(offset + dof), result);
  }
  return *this;
}
template<int dim>
auto
Lobatto::sum(toki::vector::Vec const& sample, toki::Size const& offset) const
  -> toki::Number
{
  toki::Number result{};
  sum<dim>(result, false, sample, offset);
  return result;
}
template<int dim>
auto
Lobatto::sum(
  toki::Number& result,
  bool const update,
  std::function<toki::Number(std::array<toki::Number, dim> const&)> const& op)
  const -> Lobatto const&
{
  if (!update) {
    result = 0.0;
  }
  std::array<toki::Size, dim> size{};
  toki::Size dof_total{1};
  for (int d = 0; d < dim; ++d) {
    size[d] = info.point;
    dof_total *= info.point;
  }
  std::array<toki::Number, dim> coord{};
  for (toki::Size dof = 0; dof < dof_total; ++dof) {
    auto index = toki::math::index::ind2sub<dim>(size, dof);
    toki::Number weight{1.0};
    for (int d = 0; d < dim; ++d) {
      weight *= info.weight.at(index[d]);
      coord[d] = info.abscissa.at(index[d]);
    }
    result = std::fma(weight, op(coord), result);
  }
  return *this;
}
template<int dim>
auto
Lobatto::sum(
  std::function<toki::Number(std::array<toki::Number, dim> const&)> const& op)
  const -> toki::Number
{
  toki::Number result{};
  sum<dim>(result, false, op);
  return result;
}
template<int dim>
auto
Lobatto::sum(
  toki::Number& result,
  bool const update,
  std::function<toki::Number(
    std::array<toki::Number, dim> const&,
    toki::Size const&)> const& op) const -> Lobatto const&
{
  if (!update) {
    result = 0.0;
  }
  std::array<toki::Size, dim> size{};
  toki::Size dof_total{1};
  for (int d = 0; d < dim; ++d) {
    size[d] = info.point;
    dof_total *= info.point;
  }
  std::array<toki::Number, dim> coord{};
  for (toki::Size dof = 0; dof < dof_total; ++dof) {
    auto index = toki::math::index::ind2sub<dim>(size, dof);
    toki::Number weight{1.0};
    for (int d = 0; d < dim; ++d) {
      weight *= info.weight.at(index[d]);
      coord[d] = info.abscissa.at(index[d]);
    }
    result += weight * op(coord, dof);
  }
  return *this;
}
template<int dim>
auto
Lobatto::sum(std::function<toki::Number(
               std::array<toki::Number, dim> const&,
               toki::Size const&)> const& op) const -> toki::Number
{
  toki::Number result{};
  sum<dim>(result, false, op);
  return result;
}
} // namespace toki::quadrature
namespace toki::quadrature {
#define BOOST_PP_LOCAL_MACRO(dim)                                              \
  template auto Lobatto::sum<dim>(                                             \
    toki::Number&, bool const, toki::vector::Vec const&, toki::Size const&)    \
    const -> Lobatto const&;                                                   \
  template auto Lobatto::sum<dim>(toki::vector::Vec const&, toki::Size const&) \
    const -> toki::Number;                                                     \
  template auto Lobatto::sum<dim>(                                             \
    toki::Number&,                                                             \
    bool const,                                                                \
    std::function<toki::Number(std::array<toki::Number, dim> const&)> const&)  \
    const -> Lobatto const&;                                                   \
  template auto Lobatto::sum<dim>(                                             \
    std::function<toki::Number(std::array<toki::Number, dim> const&)> const&)  \
    const -> toki::Number;                                                     \
  template auto Lobatto::sum<dim>(                                             \
    toki::Number&,                                                             \
    bool const,                                                                \
    std::function<toki::Number(                                                \
      std::array<toki::Number, dim> const&, toki::Size const&)> const&)        \
    const -> Lobatto const&;                                                   \
  template auto Lobatto::sum<dim>(                                             \
    std::function<toki::Number(                                                \
      std::array<toki::Number, dim> const&, toki::Size const&)> const&)        \
    const -> toki::Number;                                                     \
  /* */
#define BOOST_PP_LOCAL_LIMITS (1, 4)
#include BOOST_PP_LOCAL_ITERATE()
} // namespace toki::quadrature
