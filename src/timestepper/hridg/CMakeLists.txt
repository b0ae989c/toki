set(CPP_SOURCE
    hridg_1d.cpp
    hridg_1d_create_domain.cpp
    hridg_1d_set_element.cpp
    hridg_1d_create_flux_uniform.cpp
    hridg_1d_create_flux_uniform_upwind.cpp
    hridg_1d_prepare.cpp
    hridg_1d_halo_sync.cpp
    hridg_1d_build_p0_lhs.cpp
    hridg_1d_build_p0_rhs.cpp
    hridg_1d_build_p1_lhs.cpp
    hridg_1d_build_p1_rhs.cpp
    hridg_1d_build_c0_lhs.cpp
    hridg_1d_build_c0_rhs.cpp
    hridg_1d_solve.cpp
    hridg_1d_stop.cpp
    hridg_1d_solve_prediction.cpp
    hridg_1d_solve_correction.cpp
    hridg_1d_report.cpp
    hridg_1d_save.cpp
    hridg_2d.cpp
    hridg_2d_create_domain.cpp
    hridg_2d_set_element.cpp
    hridg_2d_create_flux_uniform.cpp
    hridg_2d_create_flux_uniform_upwind.cpp
    hridg_2d_create_flux_external.cpp
    hridg_2d_create_flux_external_upwind.cpp
    hridg_2d_prepare.cpp
    hridg_2d_halo_sync.cpp
    hridg_2d_build_p0_lhs.cpp
    hridg_2d_build_p0_rhs.cpp
    hridg_2d_build_p1_lhs.cpp
    hridg_2d_build_p1_rhs.cpp
    hridg_2d_build_p2_lhs.cpp
    hridg_2d_build_p2_rhs.cpp
    hridg_2d_build_c0_lhs.cpp
    hridg_2d_build_c0_rhs.cpp
    hridg_2d_solve.cpp
    hridg_2d_stop.cpp
    hridg_2d_solve_prediction.cpp
    hridg_2d_solve_correction.cpp
    hridg_2d_apply_source.cpp
    hridg_2d_report.cpp
    hridg_2d_save.cpp
)

target_sources(${TOKI_LIB_DEBUG} PRIVATE ${CPP_SOURCE})

target_sources(${TOKI_LIB_RELEASE} PRIVATE ${CPP_SOURCE})
