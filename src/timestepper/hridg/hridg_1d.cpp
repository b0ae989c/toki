#include "toki/timestepper/detail/hridg_1d.hpp"
namespace toki::timestepper::detail {
HRIDG_1D::HRIDG_1D()
  : info{}
  , cache{}
{
}
HRIDG_1D::HRIDG_1D(std::string const& name)
  : info{.name = name}
  , cache{}
{
}
HRIDG_1D::HRIDG_1D(HRIDG_1D const& other)
  : info(other.info)
  , cache(other.cache)
{
}
HRIDG_1D::HRIDG_1D(HRIDG_1D&& other) noexcept
  : info(std::exchange(other.info, decltype(info){}))
  , cache(std::exchange(other.cache, decltype(cache){}))
{
}
HRIDG_1D::~HRIDG_1D() {}
auto
HRIDG_1D::operator=(HRIDG_1D other) -> HRIDG_1D&
{
  swap(*this, other);
  return *this;
}
} // namespace toki::timestepper::detail
