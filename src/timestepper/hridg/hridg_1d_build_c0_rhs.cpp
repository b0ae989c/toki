#include <array>
#include "toki/log.hpp"
#include "toki/math/index.hpp"
#include "toki/quadrature/lobatto.hpp"
#include "toki/timestepper/detail/hridg_1d.hpp"
namespace toki::timestepper::detail {
auto
HRIDG_1D::build_C0_RHS() -> std::array<toki::matrix::Mat, 3>
{
  auto const& dof = info.domain_spec.at("default").quadrature_point;
  toki::Size const dof_C{dof};
  toki::Size const dof_P{dof_C * dof};
  std::array<toki::Size, 2> size_col{dof, dof};
  std::array<toki::Size, 1> size_row{dof};
  auto const& basis = info.domain_spec.at("default").basis;
  auto const& integrator = info.domain_spec.at("default").integrator;
  auto const& dx = info.domain.at("default").info.local.element.volume;
  std::array<toki::matrix::Mat, 3> output{};
  output[0] = toki::matrix::Mat(dof_C, dof_P);
  for (toki::Size col = 0; col < output[0].extent[1]; ++col) {
    auto index_col = toki::math::index::ind2sub<2>(size_col, col);
    for (toki::Size row = 0; row < output[0].extent[0]; ++row) {
      auto index_row = toki::math::index::ind2sub<1>(size_row, row);
      auto op = [&](std::array<toki::Number, 2> const& coord) -> toki::Number {
        toki::Number result{0};
        // flux: space 0-
        do {
          toki::Number result_flux_0n{1.0};
          // col
          if (info.flux_type[0] == toki::timestepper::tag::Flux::Uniform) {
            result_flux_0n *= info.op.get_flux_uniform_upwind[0](
              false, 0.0, {basis.eval<2>({coord[0], -1.0}, index_col)});
          } else {
            toki::log::error("no implementation");
          }
          // row
          result_flux_0n *= basis.eval<1>({-1.0}, index_row);
          // update
          result += 0.5 * (+info.param.time_delta / dx[0]) * result_flux_0n;
        } while (0);
        // flux: space 0+
        do {
          toki::Number result_flux_0p{1.0};
          // col
          if (info.flux_type[0] == toki::timestepper::tag::Flux::Uniform) {
            result_flux_0p *= info.op.get_flux_uniform_upwind[0](
              true, 0.0, {basis.eval<2>({coord[0], +1.0}, index_col)});
          } else {
            toki::log::error("no implementation");
          }
          // row
          result_flux_0p *= basis.eval<1>({+1.0}, index_row);
          // update
          result += 0.5 * (-info.param.time_delta / dx[0]) * result_flux_0p;
        } while (0);
        // evolution
        do {
          toki::Number result_evolution{1.0};
          // col
          result_evolution *= basis.eval<2>(coord, index_col);
          // row
          if (info.flux_type[0] == toki::timestepper::tag::Flux::Uniform) {
            auto result_evolution_space_0 = info.op.get_flux_uniform[0](
              0.0, {basis.eval<1>({coord[1]}, index_row, 0, 1)});
            result_evolution *= result_evolution_space_0;
          } else {
            toki::log::error("no implementation");
          }
          // update
          result += (+info.param.time_delta / dx[0]) * result_evolution;
        } while (0);
        return result;
      };
      output[0].set(integrator.sum<2>(op), row, col);
    }
  }
  output[1] = toki::matrix::Mat(dof_C, dof_P);
  for (toki::Size col = 0; col < output[1].extent[1]; ++col) {
    auto index_col = toki::math::index::ind2sub<2>(size_col, col);
    for (toki::Size row = 0; row < output[1].extent[0]; ++row) {
      auto index_row = toki::math::index::ind2sub<1>(size_row, row);
      auto op = [&](std::array<toki::Number, 2> const& coord) -> toki::Number {
        toki::Number result{0};
        // flux: space 0-
        do {
          toki::Number result_flux_0n{1.0};
          // col
          if (info.flux_type[0] == toki::timestepper::tag::Flux::Uniform) {
            result_flux_0n *= info.op.get_flux_uniform_upwind[0](
              true, 0.0, {basis.eval<2>({coord[0], +1.0}, index_col)});
          } else {
            toki::log::error("no implementation");
          }
          // row
          result_flux_0n *= basis.eval<1>({-1.0}, index_row);
          // update
          result += 0.5 * (+info.param.time_delta / dx[0]) * result_flux_0n;
        } while (0);
        return result;
      };
      output[1].set(integrator.sum<2>(op), row, col);
    }
  }
  output[2] = toki::matrix::Mat(dof_C, dof_P);
  for (toki::Size col = 0; col < output[2].extent[1]; ++col) {
    auto index_col = toki::math::index::ind2sub<2>(size_col, col);
    for (toki::Size row = 0; row < output[2].extent[0]; ++row) {
      auto index_row = toki::math::index::ind2sub<1>(size_row, row);
      auto op = [&](std::array<toki::Number, 2> const& coord) -> toki::Number {
        toki::Number result{0};
        // flux: space 0+
        do {
          toki::Number result_flux_0p{1.0};
          // col
          if (info.flux_type[0] == toki::timestepper::tag::Flux::Uniform) {
            result_flux_0p *= info.op.get_flux_uniform_upwind[0](
              false, 0.0, {basis.eval<2>({coord[0], -1.0}, index_col)});
          } else {
            toki::log::error("no implementation");
          }
          // row
          result_flux_0p *= basis.eval<1>({+1.0}, index_row);
          // update
          result += 0.5 * (-info.param.time_delta / dx[0]) * result_flux_0p;
        } while (0);
        return result;
      };
      output[2].set(integrator.sum<2>(op), row, col);
    }
  }
  return output;
}
} // namespace toki::timestepper::detail
