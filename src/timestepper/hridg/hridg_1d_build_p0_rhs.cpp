#include <array>
#include "toki/log.hpp"
#include "toki/math/index.hpp"
#include "toki/quadrature/lobatto.hpp"
#include "toki/timestepper/detail/hridg_1d.hpp"
namespace toki::timestepper::detail {
auto
HRIDG_1D::build_P0_RHS() -> toki::matrix::Mat
{
  auto const& dof = info.domain_spec.at("default").quadrature_point;
  toki::Size const dof_C{dof};
  toki::Size const dof_P{dof_C * dof};
  std::array<toki::Size, 1> size_col{dof};
  std::array<toki::Size, 2> size_row{dof, dof};
  auto const& basis = info.domain_spec.at("default").basis;
  auto const& integrator = info.domain_spec.at("default").integrator;
  toki::matrix::Mat output(dof_P, dof_C);
  for (toki::Size col = 0; col < output.extent[1]; ++col) {
    auto index_col = toki::math::index::ind2sub<1>(size_col, col);
    for (toki::Size row = 0; row < output.extent[0]; ++row) {
      auto index_row = toki::math::index::ind2sub<2>(size_row, row);
      auto op = [&](std::array<toki::Number, 2> const& coord) -> toki::Number {
        toki::Number result{0};
        // flux: time
        do {
          toki::Number result_flux{1.0};
          // col
          result_flux *= basis.eval<1>({coord[1]}, index_col);
          // row
          result_flux *= basis.eval<2>({-1.0, coord[1]}, index_row);
          // update
          result += 0.5 * result_flux;
        } while (0);
        return result;
      };
      output.set(integrator.sum<2>(op), row, col);
    }
  }
  return output;
}
} // namespace toki::timestepper::detail
