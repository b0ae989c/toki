#include "toki/timestepper/detail/hridg_1d.hpp"
namespace toki::timestepper::detail {
auto
HRIDG_1D::create_domain(
  std::string const& name,
  toki::Size const& variable,
  toki::Size const& dof,
  toki::Size const& quadrature_point,
  std::array<toki::Size, 1> const& resolution,
  std::array<toki::Size, 1> const& halo,
  std::array<toki::Number, 1> const& range,
  std::array<toki::Number, 1> const& anchor,
  std::array<int, 1> const& mpi_extent) -> HRIDG_1D&
{
  info.domain_spec.insert_or_assign(
    name,
    HRIDG_Info<1>::DomainSpec{
      .variable = variable,
      .dof = dof,
      .quadrature_point = quadrature_point,
      .basis = toki::basis::Nodal(quadrature_point),
      .integrator = toki::quadrature::Lobatto(quadrature_point + 1),
    });
  info.domain.insert_or_assign(
    name,
    decltype(info.domain)::mapped_type(
      variable * dof,
      quadrature_point,
      resolution,
      halo,
      range,
      anchor,
      mpi_extent));
  return *this;
}
} // namespace toki::timestepper::detail
