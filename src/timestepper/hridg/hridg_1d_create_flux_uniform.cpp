#include "toki/timestepper/detail/hridg_1d.hpp"
#include "toki/utility.hpp"
namespace toki::timestepper::detail {
auto
HRIDG_1D::create_flux_uniform(
  toki::Size const& direction,
  std::function<
    toki::Number(toki::Number const&, std::vector<toki::Number> const&)> const&
    op) -> HRIDG_1D&
{
  toki::utility::confirm(direction < 1, "invalid parameter: direction");
  info.flux_type[direction] = toki::timestepper::tag::Flux::Uniform;
  info.op.get_flux_uniform[direction] = op;
  return *this;
}
} // namespace toki::timestepper::detail
