#include "toki/timestepper/detail/hridg_1d.hpp"
#include "toki/utility.hpp"
namespace toki::timestepper::detail {
auto
HRIDG_1D::create_flux_uniform_upwind(
  toki::Size const& direction,
  std::function<toki::Number(
    bool const&,
    toki::Number const&,
    std::vector<toki::Number> const&)> const& op) -> HRIDG_1D&
{
  toki::utility::confirm(direction < 1, "invalid parameter: direction");
  toki::utility::confirm(
    info.flux_type[direction] == toki::timestepper::tag::Flux::Uniform,
    "incompatible flux type");
  info.op.get_flux_uniform_upwind[direction] = op;
  return *this;
}
} // namespace toki::timestepper::detail
