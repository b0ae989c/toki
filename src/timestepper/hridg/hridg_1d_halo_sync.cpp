#include "mpi.h"
#include "toki/timestepper/detail/hridg_1d.hpp"
namespace toki::timestepper::detail {
auto
HRIDG_1D::halo_sync() -> HRIDG_1D&
{
  MPI_Barrier(MPI_COMM_WORLD);
  info.domain.at("default").halo_dispatch(0).halo_wait(0);
  MPI_Barrier(MPI_COMM_WORLD);
  info.domain.at("default").halo_update(0);
  return *this;
}
} // namespace toki::timestepper::detail
