#include "toki/log.hpp"
#include "toki/math/permutation.hpp"
#include "toki/math/seq.hpp"
#include "toki/timestepper/detail/hridg_1d.hpp"
namespace toki::timestepper::detail {
auto
HRIDG_1D::prepare() -> HRIDG_1D&
{
  auto const& domain_spec = info.domain_spec.at("default");
  // build cache
  auto const dof_C = domain_spec.variable * domain_spec.dof;
  auto const dof_P = dof_C * domain_spec.quadrature_point;
  if (info.flux_type[0] == toki::timestepper::tag::Flux::Uniform) {
    if (domain_spec.variable == 1) {
      // P0
      cache.P0_LHS = build_P0_LHS();
      cache.P0_LHS_p =
        toki::math::permutation::Permutation(cache.P0_LHS.extent[0]);
      toki::math::seq::getf(
        dof_P, dof_P, cache.P0_LHS, 0, 1, 0, 1, cache.P0_LHS_p, 0, 1);
      cache.P0_RHS = build_P0_RHS();
      // P1
      cache.P1_LHS = build_P1_LHS();
      cache.P1_LHS_p =
        toki::math::permutation::Permutation(cache.P1_LHS.extent[0]);
      toki::math::seq::getf(
        dof_P, dof_P, cache.P1_LHS, 0, 1, 0, 1, cache.P1_LHS_p, 0, 1);
      cache.P1_RHS = build_P1_RHS();
      // P2
      cache.P2_LHS = cache.P1_LHS;
      cache.P2_LHS_p = cache.P1_LHS_p;
      cache.P2_RHS = cache.P1_RHS;
      // C0
      cache.C0_LHS = build_C0_LHS();
      cache.C0_LHS_p =
        toki::math::permutation::Permutation(cache.C0_LHS.extent[0]);
      toki::math::seq::getf(
        dof_C, dof_C, cache.C0_LHS, 0, 1, 0, 1, cache.C0_LHS_p, 0, 1);
      cache.C0_RHS = build_C0_RHS();
    } else {
      toki::log::error("no implementation");
    }
  } else {
    toki::log::error("no implementation");
  }
  return *this;
}
} // namespace toki::timestepper::detail
