#include <array>
#include <cmath>
#include "fmt/format.h"
#include "mpi.h"
#include "toki/config.hpp"
#include "toki/log.hpp"
#include "toki/timestepper/detail/hridg_1d.hpp"
namespace toki::timestepper::detail {
auto
HRIDG_1D::report(std::string const& name) -> HRIDG_1D&
{
  save(name, "default");
  if (info.param.flag_report_error) {
    auto& domain = info.domain.at("default");
    auto const& halo = domain.info.halo;
    auto const& resolution = domain.info.local.resolution;
    auto const& anchor = domain.info.local.anchor;
    auto const& volume = domain.info.local.element.volume;
    auto const& basis = info.domain_spec.at("default").basis;
    toki::vector::Vec Q(domain.info.dof);
    std::array<toki::Number, 1> coord{};
    toki::Number local_diff{0.0};
    toki::Number local_exact{0.0};
    for (toki::Size index_0 = halo[0]; index_0 < resolution[0] - halo[0];
         ++index_0) {
      domain.get_element(Q, 0, {index_0});
      for (toki::Size ell = 0; ell < domain.info.dof; ++ell) {
        coord[0] = anchor[0] + (static_cast<toki::Number>(index_0) +
                                (basis.info.abscissa.at(ell) + 1.0) / 2.0) *
                                 volume[0];
        auto const solution = info.op.find_solution(0, info.param.time, coord);
        local_diff += std::pow(solution - Q.at(ell), 2.0);
        local_exact += std::pow(solution, 2.0);
      }
    }
    toki::Number buffer_out[]{local_diff, local_exact};
    toki::Number buffer_in[2];
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Allreduce(
      buffer_out, buffer_in, 2, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    toki::log::info(
      fmt::format("L2 error: {:+.6e}", std::sqrt(buffer_in[0] / buffer_in[1])));
  }
  return *this;
}
} // namespace toki::timestepper::detail
