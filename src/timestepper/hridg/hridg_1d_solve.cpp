#include "fmt/format.h"
#include "mpi.h"
#include "toki/log.hpp"
#include "toki/timestepper/detail/hridg_1d.hpp"
namespace toki::timestepper::detail {
[[deprecated]]
auto
HRIDG_1D::solve(toki::Size step_max) -> HRIDG_1D&
{
  if (step_max) {
    while (step_max--) {
      stop(info.param.CFL, info.param.signal_speed);
      prepare().halo_sync().solve_prediction().solve_correction();
      info.param.time += info.param.time_delta;
      info.param.step += 1;
      toki::log::info(fmt::format(
        "{:09d}│{:+.6e}│{:7.3f}%│dt:{:+.3e}",
        info.param.step,
        info.param.time,
        info.param.time / info.param.time_max * 100.0,
        info.param.time_delta));
      MPI_Barrier(MPI_COMM_WORLD);
    }
  } else {
    while (!stop(info.param.CFL, info.param.signal_speed)) {
      prepare().halo_sync().solve_prediction().solve_correction();
      info.param.time += info.param.time_delta;
      info.param.step += 1;
      toki::log::info(fmt::format(
        "{:09d}│{:+.6e}│{:7.3f}%│dt:{:+.3e}",
        info.param.step,
        info.param.time,
        info.param.time / info.param.time_max * 100.0,
        info.param.time_delta));
      MPI_Barrier(MPI_COMM_WORLD);
    }
  }
  return *this;
}
} // namespace toki::timestepper::detail
