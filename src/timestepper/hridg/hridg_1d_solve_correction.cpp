#include "omp.h"
#include "toki/config.hpp"
#include "toki/math/seq.hpp"
#include "toki/timestepper/detail/hridg_1d.hpp"
#include "toki/vector/vec.hpp"
namespace toki::timestepper::detail {
auto
HRIDG_1D::solve_correction() -> HRIDG_1D&
{
  auto const& halo = info.domain.at("default").info.halo;
  auto const& resolution = info.domain.at("default").info.local.resolution;
  auto const dof_C = cache.C0_LHS.extent[1];
  auto const dof_P = cache.C0_RHS[0].extent[1];
#pragma omp parallel default(none) shared(halo, resolution, dof_C, dof_P)
  {
    toki::vector::Vec W(3 * dof_P);
    toki::vector::Vec RHS(dof_C);
    toki::Size index_0;
#pragma omp for schedule(runtime)
    for (index_0 = halo[0]; index_0 < resolution[0] - halo[0]; ++index_0) {
      info.domain.at("result_P2")
        .get_element(W, 0 * dof_P, {index_0})
        .get_element(W, 1 * dof_P, {index_0 - 1})
        .get_element(W, 2 * dof_P, {index_0 + 1});
      toki::math::seq::gemv(
        cache.C0_RHS[0].extent[0],
        cache.C0_RHS[0].extent[1],
        toki::one<toki::Number>,
        toki::zero<toki::Number>,
        W,
        0 * dof_P,
        1,
        RHS,
        0,
        1,
        cache.C0_RHS[0],
        0,
        1,
        0,
        1);
      for (toki::Size tag = 0; tag < 2; ++tag) {
        toki::math::seq::gemv(
          cache.C0_RHS[tag + 1].extent[0],
          cache.C0_RHS[tag + 1].extent[1],
          toki::one<toki::Number>,
          toki::one<toki::Number>,
          W,
          (tag + 1) * dof_P,
          1,
          RHS,
          0,
          1,
          cache.C0_RHS[tag + 1],
          0,
          1,
          0,
          1);
      }
      toki::math::seq::gets(
        RHS.extent[0],
        1,
        toki::one<toki::Number>,
        cache.C0_LHS,
        cache.C0_LHS_p,
        RHS);
      info.domain.at("default").adjust_element(RHS, 0, {index_0});
    }
  }
  return *this;
}
} // namespace toki::timestepper::detail
