#include "omp.h"
#include "toki/config.hpp"
#include "toki/math/seq.hpp"
#include "toki/timestepper/detail/hridg_1d.hpp"
#include "toki/vector/vec.hpp"
namespace toki::timestepper::detail {
auto
HRIDG_1D::solve_prediction() -> HRIDG_1D&
{
  auto const& resolution = info.domain.at("default").info.local.resolution;
  auto const dof_C = cache.P0_RHS.extent[1];
  auto const dof_P = cache.P0_LHS.extent[1];
#pragma omp parallel default(none) shared(resolution, dof_C, dof_P)
  {
    toki::vector::Vec Q(dof_C);
    toki::vector::Vec W(2 * dof_P);
    toki::vector::Vec RHS(dof_P);
    toki::Size index_0;
    // P0
#pragma omp for schedule(runtime)
    for (index_0 = 0; index_0 < resolution[0]; ++index_0) {
      info.domain.at("default").get_element(Q, 0, {index_0});
      toki::math::seq::gemv(
        cache.P0_RHS.extent[0],
        cache.P0_RHS.extent[1],
        toki::one<toki::Number>,
        toki::zero<toki::Number>,
        Q,
        0,
        1,
        RHS,
        0,
        1,
        cache.P0_RHS,
        0,
        1,
        0,
        1);
      toki::math::seq::gets(
        RHS.extent[0],
        1,
        toki::one<toki::Number>,
        cache.P0_LHS,
        cache.P0_LHS_p,
        RHS);
      info.domain.at("result_P0").set_element(RHS, 0, {index_0});
    }
    // P1
#pragma omp for schedule(runtime)
    for (index_0 = 1; index_0 < resolution[0] - 1; ++index_0) {
      info.domain.at("default").get_element(Q, 0, {index_0});
      info.domain.at("result_P0")
        .get_element(W, 0 * dof_P, {index_0 - 1})
        .get_element(W, 1 * dof_P, {index_0 + 1});
      toki::math::seq::gemv(
        cache.P1_RHS[0].extent[0],
        cache.P1_RHS[0].extent[1],
        toki::one<toki::Number>,
        toki::zero<toki::Number>,
        Q,
        0,
        1,
        RHS,
        0,
        1,
        cache.P1_RHS[0],
        0,
        1,
        0,
        1);
      for (toki::Size tag = 0; tag < 2; ++tag) {
        toki::math::seq::gemv(
          cache.P1_RHS[tag + 1].extent[0],
          cache.P1_RHS[tag + 1].extent[1],
          toki::one<toki::Number>,
          toki::one<toki::Number>,
          W,
          tag * dof_P,
          1,
          RHS,
          0,
          1,
          cache.P1_RHS[tag + 1],
          0,
          1,
          0,
          1);
      }
      toki::math::seq::gets(
        RHS.extent[0],
        1,
        toki::one<toki::Number>,
        cache.P1_LHS,
        cache.P1_LHS_p,
        RHS);
      info.domain.at("result_P1").set_element(RHS, 0, {index_0});
    }
    // P2
#pragma omp for schedule(runtime)
    for (index_0 = 2; index_0 < resolution[0] - 2; ++index_0) {
      info.domain.at("default").get_element(Q, 0, {index_0});
      info.domain.at("result_P1")
        .get_element(W, 0 * dof_P, {index_0 - 1})
        .get_element(W, 1 * dof_P, {index_0 + 1});
      toki::math::seq::gemv(
        cache.P2_RHS[0].extent[0],
        cache.P2_RHS[0].extent[1],
        toki::one<toki::Number>,
        toki::zero<toki::Number>,
        Q,
        0,
        1,
        RHS,
        0,
        1,
        cache.P2_RHS[0],
        0,
        1,
        0,
        1);
      for (toki::Size tag = 0; tag < 2; ++tag) {
        toki::math::seq::gemv(
          cache.P2_RHS[tag + 1].extent[0],
          cache.P2_RHS[tag + 1].extent[1],
          toki::one<toki::Number>,
          toki::one<toki::Number>,
          W,
          tag * dof_P,
          1,
          RHS,
          0,
          1,
          cache.P2_RHS[tag + 1],
          0,
          1,
          0,
          1);
      }
      toki::math::seq::gets(
        RHS.extent[0],
        1,
        toki::one<toki::Number>,
        cache.P2_LHS,
        cache.P2_LHS_p,
        RHS);
      info.domain.at("result_P2").set_element(RHS, 0, {index_0});
    }
  }
  return *this;
}
} // namespace toki::timestepper::detail
