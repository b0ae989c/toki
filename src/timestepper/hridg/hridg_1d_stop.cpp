#include <algorithm>
#include "toki/timestepper/detail/hridg_1d.hpp"
namespace toki::timestepper::detail {
auto
HRIDG_1D::stop(toki::Number const& CFL, toki::Number const& signal_speed)
  -> bool
{
  if (info.param.time == info.param.time_max) {
    return true;
  }
  auto const& h = info.domain.at("default").info.local.element.volume[0];
  info.param.time_delta =
    std::min(CFL * h / signal_speed, info.param.time_delta_max);
  if (info.param.time + info.param.time_delta > info.param.time_max) {
    info.param.time_delta = info.param.time_max - info.param.time;
  } else if (
    info.param.time_max - (info.param.time + info.param.time_delta) < 1e-9) {
    info.param.time_delta *= 0.5;
  }
  if (info.param.step >= info.param.step_max) {
    return true;
  }
  return false;
}
} // namespace toki::timestepper::detail
