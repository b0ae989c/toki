#include "toki/timestepper/detail/hridg_2d.hpp"
namespace toki::timestepper::detail {
HRIDG_2D::HRIDG_2D()
  : info{}
  , cache{}
{
}
HRIDG_2D::HRIDG_2D(std::string const& name)
  : info{.name = name}
  , cache{}
{
}
HRIDG_2D::HRIDG_2D(HRIDG_2D const& other)
  : info(other.info)
  , cache(other.cache)
{
}
HRIDG_2D::HRIDG_2D(HRIDG_2D&& other) noexcept
  : info(std::exchange(other.info, decltype(info){}))
  , cache(std::exchange(other.cache, decltype(cache){}))
{
}
HRIDG_2D::~HRIDG_2D() {}
auto
HRIDG_2D::operator=(HRIDG_2D other) -> HRIDG_2D&
{
  swap(*this, other);
  return *this;
}
} // namespace toki::timestepper::detail
