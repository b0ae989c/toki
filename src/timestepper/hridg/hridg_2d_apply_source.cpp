#include "toki/timestepper/detail/hridg_2d.hpp"
namespace toki::timestepper::detail {
auto
HRIDG_2D::apply_source(
  std::array<toki::Size, 2> const& index,
  toki::vector::Vec& buffer) -> HRIDG_2D&
{
  if (!info.param.flag_apply_source) {
    return *this;
  }
  auto const& dof = info.domain_spec.at("default").quadrature_point;
  auto const& basis = info.domain_spec.at("default").basis;
  auto const& integrator = info.domain_spec.at("default").integrator;
  auto const& anchor = info.domain.at("default").info.local.anchor;
  auto const& volume = info.domain.at("default").info.local.element.volume;
  for (toki::Size index_v0 = 0; index_v0 < dof; ++index_v0) {
    for (toki::Size index_r0 = 0; index_r0 < dof; ++index_r0) {
      if (buffer.extent[0] == dof * dof) {
        auto op =
          [&](std::array<toki::Number, 3> const& coord) -> toki::Number {
          auto const basis_r0 = basis.eval<1>({coord[1]}, {index_r0});
          auto const basis_v0 = basis.eval<1>({coord[2]}, {index_v0});
          toki::Number const time =
            info.param.time + ((coord[0] + 1.0) / 2.0) * info.param.time_delta;
          toki::Number const r0 =
            anchor[0] +
            (static_cast<toki::Number>(index[0]) + (coord[1] + 1.0) / 2.0) *
              volume[0];
          toki::Number const v0 =
            anchor[1] +
            (static_cast<toki::Number>(index[1]) + (coord[2] + 1.0) / 2.0) *
              volume[1];
          toki::Number const s = info.op.find_source(0, time, {r0, v0});
          return s * basis_r0 * basis_v0;
        };
        buffer.set(
          buffer.at(index_r0 + dof * index_v0) +
            integrator.sum<3>(op) * info.param.time_delta / 2.0,
          index_r0 + dof * index_v0);
      } else {
        for (toki::Size index_time = 0; index_time < dof; ++index_time) {
          auto op =
            [&](std::array<toki::Number, 3> const& coord) -> toki::Number {
            auto const basis_time = basis.eval<1>({coord[0]}, {index_time});
            auto const basis_r0 = basis.eval<1>({coord[1]}, {index_r0});
            auto const basis_v0 = basis.eval<1>({coord[2]}, {index_v0});
            toki::Number const time =
              info.param.time +
              ((coord[0] + 1.0) / 2.0) * info.param.time_delta;
            toki::Number const r0 =
              anchor[0] +
              (static_cast<toki::Number>(index[0]) + (coord[1] + 1.0) / 2.0) *
                volume[0];
            toki::Number const v0 =
              anchor[1] +
              (static_cast<toki::Number>(index[1]) + (coord[2] + 1.0) / 2.0) *
                volume[1];
            toki::Number const s = info.op.find_source(0, time, {r0, v0});
            return s * basis_time * basis_r0 * basis_v0;
          };
          buffer.set(
            buffer.at(index_time + dof * (index_r0 + dof * index_v0)) +
              integrator.sum<3>(op) * info.param.time_delta / 2.0,
            index_time + dof * (index_r0 + dof * index_v0));
        }
      }
    }
  }
  return *this;
}
} // namespace toki::timestepper::detail
