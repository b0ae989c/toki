#include <array>
#include "toki/log.hpp"
#include "toki/math/index.hpp"
#include "toki/quadrature/lobatto.hpp"
#include "toki/timestepper/detail/hridg_2d.hpp"
namespace toki::timestepper::detail {
auto
HRIDG_2D::build_C0_LHS() -> toki::matrix::Mat
{
  auto const& dof = info.domain_spec.at("default").quadrature_point;
  toki::Size const dof_C{dof * dof};
  std::array<toki::Size, 2> size_col{dof, dof};
  auto const& basis = info.domain_spec.at("default").basis;
  auto const& integrator = info.domain_spec.at("default").integrator;
  toki::matrix::Mat output(dof_C, dof_C);
  for (toki::Size col = 0; col < output.extent[1]; ++col) {
    auto index_col = toki::math::index::ind2sub<2>(size_col, col);
    for (toki::Size row = 0; row < output.extent[0]; ++row) {
      auto index_row = toki::math::index::ind2sub<2>(size_col, row);
      auto op = [&](std::array<toki::Number, 3> const& coord) -> toki::Number {
        toki::Number result{1.0};
        // col
        result *= basis.eval<2>({coord[1], coord[2]}, index_col);
        // row
        result *= basis.eval<2>({coord[1], coord[2]}, index_row);
        return 0.5 * result;
      };
      output.set(integrator.sum<3>(op), row, col);
    }
  }
  return output;
}
} // namespace toki::timestepper::detail
