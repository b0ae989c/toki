#include <array>
#include "toki/log.hpp"
#include "toki/math/index.hpp"
#include "toki/quadrature/lobatto.hpp"
#include "toki/timestepper/detail/hridg_2d.hpp"
#include "toki/utility.hpp"
namespace toki::timestepper::detail {
auto
HRIDG_2D::build_C0_RHS() -> std::array<toki::matrix::Mat, 5>
{
  auto const& dof = info.domain_spec.at("default").quadrature_point;
  toki::Size const dof_C{dof * dof};
  toki::Size const dof_P{dof_C * dof};
  std::array<toki::Size, 3> size_col{dof, dof, dof};
  std::array<toki::Size, 2> size_row{dof, dof};
  auto const& basis = info.domain_spec.at("default").basis;
  auto const& integrator = info.domain_spec.at("default").integrator;
  auto const& dx = info.domain.at("default").info.local.element.volume;
  auto apply_flux_space =
    [&](
      toki::Number& result,
      toki::Number const& sign,
      std::array<toki::Number, 3> coord,
      toki::Size const& target,
      std::array<toki::Size, 3> const& index_col,
      std::array<toki::Size, 2> const& index_row) -> void {
    toki::Number result_flux{1.0};
    // col
    coord[target + 1] = sign;
    if (info.flux_type[target] == toki::timestepper::tag::Flux::Uniform) {
      result_flux *= info.op.get_flux_uniform_upwind[target](
        (sign > 0.0), 0.0, {basis.eval<3>(coord, index_col)});
    } else {
      toki::log::error("no implementation");
    }
    // row
    std::array<toki::Number, 2> tmp_coord{coord[1], coord[2]};
    tmp_coord[target] = sign;
    result_flux *= basis.eval<2>(tmp_coord, index_row);
    // update
    result += 0.5 * (-sign * info.param.time_delta / dx[target]) * result_flux;
    return;
  };
  auto apply_flux_space_branch =
    [&](
      toki::Number& result,
      toki::Number const& sign,
      std::array<toki::Number, 3> coord,
      toki::Size const& target,
      std::array<toki::Size, 3> const& index_col,
      std::array<toki::Size, 2> const& index_row) -> void {
    toki::Number result_flux{1.0};
    // col
    coord[target + 1] = -sign;
    if (info.flux_type[target] == toki::timestepper::tag::Flux::Uniform) {
      result_flux *= info.op.get_flux_uniform_upwind[target](
        (-sign > 0.0), 0.0, {basis.eval<3>(coord, index_col)});
    } else {
      toki::log::error("no implementation");
    }
    // row
    std::array<toki::Number, 2> tmp_coord{coord[1], coord[2]};
    tmp_coord[target] = sign;
    result_flux *= basis.eval<2>(tmp_coord, index_row);
    // update
    result += 0.5 * (-sign * info.param.time_delta / dx[target]) * result_flux;
    return;
  };
  auto apply_evolution = [&](
                           toki::Number& result,
                           std::array<toki::Number, 3> const& coord,
                           std::array<toki::Size, 3> const& index_col,
                           std::array<toki::Size, 2> const& index_row) -> void {
    toki::Number result_evolution{1.0};
    // col
    result_evolution *= basis.eval<3>(coord, index_col);
    // row
    std::array<toki::Number, 2> tmp_coord{coord[1], coord[2]};
    toki::Number result_evolution_space[2];
    for (toki::Size target = 0; target < 2; ++target) {
      if (info.flux_type[target] == toki::timestepper::tag::Flux::Uniform) {
        result_evolution_space[target] = info.op.get_flux_uniform[target](
          0.0, {basis.eval<2>(tmp_coord, index_row, target, 1)});
      } else {
        toki::log::error("no implementation");
      }
    }
    // update
    result += result_evolution *
              ((info.param.time_delta / dx[0] * result_evolution_space[0]) +
               (info.param.time_delta / dx[1] * result_evolution_space[1]));
    return;
  };
  std::array<toki::matrix::Mat, 5> output{};
  output[0] = toki::matrix::Mat(dof_C, dof_P);
  for (toki::Size col = 0; col < output[0].extent[1]; ++col) {
    auto index_col = toki::math::index::ind2sub<3>(size_col, col);
    for (toki::Size row = 0; row < output[0].extent[0]; ++row) {
      auto index_row = toki::math::index::ind2sub<2>(size_row, row);
      auto op = [&](std::array<toki::Number, 3> const& coord) -> toki::Number {
        toki::Number result{0};
        // flux: space
        for (toki::Size target = 0; target < 2; ++target) {
          apply_flux_space(result, -1.0, coord, target, index_col, index_row);
          apply_flux_space(result, +1.0, coord, target, index_col, index_row);
        }
        // evolution
        apply_evolution(result, coord, index_col, index_row);
        return result;
      };
      output[0].set(integrator.sum<3>(op), row, col);
    }
  }
  toki::Number sign[2]{-1.0, +1.0};
  for (toki::Size direction = 0; direction < 2; ++direction) {
    for (toki::Size tag = 0; tag < 2; ++tag) {
      output[direction * 2 + tag + 1] = toki::matrix::Mat(dof_C, dof_P);
      for (toki::Size col = 0; col < output[direction * 2 + tag + 1].extent[1];
           ++col) {
        auto index_col = toki::math::index::ind2sub<3>(size_col, col);
        for (toki::Size row = 0;
             row < output[direction * 2 + tag + 1].extent[0];
             ++row) {
          auto index_row = toki::math::index::ind2sub<2>(size_row, row);
          auto op =
            [&](std::array<toki::Number, 3> const& coord) -> toki::Number {
            toki::Number result{0};
            // flux: space
            apply_flux_space_branch(
              result, sign[tag], coord, direction, index_col, index_row);
            return result;
          };
          output[direction * 2 + tag + 1].set(integrator.sum<3>(op), row, col);
        }
      }
    }
  }
  return output;
}
auto
HRIDG_2D::build_C0_RHS(std::array<toki::Size, 2> const& index_center)
  -> std::array<toki::matrix::Mat, 5>
{
  toki::utility::confirm(
    (info.flux_type[0] == toki::timestepper::tag::Flux::External) ||
      (info.flux_type[1] == toki::timestepper::tag::Flux::External),
    "incompatible flux type");
  auto const& dof = info.domain_spec.at("default").quadrature_point;
  toki::Size const dof_C{dof * dof};
  toki::Size const dof_P{dof_C * dof};
  std::array<toki::Size, 3> size_col{dof, dof, dof};
  std::array<toki::Size, 2> size_row{dof, dof};
  auto const& basis = info.domain_spec.at("default").basis;
  auto const& integrator = info.domain_spec.at("default").integrator;
  auto const& dx = info.domain.at("default").info.local.element.volume;
  auto apply_flux_space = [&](
                            toki::Number& result,
                            toki::Number const& sign,
                            std::array<toki::Number, 3> coord,
                            toki::Size const& target,
                            std::array<toki::Size, 3> const& index_col,
                            std::array<toki::Size, 2> const& index_row,
                            std::array<toki::Size, 2> const& index,
                            toki::Size const& offset) -> void {
    if (sign < 0.0) {
      if ((index_col[target + 1] != 0) && (index_row[target] != 0)) {
        return;
      }
    } else {
      if (
        (index_col[target + 1] != dof - 1) && (index_row[target] != dof - 1)) {
        return;
      }
    }
    toki::Number result_flux{1.0};
    // col
    coord[target + 1] = sign;
    if (info.flux_type[target] == toki::timestepper::tag::Flux::External) {
      result_flux *= info.op.get_flux_external_upwind[target](
        (sign > 0.0), 0.0, {basis.eval<3>(coord, index_col)}, index, offset);
    } else {
      toki::log::error("no implementation");
    }
    // row
    std::array<toki::Number, 2> tmp_coord{coord[1], coord[2]};
    tmp_coord[target] = sign;
    result_flux *= basis.eval<2>(tmp_coord, index_row);
    // update
    result += 0.5 * (-sign * info.param.time_delta / dx[target]) * result_flux;
    return;
  };
  auto apply_flux_space_branch = [&](
                                   toki::Number& result,
                                   toki::Number const& sign,
                                   std::array<toki::Number, 3> coord,
                                   toki::Size const& target,
                                   std::array<toki::Size, 3> const& index_col,
                                   std::array<toki::Size, 2> const& index_row,
                                   std::array<toki::Size, 2> const& index,
                                   toki::Size const& offset) -> void {
    if (sign < 0.0) {
      if ((index_col[target + 1] != dof - 1) && (index_row[target] != 0)) {
        return;
      }
    } else {
      if ((index_col[target + 1] != 0) && (index_row[target] != dof - 1)) {
        return;
      }
    }
    toki::Number result_flux{1.0};
    // col
    coord[target + 1] = -sign;
    if (info.flux_type[target] == toki::timestepper::tag::Flux::External) {
      result_flux *= info.op.get_flux_external_upwind[target](
        (-sign > 0.0), 0.0, {basis.eval<3>(coord, index_col)}, index, offset);
    } else {
      toki::log::error("no implementation");
    }
    // row
    std::array<toki::Number, 2> tmp_coord{coord[1], coord[2]};
    tmp_coord[target] = sign;
    result_flux *= basis.eval<2>(tmp_coord, index_row);
    // update
    result += 0.5 * (-sign * info.param.time_delta / dx[target]) * result_flux;
    return;
  };
  auto apply_evolution = [&](
                           toki::Number& result,
                           std::array<toki::Number, 3> const& coord,
                           std::array<toki::Size, 3> const& index_col,
                           std::array<toki::Size, 2> const& index_row,
                           std::array<toki::Size, 2> const& index,
                           toki::Size const& offset) -> void {
    toki::Number result_evolution{1.0};
    // col
    result_evolution *= basis.eval<3>(coord, index_col);
    // row
    std::array<toki::Number, 2> tmp_coord{coord[1], coord[2]};
    toki::Number result_evolution_space[2];
    for (toki::Size target = 0; target < 2; ++target) {
      if (info.flux_type[target] == toki::timestepper::tag::Flux::External) {
        result_evolution_space[target] = info.op.get_flux_external[target](
          0.0, {basis.eval<2>(tmp_coord, index_row, target, 1)}, index, offset);
      } else {
        toki::log::error("no implementation");
      }
    }
    // update
    result += result_evolution *
              ((info.param.time_delta / dx[0] * result_evolution_space[0]) +
               (info.param.time_delta / dx[1] * result_evolution_space[1]));
    return;
  };
  std::array<toki::matrix::Mat, 5> output{};
  output[0] = toki::matrix::Mat(dof_C, dof_P);
  for (toki::Size col = 0; col < output[0].extent[1]; ++col) {
    auto index_col = toki::math::index::ind2sub<3>(size_col, col);
    for (toki::Size row = 0; row < output[0].extent[0]; ++row) {
      auto index_row = toki::math::index::ind2sub<2>(size_row, row);
      auto op = [&](
                  std::array<toki::Number, 3> const& coord,
                  toki::Size const& offset) -> toki::Number {
        toki::Number result{0};
        // flux: space
        for (toki::Size target = 0; target < 2; ++target) {
          apply_flux_space(
            result,
            -1.0,
            coord,
            target,
            index_col,
            index_row,
            index_center,
            offset);
          apply_flux_space(
            result,
            +1.0,
            coord,
            target,
            index_col,
            index_row,
            index_center,
            offset);
        }
        // evolution
        apply_evolution(
          result, coord, index_col, index_row, index_center, offset);
        return result;
      };
      output[0].set(integrator.sum<3>(op), row, col);
    }
  }
  toki::Number sign[2]{-1.0, +1.0};
  for (toki::Size direction = 0; direction < 2; ++direction) {
    for (toki::Size tag = 0; tag < 2; ++tag) {
      output[direction * 2 + tag + 1] = toki::matrix::Mat(dof_C, dof_P);
      for (toki::Size col = 0; col < output[direction * 2 + tag + 1].extent[1];
           ++col) {
        auto index_col = toki::math::index::ind2sub<3>(size_col, col);
        for (toki::Size row = 0;
             row < output[direction * 2 + tag + 1].extent[0];
             ++row) {
          auto index_row = toki::math::index::ind2sub<2>(size_row, row);
          auto op = [&](
                      std::array<toki::Number, 3> const& coord,
                      toki::Size const& offset) -> toki::Number {
            toki::Number result{0};
            // flux: space
            if (direction == 0) {
              apply_flux_space_branch(
                result,
                sign[tag],
                coord,
                direction,
                index_col,
                index_row,
                {index_center[0] - 1 + tag * 2, index_center[1]},
                offset);
            } else {
              apply_flux_space_branch(
                result,
                sign[tag],
                coord,
                direction,
                index_col,
                index_row,
                {index_center[0], index_center[1] - 1 + tag * 2},
                offset);
            }
            return result;
          };
          output[direction * 2 + tag + 1].set(integrator.sum<3>(op), row, col);
        }
      }
    }
  }
  return output;
}
} // namespace toki::timestepper::detail
