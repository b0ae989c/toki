#include <array>
#include "toki/log.hpp"
#include "toki/math/index.hpp"
#include "toki/math/seq.hpp"
#include "toki/quadrature/lobatto.hpp"
#include "toki/timestepper/detail/hridg_2d.hpp"
#include "toki/utility.hpp"
namespace toki::timestepper::detail {
auto
HRIDG_2D::build_P0_LHS() -> toki::matrix::Mat
{
  auto const& dof = info.domain_spec.at("default").quadrature_point;
  toki::Size const dof_P{dof * dof * dof};
  std::array<toki::Size, 3> size_col{dof, dof, dof};
  auto const& basis = info.domain_spec.at("default").basis;
  auto const& integrator = info.domain_spec.at("default").integrator;
  auto const& dx = info.domain.at("default").info.local.element.volume;
  auto apply_flux_time = [&](
                           toki::Number& result,
                           std::array<toki::Number, 3> coord,
                           std::array<toki::Size, 3> const& index_col,
                           std::array<toki::Size, 3> const& index_row) -> void {
    toki::Number result_flux{1.0};
    coord[0] = +1.0;
    // col
    result_flux *= basis.eval<3>(coord, index_col);
    // row
    result_flux *= basis.eval<3>(coord, index_row);
    // update
    result += 0.5 * result_flux;
    return;
  };
  auto apply_flux_space =
    [&](
      toki::Number& result,
      std::array<toki::Number, 3> coord,
      toki::Size const& target,
      std::array<toki::Size, 3> const& index_col,
      std::array<toki::Size, 3> const& index_row) -> void {
    toki::Number result_flux{};
    // flux: -
    result_flux = 1.0;
    coord[target + 1] = -1.0;
    // col
    if (info.flux_type[target] == toki::timestepper::tag::Flux::Uniform) {
      result_flux *= info.op.get_flux_uniform[target](
        0.0, {basis.eval<3>(coord, index_col)});
    } else {
      toki::log::error("no implementation");
    }
    // row
    result_flux *= basis.eval<3>(coord, index_row);
    // update
    result += 0.5 * (-info.param.time_delta / dx[target]) * result_flux;
    // flux: +
    result_flux = 1.0;
    coord[target + 1] = +1.0;
    // col
    if (info.flux_type[target] == toki::timestepper::tag::Flux::Uniform) {
      result_flux *= info.op.get_flux_uniform[target](
        0.0, {basis.eval<3>(coord, index_col)});
    } else {
      toki::log::error("no implementation");
    }
    // row
    result_flux *= basis.eval<3>(coord, index_row);
    // update
    result += 0.5 * (+info.param.time_delta / dx[target]) * result_flux;
    return;
  };
  auto apply_evolution = [&](
                           toki::Number& result,
                           std::array<toki::Number, 3> const& coord,
                           std::array<toki::Size, 3> const& index_col,
                           std::array<toki::Size, 3> const& index_row) -> void {
    toki::Number result_evolution{1.0};
    // col
    result_evolution *= basis.eval<3>(coord, index_col);
    // row
    auto const result_evolution_time = basis.eval<3>(coord, index_row, 0, 1);
    toki::Number result_evolution_space[2];
    for (toki::Size target = 0; target < 2; ++target) {
      if (info.flux_type[target] == toki::timestepper::tag::Flux::Uniform) {
        result_evolution_space[target] = info.op.get_flux_uniform[target](
          0.0, {basis.eval<3>(coord, index_row, target + 1, 1)});
      } else {
        toki::log::error("no implementation");
      }
    }
    // update
    result += result_evolution *
              (-result_evolution_time +
               (-info.param.time_delta / dx[0] * result_evolution_space[0]) +
               (-info.param.time_delta / dx[1] * result_evolution_space[1]));
    return;
  };
  toki::matrix::Mat output(dof_P, dof_P);
  for (toki::Size col = 0; col < output.extent[1]; ++col) {
    auto index_col = toki::math::index::ind2sub<3>(size_col, col);
    for (toki::Size row = 0; row < output.extent[0]; ++row) {
      auto index_row = toki::math::index::ind2sub<3>(size_col, row);
      auto op = [&](std::array<toki::Number, 3> const& coord) -> toki::Number {
        toki::Number result{0};
        // flux: time
        apply_flux_time(result, coord, index_col, index_row);
        // flux: space
        apply_flux_space(result, coord, 0, index_col, index_row);
        apply_flux_space(result, coord, 1, index_col, index_row);
        // evolution
        apply_evolution(result, coord, index_col, index_row);
        return result;
      };
      output.set(integrator.sum<3>(op), row, col);
    }
  }
  return output;
}
auto
HRIDG_2D::build_P0_LHS(std::array<toki::Size, 2> const& index)
  -> toki::matrix::Mat
{
  toki::utility::confirm(
    (info.flux_type[0] == toki::timestepper::tag::Flux::External) ||
      (info.flux_type[1] == toki::timestepper::tag::Flux::External),
    "incompatible flux type");
  auto const& dof = info.domain_spec.at("default").quadrature_point;
  toki::Size const dof_P{dof * dof * dof};
  std::array<toki::Size, 3> size_col{dof, dof, dof};
  auto const& basis = info.domain_spec.at("default").basis;
  auto const& integrator = info.domain_spec.at("default").integrator;
  auto const& dx = info.domain.at("default").info.local.element.volume;
  auto apply_flux_time = [&](
                           toki::Number& result,
                           std::array<toki::Number, 3> coord,
                           std::array<toki::Size, 3> const& index_col,
                           std::array<toki::Size, 3> const& index_row) -> void {
    if (index_row[0] != dof - 1) {
      return;
    }
    toki::Number result_flux{1.0};
    coord[0] = +1.0;
    // col
    result_flux *= basis.eval<3>(coord, index_col);
    // row
    result_flux *= basis.eval<3>(coord, index_row);
    // update
    result += 0.5 * result_flux;
    return;
  };
  auto apply_flux_space = [&](
                            toki::Number& result,
                            std::array<toki::Number, 3> coord,
                            toki::Size const& target,
                            std::array<toki::Size, 3> const& index_col,
                            std::array<toki::Size, 3> const& index_row,
                            toki::Size const& offset) -> void {
    toki::Number result_flux{};
    // flux: -
    if ((index_col[target + 1] == 0) && (index_row[target + 1] == 0)) {
      result_flux = 1.0;
      coord[target + 1] = -1.0;
      // col
      if (info.flux_type[target] == toki::timestepper::tag::Flux::External) {
        result_flux *= info.op.get_flux_external[target](
          0.0, {basis.eval<3>(coord, index_col)}, index, offset);
      } else {
        toki::log::error("no implementation");
      }
      // row
      result_flux *= basis.eval<3>(coord, index_row);
      // update
      result += 0.5 * (-info.param.time_delta / dx[target]) * result_flux;
    }
    // flux: +
    if (
      (index_col[target + 1] == dof - 1) &&
      (index_row[target + 1] == dof - 1)) {
      result_flux = 1.0;
      coord[target + 1] = +1.0;
      // col
      if (info.flux_type[target] == toki::timestepper::tag::Flux::External) {
        result_flux *= info.op.get_flux_external[target](
          0.0, {basis.eval<3>(coord, index_col)}, index, offset);
      } else {
        toki::log::error("no implementation");
      }
      // row
      result_flux *= basis.eval<3>(coord, index_row);
      // update
      result += 0.5 * (+info.param.time_delta / dx[target]) * result_flux;
    }
    return;
  };
  auto apply_evolution_space = [&](
                                 toki::Number& result,
                                 std::array<toki::Number, 3> const& coord,
                                 std::array<toki::Size, 3> const& index_col,
                                 std::array<toki::Size, 3> const& index_row,
                                 toki::Size const& offset) -> void {
    toki::Number result_evolution{1.0};
    // col
    result_evolution *= basis.eval<3>(coord, index_col);
    // row
    auto const result_evolution_time = basis.eval<3>(coord, index_row, 0, 1);
    toki::Number result_evolution_space[2];
    for (toki::Size target = 0; target < 2; ++target) {
      if (info.flux_type[target] == toki::timestepper::tag::Flux::External) {
        result_evolution_space[target] = info.op.get_flux_external[target](
          0.0, {basis.eval<3>(coord, index_row, target + 1, 1)}, index, offset);
      } else {
        toki::log::error("no implementation");
      }
    }
    // update
    result += result_evolution *
              (-result_evolution_time +
               (-info.param.time_delta / dx[0] * result_evolution_space[0]) +
               (-info.param.time_delta / dx[1] * result_evolution_space[1]));
    return;
  };
  toki::matrix::Mat output(dof_P, dof_P);
  for (toki::Size col = 0; col < output.extent[1]; ++col) {
    auto index_col = toki::math::index::ind2sub<3>(size_col, col);
    for (toki::Size row = 0; row < output.extent[0]; ++row) {
      auto index_row = toki::math::index::ind2sub<3>(size_col, row);
      auto op = [&](
                  std::array<toki::Number, 3> const& coord,
                  toki::Size const& offset) -> toki::Number {
        toki::Number result{0};
        // flux: time
        apply_flux_time(result, coord, index_col, index_row);
        // flux: space
        apply_flux_space(result, coord, 0, index_col, index_row, offset);
        apply_flux_space(result, coord, 1, index_col, index_row, offset);
        // evolution: space
        apply_evolution_space(result, coord, index_col, index_row, offset);
        return result;
      };
      output.set(integrator.sum<3>(op), row, col);
    }
  }
  return output;
}
} // namespace toki::timestepper::detail
