#include <array>
#include "toki/log.hpp"
#include "toki/math/index.hpp"
#include "toki/quadrature/lobatto.hpp"
#include "toki/timestepper/detail/hridg_2d.hpp"
namespace toki::timestepper::detail {
auto
HRIDG_2D::build_P0_RHS() -> toki::matrix::Mat
{
  auto const& dof = info.domain_spec.at("default").quadrature_point;
  toki::Size const dof_C{dof * dof};
  toki::Size const dof_P{dof_C * dof};
  std::array<toki::Size, 2> size_col{dof, dof};
  std::array<toki::Size, 3> size_row{dof, dof, dof};
  auto const& basis = info.domain_spec.at("default").basis;
  auto const& integrator = info.domain_spec.at("default").integrator;
  toki::matrix::Mat output(dof_P, dof_C);
  for (toki::Size col = 0; col < output.extent[1]; ++col) {
    auto index_col = toki::math::index::ind2sub<2>(size_col, col);
    for (toki::Size row = 0; row < output.extent[0]; ++row) {
      auto index_row = toki::math::index::ind2sub<3>(size_row, row);
      if (index_row[0] != 0) {
        continue;
      }
      auto op = [&](std::array<toki::Number, 3> const& coord) -> toki::Number {
        toki::Number result{0};
        // flux: time
        do {
          toki::Number result_flux{1.0};
          // col
          result_flux *= basis.eval<2>({coord[1], coord[2]}, index_col);
          // row
          result_flux *= basis.eval<3>({-1.0, coord[1], coord[2]}, index_row);
          // update
          result += 0.5 * result_flux;
        } while (0);
        return result;
      };
      output.set(integrator.sum<3>(op), row, col);
    }
  }
  return output;
}
} // namespace toki::timestepper::detail
