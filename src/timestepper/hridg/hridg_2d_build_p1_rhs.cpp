#include <array>
#include "toki/log.hpp"
#include "toki/math/index.hpp"
#include "toki/quadrature/lobatto.hpp"
#include "toki/timestepper/detail/hridg_2d.hpp"
#include "toki/utility.hpp"
namespace toki::timestepper::detail {
auto
HRIDG_2D::build_P1_RHS(toki::Size const& direction)
  -> std::array<toki::matrix::Mat, 3>
{
  auto const& dof = info.domain_spec.at("default").quadrature_point;
  toki::Size const dof_P{dof * dof * dof};
  std::array<toki::Size, 3> size_col{dof, dof, dof};
  auto const& basis = info.domain_spec.at("default").basis;
  auto const& integrator = info.domain_spec.at("default").integrator;
  auto const& dx = info.domain.at("default").info.local.element.volume;
  auto apply_flux_space =
    [&](
      toki::Number& result,
      toki::Number const& sign,
      std::array<toki::Number, 3> coord,
      toki::Size const& target,
      std::array<toki::Size, 3> const& index_col,
      std::array<toki::Size, 3> const& index_row) -> void {
    toki::Number result_flux{1.0};
    // col
    coord[target + 1] = -sign;
    if (info.flux_type[target] == toki::timestepper::tag::Flux::Uniform) {
      result_flux *= info.op.get_flux_uniform_upwind[target](
        (sign < 0.0), 0.0, {basis.eval<3>(coord, index_col)});
    } else {
      toki::log::error("no implementation");
    }
    // row
    coord[target + 1] = sign;
    result_flux *= basis.eval<3>(coord, index_row);
    // update
    result += 0.5 * (-sign * info.param.time_delta / dx[target]) * result_flux;
    return;
  };
  std::array<toki::matrix::Mat, 3> output{};
  output[0] = build_P0_RHS();
  toki::Number sign[2]{-1.0, +1.0};
  for (toki::Size tag = 0; tag < 2 * 1; ++tag) {
    output[tag + 1] = toki::matrix::Mat(dof_P, dof_P);
    for (toki::Size col = 0; col < output[tag + 1].extent[1]; ++col) {
      auto index_col = toki::math::index::ind2sub<3>(size_col, col);
      for (toki::Size row = 0; row < output[tag + 1].extent[0]; ++row) {
        auto index_row = toki::math::index::ind2sub<3>(size_col, row);
        auto op =
          [&](std::array<toki::Number, 3> const& coord) -> toki::Number {
          toki::Number result{0};
          // flux: space
          apply_flux_space(
            result, sign[tag % 2], coord, direction, index_col, index_row);
          return result;
        };
        output[tag + 1].set(integrator.sum<3>(op), row, col);
      }
    }
  }
  return output;
}
auto
HRIDG_2D::build_P1_RHS(
  toki::Size const& direction,
  std::array<toki::Size, 2> const& index_center)
  -> std::array<toki::matrix::Mat, 3>
{
  toki::utility::confirm(
    info.flux_type[direction] == toki::timestepper::tag::Flux::External,
    "incompatible flux type");
  auto const& dof = info.domain_spec.at("default").quadrature_point;
  toki::Size const dof_P{dof * dof * dof};
  std::array<toki::Size, 3> size_col{dof, dof, dof};
  auto const& basis = info.domain_spec.at("default").basis;
  auto const& integrator = info.domain_spec.at("default").integrator;
  auto const& dx = info.domain.at("default").info.local.element.volume;
  auto apply_flux_space = [&](
                            toki::Number& result,
                            toki::Number const& sign,
                            std::array<toki::Number, 3> coord,
                            toki::Size const& target,
                            std::array<toki::Size, 3> const& index_col,
                            std::array<toki::Size, 3> const& index_row,
                            toki::Size const& offset) -> void {
    if (sign < 0.0) {
      if ((index_col[target + 1] != dof - 1) && (index_row[target + 1] != 0)) {
        return;
      }
    } else {
      if ((index_col[target + 1] != 0) && (index_row[target + 1] != dof - 1)) {
        return;
      }
    }
    toki::Number result_flux{1.0};
    // col
    coord[target + 1] = -sign;
    if (info.flux_type[target] == toki::timestepper::tag::Flux::External) {
      toki::Size const stride = ((sign < 0.0) ? 0 : 2);
      if (direction == 0) {
        result_flux *= info.op.get_flux_external_upwind[target](
          (sign < 0.0),
          0.0,
          {basis.eval<3>(coord, index_col)},
          {index_center[0] - 1 + stride, index_center[1]},
          offset);
      } else {
        result_flux *= info.op.get_flux_external_upwind[target](
          (sign < 0.0),
          0.0,
          {basis.eval<3>(coord, index_col)},
          {index_center[0], index_center[1] - 1 + stride},
          offset);
      }
    } else {
      toki::log::error("no implementation");
    }
    // row
    coord[target + 1] = sign;
    result_flux *= basis.eval<3>(coord, index_row);
    // update
    result += 0.5 * (-sign * info.param.time_delta / dx[target]) * result_flux;
    return;
  };
  std::array<toki::matrix::Mat, 3> output{};
  output[0] = build_P0_RHS();
  toki::Number sign[2]{-1.0, +1.0};
  for (toki::Size tag = 0; tag < 2 * 1; ++tag) {
    output[tag + 1] = toki::matrix::Mat(dof_P, dof_P);
    for (toki::Size col = 0; col < output[tag + 1].extent[1]; ++col) {
      auto index_col = toki::math::index::ind2sub<3>(size_col, col);
      for (toki::Size row = 0; row < output[tag + 1].extent[0]; ++row) {
        auto index_row = toki::math::index::ind2sub<3>(size_col, row);
        auto op = [&](
                    std::array<toki::Number, 3> const& coord,
                    toki::Size const& offset) -> toki::Number {
          toki::Number result{0};
          // flux: space
          apply_flux_space(
            result,
            sign[tag % 2],
            coord,
            direction,
            index_col,
            index_row,
            offset);
          return result;
        };
        output[tag + 1].set(integrator.sum<3>(op), row, col);
      }
    }
  }
  return output;
}
} // namespace toki::timestepper::detail
