#include "toki/timestepper/detail/hridg_2d.hpp"
namespace toki::timestepper::detail {
auto
HRIDG_2D::create_domain(
  std::string const& name,
  toki::Size const& variable,
  toki::Size const& dof,
  toki::Size const& quadrature_point,
  std::array<toki::Size, 2> const& resolution,
  std::array<toki::Size, 2> const& halo,
  std::array<toki::Number, 2> const& range,
  std::array<toki::Number, 2> const& anchor,
  std::array<int, 2> const& mpi_extent) -> HRIDG_2D&
{
  info.domain_spec.insert_or_assign(
    name,
    HRIDG_Info<2>::DomainSpec{
      .variable = variable,
      .dof = dof,
      .quadrature_point = quadrature_point,
      .basis = toki::basis::Nodal(quadrature_point),
      .integrator = toki::quadrature::Lobatto(quadrature_point + 1),
    });
  info.domain.insert_or_assign(
    name,
    decltype(info.domain)::mapped_type(
      variable * dof,
      quadrature_point,
      resolution,
      halo,
      range,
      anchor,
      mpi_extent));
  return *this;
}
auto
HRIDG_2D::create_domain(
  toki::Size const& variable,
  toki::Size const& quadrature_point,
  std::array<toki::Size, 2> const& resolution,
  std::array<toki::Number, 2> const& range,
  std::array<toki::Number, 2> const& anchor,
  std::array<int, 2> const& mpi_extent) -> HRIDG_2D&
{
  toki::Size const dof_C = quadrature_point * quadrature_point;
  toki::Size const dof_P = quadrature_point * dof_C;
  return create_domain(
           "default",
           variable,
           dof_C,
           quadrature_point,
           resolution,
           {3, 3},
           range,
           anchor,
           mpi_extent)
    .create_domain(
      "result_P0",
      variable,
      dof_P,
      quadrature_point,
      resolution,
      {3, 3},
      range,
      anchor,
      mpi_extent)
    .create_domain(
      "result_P1_0",
      variable,
      dof_P,
      quadrature_point,
      resolution,
      {3, 3},
      range,
      anchor,
      mpi_extent)
    .create_domain(
      "result_P1_1",
      variable,
      dof_P,
      quadrature_point,
      resolution,
      {3, 3},
      range,
      anchor,
      mpi_extent)
    .create_domain(
      "result_P2",
      variable,
      dof_P,
      quadrature_point,
      resolution,
      {3, 3},
      range,
      anchor,
      mpi_extent)
    .create_domain(
      "result_P3",
      variable,
      dof_P,
      quadrature_point,
      resolution,
      {3, 3},
      range,
      anchor,
      mpi_extent);
}
} // namespace toki::timestepper::detail
