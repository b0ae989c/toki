#include "toki/timestepper/detail/hridg_2d.hpp"
#include "toki/utility.hpp"
namespace toki::timestepper::detail {
auto
HRIDG_2D::create_flux_external(
  toki::Size const& direction,
  std::function<toki::Number(
    toki::Number const&,
    std::vector<toki::Number> const&,
    std::array<toki::Size, 2> const&,
    toki::Size const&)> const& op) -> HRIDG_2D&
{
  toki::utility::confirm(direction < 2, "invalid parameter: direction");
  info.flux_type[direction] = toki::timestepper::tag::Flux::External;
  info.op.get_flux_external[direction] = op;
  return *this;
}
} // namespace toki::timestepper::detail
