#include "toki/timestepper/detail/hridg_2d.hpp"
#include "toki/utility.hpp"
namespace toki::timestepper::detail {
auto
HRIDG_2D::create_flux_uniform_upwind(
  toki::Size const& direction,
  std::function<toki::Number(
    bool const&,
    toki::Number const&,
    std::vector<toki::Number> const&)> const& op) -> HRIDG_2D&
{
  toki::utility::confirm(direction < 2, "invalid parameter: direction");
  toki::utility::confirm(
    info.flux_type[direction] == toki::timestepper::tag::Flux::Uniform,
    "incompatible flux type");
  info.op.get_flux_uniform_upwind[direction] = op;
  return *this;
}
} // namespace toki::timestepper::detail
