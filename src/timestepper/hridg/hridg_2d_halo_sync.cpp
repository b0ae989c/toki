#include "mpi.h"
#include "toki/timestepper/detail/hridg_2d.hpp"
namespace toki::timestepper::detail {
auto
HRIDG_2D::halo_sync() -> HRIDG_2D&
{
  for (toki::Size dim = 0; dim < 2; ++dim) {
    MPI_Barrier(MPI_COMM_WORLD);
    info.domain.at("default").halo_dispatch(dim).halo_wait(dim);
    MPI_Barrier(MPI_COMM_WORLD);
    info.domain.at("default").halo_update(dim);
  }
  return *this;
}
} // namespace toki::timestepper::detail
