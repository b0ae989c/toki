#include "toki/log.hpp"
#include "toki/math/index.hpp"
#include "toki/math/permutation.hpp"
#include "toki/math/seq.hpp"
#include "toki/timestepper/detail/hridg_2d.hpp"
namespace toki::timestepper::detail {
auto
HRIDG_2D::prepare() -> HRIDG_2D&
{
  auto const& domain_spec = info.domain_spec.at("default");
  // build cache
  auto const dof_C = domain_spec.variable * domain_spec.dof;
  auto const dof_P = dof_C * domain_spec.quadrature_point;
  if (
    (info.flux_type[0] == toki::timestepper::tag::Flux::Uniform) &&
    (info.flux_type[1] == toki::timestepper::tag::Flux::Uniform)) {
    if (domain_spec.variable == 1) {
      // P0
      cache.P0_LHS = build_P0_LHS();
      cache.P0_LHS_p =
        toki::math::permutation::Permutation(cache.P0_LHS.extent[0]);
      toki::math::seq::getf(
        dof_P, dof_P, cache.P0_LHS, 0, 1, 0, 1, cache.P0_LHS_p, 0, 1);
      cache.P0_RHS = build_P0_RHS();
      // P1_0
      cache.P1_0_LHS = build_P1_LHS(0);
      cache.P1_0_LHS_p =
        toki::math::permutation::Permutation(cache.P1_0_LHS.extent[0]);
      toki::math::seq::getf(
        dof_P, dof_P, cache.P1_0_LHS, 0, 1, 0, 1, cache.P1_0_LHS_p, 0, 1);
      cache.P1_0_RHS = build_P1_RHS(0);
      // P1_1
      cache.P1_1_LHS = build_P1_LHS(1);
      cache.P1_1_LHS_p =
        toki::math::permutation::Permutation(cache.P1_1_LHS.extent[0]);
      toki::math::seq::getf(
        dof_P, dof_P, cache.P1_1_LHS, 0, 1, 0, 1, cache.P1_1_LHS_p, 0, 1);
      cache.P1_1_RHS = build_P1_RHS(1);
      // P2
      cache.P2_LHS = build_P2_LHS();
      cache.P2_LHS_p =
        toki::math::permutation::Permutation(cache.P2_LHS.extent[0]);
      toki::math::seq::getf(
        dof_P, dof_P, cache.P2_LHS, 0, 1, 0, 1, cache.P2_LHS_p, 0, 1);
      cache.P2_RHS = build_P2_RHS();
      // P3
      cache.P3_LHS = cache.P2_LHS;
      cache.P3_LHS_p = cache.P2_LHS_p;
      cache.P3_RHS = cache.P2_RHS;
      // C0
      cache.C0_LHS = build_C0_LHS();
      cache.C0_LHS_p =
        toki::math::permutation::Permutation(cache.C0_LHS.extent[0]);
      toki::math::seq::getf(
        dof_C, dof_C, cache.C0_LHS, 0, 1, 0, 1, cache.C0_LHS_p, 0, 1);
      cache.C0_RHS = build_C0_RHS();
    } else {
      toki::log::error("no implementation");
    }
  } else if (
    (info.flux_type[0] == toki::timestepper::tag::Flux::External) &&
    (info.flux_type[1] == toki::timestepper::tag::Flux::External)) {
    if (domain_spec.variable == 1) {
      cache.component.T_RHS = build_P0_RHS();
      cache.component.M = build_C0_LHS();
      cache.component.M_p = toki::math::permutation::Permutation(dof_C);
      toki::math::seq::getf(
        dof_C, dof_C, cache.component.M, 0, 1, 0, 1, cache.component.M_p, 0, 1);
    } else {
      toki::log::error("no implementation");
    }
  } else {
    toki::log::error("no implementation");
  }
  return *this;
}
} // namespace toki::timestepper::detail
