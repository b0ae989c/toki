#include "toki/timestepper/detail/hridg_2d.hpp"
#include "toki/utility.hpp"
namespace toki::timestepper::detail {
auto
HRIDG_2D::save(std::string const& name, std::string const& domain_name)
  -> HRIDG_2D&
{
  toki::utility::confirm(
    info.domain.contains(domain_name), "invalid parameter: domain_name");
  toki::utility::confirm(
    info.domain_spec.at(domain_name).variable == 1,
    "invalid parameter: domain_name");
  info.domain.at(domain_name).save(name, info.param.step);
  return *this;
}
} // namespace toki::timestepper::detail
