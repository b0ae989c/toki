#include "toki/timestepper/detail/hridg_2d.hpp"
#include "toki/utility.hpp"
namespace toki::timestepper::detail {
auto
HRIDG_2D::set_element(
  std::string const& name,
  toki::Size const& variable,
  toki::Number const& value,
  std::array<toki::Size, 2> const& index,
  toki::Size const& point) -> HRIDG_2D&
{
  toki::utility::confirm(info.domain.contains(name), "invalid parameter: name");
  toki::utility::confirm(
    variable < info.domain_spec.at(name).variable,
    "invalid parameter: variable");
  info.domain.at(name).set_element(
    value, index, variable * info.domain_spec.at(name).dof + point);
  return *this;
}
auto
HRIDG_2D::set_element(
  std::string const& name,
  std::function<toki::Number(
    toki::Size const&,
    std::array<toki::Size, 2> const&,
    toki::Size const&)> const& op) -> HRIDG_2D&
{
  toki::utility::confirm(info.domain.contains(name), "invalid parameter: name");
  auto const& dof = info.domain_spec.at(name).dof;
  info.domain.at(name).set_element(
    [&](std::array<toki::Size, 2> const& index, toki::Size const& point)
      -> toki::Number {
    return op(point / dof, index, point % dof);
  });
  return *this;
}
auto
HRIDG_2D::set_element(
  std::string const& name,
  std::function<toki::Number(
    toki::Size const&,
    toki::Number const&,
    std::array<toki::Number, 2> const&)> const& op) -> HRIDG_2D&
{
  toki::utility::confirm(info.domain.contains(name), "invalid parameter: name");
  auto const& variable_max = info.domain_spec.at(name).variable;
  auto const& dof = info.domain_spec.at(name).dof;
  auto const& basis = info.domain_spec.at(name).basis;
  auto const& point = basis.info.point;
  auto const& time = info.param.time;
  auto const& anchor = info.domain.at(name).info.local.anchor;
  auto const& element_volume = info.domain.at(name).info.local.element.volume;
  std::array<toki::Number, 2> coord{};
  for (toki::Size variable = 0; variable < variable_max; ++variable) {
    info.domain.at(name).set_element(
      [&](std::array<toki::Size, 2> const& index, toki::Size const& offset)
        -> toki::Number {
      toki::Size ell[]{
        (offset - variable * dof) % point,
        (offset - variable * dof) / point,
      };
      coord[0] = anchor[0] + (static_cast<toki::Number>(index[0]) +
                              (basis.info.abscissa.at(ell[0]) + 1.0) / 2.0) *
                               element_volume[0];
      coord[1] = anchor[1] + (static_cast<toki::Number>(index[1]) +
                              (basis.info.abscissa.at(ell[1]) + 1.0) / 2.0) *
                               element_volume[1];
      return op(variable, time, coord);
    });
  }
  return *this;
}
} // namespace toki::timestepper::detail
