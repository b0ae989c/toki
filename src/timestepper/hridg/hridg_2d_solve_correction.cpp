#include "omp.h"
#include "toki/config.hpp"
#include "toki/math/seq.hpp"
#include "toki/timestepper/detail/hridg_2d.hpp"
#include "toki/vector/vec.hpp"
namespace toki::timestepper::detail {
auto
HRIDG_2D::solve_correction() -> HRIDG_2D&
{
  auto const& halo = info.domain.at("default").info.halo;
  auto const& resolution = info.domain.at("default").info.local.resolution;
  auto const& dof = info.domain_spec.at("default").quadrature_point;
  toki::Size const dof_C{dof * dof};
  toki::Size const dof_P{dof_C * dof};
  if (
    (info.flux_type[0] == toki::timestepper::tag::Flux::Uniform) &&
    (info.flux_type[1] == toki::timestepper::tag::Flux::Uniform)) {
#pragma omp parallel default(none) shared(halo, resolution, dof_C, dof_P)
    {
      toki::vector::Vec W(5 * dof_P);
      toki::vector::Vec RHS(dof_C);
      toki::Size index_0;
      toki::Size index_1;
#pragma omp for schedule(runtime) collapse(2)
      for (index_1 = halo[1]; index_1 < resolution[1] - halo[1]; ++index_1) {
        for (index_0 = halo[0]; index_0 < resolution[0] - halo[0]; ++index_0) {
          info.domain.at("result_P3")
            .get_element(W, 0 * dof_P, {index_0, index_1})
            .get_element(W, 1 * dof_P, {index_0 - 1, index_1})
            .get_element(W, 2 * dof_P, {index_0 + 1, index_1})
            .get_element(W, 3 * dof_P, {index_0, index_1 - 1})
            .get_element(W, 4 * dof_P, {index_0, index_1 + 1});
          if (W.get_norm() < toki::epsilon<toki::Number>) {
            continue;
          }
          toki::math::seq::gemv(
            cache.C0_RHS[0].extent[0],
            cache.C0_RHS[0].extent[1],
            toki::one<toki::Number>,
            toki::zero<toki::Number>,
            W,
            0 * dof_P,
            1,
            RHS,
            0,
            1,
            cache.C0_RHS[0],
            0,
            1,
            0,
            1);
          for (toki::Size tag = 0; tag < 4; ++tag) {
            toki::math::seq::gemv(
              cache.C0_RHS[tag + 1].extent[0],
              cache.C0_RHS[tag + 1].extent[1],
              toki::one<toki::Number>,
              toki::one<toki::Number>,
              W,
              (tag + 1) * dof_P,
              1,
              RHS,
              0,
              1,
              cache.C0_RHS[tag + 1],
              0,
              1,
              0,
              1);
          }
          if (RHS.get_norm() < toki::epsilon<toki::Number>) {
            continue;
          }
          toki::math::seq::gets(
            RHS.extent[0],
            1,
            toki::one<toki::Number>,
            cache.C0_LHS,
            cache.C0_LHS_p,
            RHS);
          info.domain.at("default").adjust_element(RHS, 0, {index_0, index_1});
        }
      }
    }
  } else if (
    (info.flux_type[0] == toki::timestepper::tag::Flux::External) &&
    (info.flux_type[1] == toki::timestepper::tag::Flux::External)) {
#pragma omp parallel default(none) shared(halo, resolution, dof_C, dof_P)
    {
      toki::vector::Vec W(5 * dof_P);
      toki::vector::Vec RHS(dof_C);
      toki::Size index_0;
      toki::Size index_1;
#pragma omp for schedule(runtime) collapse(2)
      for (index_1 = halo[1]; index_1 < resolution[1] - halo[1]; ++index_1) {
        for (index_0 = halo[0]; index_0 < resolution[0] - halo[0]; ++index_0) {
          info.domain.at("result_P3")
            .get_element(W, 0 * dof_P, {index_0, index_1})
            .get_element(W, 1 * dof_P, {index_0 - 1, index_1})
            .get_element(W, 2 * dof_P, {index_0 + 1, index_1})
            .get_element(W, 3 * dof_P, {index_0, index_1 - 1})
            .get_element(W, 4 * dof_P, {index_0, index_1 + 1});
          if (
            (!info.param.flag_apply_source) &&
            (W.get_norm() < toki::epsilon<toki::Number>)) {
            continue;
          }
          auto const& C0_LHS = cache.component.M;
          auto const& C0_LHS_p = cache.component.M_p;
          auto const C0_RHS = build_C0_RHS({index_0, index_1});
          toki::math::seq::gemv(
            C0_RHS[0].extent[0],
            C0_RHS[0].extent[1],
            toki::one<toki::Number>,
            toki::zero<toki::Number>,
            W,
            0 * dof_P,
            1,
            RHS,
            0,
            1,
            C0_RHS[0],
            0,
            1,
            0,
            1);
          for (toki::Size tag = 0; tag < 4; ++tag) {
            toki::math::seq::gemv(
              C0_RHS[tag + 1].extent[0],
              C0_RHS[tag + 1].extent[1],
              toki::one<toki::Number>,
              toki::one<toki::Number>,
              W,
              (tag + 1) * dof_P,
              1,
              RHS,
              0,
              1,
              C0_RHS[tag + 1],
              0,
              1,
              0,
              1);
          }
          apply_source({index_0, index_1}, RHS);
          if (RHS.get_norm() < toki::epsilon<toki::Number>) {
            continue;
          }
          toki::math::seq::gets(
            RHS.extent[0], 1, toki::one<toki::Number>, C0_LHS, C0_LHS_p, RHS);
          info.domain.at("default").adjust_element(RHS, 0, {index_0, index_1});
        }
      }
    }
  } else {
    toki::log::error("no implementation");
  }
  return *this;
}
} // namespace toki::timestepper::detail
