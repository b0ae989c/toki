#include "omp.h"
#include "toki/config.hpp"
#include "toki/math/seq.hpp"
#include "toki/timestepper/detail/hridg_2d.hpp"
#include "toki/vector/vec.hpp"
namespace toki::timestepper::detail {
auto
HRIDG_2D::solve_prediction() -> HRIDG_2D&
{
  auto const& resolution = info.domain.at("default").info.local.resolution;
  auto const& dof = info.domain_spec.at("default").quadrature_point;
  toki::Size const dof_C{dof * dof};
  toki::Size const dof_P{dof_C * dof};
  if (
    (info.flux_type[0] == toki::timestepper::tag::Flux::Uniform) &&
    (info.flux_type[1] == toki::timestepper::tag::Flux::Uniform)) {
#pragma omp parallel default(none) shared(resolution, dof_C, dof_P)
    {
      toki::vector::Vec Q(dof_C);
      toki::vector::Vec W(4 * dof_P);
      toki::vector::Vec RHS(dof_P);
      toki::Size index_0;
      toki::Size index_1;
      // P0
#pragma omp for schedule(runtime) collapse(2)
      for (index_1 = 0; index_1 < resolution[1]; ++index_1) {
        for (index_0 = 0; index_0 < resolution[0]; ++index_0) {
          info.domain.at("default").get_element(Q, 0, {index_0, index_1});
          if (Q.get_norm() < toki::epsilon<toki::Number>) {
            continue;
          }
          toki::math::seq::gemv(
            cache.P0_RHS.extent[0],
            cache.P0_RHS.extent[1],
            toki::one<toki::Number>,
            toki::zero<toki::Number>,
            Q,
            0,
            1,
            RHS,
            0,
            1,
            cache.P0_RHS,
            0,
            1,
            0,
            1);
          toki::math::seq::gets(
            RHS.extent[0],
            1,
            toki::one<toki::Number>,
            cache.P0_LHS,
            cache.P0_LHS_p,
            RHS);
          info.domain.at("result_P0").set_element(RHS, 0, {index_0, index_1});
        }
      }
      // P1_0
#pragma omp for schedule(runtime) collapse(2)
      for (index_1 = 0; index_1 < resolution[1]; ++index_1) {
        for (index_0 = 1; index_0 < resolution[0] - 1; ++index_0) {
          info.domain.at("default").get_element(Q, 0, {index_0, index_1});
          info.domain.at("result_P0")
            .get_element(W, 0 * dof_P, {index_0 - 1, index_1})
            .get_element(W, 1 * dof_P, {index_0 + 1, index_1});
          if (
            (Q.get_norm() < toki::epsilon<toki::Number>) &&
            (W.get_norm(0, 2 * dof_P, 1) < toki::epsilon<toki::Number>)) {
            continue;
          }
          toki::math::seq::gemv(
            cache.P1_0_RHS[0].extent[0],
            cache.P1_0_RHS[0].extent[1],
            toki::one<toki::Number>,
            toki::zero<toki::Number>,
            Q,
            0,
            1,
            RHS,
            0,
            1,
            cache.P1_0_RHS[0],
            0,
            1,
            0,
            1);
          for (toki::Size tag = 0; tag < 2; ++tag) {
            toki::math::seq::gemv(
              cache.P1_0_RHS[tag + 1].extent[0],
              cache.P1_0_RHS[tag + 1].extent[1],
              toki::one<toki::Number>,
              toki::one<toki::Number>,
              W,
              tag * dof_P,
              1,
              RHS,
              0,
              1,
              cache.P1_0_RHS[tag + 1],
              0,
              1,
              0,
              1);
          }
          toki::math::seq::gets(
            RHS.extent[0],
            1,
            toki::one<toki::Number>,
            cache.P1_0_LHS,
            cache.P1_0_LHS_p,
            RHS);
          info.domain.at("result_P1_0").set_element(RHS, 0, {index_0, index_1});
        }
      }
      // P1_1
#pragma omp for schedule(runtime) collapse(2)
      for (index_1 = 1; index_1 < resolution[1] - 1; ++index_1) {
        for (index_0 = 0; index_0 < resolution[0]; ++index_0) {
          info.domain.at("default").get_element(Q, 0, {index_0, index_1});
          info.domain.at("result_P0")
            .get_element(W, 0 * dof_P, {index_0, index_1 - 1})
            .get_element(W, 1 * dof_P, {index_0, index_1 + 1});
          if (
            (Q.get_norm() < toki::epsilon<toki::Number>) &&
            (W.get_norm(0, 2 * dof_P, 1) < toki::epsilon<toki::Number>)) {
            continue;
          }
          toki::math::seq::gemv(
            cache.P1_1_RHS[0].extent[0],
            cache.P1_1_RHS[0].extent[1],
            toki::one<toki::Number>,
            toki::zero<toki::Number>,
            Q,
            0,
            1,
            RHS,
            0,
            1,
            cache.P1_1_RHS[0],
            0,
            1,
            0,
            1);
          for (toki::Size tag = 0; tag < 2; ++tag) {
            toki::math::seq::gemv(
              cache.P1_1_RHS[tag + 1].extent[0],
              cache.P1_1_RHS[tag + 1].extent[1],
              toki::one<toki::Number>,
              toki::one<toki::Number>,
              W,
              tag * dof_P,
              1,
              RHS,
              0,
              1,
              cache.P1_1_RHS[tag + 1],
              0,
              1,
              0,
              1);
          }
          toki::math::seq::gets(
            RHS.extent[0],
            1,
            toki::one<toki::Number>,
            cache.P1_1_LHS,
            cache.P1_1_LHS_p,
            RHS);
          info.domain.at("result_P1_1").set_element(RHS, 0, {index_0, index_1});
        }
      }
      // P2
#pragma omp for schedule(runtime) collapse(2)
      for (index_1 = 1; index_1 < resolution[1] - 1; ++index_1) {
        for (index_0 = 1; index_0 < resolution[0] - 1; ++index_0) {
          info.domain.at("default").get_element(Q, 0, {index_0, index_1});
          info.domain.at("result_P1_1")
            .get_element(W, 0 * dof_P, {index_0 - 1, index_1})
            .get_element(W, 1 * dof_P, {index_0 + 1, index_1});
          info.domain.at("result_P1_0")
            .get_element(W, 2 * dof_P, {index_0, index_1 - 1})
            .get_element(W, 3 * dof_P, {index_0, index_1 + 1});
          if (
            (Q.get_norm() < toki::epsilon<toki::Number>) &&
            (W.get_norm() < toki::epsilon<toki::Number>)) {
            continue;
          }
          toki::math::seq::gemv(
            cache.P2_RHS[0].extent[0],
            cache.P2_RHS[0].extent[1],
            toki::one<toki::Number>,
            toki::zero<toki::Number>,
            Q,
            0,
            1,
            RHS,
            0,
            1,
            cache.P2_RHS[0],
            0,
            1,
            0,
            1);
          for (toki::Size tag = 0; tag < 4; ++tag) {
            toki::math::seq::gemv(
              cache.P2_RHS[tag + 1].extent[0],
              cache.P2_RHS[tag + 1].extent[1],
              toki::one<toki::Number>,
              toki::one<toki::Number>,
              W,
              tag * dof_P,
              1,
              RHS,
              0,
              1,
              cache.P2_RHS[tag + 1],
              0,
              1,
              0,
              1);
          }
          toki::math::seq::gets(
            RHS.extent[0],
            1,
            toki::one<toki::Number>,
            cache.P2_LHS,
            cache.P2_LHS_p,
            RHS);
          info.domain.at("result_P2").set_element(RHS, 0, {index_0, index_1});
        }
      }
      // P3
#pragma omp for schedule(runtime) collapse(2)
      for (index_1 = 2; index_1 < resolution[1] - 2; ++index_1) {
        for (index_0 = 2; index_0 < resolution[0] - 2; ++index_0) {
          info.domain.at("default").get_element(Q, 0, {index_0, index_1});
          info.domain.at("result_P2")
            .get_element(W, 0 * dof_P, {index_0 - 1, index_1})
            .get_element(W, 1 * dof_P, {index_0 + 1, index_1})
            .get_element(W, 2 * dof_P, {index_0, index_1 - 1})
            .get_element(W, 3 * dof_P, {index_0, index_1 + 1});
          if (
            (Q.get_norm() < toki::epsilon<toki::Number>) &&
            (W.get_norm() < toki::epsilon<toki::Number>)) {
            continue;
          }
          toki::math::seq::gemv(
            cache.P3_RHS[0].extent[0],
            cache.P3_RHS[0].extent[1],
            toki::one<toki::Number>,
            toki::zero<toki::Number>,
            Q,
            0,
            1,
            RHS,
            0,
            1,
            cache.P3_RHS[0],
            0,
            1,
            0,
            1);
          for (toki::Size tag = 0; tag < 4; ++tag) {
            toki::math::seq::gemv(
              cache.P3_RHS[tag + 1].extent[0],
              cache.P3_RHS[tag + 1].extent[1],
              toki::one<toki::Number>,
              toki::one<toki::Number>,
              W,
              tag * dof_P,
              1,
              RHS,
              0,
              1,
              cache.P3_RHS[tag + 1],
              0,
              1,
              0,
              1);
          }
          toki::math::seq::gets(
            RHS.extent[0],
            1,
            toki::one<toki::Number>,
            cache.P3_LHS,
            cache.P3_LHS_p,
            RHS);
          info.domain.at("result_P3").set_element(RHS, 0, {index_0, index_1});
        }
      }
    }
  } else if (
    (info.flux_type[0] == toki::timestepper::tag::Flux::External) &&
    (info.flux_type[1] == toki::timestepper::tag::Flux::External)) {
#pragma omp parallel default(none) shared(resolution, dof_C, dof_P)
    {
      toki::vector::Vec Q(dof_C);
      toki::vector::Vec W(4 * dof_P);
      toki::vector::Vec RHS(dof_P);
      toki::Size index_0;
      toki::Size index_1;
      // P0
#pragma omp for schedule(runtime) collapse(2)
      for (index_1 = 0; index_1 < resolution[1]; ++index_1) {
        for (index_0 = 0; index_0 < resolution[0]; ++index_0) {
          info.domain.at("default").get_element(Q, 0, {index_0, index_1});
          if (
            (!info.param.flag_apply_source) &&
            (Q.get_norm() < toki::epsilon<toki::Number>)) {
            continue;
          }
          auto P0_LHS = build_P0_LHS({index_0, index_1});
          auto P0_LHS_p =
            toki::math::permutation::Permutation(P0_LHS.extent[0]);
          toki::math::seq::getf(
            dof_P, dof_P, P0_LHS, 0, 1, 0, 1, P0_LHS_p, 0, 1);
          auto const& P0_RHS = cache.component.T_RHS;
          toki::math::seq::gemv(
            P0_RHS.extent[0],
            P0_RHS.extent[1],
            toki::one<toki::Number>,
            toki::zero<toki::Number>,
            Q,
            0,
            1,
            RHS,
            0,
            1,
            P0_RHS,
            0,
            1,
            0,
            1);
          apply_source({index_0, index_1}, RHS);
          if (RHS.get_norm() < toki::epsilon<toki::Number>) {
            continue;
          }
          toki::math::seq::gets(
            RHS.extent[0], 1, toki::one<toki::Number>, P0_LHS, P0_LHS_p, RHS);
          info.domain.at("result_P0").set_element(RHS, 0, {index_0, index_1});
        }
      }
      // P1_0
#pragma omp for schedule(runtime) collapse(2)
      for (index_1 = 0; index_1 < resolution[1]; ++index_1) {
        for (index_0 = 1; index_0 < resolution[0] - 1; ++index_0) {
          info.domain.at("default").get_element(Q, 0, {index_0, index_1});
          info.domain.at("result_P0")
            .get_element(W, 0 * dof_P, {index_0 - 1, index_1})
            .get_element(W, 1 * dof_P, {index_0 + 1, index_1});
          if (
            (!info.param.flag_apply_source) &&
            (Q.get_norm() < toki::epsilon<toki::Number>) &&
            (W.get_norm(0, 2 * dof_P, 1) < toki::epsilon<toki::Number>)) {
            continue;
          }
          auto P1_0_LHS = build_P1_LHS(0, {index_0, index_1});
          auto P1_0_LHS_p =
            toki::math::permutation::Permutation(P1_0_LHS.extent[0]);
          toki::math::seq::getf(
            dof_P, dof_P, P1_0_LHS, 0, 1, 0, 1, P1_0_LHS_p, 0, 1);
          auto const P1_0_RHS = build_P1_RHS(0, {index_0, index_1});
          toki::math::seq::gemv(
            P1_0_RHS[0].extent[0],
            P1_0_RHS[0].extent[1],
            toki::one<toki::Number>,
            toki::zero<toki::Number>,
            Q,
            0,
            1,
            RHS,
            0,
            1,
            P1_0_RHS[0],
            0,
            1,
            0,
            1);
          for (toki::Size tag = 0; tag < 2; ++tag) {
            toki::math::seq::gemv(
              P1_0_RHS[tag + 1].extent[0],
              P1_0_RHS[tag + 1].extent[1],
              toki::one<toki::Number>,
              toki::one<toki::Number>,
              W,
              tag * dof_P,
              1,
              RHS,
              0,
              1,
              P1_0_RHS[tag + 1],
              0,
              1,
              0,
              1);
          }
          apply_source({index_0, index_1}, RHS);
          if (RHS.get_norm() < toki::epsilon<toki::Number>) {
            continue;
          }
          toki::math::seq::gets(
            RHS.extent[0],
            1,
            toki::one<toki::Number>,
            P1_0_LHS,
            P1_0_LHS_p,
            RHS);
          info.domain.at("result_P1_0").set_element(RHS, 0, {index_0, index_1});
        }
      }
      // P1_1
#pragma omp for schedule(runtime) collapse(2)
      for (index_1 = 1; index_1 < resolution[1] - 1; ++index_1) {
        for (index_0 = 0; index_0 < resolution[0]; ++index_0) {
          info.domain.at("default").get_element(Q, 0, {index_0, index_1});
          info.domain.at("result_P0")
            .get_element(W, 0 * dof_P, {index_0, index_1 - 1})
            .get_element(W, 1 * dof_P, {index_0, index_1 + 1});
          if (
            (!info.param.flag_apply_source) &&
            (Q.get_norm() < toki::epsilon<toki::Number>) &&
            (W.get_norm(0, 2 * dof_P, 1) < toki::epsilon<toki::Number>)) {
            continue;
          }
          auto P1_1_LHS = build_P1_LHS(1, {index_0, index_1});
          auto P1_1_LHS_p =
            toki::math::permutation::Permutation(P1_1_LHS.extent[0]);
          toki::math::seq::getf(
            dof_P, dof_P, P1_1_LHS, 0, 1, 0, 1, P1_1_LHS_p, 0, 1);
          auto const P1_1_RHS = build_P1_RHS(1, {index_0, index_1});
          toki::math::seq::gemv(
            P1_1_RHS[0].extent[0],
            P1_1_RHS[0].extent[1],
            toki::one<toki::Number>,
            toki::zero<toki::Number>,
            Q,
            0,
            1,
            RHS,
            0,
            1,
            P1_1_RHS[0],
            0,
            1,
            0,
            1);
          for (toki::Size tag = 0; tag < 2; ++tag) {
            toki::math::seq::gemv(
              P1_1_RHS[tag + 1].extent[0],
              P1_1_RHS[tag + 1].extent[1],
              toki::one<toki::Number>,
              toki::one<toki::Number>,
              W,
              tag * dof_P,
              1,
              RHS,
              0,
              1,
              P1_1_RHS[tag + 1],
              0,
              1,
              0,
              1);
          }
          apply_source({index_0, index_1}, RHS);
          if (RHS.get_norm() < toki::epsilon<toki::Number>) {
            continue;
          }
          toki::math::seq::gets(
            RHS.extent[0],
            1,
            toki::one<toki::Number>,
            P1_1_LHS,
            P1_1_LHS_p,
            RHS);
          info.domain.at("result_P1_1").set_element(RHS, 0, {index_0, index_1});
        }
      }
      // P2
#pragma omp for schedule(runtime) collapse(2)
      for (index_1 = 1; index_1 < resolution[1] - 1; ++index_1) {
        for (index_0 = 1; index_0 < resolution[0] - 1; ++index_0) {
          info.domain.at("default").get_element(Q, 0, {index_0, index_1});
          info.domain.at("result_P1_1")
            .get_element(W, 0 * dof_P, {index_0 - 1, index_1})
            .get_element(W, 1 * dof_P, {index_0 + 1, index_1});
          info.domain.at("result_P1_0")
            .get_element(W, 2 * dof_P, {index_0, index_1 - 1})
            .get_element(W, 3 * dof_P, {index_0, index_1 + 1});
          if (
            (!info.param.flag_apply_source) &&
            (Q.get_norm() < toki::epsilon<toki::Number>) &&
            (W.get_norm() < toki::epsilon<toki::Number>)) {
            continue;
          }
          auto P2_LHS = build_P2_LHS({index_0, index_1});
          auto P2_LHS_p =
            toki::math::permutation::Permutation(P2_LHS.extent[0]);
          toki::math::seq::getf(
            dof_P, dof_P, P2_LHS, 0, 1, 0, 1, P2_LHS_p, 0, 1);
          auto const P2_RHS = build_P2_RHS({index_0, index_1});
          toki::math::seq::gemv(
            P2_RHS[0].extent[0],
            P2_RHS[0].extent[1],
            toki::one<toki::Number>,
            toki::zero<toki::Number>,
            Q,
            0,
            1,
            RHS,
            0,
            1,
            P2_RHS[0],
            0,
            1,
            0,
            1);
          for (toki::Size tag = 0; tag < 4; ++tag) {
            toki::math::seq::gemv(
              P2_RHS[tag + 1].extent[0],
              P2_RHS[tag + 1].extent[1],
              toki::one<toki::Number>,
              toki::one<toki::Number>,
              W,
              tag * dof_P,
              1,
              RHS,
              0,
              1,
              P2_RHS[tag + 1],
              0,
              1,
              0,
              1);
          }
          apply_source({index_0, index_1}, RHS);
          if (RHS.get_norm() < toki::epsilon<toki::Number>) {
            continue;
          }
          toki::math::seq::gets(
            RHS.extent[0], 1, toki::one<toki::Number>, P2_LHS, P2_LHS_p, RHS);
          info.domain.at("result_P2").set_element(RHS, 0, {index_0, index_1});
        }
      }
      // P3
#pragma omp for schedule(runtime) collapse(2)
      for (index_1 = 2; index_1 < resolution[1] - 2; ++index_1) {
        for (index_0 = 2; index_0 < resolution[0] - 2; ++index_0) {
          info.domain.at("default").get_element(Q, 0, {index_0, index_1});
          info.domain.at("result_P2")
            .get_element(W, 0 * dof_P, {index_0 - 1, index_1})
            .get_element(W, 1 * dof_P, {index_0 + 1, index_1})
            .get_element(W, 2 * dof_P, {index_0, index_1 - 1})
            .get_element(W, 3 * dof_P, {index_0, index_1 + 1});
          if (
            (!info.param.flag_apply_source) &&
            (Q.get_norm() < toki::epsilon<toki::Number>) &&
            (W.get_norm() < toki::epsilon<toki::Number>)) {
            continue;
          }
          auto P3_LHS = build_P2_LHS({index_0, index_1});
          auto P3_LHS_p =
            toki::math::permutation::Permutation(P3_LHS.extent[0]);
          toki::math::seq::getf(
            dof_P, dof_P, P3_LHS, 0, 1, 0, 1, P3_LHS_p, 0, 1);
          auto const P3_RHS = build_P2_RHS({index_0, index_1});
          toki::math::seq::gemv(
            P3_RHS[0].extent[0],
            P3_RHS[0].extent[1],
            toki::one<toki::Number>,
            toki::zero<toki::Number>,
            Q,
            0,
            1,
            RHS,
            0,
            1,
            P3_RHS[0],
            0,
            1,
            0,
            1);
          for (toki::Size tag = 0; tag < 4; ++tag) {
            toki::math::seq::gemv(
              P3_RHS[tag + 1].extent[0],
              P3_RHS[tag + 1].extent[1],
              toki::one<toki::Number>,
              toki::one<toki::Number>,
              W,
              tag * dof_P,
              1,
              RHS,
              0,
              1,
              P3_RHS[tag + 1],
              0,
              1,
              0,
              1);
          }
          apply_source({index_0, index_1}, RHS);
          if (RHS.get_norm() < toki::epsilon<toki::Number>) {
            continue;
          }
          toki::math::seq::gets(
            RHS.extent[0], 1, toki::one<toki::Number>, P3_LHS, P3_LHS_p, RHS);
          info.domain.at("result_P3").set_element(RHS, 0, {index_0, index_1});
        }
      }
    }
  } else {
    toki::log::error("no implementation");
  }
  return *this;
}
} // namespace toki::timestepper::detail
