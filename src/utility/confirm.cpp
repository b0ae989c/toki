#include "toki/log.hpp"
#include "toki/utility.hpp"
namespace toki::utility {
auto
confirm(bool const& value, std::string const& message) -> void
{
#ifdef TOKI_DEBUG
  if (!value) {
    toki::log::error(message);
  }
#endif
  return;
}
} // namespace toki::utility
