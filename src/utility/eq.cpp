#include <cmath>
#include "toki/utility.hpp"
namespace toki::utility {
auto
eq(toki::Number const& lhs, toki::Number const& rhs) -> bool
{
  return std::fabs(lhs - rhs) < 1e-14;
}
} // namespace toki::utility
