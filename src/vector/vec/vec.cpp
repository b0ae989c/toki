#include "toki/vector/vec.hpp"
#include <algorithm>
namespace toki::vector {
Vec::Vec()
  : extent({0})
  , data{nullptr}
{
}
Vec::Vec(toki::Size const& extent_0)
  : extent({extent_0})
  , data(std::make_shared<Vec::value_type[]>(extent_0, 0))
{
}
Vec::Vec(Vec const& other)
  : extent(other.extent)
  , data(std::make_shared_for_overwrite<Vec::value_type[]>(other.extent[0]))
{
  std::copy(other.data.get(), other.data.get() + extent[0], data.get());
}
Vec::Vec(Vec&& other) noexcept
  : extent(std::exchange(other.extent, decltype(extent){}))
  , data(std::exchange(other.data, decltype(data){}))
{
}
Vec::~Vec() {}
auto
Vec::operator=(Vec other) -> Vec&
{
  swap(*this, other);
  return *this;
}
} // namespace toki::vector
