#include "toki/utility.hpp"
#include "toki/vector/vec.hpp"
namespace toki::vector {
auto
Vec::at(toki::Size const& index_0) const -> Vec::value_type const&
{
  toki::utility::confirm(valid_at(index_0), "invalid parameter: index_0");
  return data[index_0];
}
} // namespace toki::vector
