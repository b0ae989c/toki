#include <algorithm>
#include "toki/vector/vec.hpp"
namespace toki::vector {
auto
Vec::get_min() const -> value_type
{
  return *std::min_element(&(data[0]), &(data[0]) + extent.at(0));
}
} // namespace toki::vector
