#include <cmath>
#include "toki/vector/vec.hpp"
namespace toki::vector {
auto
Vec::get_norm() const -> value_type
{
  return get_norm(0, extent[0], 1);
}
auto
Vec::get_norm(
  toki::Size const& offset,
  toki::Size const& size,
  toki::Size const& stride) const -> value_type
{
  toki::Number result = 0.0;
  for (toki::Size index = 0; index < size; ++index) {
    result += std::pow(at(offset + index * stride), 2.0);
  }
  return std::sqrt(result);
}
} // namespace toki::vector
