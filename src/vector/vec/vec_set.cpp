#include "toki/utility.hpp"
#include "toki/vector/vec.hpp"
namespace toki::vector {
auto
Vec::set(Vec::value_type const& value, toki::Size const& index_0) -> Vec&
{
  toki::utility::confirm(index_0 < extent[0], "invalid parameter: index_0");
  data[index_0] = value;
  return *this;
}
auto
Vec::set(std::function<Vec::value_type(toki::Size const&)> const& op) -> Vec&
{
  for (toki::Size index_0 = 0; index_0 < extent[0]; ++index_0) {
    data[index_0] = op(index_0);
  }
  return *this;
}
} // namespace toki::vector
