#include "toki/vector/vec.hpp"
namespace toki::vector {
auto
Vec::valid_at(toki::Size const& index_0) const -> bool
{
  return index_0 < extent[0];
}
} // namespace toki::vector
