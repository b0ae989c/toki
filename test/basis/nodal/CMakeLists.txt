set(TOKI_TEST basis_nodal)

add_executable(${TOKI_TEST})

target_include_directories(
  ${TOKI_TEST} PRIVATE ${CMAKE_CURRENT_LIST_DIR}/include
)
target_link_libraries(${TOKI_TEST} PUBLIC toki::toki_debug)
target_sources(${TOKI_TEST} PRIVATE ${CMAKE_CURRENT_LIST_DIR}/src/main.cpp)

add_test(NAME ${TOKI_TEST} COMMAND ${TOKI_TEST})
