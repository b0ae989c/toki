#include "gtest/gtest.h"
// \todo
// #include "toki_test/approx.hpp"
// #include "toki_test/eval.hpp"
#include "toki_test/fill.hpp"
auto
main(int argc, char** argv) -> int
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
