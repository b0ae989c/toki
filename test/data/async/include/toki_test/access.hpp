#ifndef TOKI_TEST_ACCESS_HPP
#define TOKI_TEST_ACCESS_HPP
#include "gtest/gtest.h"
#include "toki/data/async.hpp"
TEST(access, mpi_static_get)
{
  using Data = toki::data::
    Async<toki::data::AsyncBackend::MPI, toki::data::AsyncVariant::Static>;
  Data data;
  data.create_buffer("test", 10, 2);
  data.set("test", 20.0, 3, 1);
  EXPECT_DOUBLE_EQ(*(data.get("test", 3) + 1), 20.0);
}
// \todo
TEST(access, mpi_static_at) {}
// \todo
TEST(access, mpi_static_update_index) {}
// \todo
TEST(access, mpi_static_update_functor) {}
#endif
