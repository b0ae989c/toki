#ifndef TOKI_TEST_BASIC_HPP
#define TOKI_TEST_BASIC_HPP
#include <utility>
#include "gtest/gtest.h"
#include "toki/data/async.hpp"
TEST(basic, mpi_static_ctor)
{
  using Data = toki::data::
    Async<toki::data::AsyncBackend::MPI, toki::data::AsyncVariant::Static>;
  Data data_0;
  Data data_1(data_0);
  Data data_2(std::move(data_1));
}
TEST(basic, mpi_static_op)
{
  using Data = toki::data::
    Async<toki::data::AsyncBackend::MPI, toki::data::AsyncVariant::Static>;
  Data data_0;
  Data data_1 = data_0;
  Data data_2;
  data_2 = data_1;
  data_2 = std::move(data_1);
}
#endif
