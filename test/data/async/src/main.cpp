#include "gtest/gtest.h"
#include "toki_test/access.hpp"
#include "toki_test/basic.hpp"
#include "toki_test/buffer.hpp"
#include "toki_test/sync.hpp"
#include "toki_test/test_environment.hpp"
auto
main(int argc, char** argv) -> int
{
  testing::InitGoogleTest(&argc, argv);
  testing::AddGlobalTestEnvironment(new toki_test::TestEnvironment);
  return RUN_ALL_TESTS();
}
