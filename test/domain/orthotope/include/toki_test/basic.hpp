#ifndef TOKI_TEST_BASIC_HPP
#define TOKI_TEST_BASIC_HPP
#include <utility>
#include "gtest/gtest.h"
#include "toki/domain/orthotope.hpp"
TEST(basic, dg_1d_ctor)
{
  using Domain = toki::domain::Orthotope<
    1,
    toki::domain::detail::Orthotope_Element::DG,
    toki::domain::detail::Orthotope_Adaptivity::None>;
  Domain domain_0;
  Domain domain_1(4, 4, {16}, {2}, {3.0}, {-2.0}, {4});
  Domain domain_2(domain_1);
  Domain domain_3(std::move(domain_2));
}
TEST(basic, dg_1d_op)
{
  using Domain = toki::domain::Orthotope<
    1,
    toki::domain::detail::Orthotope_Element::DG,
    toki::domain::detail::Orthotope_Adaptivity::None>;
  Domain domain_0(4, 4, {16}, {2}, {3.0}, {-2.0}, {4});
  Domain domain_1 = domain_0;
  Domain domain_2;
  domain_2 = domain_1;
  domain_2 = std::move(domain_1);
}
#endif
