#ifndef TOKI_TEST_DATA_HPP
#define TOKI_TEST_DATA_HPP
#include <array>
#include "gtest/gtest.h"
#include "toki/domain/orthotope.hpp"
TEST(data, dg_1d_default)
{
  using Domain = toki::domain::Orthotope<
    1,
    toki::domain::detail::Orthotope_Element::DG,
    toki::domain::detail::Orthotope_Adaptivity::None>;
  Domain domain(4, 4, {32}, {3}, {4.0}, {-1.0}, {4});
  for (auto const& name : std::array<std::string, 4>{
         "halo_inner_n", "halo_inner_p", "halo_outer_n", "halo_outer_p"}) {
    EXPECT_TRUE(domain.data.info.buffer.contains(name));
    EXPECT_EQ(domain.data.info.buffer.at(name).size, 3);
    EXPECT_EQ(domain.data.info.buffer.at(name).dof, 4);
    if (name.starts_with("halo_outer_")) {
      EXPECT_TRUE(domain.data.info.buffer.at(name).flag_remote);
      if (name.ends_with("_n")) {
        EXPECT_EQ(
          domain.data.info.buffer.at(name).remote_mpi_rank,
          domain.info.local.mpi.neighbor[0].n);
        EXPECT_EQ(domain.data.info.buffer.at(name).remote_name, "halo_inner_p");
      } else {
        EXPECT_EQ(
          domain.data.info.buffer.at(name).remote_mpi_rank,
          domain.info.local.mpi.neighbor[0].p);
        EXPECT_EQ(domain.data.info.buffer.at(name).remote_name, "halo_inner_n");
      }
    } else {
      EXPECT_FALSE(domain.data.info.buffer.at(name).flag_remote);
    }
  }
}
#endif
