#ifndef TOKI_TEST_ELEMENT_HPP
#define TOKI_TEST_ELEMENT_HPP
#include "gtest/gtest.h"
#include "toki/domain/orthotope.hpp"
TEST(element, dg_1d_get)
{
  using Domain = toki::domain::Orthotope<
    1,
    toki::domain::detail::Orthotope_Element::DG,
    toki::domain::detail::Orthotope_Adaptivity::None>;
  Domain domain(4, 4, {32}, {3}, {4.0}, {-1.0}, {4});
  domain.data.info.buffer.at("main").content.set(7.0, 12 * 4 + 2);
  toki::vector::Vec output(4 + 9);
  domain.get_element(output, 9, {12});
  EXPECT_DOUBLE_EQ(output.at(9 + 2), 7.0);
}
TEST(element, dg_1d_update_point)
{
  using Domain = toki::domain::Orthotope<
    1,
    toki::domain::detail::Orthotope_Element::DG,
    toki::domain::detail::Orthotope_Adaptivity::None>;
  Domain domain(4, 4, {32}, {3}, {4.0}, {-1.0}, {4});
  domain.set_element(1.0, {5}, 3);
  EXPECT_DOUBLE_EQ(domain.data.at("main", 5, 3), 1.0);
}
TEST(element, dg_1d_update_all)
{
  using Domain = toki::domain::Orthotope<
    1,
    toki::domain::detail::Orthotope_Element::DG,
    toki::domain::detail::Orthotope_Adaptivity::None>;
  Domain domain(4, 4, {32}, {3}, {4.0}, {-1.0}, {4});
  domain.set_element(
    [](std::array<toki::Size, 1> const& index, toki::Size const& ell)
      -> toki::Number {
    return static_cast<toki::Number>(ell + index[0] * 4 + 1);
  });
  EXPECT_DOUBLE_EQ(domain.data.at("main", 6, 2), 27.0);
}
#endif
