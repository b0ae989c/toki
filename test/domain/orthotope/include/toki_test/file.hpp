#ifndef TOKI_TEST_FILE_HPP
#define TOKI_TEST_FILE_HPP
#include "gtest/gtest.h"
#include "toki/domain/orthotope.hpp"
TEST(file, dg_1d_save)
{
  using Domain = toki::domain::Orthotope<
    1,
    toki::domain::detail::Orthotope_Element::DG,
    toki::domain::detail::Orthotope_Adaptivity::None>;
  Domain domain(4, 4, {32}, {3}, {4.0}, {-1.0}, {4});
  domain.set_element(0.5, {5}, 2);
  domain.set_element(1.0, {6}, 3);
  domain.set_element(-1.0, {7}, 0);
  domain.set_element(-0.5, {8}, 1);
  domain.save("test", 0);
}
// \todo
TEST(file, dg_1d_load) {}
#endif
