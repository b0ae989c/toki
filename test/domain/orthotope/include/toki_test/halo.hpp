#ifndef TOKI_TEST_HALO_HPP
#define TOKI_TEST_HALO_HPP
#include "gtest/gtest.h"
#include "toki/domain/orthotope.hpp"
TEST(halo, dg_1d_sync)
{
  using Domain = toki::domain::Orthotope<
    1,
    toki::domain::detail::Orthotope_Element::DG,
    toki::domain::detail::Orthotope_Adaptivity::None>;
  Domain domain(4, 4, {32}, {3}, {4.0}, {-1.0}, {4});
  domain.halo_dispatch(0).halo_wait(0);
}
#endif
