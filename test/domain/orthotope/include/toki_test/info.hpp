#ifndef TOKI_TEST_INFO_HPP
#define TOKI_TEST_INFO_HPP
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include "toki/config.hpp"
#include "toki/domain/orthotope.hpp"
TEST(info, dg_1d)
{
  using Domain = toki::domain::Orthotope<
    1,
    toki::domain::detail::Orthotope_Element::DG,
    toki::domain::detail::Orthotope_Adaptivity::None>;
  using testing::ElementsAreArray;
  Domain domain(4, 4, {32}, {3}, {4.0}, {-1.0}, {4});
  EXPECT_EQ(domain.info.dof, 4);
  EXPECT_EQ(domain.info.quadrature_point, 4);
  EXPECT_THAT(domain.info.resolution, ElementsAreArray({32}));
  EXPECT_THAT(domain.info.halo, ElementsAreArray({3}));
  EXPECT_THAT(domain.info.range, ElementsAreArray({4.0}));
  EXPECT_THAT(domain.info.anchor, ElementsAreArray({-1.0}));
  EXPECT_THAT(domain.info.mpi_extent, ElementsAreArray({4}));
  EXPECT_THAT(domain.info.local.resolution, ElementsAreArray({32 / 4 + 2 * 3}));
  EXPECT_THAT(
    domain.info.local.range,
    ElementsAreArray(
      {4.0 / static_cast<toki::Number>(32) *
       static_cast<toki::Number>(32 / 4 + 2 * 3)}));
  EXPECT_THAT(
    domain.info.local.anchor,
    ElementsAreArray(
      {-1.0 +
       4.0 / static_cast<toki::Number>(4) *
         static_cast<toki::Number>(domain.info.local.mpi.index[0]) -
       static_cast<toki::Number>(3) * (4.0 / static_cast<toki::Number>(32))}));
  EXPECT_EQ(
    domain.info.local.mpi.neighbor[0].n,
    (domain.info.local.mpi.rank + 4 - 1) % 4);
  EXPECT_EQ(
    domain.info.local.mpi.neighbor[0].p, (domain.info.local.mpi.rank + 1) % 4);
  EXPECT_EQ(domain.info.local.element.N, 32 / 4 + 2 * 3);
  EXPECT_THAT(
    domain.info.local.element.volume,
    ElementsAreArray({4.0 / static_cast<toki::Number>(32)}));
}
#endif
