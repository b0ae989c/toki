#ifndef TOKI_TEST_STENCIL_HPP
#define TOKI_TEST_STENCIL_HPP
#include "gtest/gtest.h"
#include "toki/domain/orthotope.hpp"
TEST(stencil, dg_1d_get)
{
  using Domain = toki::domain::Orthotope<
    1,
    toki::domain::detail::Orthotope_Element::DG,
    toki::domain::detail::Orthotope_Adaptivity::None>;
  Domain domain(4, 4, {32}, {3}, {4.0}, {-1.0}, {4});
  domain.set_element(1.0, {5}, 1);
  std::vector<std::array<toki::Size, 1>> index_list{
    std::array<toki::Size, 1>{3},
    std::array<toki::Size, 1>{4},
    std::array<toki::Size, 1>{5},
  };
  toki::vector::Vec output(3 * 4);
  domain.get_stencil(output, index_list);
  EXPECT_DOUBLE_EQ(output.at(2 * 4 + 1), 1.0);
}
#endif
