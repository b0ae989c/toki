#ifndef TOKI_TEST_TEST_ENVIRONMENT_HPP
#define TOKI_TEST_TEST_ENVIRONMENT_HPP
#include <memory>
#include "gtest/gtest.h"
#include "toki/context/default.hpp"
namespace toki_test {
struct TestEnvironment : testing::Environment
{
  std::unique_ptr<toki::context::Default> context;
  ~TestEnvironment() {}
  void
  SetUp() override
  {
    context = std::make_unique<toki::context::Default>();
  }
  void
  TearDown() override
  {
    context.reset();
  }
};
} // namespace toki_test
#endif
