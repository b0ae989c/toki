#ifndef TOKI_TEST_CONVERT_HPP
#define TOKI_TEST_CONVERT_HPP
#include <array>
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include "toki/math/index.hpp"
TEST(convert, ind2sub)
{
  using testing::ElementsAreArray;
  using toki::math::index::ind2sub;
  EXPECT_THAT(ind2sub<1>(std::array<int, 1>{4}, 0), ElementsAreArray({0}));
  EXPECT_THAT(ind2sub<1>(std::array<int, 1>{4}, 2), ElementsAreArray({2}));
  EXPECT_THAT(
    ind2sub<2>(std::array<int, 2>{4, 5}, 0), ElementsAreArray({0, 0}));
  EXPECT_THAT(
    ind2sub<2>(std::array<int, 2>{4, 5}, 9), ElementsAreArray({1, 2}));
  EXPECT_THAT(
    ind2sub<3>(std::array<int, 3>{4, 5, 6}, 0), ElementsAreArray({0, 0, 0}));
  EXPECT_THAT(
    ind2sub<3>(std::array<int, 3>{4, 5, 6}, 64), ElementsAreArray({0, 1, 3}));
  EXPECT_THAT(
    ind2sub<3>(std::array<int, 3>{4, 5, 6}, 119), ElementsAreArray({3, 4, 5}));
}
// \todo
TEST(convert, sub2ind) {}
// \todo
TEST(convert, shift) {}
#endif
