#include "gtest/gtest.h"
#include "toki_test/convert.hpp"
auto
main(int argc, char** argv) -> int
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
