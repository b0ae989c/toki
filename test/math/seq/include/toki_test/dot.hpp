#ifndef TOKI_TEST_DOT_HPP
#define TOKI_TEST_DOT_HPP
#include "gtest/gtest.h"
#include "toki/config.hpp"
#include "toki/math/seq.hpp"
#include "toki/vector/vec.hpp"
TEST(dot, vec_vec)
{
  using toki::math::seq::dot;
  using toki::vector::Vec;
  Vec lhs(4);
  Vec rhs(4);
  lhs.set([](toki::Size const& index_0) -> Vec::value_type {
    return static_cast<Vec::value_type>(index_0 + 1);
  });
  rhs.set([](toki::Size const& index_0) -> Vec::value_type {
    return static_cast<Vec::value_type>(index_0 + 2);
  });
  EXPECT_DOUBLE_EQ(dot(4, lhs, 0, 1, rhs, 0, 1), 40.0);
}
#endif
