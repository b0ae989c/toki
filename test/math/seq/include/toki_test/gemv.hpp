#ifndef TOKI_TEST_GEMV_HPP
#define TOKI_TEST_GEMV_HPP
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include "toki/math/seq.hpp"
#include "toki/matrix/mat.hpp"
#include "toki/vector/vec.hpp"
TEST(gemv, dense)
{
  using testing::DoubleNear;
  using toki::math::seq::gemv;
  using toki::matrix::Mat;
  using toki::vector::Vec;
  double A_entry[]{
    1.0, 8.0, 3.0, 7.0, 6.0, 3.0, 8.0, 2.0, 4.0, 8.0,
    5.0, 0.0, 9.0, 5.0, 9.0, 6.0, 5.0, 9.0, 4.0, 8.0,
  };
  Mat A(4, 5);
  A.set([&](toki::Size const& offset) -> toki::Number {
    return A_entry[offset];
  });
  Vec x(5);
  x.set([](toki::Size const& index) -> toki::Number {
    return static_cast<toki::Number>(10 * (index + 1));
  });
  Vec y(4);
  y.set([](toki::Size const& index) -> toki::Number {
    return static_cast<toki::Number>(10 * (index + 6));
  });
  gemv(4, 5, 2.0, 3.0, x, 0, 1, y, 0, 1, A, 0, 1, 0, 1);
  EXPECT_THAT(y.at(0), DoubleNear(1900.0, 1e-14));
  EXPECT_THAT(y.at(1), DoubleNear(2270.0, 1e-14));
  EXPECT_THAT(y.at(2), DoubleNear(2040.0, 1e-14));
  EXPECT_THAT(y.at(3), DoubleNear(1770.0, 1e-14));
}
#endif
