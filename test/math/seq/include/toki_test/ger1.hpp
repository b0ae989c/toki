#ifndef TOKI_TEST_GER1_HPP
#define TOKI_TEST_GER1_HPP
#include "gtest/gtest.h"
#include "toki/config.hpp"
#include "toki/math/seq.hpp"
#include "toki/matrix/mat.hpp"
#include "toki/vector/vec.hpp"
TEST(ger1, dense)
{
  using toki::math::seq::ger1;
  using toki::matrix::Mat;
  using toki::vector::Vec;
  Vec x(5);
  Vec y(7);
  Mat A(5, 4);
  x.set([](toki::Size const& index_0) -> Vec::value_type {
    return static_cast<Vec::value_type>(index_0 + 1);
  });
  y.set([](toki::Size const& index_0) -> Vec::value_type {
    return static_cast<Vec::value_type>(index_0 + 2);
  });
  A.set(
    [](
      toki::Size const& index_0, toki::Size const& index_1) -> Vec::value_type {
    return static_cast<Mat::value_type>(index_0 + 5 * index_1 + 3);
  });
  ger1(5, 4, 4.0, x, 0, 1, y, 2, 1, A, 0, 1, 0, 1);
  for (toki::Size col = 0; col < 4; ++col) {
    for (toki::Size row = 0; row < 5; ++row) {
      EXPECT_DOUBLE_EQ(
        A.at(row, col),
        static_cast<Mat::value_type>(
          row + 5 * col + 3 + 4 * x.at(row) * y.at(col + 2)));
    }
  }
}
// \todo
TEST(ger1, sparse) {}
#endif
