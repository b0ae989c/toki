#ifndef TOKI_TEST_GETF_HPP
#define TOKI_TEST_GETF_HPP
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include "toki/math/permutation.hpp"
#include "toki/math/seq.hpp"
#include "toki/matrix/mat.hpp"
TEST(getf, dense)
{
  using testing::DoubleNear;
  using toki::math::permutation::Permutation;
  using toki::math::seq::getf;
  using toki::matrix::Mat;
  double A_entry[]{
    1.0, 8.0, 3.0, 7.0, 6.0, 3.0, 8.0, 2.0, 4.0, 8.0,
    5.0, 0.0, 9.0, 5.0, 9.0, 6.0, 5.0, 9.0, 4.0, 8.0,
  };
  Mat A(4, 5);
  A.set([&](toki::Size const& offset) -> toki::Number {
    return A_entry[offset];
  });
  Permutation p(4);
  getf(4, 5, A, 0, 1, 0, 1, p, 0, 1);
  EXPECT_THAT(A.at(0, 0), DoubleNear(+8.000000000000000, 1e-14));
  EXPECT_THAT(A.at(1, 0), DoubleNear(+0.375000000000000, 1e-14));
  EXPECT_THAT(A.at(2, 0), DoubleNear(+0.875000000000000, 1e-14));
  EXPECT_THAT(A.at(3, 0), DoubleNear(+0.125000000000000, 1e-14));
  EXPECT_THAT(A.at(0, 1), DoubleNear(+3.000000000000000, 1e-14));
  EXPECT_THAT(A.at(1, 1), DoubleNear(+6.875000000000000, 1e-14));
  EXPECT_THAT(A.at(2, 1), DoubleNear(-0.090909090909091, 1e-14));
  EXPECT_THAT(A.at(3, 1), DoubleNear(+0.818181818181818, 1e-14));
  EXPECT_THAT(A.at(0, 2), DoubleNear(+8.000000000000000, 1e-14));
  EXPECT_THAT(A.at(1, 2), DoubleNear(+2.000000000000000, 1e-14));
  EXPECT_THAT(A.at(2, 2), DoubleNear(-6.818181818181818, 1e-14));
  EXPECT_THAT(A.at(3, 2), DoubleNear(-0.200000000000000, 1e-14));
  EXPECT_THAT(A.at(0, 3), DoubleNear(+5.000000000000000, 1e-14));
  EXPECT_THAT(A.at(1, 3), DoubleNear(+7.125000000000000, 1e-14));
  EXPECT_THAT(A.at(2, 3), DoubleNear(+2.272727272727273, 1e-14));
  EXPECT_THAT(A.at(3, 3), DoubleNear(+3.000000000000000, 1e-14));
  EXPECT_THAT(A.at(0, 4), DoubleNear(+9.000000000000000, 1e-14));
  EXPECT_THAT(A.at(1, 4), DoubleNear(+0.625000000000000, 1e-14));
  EXPECT_THAT(A.at(2, 4), DoubleNear(+0.181818181818182, 1e-14));
  EXPECT_THAT(A.at(3, 4), DoubleNear(+3.400000000000000, 1e-14));
  EXPECT_EQ(p.at(0), 1);
  EXPECT_EQ(p.at(1), 2);
  EXPECT_EQ(p.at(2), 3);
  EXPECT_EQ(p.at(3), 3);
}
#endif
