#ifndef TOKI_TEST_RSWP_HPP
#define TOKI_TEST_RSWP_HPP
#include "gtest/gtest.h"
#include "toki/math/permutation.hpp"
#include "toki/math/seq.hpp"
#include "toki/matrix/mat.hpp"
TEST(rswp, dense)
{
  using toki::math::permutation::Permutation;
  using toki::math::seq::rswp;
  using toki::matrix::Mat;
  Mat A(4, 5);
  A.set([](toki::Size const& offset) -> toki::Number {
    return static_cast<toki::Number>(offset);
  });
  Permutation p(4);
  p.set(2, 0).set(2, 1).set(3, 2).set(3, 3);
  rswp(4, 5, A, 0, 1, 0, 1, p, 0, 1);
  EXPECT_DOUBLE_EQ(A.at(0, 0), 2.0);
  EXPECT_DOUBLE_EQ(A.at(1, 0), 0.0);
  EXPECT_DOUBLE_EQ(A.at(2, 0), 3.0);
  EXPECT_DOUBLE_EQ(A.at(3, 0), 1.0);
  EXPECT_DOUBLE_EQ(A.at(0, 1), 6.0);
  EXPECT_DOUBLE_EQ(A.at(1, 1), 4.0);
  EXPECT_DOUBLE_EQ(A.at(2, 1), 7.0);
  EXPECT_DOUBLE_EQ(A.at(3, 1), 5.0);
  EXPECT_DOUBLE_EQ(A.at(0, 2), 10.0);
  EXPECT_DOUBLE_EQ(A.at(1, 2), 8.0);
  EXPECT_DOUBLE_EQ(A.at(2, 2), 11.0);
  EXPECT_DOUBLE_EQ(A.at(3, 2), 9.0);
  EXPECT_DOUBLE_EQ(A.at(0, 3), 14.0);
  EXPECT_DOUBLE_EQ(A.at(1, 3), 12.0);
  EXPECT_DOUBLE_EQ(A.at(2, 3), 15.0);
  EXPECT_DOUBLE_EQ(A.at(3, 3), 13.0);
  EXPECT_DOUBLE_EQ(A.at(0, 4), 18.0);
  EXPECT_DOUBLE_EQ(A.at(1, 4), 16.0);
  EXPECT_DOUBLE_EQ(A.at(2, 4), 19.0);
  EXPECT_DOUBLE_EQ(A.at(3, 4), 17.0);
}
#endif
