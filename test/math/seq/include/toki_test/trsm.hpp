#ifndef TOKI_TEST_TRSM_HPP
#define TOKI_TEST_TRSM_HPP
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include "toki/math/permutation.hpp"
#include "toki/math/seq.hpp"
#include "toki/matrix/mat.hpp"
TEST(trsm, dense)
{
  using testing::DoubleNear;
  using toki::math::permutation::Permutation;
  using toki::math::seq::getf;
  using toki::math::seq::rswp;
  using toki::math::seq::trsm;
  using toki::matrix::Mat;
  double A_entry[]{
    1.0,
    8.0,
    3.0,
    7.0,
    6.0,
    3.0,
    8.0,
    2.0,
    4.0,
    8.0,
    5.0,
    0.0,
    9.0,
    5.0,
    9.0,
    6.0,
  };
  Mat A(4, 4);
  A.set([&](toki::Size const& offset) -> toki::Number {
    return A_entry[offset];
  });
  Permutation p(4);
  getf(4, 4, A, 0, 1, 0, 1, p, 0, 1);
  Mat B(4, 2);
  B.set([](toki::Size const& offset) -> toki::Number {
    return static_cast<toki::Number>(offset);
  });
  rswp(4, 2, B, 0, 1, 0, 1, p, 0, 1);
  trsm<true>(4, 2, 2.0, A, 0, 1, 0, 1, B, 0, 1, 0, 1);
  trsm<false>(4, 2, 1.0, A, 0, 1, 0, 1, B, 0, 1, 0, 1);
  EXPECT_THAT(B.at(0, 0), DoubleNear(+1.022222222222222, 1e-14));
  EXPECT_THAT(B.at(1, 0), DoubleNear(+1.422222222222222, 1e-14));
  EXPECT_THAT(B.at(2, 0), DoubleNear(-0.888888888888889, 1e-14));
  EXPECT_THAT(B.at(3, 0), DoubleNear(-0.666666666666667, 1e-14));
  EXPECT_THAT(B.at(0, 1), DoubleNear(+1.370666666666667, 1e-14));
  EXPECT_THAT(B.at(1, 1), DoubleNear(+1.002666666666667, 1e-14));
  EXPECT_THAT(B.at(2, 1), DoubleNear(-0.746666666666667, 1e-14));
  EXPECT_THAT(B.at(3, 1), DoubleNear(+0.400000000000000, 1e-14));
}
#endif
