#ifndef TOKI_TEST_TRSV_HPP
#define TOKI_TEST_TRSV_HPP
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include "toki/math/permutation.hpp"
#include "toki/math/seq.hpp"
#include "toki/matrix/mat.hpp"
#include "toki/vector/vec.hpp"
TEST(trsv, dense)
{
  using testing::DoubleNear;
  using toki::math::permutation::Permutation;
  using toki::math::seq::getf;
  using toki::math::seq::rswp;
  using toki::math::seq::trsv;
  using toki::matrix::Mat;
  using toki::vector::Vec;
  double A_entry[]{
    1.0,
    8.0,
    3.0,
    7.0,
    6.0,
    3.0,
    8.0,
    2.0,
    4.0,
    8.0,
    5.0,
    0.0,
    9.0,
    5.0,
    9.0,
    6.0,
  };
  Mat A(4, 4);
  A.set([&](toki::Size const& offset) -> toki::Number {
    return A_entry[offset];
  });
  Permutation p(4);
  getf(4, 4, A, 0, 1, 0, 1, p, 0, 1);
  Vec b(4);
  b.set([](toki::Size const& index) -> toki::Number {
    return static_cast<toki::Number>(4 + index);
  });
  rswp(4, 1, b, 0, 1, 0, 1, p, 0, 1);
  trsv<true>(4, 2.0, A, 0, 1, 0, 1, b, 0, 1);
  trsv<false>(4, 1.0, A, 0, 1, 0, 1, b, 0, 1);
  EXPECT_THAT(b.at(0), DoubleNear(+1.370666666666667, 1e-14));
  EXPECT_THAT(b.at(1), DoubleNear(+1.002666666666667, 1e-14));
  EXPECT_THAT(b.at(2), DoubleNear(-0.746666666666667, 1e-14));
  EXPECT_THAT(b.at(3), DoubleNear(+0.400000000000000, 1e-14));
}
#endif
