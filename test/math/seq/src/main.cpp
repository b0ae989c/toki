#include "gtest/gtest.h"
#include "toki_test/dot.hpp"
#include "toki_test/gemv.hpp"
#include "toki_test/ger1.hpp"
#include "toki_test/getf.hpp"
#include "toki_test/gets.hpp"
#include "toki_test/rswp.hpp"
#include "toki_test/trsm.hpp"
#include "toki_test/trsv.hpp"
auto
main(int argc, char** argv) -> int
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
