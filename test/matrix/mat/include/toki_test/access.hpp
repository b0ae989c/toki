#ifndef TOKI_TEST_ACCESS_HPP
#define TOKI_TEST_ACCESS_HPP
#include "gtest/gtest.h"
#include "toki/matrix/mat.hpp"
TEST(access, valid_at)
{
  using toki::matrix::Mat;
  Mat mat(3, 4);
  EXPECT_TRUE(mat.valid_at(0, 0));
  EXPECT_TRUE(mat.valid_at(1, 2));
  EXPECT_TRUE(mat.valid_at(2, 3));
  EXPECT_FALSE(mat.valid_at(4, 3));
  EXPECT_FALSE(mat.valid_at(10, 10));
}
TEST(access, at)
{
  using toki::matrix::Mat;
  Mat mat(3, 4);
  mat.set(3.0, 1, 2);
  EXPECT_DOUBLE_EQ(mat.at(1, 2), 3.0);
}
TEST(access, set_index)
{
  using toki::matrix::Mat;
  Mat mat(3, 4);
  mat.set(3.0, 1, 2);
  EXPECT_DOUBLE_EQ(mat.at(1, 2), 3.0);
}
TEST(access, set_functor)
{
  using toki::matrix::Mat;
  Mat mat(3, 4);
  mat.set(
    [&](
      toki::Size const& index_0, toki::Size const& index_1) -> Mat::value_type {
    return static_cast<Mat::value_type>(index_0 + mat.extent[0] * index_1 + 1);
  });
  EXPECT_DOUBLE_EQ(mat.at(1, 2), 8.0);
  EXPECT_DOUBLE_EQ(mat.at(2, 3), 12.0);
}
#endif
