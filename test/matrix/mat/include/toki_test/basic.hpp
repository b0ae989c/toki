#ifndef TOKI_TEST_BASIC_HPP
#define TOKI_TEST_BASIC_HPP
#include <utility>
#include "gtest/gtest.h"
#include "toki/matrix/mat.hpp"
TEST(basic, ctor)
{
  using toki::matrix::Mat;
  Mat mat_0;
  Mat mat_1(3, 4);
  Mat mat_2(mat_1);
  Mat mat_3(std::move(mat_2));
}
TEST(basic, op)
{
  using toki::matrix::Mat;
  Mat mat_0(3, 4);
  Mat mat_1 = mat_0;
  Mat mat_2;
  mat_2 = mat_1;
  mat_2 = std::move(mat_1);
}
#endif
