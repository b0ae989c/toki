#ifndef TOKI_TEST_ACCESS_HPP
#define TOKI_TEST_ACCESS_HPP
#include "gtest/gtest.h"
#include "toki/vector/vec.hpp"
TEST(access, valid_at)
{
  using toki::vector::Vec;
  Vec vec(3);
  EXPECT_TRUE(vec.valid_at(0));
  EXPECT_TRUE(vec.valid_at(2));
  EXPECT_FALSE(vec.valid_at(3));
  EXPECT_FALSE(vec.valid_at(10));
}
TEST(access, at)
{
  using toki::vector::Vec;
  Vec vec(3);
  vec.set(2.0, 1);
  EXPECT_DOUBLE_EQ(vec.at(1), 2.0);
}
TEST(access, set_index)
{
  using toki::vector::Vec;
  Vec vec(8);
  vec.set(1.0, 3);
  EXPECT_DOUBLE_EQ(vec.at(3), 1.0);
}
TEST(access, set_functor)
{
  using toki::vector::Vec;
  Vec vec(8);
  vec.set([](toki::Size const& index_0) -> Vec::value_type {
    return static_cast<Vec::value_type>(index_0 + 1);
  });
  EXPECT_DOUBLE_EQ(vec.at(3), 4.0);
  EXPECT_DOUBLE_EQ(vec.at(7), 8.0);
}
#endif
