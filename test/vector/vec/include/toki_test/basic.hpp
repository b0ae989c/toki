#ifndef TOKI_TEST_BASIC_HPP
#define TOKI_TEST_BASIC_HPP
#include <utility>
#include "gtest/gtest.h"
#include "toki/vector/vec.hpp"
TEST(basic, ctor)
{
  using toki::vector::Vec;
  Vec vec_0;
  Vec vec_1(3);
  Vec vec_2(vec_1);
  Vec vec_3(std::move(vec_2));
}
TEST(basic, op)
{
  using toki::vector::Vec;
  Vec vec_0(3);
  Vec vec_1 = vec_0;
  Vec vec_2;
  vec_2 = vec_1;
  vec_2 = std::move(vec_1);
}
#endif
